<?php

use Artixgroup\Shop\FormTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/options.php');
Loc::loadMessages(__FILE__);

$moduleId = 'artixgroup.shop';
$MOD_RIGHT = $APPLICATION->getGroupRight($moduleId);
if ($MOD_RIGHT < 'R')
	return;

if (\Bitrix\Main\Loader::includeModule($moduleId) && $MOD_RIGHT >= 'W' && check_bitrix_sessid())
{
	if ($REQUEST_METHOD == 'POST' && strlen($Update.$Apply.$RestoreDefaults) > 0)
	{
		if (strlen($RestoreDefaults) > 0)
		{
            Option::set($moduleId, 'default_captcha', FormTable::CAPTCHA_NONE);
            Option::set($moduleId, 'recaptcha_v2_public_key', '');
            Option::set($moduleId, 'recaptcha_v3_public_key', '');
            Option::set($moduleId, 'recaptcha_v2_secret_key', '');
            Option::set($moduleId, 'recaptcha_v3_secret_key', '');
		}
		else
		{
            Option::set($moduleId, 'default_captcha', trim($_POST['default_captcha']));
			Option::set($moduleId, 'recaptcha_v2_public_key', $_POST['recaptcha_v2_public_key']);
			Option::set($moduleId, 'recaptcha_v3_public_key', $_POST['recaptcha_v3_public_key']);
			Option::set($moduleId, 'recaptcha_v2_secret_key', $_POST['recaptcha_v2_secret_key']);
			Option::set($moduleId, 'recaptcha_v3_secret_key', $_POST['recaptcha_v3_secret_key']);
		}
	}
}

// VIEW

$tabControl = new CAdminTabControl('tabControl', array(
	array('DIV' => 'edit1', 'TAB' => Loc::getMessage('MAIN_TAB_SET'), 'ICON' => 'ib_settings', 'TITLE' => Loc::getMessage('MAIN_TAB_TITLE_SET')),
	//array('DIV' => 'edit2', 'TAB' => Loc::getMessage('CONVERSION_TAB_MODULES_NAME'), 'ICON' => 'ib_settings', 'TITLE' => Loc::getMessage('CONVERSION_TAB_MODULES_DESC')),
));

$tabControl->Begin();

?>
<form method="post" action="<?=$APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?echo LANGUAGE_ID?>">
	<?=bitrix_sessid_post()?>

	<?$tabControl->BeginNextTab()?>

	<tr>
		<td width="40%"><?=Loc::getMessage('ARTIXGROUP_FORM_CAPTCHA')?>:</td>
		<td width="60%">
            <?$captcha = Option::get($moduleId, 'default_captcha',  FormTable::CAPTCHA_NONE);?>
            <select name="default_captcha">
                <option<?= ($captcha === FormTable::CAPTCHA_NONE) ? ' selected' : ''?> value="<?= FormTable::CAPTCHA_NONE?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FORM_CAPTCHA_NONE")?>
                </option>
                <option<?= ($captcha == FormTable::CAPTCHA_BITRIX) ? ' selected' : ''?> value="<?= FormTable::CAPTCHA_BITRIX?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FORM_CAPTCHA_BITRIX")?><?($captcha == FormTable::CAPTCHA_BITRIX) ? 'true' : ''?>
                </option>
                <option<?= ($captcha === FormTable::CAPTCHA_GOOGLE_V2) ? ' selected' : ''?> value="<?= FormTable::CAPTCHA_GOOGLE_V2?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FORM_CAPTCHA_GOOGLE_V2")?>
                </option>
                <option<?= ($captcha === FormTable::CAPTCHA_GOOGLE_V3) ? ' selected' : ''?> value="<?= FormTable::CAPTCHA_GOOGLE_V3?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FORM_CAPTCHA_GOOGLE_V3")?>
                </option>
            </select>
		</td>
	</tr>
    <tr>
        <td width="40%"><?=Loc::getMessage('ARTIXGROUP_FORM_RECAPTCHA_V2_PUB_KEY')?>:</td>
        <td width="60%">
            <input type="text" name="recaptcha_v2_public_key" value="<?= Option::get($moduleId, 'recaptcha_v2_public_key')?>">
        </td>
    </tr>
    <tr>
        <td width="40%"><?=Loc::getMessage('ARTIXGROUP_FORM_RECAPTCHA_V2_SEC_KEY')?>:</td>
        <td width="60%">
            <input type="text" name="recaptcha_v2_secret_key" value="<?= Option::get($moduleId, 'recaptcha_v2_secret_key')?>">
        </td>
    </tr>
    <tr>
        <td width="40%"><?=Loc::getMessage('ARTIXGROUP_FORM_RECAPTCHA_V3_PUB_KEY')?>:</td>
        <td width="60%">
            <input type="text" name="recaptcha_v3_public_key" value="<?= Option::get($moduleId, 'recaptcha_v3_public_key')?>">
        </td>
    </tr>
    <tr>
        <td width="40%"><?=Loc::getMessage('ARTIXGROUP_FORM_RECAPTCHA_V2_SEC_KEY')?>:</td>
        <td width="60%">
            <input type="text" name="recaptcha_v3_secret_key" value="<?= Option::get($moduleId, 'recaptcha_v3_secret_key')?>">
        </td>
    </tr>

	<?$tabControl->Buttons()?>

	<input type="submit" name="Update" <? if ($MOD_RIGHT < 'W') echo 'disabled'; ?> value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
	<input type="submit" name="Apply" <? if ($MOD_RIGHT < 'W') echo 'disabled'; ?> value="<?=GetMessage("MAIN_OPT_APPLY")?>" title="<?=GetMessage("MAIN_OPT_APPLY_TITLE")?>">
	<?if(strlen($_REQUEST["back_url_settings"])>0):?>
		<input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" title="<?=GetMessage("MAIN_OPT_CANCEL_TITLE")?>" onclick="window.location='<?echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"]))?>'">
		<input type="hidden" name="back_url_settings" value="<?=htmlspecialcharsbx($_REQUEST["back_url_settings"])?>">
	<?endif?>
	<input type="submit" name="RestoreDefaults" <? if ($MOD_RIGHT < 'W') echo 'disabled'; ?> title="<?echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="return confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>')" value="<?echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">

	<?$tabControl->End()?>
</form>
