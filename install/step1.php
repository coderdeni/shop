<?php

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main;

Loc::loadMessages(__FILE__);
?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="lang" value="<?echo LANG?>">
    <input type="hidden" name="id" value="artixgroup.shop">
    <input type="hidden" name="install" value="Y">
    <input type="hidden" name="step" value="2">
    <input type="submit" name="inst" value="<?echo Loc::getMessage("MOD_INST")?>">
</form>