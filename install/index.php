<?php

use Artixgroup\Shop\FavouriteTable;
use Artixgroup\Shop\FormFieldTable;
use Artixgroup\Shop\FormHandlerTable;
use Artixgroup\Shop\FormResultFieldTable;
use Artixgroup\Shop\FormResultTable;
use Artixgroup\Shop\FormSiteTable;
use Artixgroup\Shop\FormTable;
use Artixgroup\Shop\SettingTable;
use \Bitrix\Main;
use Bitrix\Main\EventManager;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Entity\Base;
use \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);


class artixgroup_shop extends CModule
{
    var $errors;

    /**
     * test_task constructor.
     */
    function __construct()
    {
        $arModuleVersion = [];
        include(__DIR__ . '/version.php');

        $this->MODULE_ID = "artixgroup.shop";
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage('ARTIXGROUP_SHOP_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('ARTIXGROUP_SHOP_MODULE_DESCRIPTION');

        $this->PARTNER_NAME = "Artix group";

        $this->MODULE_SORT = 100;
        $this->SHOW_SUPER_ADMIN_GROUP_RIGHTS = 'Y';
        $this->MODULE_GROUP_RIGHTS = 'Y';
    }

    /**
     * @param $moduleId
     * @param $version
     * @return bool
     */
    public function checkVersion($moduleId, $version)
    {
        return CheckVersion(Main\ModuleManager::getVersion($moduleId), $version);
    }

    /**
     * @throws Main\LoaderException
     */
    public function checkModules()
    {
        if (!$this->checkVersion('main', '18.00.00'))
        {
            throw new Main\LoaderException(Loc::getMessage('ARTIXGROUP_SHOP_INSTALL_ERROR_VERSION'), ['#VERSION#' => '18']);
        }
    }

    /**
     * @throws Main\SystemException
     */
    function DoInstall()
    {
        global $APPLICATION;
        $request = Application::getInstance()->getContext()->getRequest();

        if ($request['step'] < 2)
        {
            $APPLICATION->IncludeAdminFile(GetMessage("ARTIXGROUP_SHOP_UNINSTALL_TITLE"), __DIR__ . "/step1.php");
        }
        elseif ($request['step'] == 2)
        {
            try
            {
                $this->checkModules();
                $this->InstallFiles();
                Main\ModuleManager::registerModule($this->MODULE_ID);
                $this->InstallDB();
                $this->InstallEvents();

                if ($request['install_data'] == 'Y')
                    $this->InstallData();
            }
            catch (Exception $e)
            {
                Main\ModuleManager::unRegisterModule($this->MODULE_ID);
                $APPLICATION->ThrowException($e->getMessage());
            };

            $APPLICATION->IncludeAdminFile(GetMessage("ARTIXGROUP_SHOP_UNINSTALL_TITLE"), __DIR__ . "/step2.php");
        }
    }

    /**
     * @return bool
     * @throws Main\ArgumentException
     * @throws Main\LoaderException
     * @throws Main\SystemException
     */
    function InstallDB()
    {
        if (!Main\Loader::includeModule($this->MODULE_ID))
            return false;

        if (!Application::getConnection(SettingTable::getConnectionName())
            ->isTableExists(Base::getInstance('\\Artixgroup\\Shop\\SettingTable')->getDBTableName()))
        {
            Base::getInstance('\\Artixgroup\\Shop\\SettingTable')->createDbTable();
        }

        if (!Application::getConnection(FavouriteTable::getConnectionName())
            ->isTableExists(Base::getInstance('\\Artixgroup\\Shop\\FavouriteTable')->getDBTableName()))
        {
            Base::getInstance('\\Artixgroup\\Shop\\FavouriteTable')->createDbTable();
        }

        if (!Application::getConnection(FormTable::getConnectionName())
            ->isTableExists(Base::getInstance('\\Artixgroup\\Shop\\FormTable')->getDBTableName()))
        {
            Base::getInstance('\\Artixgroup\\Shop\\FormTable')->createDbTable();
        }

        if (!Application::getConnection(FormSiteTable::getConnectionName())
            ->isTableExists(Base::getInstance('\\Artixgroup\\Shop\\FormSiteTable')->getDBTableName()))
        {
            Base::getInstance('\\Artixgroup\\Shop\\FormSiteTable')->createDbTable();
        }

        if (!Application::getConnection(FormFieldTable::getConnectionName())
            ->isTableExists(Base::getInstance('\\Artixgroup\\Shop\\FormFieldTable')->getDBTableName()))
        {
            Base::getInstance('\\Artixgroup\\Shop\\FormFieldTable')->createDbTable();
        }

        if (!Application::getConnection(FormHandlerTable::getConnectionName())
            ->isTableExists(Base::getInstance('\\Artixgroup\\Shop\\FormHandlerTable')->getDBTableName()))
        {
            Base::getInstance('\\Artixgroup\\Shop\\FormHandlerTable')->createDbTable();
        }

        if (!Application::getConnection(FormResultTable::getConnectionName())
            ->isTableExists(Base::getInstance('\\Artixgroup\\Shop\\FormResultTable')->getDBTableName()))
        {
            Base::getInstance('\\Artixgroup\\Shop\\FormResultTable')->createDbTable();
        }

        if (!Application::getConnection(FormResultFieldTable::getConnectionName())
            ->isTableExists(Base::getInstance('\\Artixgroup\\Shop\\FormResultFieldTable')->getDBTableName()))
        {
            Base::getInstance('\\Artixgroup\\Shop\\FormResultFieldTable')->createDbTable();
        }

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    function InstallData()
    {
        return true;
    }

    /**
     * @return bool
     */
    function InstallFiles()
    {
        CopyDirFiles(__DIR__."/components", Main\Loader::getLocal('components'), true, true);
        CopyDirFiles(__DIR__."/admin", Application::getDocumentRoot()."/bitrix/admin", true, true);
        CopyDirFiles(__DIR__."/js", Application::getDocumentRoot()."/bitrix/js", true, true);
        CopyDirFiles(__DIR__."/css", Application::getDocumentRoot()."/bitrix/css", true, true);

        return true;
    }

    /**
     * @return bool
     */
    function InstallEvents()
    {
        $eventManager = EventManager::getInstance();
        $eventManager->registerEventHandlerCompatible('main', 'OnBeforeUserRegister', $this->MODULE_ID, 'Artixgroup\\Shop\\AuthHandler', 'onBeforeUserRegisterHandler');
        $eventManager->registerEventHandlerCompatible('main', 'OnBeforeUserLogin', $this->MODULE_ID, 'Artixgroup\\Shop\\AuthHandler', 'onBeforeUserLoginHandler');
        $eventManager->registerEventHandlerCompatible('main', 'OnAfterUserLogin', $this->MODULE_ID, 'Artixgroup\\Shop\\Favourites\\Favourites', 'onAfterUserLogin');
        $eventManager->registerEventHandlerCompatible('main', 'OnAfterUserLogout', $this->MODULE_ID, 'Artixgroup\\Shop\\Favourites\\Favourites', 'onAfterUserLogout');
        $eventManager->registerEventHandlerCompatible('main', 'onMainGeoIpHandlersBuildList', $this->MODULE_ID, 'Artixgroup\\Shop\\Ipgeo\\IpGeoBase', 'getGeoIpHandlers');
        $eventManager->registerEventHandlerCompatible('sale', 'OnSalePayOrder', $this->MODULE_ID, 'Artixgroup\\Shop\\Services\\Bonus', 'onSalePayOrderHandler');
        $eventManager->registerEventHandlerCompatible('sale', 'OnSaleCancelOrder', $this->MODULE_ID, 'Artixgroup\\Shop\\Services\\Bonus', 'onSaleCancelOrderHandler');
        $eventManager->registerEventHandlerCompatible('main', 'OnBuildGlobalMenu', $this->MODULE_ID, 'Artixgroup\\Shop\\Helper', 'onBuildGlobalMenu');

        return true;
    }

    /**
     * @throws Main\ArgumentException
     * @throws Main\Db\SqlQueryException
     * @throws Main\LoaderException
     * @throws Main\SystemException
     */
    function DoUninstall()
    {
        global $APPLICATION, $step;

        $request = Application::getInstance()->getContext()->getRequest();

        if ($step < 2)
        {
            $APPLICATION->IncludeAdminFile(GetMessage("ARTIXGROUP_SHOP_UNINSTALL_TITLE"), __DIR__ . "/unstep1.php");
        }
        elseif ($step == 2)
        {
            try
            {
                $this->UnInstallFiles();
                $this->UnInstallEvents();

                if ($request['savedata'] != 'Y')
                    $this->UnInstallDB();

                Main\ModuleManager::unRegisterModule($this->MODULE_ID);

                $APPLICATION->IncludeAdminFile(GetMessage("ARTIXGROUP_SHOP_UNINSTALL_TITLE"), __DIR__ . "/unstep2.php");
            }
            catch (Exception $e)
            {
                $APPLICATION->ThrowException($e->getMessage());
                $APPLICATION->IncludeAdminFile(GetMessage("ARTIXGROUP_SHOP_UNINSTALL_TITLE"), __DIR__ . "/unstep2.php");
            }
        }
    }

    /**
     * @return bool|void
     * @throws Main\ArgumentException
     * @throws Main\Db\SqlQueryException
     * @throws Main\LoaderException
     * @throws Main\SystemException
     */
    function UnInstallDB()
    {
        if (!Main\Loader::includeModule($this->MODULE_ID))
            return false;

        Application::getConnection(SettingTable::getConnectionName())
            ->queryExecute('drop table if exists '.Base::getInstance('\\Artixgroup\\Shop\\SettingTable')->getDBTableName());

        Application::getConnection(FavouriteTable::getConnectionName())
            ->queryExecute('drop table if exists '.Base::getInstance('\\Artixgroup\\Shop\\FavouriteTable')->getDBTableName());

        Application::getConnection(FormTable::getConnectionName())
            ->queryExecute('drop table if exists '.Base::getInstance('\\Artixgroup\\Shop\\FormTable')->getDBTableName());

        Application::getConnection(FormSiteTable::getConnectionName())
            ->queryExecute('drop table if exists '.Base::getInstance('\\Artixgroup\\Shop\\FormSiteTable')->getDBTableName());

        Application::getConnection(FormFieldTable::getConnectionName())
            ->queryExecute('drop table if exists '.Base::getInstance('\\Artixgroup\\Shop\\FormFieldTable')->getDBTableName());

        Application::getConnection(FormHandlerTable::getConnectionName())
            ->queryExecute('drop table if exists '.Base::getInstance('\\Artixgroup\\Shop\\FormHandlerTable')->getDBTableName());

        Application::getConnection(FormResultTable::getConnectionName())
            ->queryExecute('drop table if exists '.Base::getInstance('\\Artixgroup\\Shop\\FormResultTable')->getDBTableName());

        Application::getConnection(FormResultFieldTable::getConnectionName())
            ->queryExecute('drop table if exists '.Base::getInstance('\\Artixgroup\\Shop\\FormResultFieldTable')->getDBTableName());

        return true;
    }

    /**
     * @param array $arParams
     * @return bool|void
     */
    function UnInstallFiles($arParams = array())
    {
        DeleteDirFiles(__DIR__ . "/components/", Main\Loader::getLocal('components'));
        DeleteDirFiles(__DIR__ . "/admin/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
        DeleteDirFiles(__DIR__ . "/js/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js");
        DeleteDirFiles(__DIR__ . "/css/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/css");

        return true;
    }

    /**
     * @return bool|void
     */
    function UnInstallEvents()
    {
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler('main', 'OnBeforeUserRegister', $this->MODULE_ID, 'Artixgroup\\Shop\\AuthHandler', 'onBeforeUserRegisterHandler');
        $eventManager->unRegisterEventHandler('main', 'OnBeforeUserLogin', $this->MODULE_ID, 'Artixgroup\\Shop\\AuthHandler', 'onBeforeUserLoginHandler');
        $eventManager->unRegisterEventHandler('main', 'OnAfterUserLogin', $this->MODULE_ID, 'Artixgroup\\Shop\\Favourites\\Favourites', 'onAfterUserLogin');
        $eventManager->unRegisterEventHandler('main', 'OnAfterUserLogout', $this->MODULE_ID, 'Artixgroup\\Shop\\Favourites\\Favourites', 'onAfterUserLogout');
        $eventManager->unRegisterEventHandler('main', 'onMainGeoIpHandlersBuildList', $this->MODULE_ID, 'Artixgroup\\Shop\\Ipgeo\\IpGeoBase', 'getGeoIpHandlers');
        $eventManager->unRegisterEventHandler('sale', 'OnSalePayOrder', $this->MODULE_ID, 'Artixgroup\\Shop\\Services\\Bonus', 'onSalePayOrderHandler');
        $eventManager->unRegisterEventHandler('sale', 'OnSaleCancelOrder', $this->MODULE_ID, 'Artixgroup\\Shop\\Services\\Bonus', 'onSaleCancelOrderHandler');
        $eventManager->unRegisterEventHandler('main', 'OnBuildGlobalMenu', $this->MODULE_ID, 'Artixgroup\\Shop\\Helper', 'onBuildGlobalMenu');

        return true;
    }

    /**
     * @return array
     */
    function GetModuleRightList()
    {
        return array(
            "reference_id" => array("D","R","W"),
            "reference" => array(
                "[D] ".Loc::getMessage("ARTIXGROUP_SHOP_DENIED"),
                "[R] ".Loc::getMessage("ARTIXGROUP_SHOP_OPENED"),
                "[W] ".Loc::getMessage("ARTIXGROUP_SHOP_FULL")
            )
        );
    }
}