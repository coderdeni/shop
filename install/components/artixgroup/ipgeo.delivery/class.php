<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Sale\Order;
use Bitrix\Sale\Shipment;
use Artixgroup\Shop\Ipgeo\Location;

class ArtixgroupIpGeoDeliveryComponent extends CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $filter = [];

    protected $limit = null;

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $cacheSections = array();


    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param $params
     * @return array
     * @internal param array $arParams
     */
    public function onPrepareComponentParams($params)
    {
        $params['PRODUCT_ID'] = (int)$params['PRODUCT_ID'];
        $params['PAYMENT_ID'] = !empty($params['PAYMENT_ID']) ? (int)$params['PAYMENT_ID'] : 1;
        $params['PERSON_TYPE_ID'] = !empty($params['PERSON_TYPE_ID']) ? (int)$params['PERSON_TYPE_ID'] : 1;
        $params['GROUP'] = !empty($params['GROUP']) ? $params['GROUP'] : 'Y';

        return $params;
    }

    public function getCurrentShipment(Order $order)
    {
        /** @var Shipment $shipment */
        foreach ($order->getShipmentCollection() as $shipment)
        {
            if (!$shipment->isSystem())
                return $shipment;
        }

        return null;
    }

    protected function addBundleProducts(&$products)
    {
        if (CCatalogProductSet::isProductHaveSet($this->arParams['PRODUCT_ID'], CCatalogProductSet::TYPE_GROUP))
        {
            $bundles = CCatalogProductSet::getAllSetsByProduct($this->arParams['PRODUCT_ID'], CCatalogProductSet::TYPE_GROUP);

            foreach ($bundles as $bundle)
            {
                if ($bundle['ACTIVE'] == 'Y')
                {
                    $currentBundle = $bundle;
                    break;
                }
            }

            foreach ($currentBundle['ITEMS'] as $item)
            {
                if ($item['ITEM_ID'] === $this->arParams['PRODUCT_ID'])
                    continue;

                $products[] = [
                    'PRODUCT_ID' => $item['ITEM_ID'],
                    'QUANTITY' => $item['QUANTITY']
                ];
            }
        }
    }

    protected function createOrder()
    {
        global $USER;

        $products = [
            [
                'PRODUCT_ID' => $this->arParams['PRODUCT_ID'],
                'QUANTITY'   => 1,
            ],
        ];

        $this->addBundleProducts($products);

        /** @var \Bitrix\Sale\Basket $basket */
        $basket = \Bitrix\Sale\Basket::create(SITE_ID);
        foreach ($products as $product)
        {
            $item = $basket->createItem("catalog", $product["PRODUCT_ID"]);
            unset($product["PRODUCT_ID"]);
            $item->setFields($product);
        }

        /** @var Order $order */
        $order = Order::create(SITE_ID, $USER->GetID());
        $order->setPersonTypeId($this->arParams['PERSON_TYPE_ID']);
        $order->setBasket($basket);

        /** @var \Bitrix\Sale\PropertyValueCollection $orderProperties */
        $orderProperties = $order->getPropertyCollection();
        /** @var \Bitrix\Sale\PropertyValue $orderDeliveryLocation */
        $orderDeliveryLocation = $orderProperties->getDeliveryLocation();
        $orderDeliveryLocation->setValue($this->arResult['CURRENT_LOCATION']['CODE']); // В какой город "доставляем" (куда доставлять).

        /** @var \Bitrix\Sale\ShipmentCollection $shipmentCollection */
        $shipmentCollection = $order->getShipmentCollection();
        $deliveryId = \Bitrix\Sale\Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId();
        $delivery = \Bitrix\Sale\Delivery\Services\Manager::getObjectById($deliveryId);
        /** @var \Bitrix\Sale\Shipment $shipment */
        $shipment = $shipmentCollection->createItem($delivery);
        $shipment->setField('CURRENCY', $order->getCurrency());

        /** @var \Bitrix\Sale\ShipmentItemCollection $shipmentItemCollection */
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        /** @var \Bitrix\Sale\BasketItem $basketItem */
        foreach ($basket as $basketItem)
        {
            $item = $shipmentItemCollection->createItem($basketItem);
            $item->setQuantity($basketItem->getQuantity());
        }

        $order->doFinalAction(true);

        $deliveryStoreList = \Bitrix\Sale\Delivery\ExtraServices\Manager::getStoresList($deliveryId);
        if (!empty($deliveryStoreList))
        {
            $shipment->setStoreId(current($deliveryStoreList));
        }

        $shipmentCollection->calculateDelivery();
        $order->doFinalAction();

        /** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
        $paymentCollection = $order->getPaymentCollection();
        /** @var \Bitrix\Sale\Payment $payment */
        $payment = $paymentCollection->createItem(
            \Bitrix\Sale\PaySystem\Manager::getObjectById($this->arParams['PAYMENT_ID'])
        );
        $payment->setField("SUM", $order->getPrice());
        $payment->setField("CURRENCY", $order->getCurrency());

        return $order;
    }

    protected function calculateDeliveries()
    {
        $this->arResult['DELIVERY'] = [];

        /** @var Order $order */
        $order = $this->createOrder();
        /** @var Order $orderClone */
        $orderClone = $order->createClone();
        /** @var Shipment $shipment */
        $shipment = $this->getCurrentShipment($order);
        $arDeliveryServiceAll = \Bitrix\Sale\Delivery\Services\Manager::getRestrictedObjectsList($shipment);

        if (!empty($arDeliveryServiceAll))
        {
            $anotherDeliveryCalculated = false;

            foreach ($arDeliveryServiceAll as $deliveryId => $deliveryObj)
            {
                $calcResult = false;
                $calcOrder = false;
                $arDelivery = [];

                if ((int)$shipment->getDeliveryId() === $deliveryId)
                {
                    $arDelivery['CHECKED'] = 'Y';
                    $calcResult = $deliveryObj->calculate($shipment);
                    $calcOrder = $order;
                }
                else
                {
                    $anotherDeliveryCalculated = true;
                    $orderClone->isStartField();

                    $clonedShipment = $this->getCurrentShipment($orderClone);
                    $clonedShipment->setField('DELIVERY_ID', $deliveryId);

                    $calculationResult = $orderClone->getShipmentCollection()->calculateDelivery();
                    if ($calculationResult->isSuccess())
                    {
                        $calcDeliveries = $calculationResult->get('CALCULATED_DELIVERIES');
                        $calcResult = reset($calcDeliveries);
                    }
                    else
                    {
                        $calcResult = new \Bitrix\Sale\Delivery\CalculationResult();
                        $calcResult->addErrors($calculationResult->getErrors());
                    }

                    $orderClone->doFinalAction(true);
                    $calcOrder = $orderClone;
                }

                if ($calcResult->isSuccess())
                {
                    $arDelivery['ID'] = $deliveryObj->getId();
                    $arDelivery['NAME'] = $deliveryObj->getNameWithParent();
                    $arDelivery['PRICE'] = \Bitrix\Sale\PriceMaths::roundPrecision($calcResult->getPrice());
                    $arDelivery['PRINT_PRICE'] = SaleFormatCurrency($arDelivery['PRICE'], $calcOrder->getCurrency());

                    $currentCalcDeliveryPrice = \Bitrix\Sale\PriceMaths::roundPrecision($calcOrder->getDeliveryPrice());
                    if ($currentCalcDeliveryPrice >= 0 && $arDelivery['PRICE'] != $currentCalcDeliveryPrice)
                    {
                        $arDelivery['DISCOUNT_PRICE'] = $currentCalcDeliveryPrice;
                        $arDelivery['PRINT_DISCOUNT_PRICE_FORMATED'] = SaleFormatCurrency($arDelivery['DELIVERY_DISCOUNT_PRICE'], $calcOrder->getCurrency());
                    }

                    $dateFrom = new Main\Type\DateTime();
                    $dateTo = new Main\Type\DateTime();

                    if ($calcResult->getPeriodFrom() > 0)
                        $dateFrom->add('P'.$calcResult->getPeriodFrom().$calcResult->getPeriodType());

                    if ($calcResult->getPeriodTo() > 0)
                        $dateTo->add('P'.$calcResult->getPeriodTo().$calcResult->getPeriodType());

                    $arDelivery['PERIOD_FROM'] = $dateFrom;
                    $arDelivery['PERIOD_TO'] = $dateTo;
                    $arDelivery['PERIOD_DIFF'] = $dateTo->getDiff($dateFrom)->days;
                    $arDelivery['IS_TODAY'] = $dateTo->format('j') === (new Main\Type\DateTime())->format('j') && $arDelivery['PERIOD_DIFF'] < 1;
                    $arDelivery['NOT_CALC_DATE'] = !$calcResult->getPeriodFrom() && !$calcResult->getPeriodTo();
                    $arDelivery['PERIOD_TEXT'] = $calcResult->getPeriodDescription();
                }
                else
                {
                    if (count($calcResult->getErrorMessages()) > 0)
                    {
                        foreach ($calcResult->getErrorMessages() as $message)
                        {
                            $arDelivery['CALCULATE_ERRORS'] .= $message.'<br>';
                        }
                    }
                    else
                    {
                        $arDelivery['CALCULATE_ERRORS'] = Loc::getMessage('SOA_DELIVERY_CALCULATE_ERROR');
                    }

                    if ($arDelivery['CHECKED'] !== 'Y')
                    {
                        unset($arDeliveryServiceAll[$deliveryId]);
                        continue;
                    }
                }

                $arDelivery['CALCULATE_DESCRIPTION'] = $calcResult->getDescription();
                $this->arResult['DELIVERY'][$deliveryId] = $arDelivery;
            }

            // for discounts: last delivery calculation need to be on real order with selected delivery
            if ($anotherDeliveryCalculated)
            {
                $order->doFinalAction(true);
            }
        }
    }

    /**
     * определяет читать данные из кеша или нет
     * @return bool
     */
    protected function readDataFromCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
            return false;

        return !($this->StartResultCache(false, $this->cacheAddon));
    }

    /**
     * кеширует ключи массива arResult
     */
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
        {
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('catalog'))
            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));

        if (!Loader::includeModule('sale'))
            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
    }

    /**
     * проверяет заполнение обязательных параметров
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        if ($this->arParams['PRODUCT_ID'] < 1)
        {
            throw new Main\ArgumentNullException('PRODUCT_ID');
        }
    }

    /**
     * выполяет действия перед кешированием
     */
    protected function executeProlog()
    {
    }

    /**
     * получение результатов
     */
    protected function getResult()
    {
        $this->arResult['CURRENT_LOCATION'] = Location::getInstance()->getLocation();

        if (!empty($this->arResult['CURRENT_LOCATION']))
        {
            $this->calculateDeliveries();
        }
    }

    /**
     * выполняет действия после выполения компонента, например установка заголовков из кеша
     */
    protected function executeEpilog()
    {
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->checkParams();
            $this->executeProlog();

            if ($this->request->isAjaxRequest())
                $this->getResult();

            $this->includeComponentTemplate();
            $this->executeEpilog();
        }
        catch (Exception $e)
        {
            $this->abortDataCache();
            ShowError($e->getMessage());
        }
    }
}
