<?php

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

if (!empty($arResult['DELIVERY']))
{
    ?>
    <div class="productDelivery__item">
        <strong><?= Loc::getMessage('ARTIXGROUP_GEOIP_YOUR_REGION');?>:</strong>
        <a class="productDelivery__blue trigger" data-dialog="locationDialog"><?= $arResult['CURRENT_LOCATION']['LOCATION_NAME']?></a>
    </div>
    <?
    foreach ($arResult['DELIVERY'] as $delivery)
    {
        ?>
        <div class="productDelivery__item">
            <strong><?= Loc::getMessage('ARTIXGROUP_GEOIP_DELIVERY');?> <?= $delivery['NAME']?></strong>
            <?
            if (!$delivery['IS_TODAY'])
            {
                echo $delivery['PRINT_PERIOD_FROM'];

                if ($delivery['PERIOD_DIFF'] > 0)
                {
                    echo '–'.$delivery['PRINT_PERIOD_TO'];
                }
            }
            else
            {
                echo Loc::getMessage('ARTIXGROUP_GEOIP_TODAY');
            }
            ?>
            —
            <?= $delivery['PRICE'] > 0 ? $delivery['PRINT_PRICE'] : 'Бесплатно'?>
        </div>
        <?
    }

    if ($arParams['ABOUT_DELIVERY_LINK'])
    {
        ?>
        <div class="productDelivery__item">
            <a href="<?= $arParams['ABOUT_DELIVERY_LINK']?>" class="productDelivery__more"><?= Loc::getMessage('ARTIXGROUP_GEOIP_ABOUT_DELIVERY_TEXT');?></a>
        </div>
        <?
    }
}
else
{
    ?>
    <div class="productDelivery" id="productDelivery">
        <div class="productDelivery__item">
            <?= Loc::getMessage('ARTIXGROUP_GEOIP_LOAD');?>
        </div>
    </div>
    <script>
        new GeoDelivery({
            templateName: '<?= $templateName?>',
            parameters: '<?= serialize($arParams)?>',
            ajaxPath: '<?= $componentPath?>/ajax.php',
        });
    </script>
    <?
}
?>



