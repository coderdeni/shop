<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arCurrentValues */

$arTemplateParameters['ABOUT_DELIVERY_LINK'] = array(
    'PARENT' => 'BASE',
    'NAME' => Bitrix\Main\Localization\Loc::getMessage('ARTIXGROUP_ABOUT_DELIVERY_LINK'),
    'TYPE' => 'TEXT',
    'MULTIPLE' => 'N',
    'REFRESH' => 'N',
    'DEFAULT' => '/delivery/'
);
