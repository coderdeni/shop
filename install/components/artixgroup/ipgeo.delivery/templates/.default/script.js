'use strict';
(function (window){
    window.GeoDelivery = function (params)
    {
        this.containerId = !!params.containerId ? params.containerId : 'productDelivery';
        this.ajaxPath = !!params.ajaxPath ? params.ajaxPath : null;
        this.templateName = !!params.templateName ? params.templateName : '.default';
        this.parameters = !!params.parameters ? params.parameters : '';

        BX.ready(BX.delegate(this.init, this));
    };

    window.GeoDelivery.prototype = {

        init: function ()
        {
            this.container = document.getElementById(this.containerId);

            if (!!this.container)
            {
                this.loadDelivery();
            }
        },

        loadDelivery: function ()
        {
            let data = {};

            data.sessid = BX.bitrix_sessid();
            data.template_name = this.templateName;
            data.parameters = this.parameters;

            BX.ajax({
                url: this.ajaxPath,
                method: 'POST',
                dataType: 'html',
                data: data,
                onsuccess: BX.delegate(function(data) {
                    this.container.innerHTML = data;
                }, this)
            });
        },
    };
})(window);
