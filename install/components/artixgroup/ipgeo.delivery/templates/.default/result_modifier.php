<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

foreach ($arResult['DELIVERY'] as &$delivery)
{
    if ($delivery['IS_TODAY']
        || ($delivery['PERIOD_DIFF'] > 0 && $delivery['PERIOD_FROM']->format('m') !== $delivery['PERIOD_TO']->format('m')))
        $delivery['PRINT_PERIOD_FROM'] = strtolower(FormatDate("j F", $delivery['PERIOD_FROM']));
    else
        $delivery['PRINT_PERIOD_FROM'] = strtolower(FormatDate("j", $delivery['PERIOD_FROM']));

    $delivery['PRINT_PERIOD_TO'] = strtolower(FormatDate("j F", $delivery['PERIOD_TO']));
}