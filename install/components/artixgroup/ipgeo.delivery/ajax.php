<?php
/** @global CMain $APPLICATION */
define('STOP_STATISTICS', true);
define('PUBLIC_AJAX_MODE', true);
define('NOT_CHECK_PERMISSIONS', true);

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

Loader::includeModule('artixgroup.shop');

$request = Application::getInstance()->getContext()->getRequest();

if ($request->isAjaxRequest())
{
    $APPLICATION->IncludeComponent(
        "artixgroup:ipgeo.delivery",
        $request->getPost('template_name'),
        unserialize($request->getPost('parameters')),
        false
    );
}
