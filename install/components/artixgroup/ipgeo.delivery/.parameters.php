<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arCurrentValues */

Loader::includeModule('sale');

$personTypes = [];
$filter = [
    'LID' => SITE_ID
];

if ($arCurrentValues['PAYMENT_ID'] > 0)
{
    $personTypeIdList = \Bitrix\Sale\PaySystem\Manager::getPersonTypeIdList((int)$arCurrentValues['PAYMENT_ID']);
    $filter['ID'] = $personTypeIdList;
}
$iterator = CSalePersonType::GetList(['SORT' => 'ASC'], $filter);
while ($personType = $iterator->Fetch())
    $personTypes[$personType['ID']] = $personTypes['NAME'];

$payments = [];
$iterator = \Bitrix\Sale\PaySystem\Manager::getList();
while ($payment = $iterator->fetch())
    $payments[$payment['ID']] = $payment['NAME'];

$group = [
    'N' => Loc::getMessage('ARTIXGROUP_GEOIP_NO_GROUP'),
    'Y' => Loc::getMessage('ARTIXGROUP_GEOIP_GROUP')
];

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS" => array(
        "PRODUCT_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_GEOIP_PRODUCT_ID"),
            "TYPE" => "TEXT",
            "DEFAULT" => ""
        ),
        "GROUP" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_GEOIP_GROUP"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "N",
            "VALUES" => $group,
            "MULTIPLE" => "N",
            "DEFAULT" => "Y",
            "REFRESH" => "N",
        ),
        "PAYMENT_ID" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_GEOIP_PAYMENT_ID"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "N",
            "VALUES" => $payments,
            "MULTIPLE" => "N",
            "DEFAULT" => "1",
            "REFRESH" => "Y",
        ),
        "PERSON_TYPE_ID" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_GEOIP_PERSON_TYPE_ID"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "N",
            "VALUES" => $personTypes,
            "MULTIPLE" => "N",
            "DEFAULT" => "1",
            "REFRESH" => "N",
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
    ),
);
