<?php

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
?>
<div class="catalog__sort">
    <div class="sort">
        <?
        if ($arParams['VIEW_MODE'] === 'LINK')
        {
            ?>
            <div class="sort__title"><?= Loc::getMessage('ARTIXGROUP_ORDER')?>: </div>
            <div class="sort__buttons">
            <?
            foreach ($arResult['SORT'] as $link)
            {
                ?>
                <a class="sortButton<?= $link['ACTIVE'] ? ' sortButton_active sortButton_'.$link['ORDER'] : '';?>" href="<?= $link['URL'] ?>"><?= $link['NAME'] ?></a>
                <?
            }
            ?>
            </div>
            <?
        }
        elseif ($arParams['VIEW_MODE'] === 'LIST')
        {
            foreach ($arResult['SORT'] as $sort)
            {
                ?>
                <div class="select-box select-box_sort">
                    <div class="select-box__current" tabindex="1">
                        <div class="select-box__value">
                            <input class="select-box__input" type="radio" checked="checked">
                            <p class="select-box__input-text"><?= $arResult['CURRENT_SORT']['NAME']?></p>
                        </div>
                        <img class="select-box__icon" src="<?= $templateFolder?>/images/arrow.svg" alt="Arrow Icon" aria-hidden="true">
                    </div>
                    <ul class="select-box__list">
                        <?
                        foreach ($sort['OPTIONS'] as $option)
                        {
                            ?>
                            <li>
                                <label class="select-box__option">
                                    <a href="<?= $option['URL'] ?>"><?= $option['NAME'] ?></a>
                                </label>
                            </li>
                            <?
                        }
                        ?>
                    </ul>
                </div>
                <?
            }
        }
        elseif ($arParams['VIEW_MODE'] === 'ONE_LIST' || $arParams['VIEW_MODE'] === 'TWO_LIST')
        {
            ?>
            <div class="select-box select-box_sort">
                <div class="select-box__current" tabindex="1">
                    <?
                    foreach ($arResult['SORT'] as $link)
                    {
                        ?>
                        <div class="select-box__value">
                            <input class="select-box__input" type="radio"<?= $link['ACTIVE'] ? ' checked="checked"' : ''?>>
                            <p class="select-box__input-text"><?= $link['NAME']?></p>
                        </div>
                        <?
                    }
                    ?>
                    <img class="select-box__icon" src="<?= $templateFolder?>/images/arrow.svg" alt="Arrow Icon" aria-hidden="true">
                </div>
                <ul class="select-box__list">
                    <?
                    foreach ($arResult['SORT'] as $link)
                    {
                        ?>
                        <li>
                            <label class="select-box__option select-box__option-<?= $link['ORDER']?>">
                                <a class="select-box__option-text-item" href="<?= $link['URL'] ?>"><?= $link['NAME'] ?></a>
                            </label>
                        </li>
                        <?
                    }
                    ?>
                </ul>
            </div>
            <?
            if ($arParams['VIEW_MODE'] === 'TWO_LIST')
            {
                ?>
                <div class="select-box select-box_sort">
                    <div class="select-box__current" tabindex="1">
                        <div class="select-box__value">
                            <input class="select-box__input" type="radio" checked="checked">
                            <p class="select-box__input-text"><?= $arResult['CURRENT_ORDER']['NAME']?></p>
                        </div>
                        <img class="select-box__icon" src="<?= $templateFolder?>/images/arrow.svg" alt="Arrow Icon" aria-hidden="true">
                    </div>
                    <ul class="select-box__list">
                        <?
                        foreach ($arResult['ORDER'] as $link)
                        {
                            ?>
                            <li>
                                <label class="select-box__option">
                                    <a href="<?= $link['URL'] ?>"><?= $link['NAME'] ?></a>
                                </label>
                            </li>
                            <?
                        }
                        ?>
                    </ul>
                </div>
                <?
            }
        }
        ?>
    </div>
    <?
    if ($arParams['SHOW_COUNT_ELEMENTS'])
    {
        ?>
        <div class="sort__title"><?= Loc::getMessage('ARTIXGROUP_COUNT_ELEMENTS')?>: <?= $arResult['ELEMENT_COUNT']?></div>
        <?
    }

    if (count($arResult['PER_PAGE']) > 0)
    {
        ?>
        <div class="paginationCount">
            <div class="paginationCount__select">
                <div class="select-box">
                    <div class="select-box__current" tabindex="1">
                        <div class="select-box__value">
                            <input class="select-box__input" type="radio" checked="checked">
                            <p class="select-box__input-text"><?= $arResult['CURRENT_PER_PAGE']['NAME']?></p>
                        </div>
                        <img class="select-box__icon" src="<?= $templateFolder?>/images/arrow.svg" alt="Arrow Icon" aria-hidden="true">
                    </div>
                    <ul class="select-box__list">
                        <?
                        foreach ($arResult['PER_PAGE'] as $link)
                        {
                            ?>
                            <li>
                                <label class="select-box__option">
                                    <a href="<?= $link['URL'] ?>"><?= $link['NAME'] ?></a>
                                </label>
                            </li>
                            <?
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <?
    }
    ?>
</div>

