<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


if(!CModule::IncludeModule("iblock"))
    return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(["all"=>" "]);

$arIBlocks = [['' => '']];
$db_iblock = CIBlock::GetList(
    ["SORT" => "ASC"],
    [
        "SITE_ID" => $_REQUEST["site"],
        "TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "all" ? $arCurrentValues["IBLOCK_TYPE"] : "")
    ]
);
while ($arRes = $db_iblock->Fetch())
{
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arFields = [
    "id"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_ID"),
    "sort"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_SORT"),
    "timestamp_x"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_TIMESTAMP_X"),
    "name"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_NAME"),
    "active_from"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_ACTIVE_FROM"),
    "active_to"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_ACTIVE_TO"),
    "status"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_STATUS"),
    "code"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_CODE"),
    "iblock_id"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_IBLOCK_ID"),
    "modified_by"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_MODIFIED_BY"),
    "active"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_ACTIVE"),
    "show_counter"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_SHOW_COUNTER"),
    "show_counter_start"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_SHOW_COUNTER_START"),
    "shows"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_SHOWS"),
    "rand"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_RAND"),
    "xml_id"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_XML_ID"),
    "tags"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_TAGS"),
    "created"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_CREATED"),
    "created_date"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_CREATED_DATE"),
    "cnt"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_CNT"),
    "IBLOCK_SECTION_ID"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_IBLOCK_SECTION_ID"),
    "HAS_PREVIEW_PICTURE"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_HAS_PREVIEW_PICTURE"),
    "HAS_DETAIL_PICTURE"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_HAS_DETAIL_PICTURE"),
];

if (\Bitrix\Main\Loader::includeModule('catalog'))
{
    $arFields = array_merge($arFields, [
        "CATALOG_QUANTITY"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_CATALOG_QUANTITY"),
        "CATALOG_WEIGHT"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_CATALOG_WEIGHT"),
        "CATALOG_AVAILABLE"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_CATALOG_AVAILABLE"),
        "CATALOG_BUNDLE"=>GetMessage("ARTIXGROUP_IBLOCK_DESC_CATALOG_BUNDLE"),
    ]);

    $dbResultList = CCatalogGroup::GetList(
        ['SORT' => 'ASC'],
        [],
        false,
        false,
        ['ID', 'NAME']
    );

    while ($price = $dbResultList->Fetch())
        $arFields['catalog_PRICE_'.$price['ID']] = '['.GetMessage("ARTIXGROUP_VIEW_PRICE").'] '.$price['NAME'];
}

$rsProp = CIBlockProperty::GetList(
    [
        "sort" => "asc",
        "name" => "asc"
    ],
    [
        "ACTIVE"=>"Y",
        "IBLOCK_ID" => (isset($arCurrentValues["IBLOCK_ID"]) ? $arCurrentValues["IBLOCK_ID"] : $arCurrentValues["ID"])
    ]
);
while ($arr = $rsProp->Fetch())
{
    $arFields['property_' . $arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
}

$arSelectedFields = [];
if ($arCurrentValues["SORT_FIELDS"])
{
    foreach ($arCurrentValues["SORT_FIELDS"] as $value)
    {
        $arSelectedFields[$value] = $arFields[$value];
    }
}

$viewModeList = [
    'LINK' => GetMessage("ARTIXGROUP_VIEW_MODE_LINK"),
    'LIST' => GetMessage("ARTIXGROUP_VIEW_MODE_LIST"),
    'ONE_LIST' => GetMessage("ARTIXGROUP_VIEW_MODE_ONE_LIST"),
    'TWO_LIST' => GetMessage("ARTIXGROUP_VIEW_MODE_TWO_LIST"),
];

$orderList = [
    'desc' => GetMessage("ARTIXGROUP_ORDER_NAME_DESC_VALUE"),
    'asc' => GetMessage("ARTIXGROUP_ORDER_NAME_ASC_VALUE")
];

$arComponentParameters = array(
    "GROUPS" => array(
        "SORT_SETTINGS" => array(
            "NAME" => GetMessage("ARTIXGROUP_SORT_SETTINGS"),
        ),
        "COUNT_ELEMENTS_SETTINGS" => array(
            "NAME" => GetMessage("ARTIXGROUP_COUNT_ELEMENTS_SETTINGS"),
        ),
        "FILTER_ELEMENTS_SETTINGS" => array(
            "NAME" => GetMessage("ARTIXGROUP_FILTER_ELEMENTS_SETTINGS"),
        ),
    ),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => Array(
            "PARENT" => "BASE",
            "NAME"=>GetMessage("ARTIXGROUP_IBLOCK_TYPE"),
            "TYPE"=>"LIST",
            "VALUES"=>$arTypesEx,
            "DEFAULT"=>"catalog",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => Array(
            "PARENT" => "BASE",
            "NAME"=>GetMessage("ARTIXGROUP_IBLOCK_ID"),
            "TYPE"=>"LIST",
            "VALUES"=>$arIBlocks,
            "DEFAULT"=>'1',
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "Y",
        ),
        "DISPLAY_ELEMENT_COUNT" => array(
            "PARENT" => "FILTER_ELEMENTS_SETTINGS",
            "NAME" => GetMessage("ARTIXGROUP_DISPLAY_ELEMENT_COUNT"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "REFRESH" => "Y",
        ),
        "DISPLAY_ELEMENT_COUNT_LIST" => array(
            "PARENT" => "COUNT_ELEMENTS_SETTINGS",
            "NAME" => GetMessage("ARTIXGROUP_DISPLAY_ELEMENT_COUNT_LIST"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
            "REFRESH" => "Y",
        ),
        "DEFAULT_ELEMENT_COUNT" => Array(
            "PARENT" => "COUNT_ELEMENTS_SETTINGS",
            "NAME"=> GetMessage("ARTIXGROUP_DEFAULT_ELEMENT_COUNT"),
            "TYPE"=>"STRING",
            "DEFAULT"=> 20,
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "N",
        ),
        "VIEW_MODE" => Array(
            "PARENT" => "BASE",
            "NAME"=>GetMessage("ARTIXGROUP_VIEW_MODE"),
            "TYPE"=>"LIST",
            "VALUES"=>$viewModeList,
            "DEFAULT"=>'LINK',
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"N",
            "REFRESH" => "Y",
        ),
        "DEFAULT_ORDER" => Array(
            "PARENT" => "SORT_SETTINGS",
            "NAME"=>GetMessage("ARTIXGROUP_DEFAULT_ORDER"),
            "TYPE"=>"LIST",
            "VALUES"=>$orderList,
            "DEFAULT"=>'desc',
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"N",
            "REFRESH" => "N",
        ),
        "SORT_FIELDS" => Array(
            "PARENT" => "SORT_SETTINGS",
            "NAME"=>GetMessage("ARTIXGROUP_SORT_FIELDS"),
            "TYPE"=>"LIST",
            "VALUES"=>$arFields,
            "DEFAULT"=>'1',
            "MULTIPLE"=>"Y",
            "ADDITIONAL_VALUES"=>"N",
            "REFRESH" => "Y",
        ),
        "DEFAULT_SORT_FIELD" => Array(
            "PARENT" => "SORT_SETTINGS",
            "NAME"=>GetMessage("ARTIXGROUP_DEFAULT_SORT_FIELD"),
            "TYPE"=>"LIST",
            "VALUES"=>$arSelectedFields,
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"N",
            "REFRESH" => "N",
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
    ),
);

if ($arCurrentValues["VIEW_MODE"] === 'LIST' || $arCurrentValues["VIEW_MODE"] === 'TWO_LIST')
{
    $arComponentParameters['PARAMETERS']["ORDER_NAME_DESC"] = Array(
        "PARENT" => "SORT_SETTINGS",
        "NAME"=> GetMessage("ARTIXGROUP_ORDER_NAME_DESC"),
        "TYPE"=>"STRING",
        "DEFAULT"=> GetMessage("ARTIXGROUP_ORDER_NAME_DESC_VALUE"),
        "MULTIPLE"=>"N",
        "ADDITIONAL_VALUES"=>"Y",
        "REFRESH" => "N",
    );
    $arComponentParameters['PARAMETERS']["ORDER_NAME_ASC"] = Array(
        "PARENT" => "SORT_SETTINGS",
        "NAME"=> GetMessage("ARTIXGROUP_ORDER_NAME_ASC"),
        "TYPE"=>"STRING",
        "DEFAULT"=> GetMessage("ARTIXGROUP_ORDER_NAME_ASC_VALUE"),
        "MULTIPLE"=>"N",
        "ADDITIONAL_VALUES"=>"Y",
        "REFRESH" => "N",
    );
}

if ($arCurrentValues["VIEW_MODE"] === 'ONE_LIST')
{
    $arComponentParameters['PARAMETERS']["UNILATERAL"] = Array(
        "PARENT" => "SORT_SETTINGS",
        "NAME"=>GetMessage("ARTIXGROUP_SORT_UNILATERAL"),
        "TYPE"=>"LIST",
        "VALUES"=>$arSelectedFields,
        "DEFAULT"=>'1',
        "MULTIPLE"=>"Y",
        "ADDITIONAL_VALUES"=>"N",
        "REFRESH" => "Y",
    );

    if (count($arCurrentValues["UNILATERAL"]) > 0)
    {
        foreach ($arCurrentValues["UNILATERAL"] as $value)
        {
            $arComponentParameters['PARAMETERS']["DEFAULT_ORDER_{$value}"] = Array(
                "PARENT" => "SORT_SETTINGS",
                "NAME"=>GetMessage("ARTIXGROUP_ORDER", ['#FIELD#' => $arFields[$value]]),
                "TYPE"=>"LIST",
                "VALUES"=>$orderList,
                "DEFAULT"=>'desc',
                "MULTIPLE"=>"N",
                "ADDITIONAL_VALUES"=>"N",
                "REFRESH" => "Y",
            );
        }
    }
}

if (count($arCurrentValues["SORT_FIELDS"]) > 0)
{
    foreach ($arCurrentValues["SORT_FIELDS"] as $value)
    {
        if ($arCurrentValues["VIEW_MODE"] === 'ONE_LIST')
        {
            foreach ($orderList as $key => $name)
            {
                if (in_array($value, $arCurrentValues["UNILATERAL"]) && $arCurrentValues["DEFAULT_ORDER_{$value}"] !== $key)
                    continue;

                $arComponentParameters['PARAMETERS']["SORT_{$value}_{$key}_NAME"] = Array(
                    "PARENT" => "SORT_SETTINGS",
                    "NAME"=> GetMessage("ARTIXGROUP_FIELD_NAME", ['#FIELD#' => $arFields[$value]]) . " ({$name})",
                    "TYPE"=>"STRING",
                    "DEFAULT"=> $arFields[$value],
                    "MULTIPLE"=>"N",
                    "ADDITIONAL_VALUES"=>"Y",
                    "REFRESH" => "N",
                );
            }
        }
        else
        {
            $arComponentParameters['PARAMETERS']["SORT_{$value}_NAME"] = Array(
                "PARENT" => "SORT_SETTINGS",
                "NAME"=> GetMessage("ARTIXGROUP_FIELD_NAME", ['#FIELD#' => $arFields[$value]]),
                "TYPE"=>"STRING",
                "DEFAULT"=> $arFields[$value],
                "MULTIPLE"=>"N",
                "ADDITIONAL_VALUES"=>"Y",
                "REFRESH" => "N",
            );
        }

        $arComponentParameters['PARAMETERS']["SORT_{$value}_CODE"] = Array(
            "PARENT" => "SORT_SETTINGS",
            "NAME"=> GetMessage("ARTIXGROUP_FIELD_CODE", ['#FIELD#' => $arFields[$value]]),
            "TYPE"=>"STRING",
            "DEFAULT"=> $value,
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "Y",
        );
    }
}

if ($arCurrentValues["DISPLAY_ELEMENT_COUNT"] === 'Y')
{
    $params = array(
        "SHOW_ONLY_ACTIVE" => array(
            "PARENT" => "FILTER_ELEMENTS_SETTINGS",
            "NAME" => GetMessage("ARTIXGROUP_SHOW_ONLY_ACTIVE"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
            "REFRESH" => "N",
        ),
        "FILTER_NAME" => Array(
            "PARENT" => "FILTER_ELEMENTS_SETTINGS",
            "NAME"=> GetMessage("ARTIXGROUP_FILTER_NAME"),
            "TYPE"=>"STRING",
            "DEFAULT"=> "arrFilter",
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "N",
        ),
        "SMART_FILTER_PATH" => array(
            "PARENT" => "FILTER_ELEMENTS_SETTINGS",
            "NAME" => GetMessage("ARTIXGROUP_SMART_FILTER_PATH"),
            "DEFAULT" => 'SMART_FILTER_PATH',
        ),
        "IBLOCK_SECTION_ID" => Array(
            "PARENT" => "FILTER_ELEMENTS_SETTINGS",
            "NAME"=> GetMessage("ARTIXGROUP_IBLOCK_SECTION_ID"),
            "TYPE"=>"STRING",
            "DEFAULT"=> '={$_REQUEST["SECTION_ID"]}',
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "N",
        ),
        "IBLOCK_SECTION_CODE" => Array(
            "PARENT" => "FILTER_ELEMENTS_SETTINGS",
            "NAME"=> GetMessage("ARTIXGROUP_IBLOCK_SECTION_CODE"),
            "TYPE"=>"STRING",
            "DEFAULT"=> "",
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "N",
        ),
    );

    $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], $params);
}

if ($arCurrentValues["DISPLAY_ELEMENT_COUNT_LIST"] === 'Y')
{
    $params = [
        "ELEMENT_COUNT_LIST" => Array(
            "PARENT" => "COUNT_ELEMENTS_SETTINGS",
            "NAME"=> GetMessage("ARTIXGROUP_ELEMENT_COUNT_LIST"),
            "TYPE"=>"STRING",
            "DEFAULT"=> array(10,20,50),
            "MULTIPLE"=>"Y",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "N",
        ),
        "TEMPLATE_ELEMENT_COUNT" => Array(
            "PARENT" => "COUNT_ELEMENTS_SETTINGS",
            "NAME"=> GetMessage("ARTIXGROUP_TEMPLATE_ELEMENT_COUNT"),
            "TYPE"=>"STRING",
            "DEFAULT"=> "по %d",
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "N",
        ),
    ];

    $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], $params);
}