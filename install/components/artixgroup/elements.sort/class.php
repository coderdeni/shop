<?php

namespace Artixgroup\Components;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use Bitrix\Main\Context;
use \Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Web\Uri;

class ElementsSort extends \CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $arFilter = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();

    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param $params
     * @return array
     * @internal param array $arParams
     */
    public function onPrepareComponentParams($params)
    {
        $params['IBLOCK_ID'] = (int)$params['IBLOCK_ID'];
        $params['DEFAULT_ELEMENT_COUNT'] = (int)$params['DEFAULT_ELEMENT_COUNT'];
        $params['SMART_FILTER_PATH'] = !empty($params['SMART_FILTER_PATH'])
            ? trim($params['SMART_FILTER_PATH']) : 'SMART_FILTER_PATH';
        $params['FILTER_NAME'] = trim($params['FILTER_NAME']);
        $params['DEFAULT_SORT_FIELD'] = !empty($params['DEFAULT_SORT_FIELD'])
            ? trim($params['DEFAULT_SORT_FIELD']) : 'SORT';
        $params['DEFAULT_ORDER'] = !empty($params['DEFAULT_ORDER'])
            ? trim($params['DEFAULT_ORDER']) : 'desc';
        $params['DISPLAY_ELEMENT_COUNT_LIST'] = !empty($params['DISPLAY_ELEMENT_COUNT_LIST'])
            ? trim($params['DISPLAY_ELEMENT_COUNT_LIST']) : 'Y';

        return $params;
    }

    /**
     * определяет читать данные из кеша или нет
     * @return bool
     */
    protected function readDataFromCache()
    {
        global $USER;

        if ($this->arParams['CACHE_TYPE'] === 'N')
            return false;

        if (is_array($this->cacheAddon))
            $this->cacheAddon[] = $USER->GetUserGroupArray();
        else
            $this->cacheAddon = [$USER->GetUserGroupArray()];

        return !($this->startResultCache(false, $this->cacheAddon, md5(serialize($this->arParams))));
    }

    /**
     * кеширует ключи массива arResult
     */
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
        {
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    /**
     * завершает кеширование
     * @return bool
     */
    protected function endCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
        {
            return false;
        }

        $this->endResultCache();
        return true;
    }

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Main\Loader::includeModule('iblock'))
        {
            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
        }
    }

    /**
     * проверяет заполнение обязательных параметров
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        if ($this->arParams['IBLOCK_ID'] < 1)
        {
            throw new Main\ArgumentNullException('IBLOCK_ID');
        }
    }

    /**
     * выполяет действия перед кешированием
     */
    protected function executeProlog()
    {
        if ($this->getRequestParam('per_page'))
        {
            $_SESSION['PER_PAGE'] = $this->getRequestParam('per_page');
        }

        $perPage = $_SESSION['PER_PAGE'] ? $_SESSION['PER_PAGE'] : $this->arParams['DEFAULT_ELEMENT_COUNT'];

        $this->cacheAddon = array(
            'path' => $this->getRequestUri()->getPath(),
            'order' => $this->getRequestParam('order', $this->arParams['DEFAULT_ORDER']),
            'sort' => $this->getRequestParam('sort', $this->arParams['DEFAULT_SORT_FIELD']),
            'per_page' => $this->getRequestParam('per_page', $perPage),
        );

        if (!empty($this->arParams['SMART_FILTER_PATH']))
        {
            $this->cacheAddon['filter_path'] = $this->getRequestParam($this->arParams['SMART_FILTER_PATH']);
        }
    }

    /**
     * получение результатов
     */
    protected function getResult()
    {
        $orderList = ['desc', 'asc'];
        $sort = $this->arParams['DEFAULT_SORT_FIELD'];
        $order = $this->getRequestParam('order', $this->arParams['DEFAULT_ORDER']);

        if ($requestSort = $this->getRequestParam('sort'))
        {
            foreach ($this->arParams['SORT_FIELDS'] as $code)
            {
                if ($this->getKey($code) === $requestSort)
                {
                    $sort = $code;
                    break;
                }
            }
        }

        if ($this->arParams['VIEW_MODE'] === 'ONE_LIST')
        {
            foreach ($this->arParams['SORT_FIELDS'] as $code)
            {
                foreach ($orderList as $key)
                {
                    if (in_array($code, $this->arParams['UNILATERAL']) && $key !== $this->arParams["DEFAULT_ORDER_{$code}"])
                    {
                        continue;
                    }

                    $this->arResult['SORT'][$code . '_' . $key] = [
                        'CODE' => $code,
                        'NAME' => $this->arParams["SORT_{$code}_{$key}_NAME"],
                        'URL' => $this->getRequestUri()->addParams([
                            'sort' => $this->getKey($code),
                            'order' => $key
                        ])->getUri(),
                        'ACTIVE' => $sort === $code && $order === $key,
                        'ORDER' => $key
                    ];
                }
            }

            $this->arResult['CURRENT_SORT'] = $this->arResult['SORT'][$sort.'_'.$order];
        }
        elseif ($this->arParams['VIEW_MODE'] === 'TWO_LIST')
        {
            $this->arResult['ORDER'] = [
                'asc' => [
                    'NAME' => $this->arParams["ORDER_NAME_ASC"],
                    'URL' => $this->getRequestUri()->addParams(['sort' => $this->getKey($sort), 'order' => 'asc'])->getUri(),
                    'ORDER' => 'asc',
                    'ACTIVE' => $order === 'asc'
                ],
                'desc' => [
                    'NAME' => $this->arParams["ORDER_NAME_DESC"],
                    'URL' => $this->getRequestUri()->addParams(['sort' => $this->getKey($sort), 'order' => 'desc'])->getUri(),
                    'ORDER' => 'asc',
                    'ACTIVE' => $order === 'desc'
                ]
            ];

            $this->arResult['CURRENT_ORDER'] = $this->arResult['ORDER'][$order];

            foreach ($this->arParams['SORT_FIELDS'] as $code)
            {
                $this->arResult['SORT'][$code] = [
                    'CODE' => $code,
                    'NAME' => $this->arParams["SORT_{$code}_NAME"],
                    'URL' => $this->getRequestUri()->addParams([
                        'sort' => $this->getKey($code),
                        'order' => $order
                    ])->getUri(),
                    'ACTIVE' => $sort === $code,
                    'ORDER' => $order
                ];
            }

            $this->arResult['CURRENT_SORT'] = $this->arResult['SORT'][$sort];
        }
        elseif ($this->arParams['VIEW_MODE'] === 'LIST')
        {
            foreach ($this->arParams['SORT_FIELDS'] as $code)
            {
                $this->arResult['SORT'][$code]['NAME'] = $this->arParams["SORT_{$code}_NAME"];

                foreach ($orderList as $key => $value)
                {
                    $this->arResult['SORT'][$code]['OPTIONS'][] = [
                        'CODE' => $code,
                        'NAME' => $this->arParams["ORDER_NAME_{$key}"],
                        'URL' => $this->getRequestUri()->addParams(array(
                            'sort' => $this->getKey($code),
                            'order' => $key
                        ))->getUri(),
                        'ACTIVE' => $sort === $code && $order === $key,
                        'ORDER' => $key
                    ];

                    if ($sort === $code && $order === $key)
                        $this->arResult['CURRENT_SORT'] = $this->arResult['SORT'][$sort]['OPTIONS'][$key];
                }
            }
        }
        else
        {
            $invertOrder = $order === 'asc' ? 'desc' : 'asc';

            foreach ($this->arParams['SORT_FIELDS'] as $code)
            {
                $this->arResult['SORT'][$code] = [
                    'CODE' => $code,
                    'NAME' => $this->arParams["SORT_{$code}_NAME"],
                    'URL' => $this->getRequestUri()->addParams([
                        'sort' => $this->getKey($code),
                        'order' => isset($invertOrder) ? $invertOrder : $order
                    ])->getUri(),
                    'ACTIVE' => $sort === $code,
                    'ORDER' => isset($invertOrder) ? $invertOrder : $order
                ];
            }

            $this->arResult['CURRENT_SORT'] = $this->arResult['SORT'][$sort];
        }

        if ($this->arParams['DISPLAY_ELEMENT_COUNT_LIST'] === 'Y')
        {
            foreach ($this->arParams['ELEMENT_COUNT_LIST'] as $count)
            {
                if (!empty($count))
                {
                    $this->arResult['PER_PAGE'][$count] = array(
                        'NAME' => sprintf($this->arParams['TEMPLATE_ELEMENT_COUNT'], $count),
                        'ACTIVE' => (int)$this->cacheAddon['per_page'] === (int)$count,
                        'COUNT' => $count,
                        'URL' => $this->getRequestUri()->addParams(array('per_page' => $count))->getUri(),
                    );
                }
            }

            if (count($this->arParams['ELEMENT_COUNT_LIST']) > 0)
            {
                $this->arResult['PER_PAGE']['all'] = array(
                    'NAME' => 'Все',
                    'ACTIVE' => $this->cacheAddon['per_page'] === 'all',
                    'COUNT' => 'all',
                    'URL' => $this->getRequestUri()->addParams(array('SHOWALL_2' => 1))->getUri(),
                );
            }

            $this->arResult['CURRENT_PER_PAGE'] = $this->arResult['PER_PAGE'][$this->cacheAddon['per_page']];
        }

        if ($this->arParams['DISPLAY_ELEMENT_COUNT'] === 'Y')
        {
            $this->arResult['ELEMENT_COUNT'] = $this->getElementsCount();
        }

        unset($requestSort, $sort, $order, $orderList);
    }

    protected function getKey($code)
    {
        if (isset($this->arParams["SORT_{$code}_CODE"]))
        {
            return $this->arParams["SORT_{$code}_CODE"];
        }

        return $code;
    }

    protected function getRequestParam($key, $default = null)
    {
        return $this->request->get($key) ? $this->request->get($key) : $default;
    }

    protected function getRequestUri()
    {
        return new Uri($this->request->getRequestUri());
    }

    protected function getElementsCount()
    {
        $globalFilter = [];
        $filter = [
            'IBLOCK_ID' => $this->arParams['IBLOCK_ID']
        ];

        if (!empty($this->arParams['FILTER_NAME']))
        {
            $globalFilter = $GLOBALS[$this->arParams['FILTER_NAME']];
        }

        if (!empty($this->arParams['IBLOCK_SECTION_ID']))
        {
            $filter['SECTION_ID'] = (int)$this->arParams['IBLOCK_SECTION_ID'];
        }
        elseif (!empty($this->arParams['IBLOCK_SECTION_CODE']))
        {
            $filter['SECTION_CODE'] = $this->arParams['IBLOCK_SECTION_CODE'];
        }

        if ($this->arParams['SHOW_ONLY_ACTIVE'] === 'Y')
        {
            $filter['ACTIVE'] = $this->arParams['SHOW_ONLY_ACTIVE'];
        }

        return CIBlockElement::GetList(false, array_merge($filter, $globalFilter), array('IBLOCK_ID'))->Fetch()['CNT'];
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->checkParams();
            $this->executeProlog();

            if (!$this->readDataFromCache())
            {
                $this->getResult();
                $this->putDataToCache();
                $this->includeComponentTemplate();
            }

            return [
                'SORT' => $this->arResult['CURRENT_SORT']['CODE'],
                'ORDER' => $this->arResult['CURRENT_SORT']['ORDER'],
                'PER_PAGE' => $this->arResult['CURRENT_PER_PAGE']['COUNT']
            ];
        }
        catch (Exception $e)
        {
            $this->abortDataCache();
            ShowError($e->getMessage());
        }

        return array(
            'SORT' => $this->arParams['DEFAULT_SORT_FIELD'],
            'ORDER' => 'desc',
            'PER_PAGE' => $this->arParams['DEFAULT_ELEMENT_COUNT']
        );
    }
}
