<?php
$MESS ['ARTIXGROUP_IBLOCK_TYPE'] = "Тип информационного блока";
$MESS ['ARTIXGROUP_IBLOCK_ID'] = "Код информационного блока";
$MESS ['ARTIXGROUP_FILTER_NAME'] = "Имя выходящего массива для фильтрации";
$MESS ['ARTIXGROUP_SMART_FILTER_PATH'] = "Имя выходящего парметра для фильтрации";
$MESS ['ARTIXGROUP_DISPLAY_ELEMENT_COUNT'] = "Отображать счетчик кол-во элементов";
$MESS ['ARTIXGROUP_DISPLAY_ELEMENT_COUNT_LIST'] = "Отображать переключатель кол-во элементов на странице";
$MESS ['ARTIXGROUP_SHOW_ONLY_ACTIVE'] = "Считать только активные элементы";
$MESS ['ARTIXGROUP_IBLOCK_SECTION_ID'] = "ID раздела ифноблока";
$MESS ['ARTIXGROUP_IBLOCK_SECTION_CODE'] = "Код раздела ифноблока";

$MESS ['ARTIXGROUP_VIEW_PRICE'] = "Цена";
$MESS ['ARTIXGROUP_VIEW_MODE'] = "Режим отображения";

$MESS ['ARTIXGROUP_ORDER_NAME_DESC'] = 'Текст "по убыванию"';
$MESS ['ARTIXGROUP_ORDER_NAME_ASC'] = 'Текст "по возрастанию"';
$MESS ['ARTIXGROUP_ORDER_NAME_DESC_VALUE'] = "По убыванию";
$MESS ['ARTIXGROUP_ORDER_NAME_ASC_VALUE'] = "По возрастанию";
$MESS ['ARTIXGROUP_ORDER'] = 'Направление сортировки для поля "#FIELD#"';

$MESS ['ARTIXGROUP_SORT_FIELDS'] = "Поля для сортировки";
$MESS ['ARTIXGROUP_SORT_UNILATERAL'] = "Поля с одностороней сортировкой";
$MESS ['ARTIXGROUP_DEFAULT_SORT_FIELD'] = "Поле для сортировки по умолчанию";
$MESS ['ARTIXGROUP_DEFAULT_ORDER'] = "Направление сортировки по умолчанию";

$MESS ['ARTIXGROUP_DEFAULT_ELEMENT_COUNT'] = "Кол-во элементов на странице по умолчанию";
$MESS ['ARTIXGROUP_ELEMENT_COUNT_LIST'] = "Переключатель кол-во элементов на странице";
$MESS ['ARTIXGROUP_TEMPLATE_ELEMENT_COUNT'] = "Шаблон для элемента переключателя кол-во элементов";

$MESS ['ARTIXGROUP_COUNT_ELEMENTS_SETTINGS'] = "Настройка переключателя кол-во элементов на странице";
$MESS ['ARTIXGROUP_FILTER_ELEMENTS_SETTINGS'] = "Настройка фильтра элементов";
$MESS ['ARTIXGROUP_SORT_SETTINGS'] = "Настройка сортировки элементов";

$MESS ["ARTIXGROUP_IBLOCK_DESC_DESC"] = "По убыванию";
$MESS ["ARTIXGROUP_IBLOCK_DESC_ID"] = "ID элемента";
$MESS ["ARTIXGROUP_IBLOCK_DESC_SORT"] = "Индекс сортировки";
$MESS ["ARTIXGROUP_IBLOCK_DESC_TIMESTAMP_X"] = "Дата изменения";
$MESS ["ARTIXGROUP_IBLOCK_DESC_NAME"] = "Название";
$MESS ["ARTIXGROUP_IBLOCK_DESC_ACTIVE_FROM"] = "Начало периода действия элемента";
$MESS ["ARTIXGROUP_IBLOCK_DESC_ACTIVE_TO"] = "Окончание периода действия элемента";
$MESS ["ARTIXGROUP_IBLOCK_DESC_STATUS"] = "Код статуса элемента в документообороте";
$MESS ["ARTIXGROUP_IBLOCK_DESC_CODE"] = "Символьный код элемента";
$MESS ["ARTIXGROUP_IBLOCK_DESC_IBLOCK_ID"] = "Числовой код информационного блока";
$MESS ["ARTIXGROUP_IBLOCK_DESC_MODIFIED_BY"] = "Код последнего изменившего пользователя";
$MESS ["ARTIXGROUP_IBLOCK_DESC_ACTIVE"] = "Признак активности элемента";
$MESS ["ARTIXGROUP_IBLOCK_DESC_SHOW_COUNTER"] = "Количество показов элемента";
$MESS ["ARTIXGROUP_IBLOCK_DESC_SHOW_COUNTER_START"] = "Время первого показа элемента";
$MESS ["ARTIXGROUP_IBLOCK_DESC_SHOWS"] = "Усредненное количество показов";
$MESS ["ARTIXGROUP_IBLOCK_DESC_RAND"] = "Случайный порядок";
$MESS ["ARTIXGROUP_IBLOCK_DESC_XML_ID"] = "Внешний код";
$MESS ["ARTIXGROUP_IBLOCK_DESC_TAGS"] = "Теги";
$MESS ["ARTIXGROUP_IBLOCK_DESC_CREATED"] = "Время создания";
$MESS ["ARTIXGROUP_IBLOCK_DESC_CREATED_DATE"] = "Дата создания без учета времени";
$MESS ["ARTIXGROUP_IBLOCK_DESC_CNT"] = "Количество элементов";
$MESS ["ARTIXGROUP_IBLOCK_DESC_IBLOCK_SECTION_ID"] = "ID раздела";
$MESS ["ARTIXGROUP_IBLOCK_DESC_HAS_PREVIEW_PICTURE"] = "Сортировка по наличию и отсутствию картинки анонса.";
$MESS ["ARTIXGROUP_IBLOCK_DESC_HAS_DETAIL_PICTURE"] = "Сортировка по наличию и отсутствию детальной картинки. ";
$MESS ["ARTIXGROUP_IBLOCK_DESC_CATALOG_QUANTITY"] = "Общее количество товара";
$MESS ["ARTIXGROUP_IBLOCK_DESC_CATALOG_WEIGHT"] = "Вес товара";
$MESS ["ARTIXGROUP_IBLOCK_DESC_CATALOG_AVAILABLE"] = "Признак доступности товара ";
$MESS ["ARTIXGROUP_IBLOCK_DESC_CATALOG_BUNDLE"] = "Сортировка по наличию набора у товара";

$MESS ["ARTIXGROUP_VIEW_MODE_LINK"] = "Ссылками";
$MESS ["ARTIXGROUP_VIEW_MODE_LIST"] = "Списками";
$MESS ["ARTIXGROUP_VIEW_MODE_ONE_LIST"] = "Одним списком";
$MESS ["ARTIXGROUP_VIEW_MODE_TWO_LIST"] = "Двумя списками";

$MESS ["ARTIXGROUP_FIELD_NAME"] = 'Название поля: "#FIELD#"';
$MESS ["ARTIXGROUP_FIELD_CODE"] = 'URL код поля: "#FIELD#"';