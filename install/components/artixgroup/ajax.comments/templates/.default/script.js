(function (window){
    window.ArtixgroupAjaxComments = function (params)
    {
        this.actionUrl = !!params.actionUrl ? params.actionUrl : '/bitrix/components/artixgroup/ajax.comments/ajax.php';
        this.templateName = !!params.templateName ? params.templateName : '.default';
        this.parameters = !!params.parameters ? params.parameters : '';
        this.selectorMoreButton = !!params.selectorMoreButton ? params.selectorMoreButton : '[data-role="comments-more"]';
        this.selectorCommentContainer = !!params.selectorCommentContainer ? params.selectorCommentContainer : '[data-role="comments"]';
        this.selectorForm = !!params.selectorCommentContainer ? params.selectorCommentContainer : '[data-role="comments-form"]';
        this.selectorButtonForm = !!params.selectorButtonForm ? params.selectorButtonForm : '[data-role="comments-send"]';
        this.selectorRating = !!params.selectorRating ? params.selectorRating : '[data-role="comments-rating"]';
        this.selectorRatingItem = !!params.selectorRatingItem ? params.selectorRatingItem : '.rating__item';
        this.selectorInputRating = !!params.selectorInputRating ? params.selectorInputRating : '[data-role="comments-rating-value"]';
        this.selectorCommentsResult = !!params.selectorCommentsResult ? params.selectorCommentsResult : '[data-role="comments-result"]';
        this.styleFullRating = !!params.styleFullRating ? params.styleFullRating : 'rating__item_full';

        BX.ready(BX.delegate(this.init, this));
    };

    window.ArtixgroupAjaxComments.prototype = {
        init: function()
        {
            this.commentContainer = document.querySelector(this.selectorCommentContainer);
            this.ratingContainer = document.querySelector(this.selectorRating);
            this.inputRating = document.querySelector(this.selectorInputRating);
            this.form = document.querySelector(this.selectorForm);
            this.sendButton = document.querySelector(this.selectorButtonForm);

            if (!!this.sendButton)
                BX.bind(this.sendButton, 'click', BX.proxy(this.sendForm, this));

            if (!!this.commentContainer)
            {
                this.moreButton = this.commentContainer.querySelector(this.selectorMoreButton);
                this.getNextPage();
            }

            if (!!this.ratingContainer && !!this.inputRating)
            {
                BX.bind(this.ratingContainer, 'mouseleave', BX.proxy(this.onMouseLeaveRating, this));
                this.ratingStars = this.ratingContainer.querySelectorAll(this.selectorRatingItem);

                if (!!this.ratingStars)
                {
                    this.setRating(parseInt(this.inputRating.value) - 1);

                    for (var i = 0; i < this.ratingStars.length; i++)
                    {
                        BX.bind(this.ratingStars[i], 'click', BX.proxy(this.onClickRatingStar, this));
                        BX.bind(this.ratingStars[i], 'mouseover', BX.proxy(this.onMouseOverRatingStar, this));
                    }
                }
            }

            this.initialized = true;
        },
        getNextPage: function()
        {
            var page = 1;

            if (!!this.moreButton)
            {
                this.moreButton.classList.add('loading');
                page = this.moreButton.getAttribute('data-next-page');
            }

            BX.ajax({
                url: this.actionUrl,
                method: 'POST',
                data: {
                    action: 'load-more',
                    sessid: BX.bitrix_sessid(),
                    parameters: this.parameters,
                    templateName: this.templateName,
                    PAGEN_1: page
                },
                dataType: 'html',
                onsuccess: BX.delegate(function(data) {
                    if (!!this.moreButton)
                        this.moreButton.remove();

                    if (page == 1)
                        this.commentContainer.innerHTML = data;
                    else
                        this.commentContainer.innerHTML += data;

                    this.moreButton = this.commentContainer.querySelector(this.selectorMoreButton);
                    if (!!this.moreButton)
                        BX.bind(this.moreButton, 'click', BX.proxy(this.getNextPage, this));
                }, this)
            });

            event.preventDefault();
        },
        setRating: function(index)
        {
            for (var i = 0; i < this.ratingStars.length; i++)
            {
                if (i <= index)
                    this.ratingStars[i].classList.add(this.styleFullRating);
                else
                    this.ratingStars[i].classList.remove(this.styleFullRating);
            }
        },
        onMouseLeaveRating: function()
        {
            var rating = parseInt(this.inputRating.value);
            this.setRating(rating - 1);
            event.preventDefault();
        },
        onMouseOverRatingStar: function()
        {
            var index = 0,
                i;

            for (i = 0; i < this.ratingStars.length; i++)
            {
                if (this.ratingStars[i] == event.target)
                    index = i;
            }

            this.setRating(index);
            event.preventDefault();
        },
        onClickRatingStar: function()
        {
            var rating = this.inputRating.value, i;

            for (i = 0; i < this.ratingStars.length; i++)
            {
                if (this.ratingStars[i] == event.target)
                    rating = i + 1;
            }

            this.inputRating.value = rating;
            event.preventDefault();
        },
        isValidField: function (field)
        {
            switch (field.type)
            {
                case 'email':
                    const email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (!email.test(String(field.value).toLowerCase()))
                        return false;
                    break;
                case 'tel':
                    const phone = /^\+?([0-9])\)?[-\s\.]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{2,4}[-\s\.]?[0-9]{2}?$/im;
                    if (!phone.test(String(field.value).toLowerCase()))
                        return false;
                    break;
                case 'text':
                    if (field.value == '')
                        return false;
                    break;
            }

            return true;
        },
        sendForm: function()
        {
            var i, field, error = false, data = {};

            this.sendButton.disabled = true;
            this.sendButton.classList.add('loading');

            for (i = 0; i < this.form.elements.length; i++)
            {
                field = this.form.elements[i];

                if (field.required && !this.isValidField(field))
                {
                    field.classList.add(this.inputErrorClass);
                    error = true;
                }
                else if (field.classList.contains(this.inputErrorClass))
                    field.classList.remove(this.inputErrorClass);

                data[field.getAttribute('name')] = field.value;
            }

            if (error)
                return;

            data.action = 'save';
            data.templateName = this.templateName;
            data.parameters = this.parameters;
            data.sessid = BX.bitrix_sessid();

            BX.ajax({
                url: this.actionUrl,
                method: 'POST',
                dataType: 'json',
                data: data,
                onsuccess: BX.delegate(function(data) {
                    if (!data.success)
                    {
                        this.sendButton.disabled = false;
                        this.sendButton.classList.remove('loading');

                        var resultContainer = document.querySelector(this.selectorCommentsResult);
                        if (!!resultContainer)
                        {
                            resultContainer.classList.add('error');
                            resultContainer.innerHTML = data.message;
                        }
                    }
                    else
                    {
                        this.form.innerHTML = data.message;
                    }
                }, this)
            });

            event.preventDefault();
        },
    };
})(window);