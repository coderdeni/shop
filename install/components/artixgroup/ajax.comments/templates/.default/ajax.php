<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<h2 class="reviews__title">Отзывы</h2>
<?
if (count($arResult['COMMENTS']) > 0)
{
    foreach ($arResult['COMMENTS'] as $comment)
    {
        ?>
        <div class="reviewItem">
            <div class="reviewItem__raiting">
                <div class="rating">
                    <?
                    for ($i = 0; $i < 5; $i++)
                    {
                        ?>
                        <i class="rating__item<?= $i < $comment['RATING'] ? " rating__item_full" : "" ?>"></i>
                        <?
                    }
                    ?>
                </div>
            </div>
            <div class="reviewItem__text"><?= $comment['COMMENT']?></div>
            <div class="reviewItem__descript">
                <div class="reviewItem__subTitle">Достоинства</div>
                <div class="reviewItem__subText"><?= $comment['DIGNITIES']?></div>
            </div>
            <div class="reviewItem__descript">
                <div class="reviewItem__subTitle">Недостатки</div>
                <div class="reviewItem__subText"><?= $comment['DISADVANTAGES']?></div>
            </div>
            <div class="reviewItem__autor">
                <div class="reviewItem__autorName"><?= $comment['USER_NAME']?></div>
                <div class="reviewItem__date"><?= $comment['CREATED_AT']?></div>
            </div>
        </div>
        <?
    }

    if ($arResult['NAV_RESULT']->NavPageCount > $arResult['NAV_RESULT']->NavPageNomer)
    {
        ?>
        <button class="button" data-role="comments-more" data-next-page="<?= $arResult['NAV_RESULT']->NavPageNomer + 1?>">Еще</button>
        <?
    }
}
else
{
    ?>
    <div class="noreviews">
        <div class="noreviews__icon"><img src="<?= SITE_TEMPLATE_PATH?>/images/testimonial.png" alt="" class="noreviews__img"></div>
        <div class="noreviews__title">Нет обратной связи для этого продукта.</div>
        <div class="noreviews__subtitle">Будь первым!</div>
    </div>
    <?
}


