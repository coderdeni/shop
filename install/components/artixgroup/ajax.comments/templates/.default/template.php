<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>

<div class="reviews">
    <div class="reviews__form">
        <form action="" class="form" data-role="comments-form">
            <div class="form__row">
                <input type="hidden" name="PRODUCT_ID" value="<?= $arParams['ELEMENT_ID']?>" required>
                <input data-role="comments-rating-value" type="hidden" name="RATING" value="5" required>
                <div class="form__label form__label_reviews">Оценка</div>
                <div class="rating rating_reviews" data-role="comments-rating">
                    <i class="rating__item"></i>
                    <i class="rating__item"></i>
                    <i class="rating__item"></i>
                    <i class="rating__item"></i>
                    <i class="rating__item"></i>
                </div>
            </div>
            <div class="form__row form__row_sb">
                <input type="text" name="USER_NAME" class="input input_midle" placeholder="Имя *" required>
                <input type="email" name="EMAIL" class="input input_midle" placeholder="Электронная почта *" required>
            </div>
            <div class="form__row">
                <textarea name="COMMENT" rows="10" class="textarea textarea_big" placeholder="Общие впечатления *" required></textarea>
            </div>
            <div class="form__row">
                <input name="DIGNITIES" type="text" class="input input_big" placeholder="Достоинства">
            </div>
            <div class="form__row">
                <input name="DISADVANTAGES" type="text" class="input input_big" placeholder="Недостатки">
            </div>
            <div class="form__row" data-role="comments-result"></div>
            <div class="form__row form__row_sb">
                <input data-role="comments-send" type="submit" class="button button_bp" value="Отправить">
            </div>
        </form>
    </div>
    <div class="reviews__content" data-role="comments">
        <h2 class="reviews__title">Отзывы</h2>
        <div class="noreviews">
            <div class="noreviews__icon"><img src="<?= SITE_TEMPLATE_PATH?>/images/testimonial.png" alt="" class="noreviews__img"></div>
            <div class="noreviews__subtitle">Загрузка...</div>
        </div>
    </div>
</div>
<script>
    new ArtixgroupAjaxComments({
        templateName: '<?= $templateName?>',
        parameters: '<?= serialize($arParams)?>',
        actionUrl: '<?= $componentPath?>/ajax.php',
    });
</script>