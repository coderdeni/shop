<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Mail;

class ArtixgroupComments extends CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $arFilter = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();


    protected $fields = array();
    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param array $params
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $params['IBLOCK_TYPE'] = trim($params['IBLOCK_TYPE']);
        $params['IBLOCK_ID'] = intval($params['IBLOCK_ID']);
        $params['ELEMENT_ID'] = intval($params['ELEMENT_ID']);
        $params['PAGE_SIZE'] = intval($params['PAGE_SIZE']) > 0 ? intval($params['PAGE_SIZE']) : 2;
        $params['ACTIVE_ELEMENT'] = trim($params['ACTIVE_ELEMENT']) ? $params['ACTIVE_ELEMENT'] : 'Y';

        $params['EVENT_NAME'] = $params['EVENT_NAME']
            ? trim($params['EVENT_NAME']) : 'FEEDBACK_ORDER_FORM';
        $params['EVENT_MESSAGE_ID'] = $params['EVENT_MESSAGE_ID']
            ? $params['EVENT_MESSAGE_ID'] : array();


        return $params;
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     */
    protected function checkModules()
    {
        if (!Main\Loader::includeModule('iblock'))
            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
    }

    /**
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        if ($this->arParams['IBLOCK_ID'] <= 0)
            throw new Main\ArgumentNullException('IBLOCK_ID');

        if (empty($this->arParams['ELEMENT_ID']))
            throw new Main\ArgumentNullException('ELEMENT_ID');

        if (empty($this->arParams['FORM_ID']))
            throw new Main\ArgumentNullException('FORM_ID');
    }

    protected function isValidCaptcha()
    {
        require_once('recaptchalib.php');

        $resp = recaptcha_check_answer(
            $this->arParams['RECAPTCHA_PRIVATE_KEY'],
            $_SERVER["REMOTE_ADDR"],
            $this->request->getPost("recaptcha_challenge_field"),
            $this->request->getPost("recaptcha_response_field")
        );

        return $resp->is_valid;
    }

    protected function getResult()
    {
        $iterator = CIBlockElement::GetList(
            ['DATE_CREATE' => 'asc'],
            [
                'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                '=PROPERTY_PRODUCT_ID' => $this->arParams['ELEMENT_ID']
            ],
            false,
            [
                'nPageSize' => $this->arParams['PAGE_SIZE'],
                'iNumPage' => $this->request->get('PAGEN_1') ? $this->request->get('PAGEN_1') : 1,
            ],
            ['ID', 'IBLOCK_ID', 'DATE_CREATE', 'CREATED_USER_NAME', 'PROPERTIES_*']
        );

        while ($ob = $iterator->GetNextElement())
        {
            $element = $ob->GetFields();
            $properties = $ob->GetProperties();

            $this->arResult['COMMENTS'][] = [
                'ID',
                'CREATED_AT' => $element['DATE_CREATE'],
                'USER_NAME' => $properties['USER_NAME']['VALUE'],
                'RATING' => (int)$properties['RATING']['VALUE'],
                'COMMENT' => $properties['COMMENT']['VALUE'],
                'DIGNITIES' => $properties['DIGNITIES']['VALUE'],
                'DISADVANTAGES' => $properties['DISADVANTAGES']['VALUE'],
            ];
        }

        $this->arResult["NAV_RESULT"] = $iterator;
    }

    protected function save()
    {
        global $USER;
        $ib = new \CIBlockElement();
        $userName = htmlspecialchars($this->request->getPost('USER_NAME'));
        $comment = htmlspecialchars($this->request->getPost('COMMENT'));
        $email = htmlspecialchars($this->request->getPost('EMAIL'));
        $element = \Bitrix\Iblock\ElementTable::getRow([
            'select' => ['NAME'],
            'filter' => ['=ID' => $this->arParams['ELEMENT_ID']]
        ]);

        if (!$element)
        {
            $this->arResult['ERRORS'][] = Loc::getMessage('PRODUCT_EMPTY');
            return false;
        }

        if (empty($userName) || empty($comment) || empty($email))
        {
            $this->arResult['ERRORS'][] = Loc::getMessage('ERROR_REQUIRED_FIELDS');
            return false;
        }

        $data = Array(
            "CREATED_BY" => $USER->GetId(),
            "IBLOCK_ID"  => $this->arParams['IBLOCK_ID'],
            "ACTIVE"     => $this->arParams['ACTIVE_ELEMENT'],
            "NAME"       => Loc::getMessage('COMMENT_NAME', [
                '#PRODUCT_NAME#' => $element['NAME'],
                '#USER_NAME#' => $userName,
            ]),
            "PROPERTY_VALUES" => [
                'RATING'        => (int)$this->request->getPost('RATING'),
                'USER_NAME'     => $userName,
                'COMMENT'       => $comment,
                'EMAIL'         => $email,
                'PRODUCT_ID'    => $this->arParams['ELEMENT_ID'],
                'DIGNITIES'     => htmlspecialchars($this->request->getPost('DIGNITIES')),
                'DISADVANTAGES' => htmlspecialchars($this->request->getPost('DISADVANTAGES'))
            ]
        );

        if (!$id = $ib->Add($data))
        {
            $this->arResult['ERRORS']['ADD'] = $ib->LAST_ERROR;
            return false;
        }

        return true;
    }

    protected function recountRating()
    {
        $arSelect = Array("ID", "IBLOCK_ID", "NAME","PROPERTY_*");
        $arFilter = Array("ID" => $this->arParams['ELEMENT_ID']);
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

        if ($ob = $res->GetNextElement())
        {
            $element = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $count = $arProps['vote_count']['VALUE'] + 1;
            $sum = $arProps['vote_sum']['VALUE'] + (int)$this->request->getPost('RATING');
            $rating = $sum / $count;

            \CIBlockElement::SetPropertyValuesEx(
                $element['ID'],
                $element['IBLOCK_ID'],
                array(
                    'vote_count' => $count,
                    'vote_sum' => $sum,
                    'rating' => $rating
                )
            );
        }
    }

    protected function sendEmail()
    {
        if (!empty($this->arParams["EVENT_MESSAGE_ID"]))
        {
            foreach($this->arParams["EVENT_MESSAGE_ID"] as $id)
            {
                if((int) $id > 0)
                {
                    \CEvent::send($this->arParams["EVENT_NAME"], SITE_ID, $this->arResult['FIELDS'], "Y", (int) $id);
                }
            }
        }
        else
        {
            \CEvent::send($this->arParams["EVENT_NAME"], SITE_ID, $this->arResult['FIELDS']);
        }
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        $this->checkModules();

        if ($this->request->getPost('action') === 'save')
        {
            if ($this->save())
            {
                if ($this->arParams['ACTIVE_ELEMENT'] === 'Y')
                    $this->recountRating();

                echo json_encode([
                    'success' => true,
                    'message' => $this->arParams['RESULT_MESSAGE']
                ]);
            }
            else
            {
                echo json_encode([
                    'success' => false,
                    'message' => implode('<br>', $this->arResult['ERRORS'])
                ]);
            }
        }
        elseif ($this->request->getPost('action') === 'load-more')
        {
            $this->getResult();
            $this->includeComponentTemplate('ajax');
        }
        else
        {
            $this->includeComponentTemplate();
        }
    }
}
