<?php
$MESS ['AG_IBLOCK_TYPE'] = "Тип информационного блока";
$MESS ['AG_IBLOCK_ID'] = "Код информационного блока";
$MESS ['AG_ELEMENT_ID'] = "ID елемента";
$MESS ['AG_FIELDS'] = "Поля";
$MESS ['AG_PAGE_SIZE'] = "Кол-во комментарий на странице";
$MESS ['AG_MANDATORY_FIELDS'] = "Обезательные поля";
$MESS ['AG_EVENT_NAME'] = "Почтовое событие";
$MESS ['AG_EVENT_MESSAGE_ID'] = "Почтовые шаблоны для отправки письма";
$MESS ['AG_ACTIVE_ELEMENT'] = "Добовлять елемент активным";
$MESS ['AG_FORM_ID'] = "ID формы";
$MESS ['AG_RESULT_MESSAGE'] = "Сообщение об успешной отправки";
