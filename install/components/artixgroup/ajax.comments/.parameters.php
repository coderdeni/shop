<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("all"=>" "));

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="all"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
{
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arElementIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["ELEMENT_IBLOCK_TYPE"]!="all"?$arCurrentValues["ELEMENT_IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
{
    $arElementIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arFields = array(
    'NAME' => 'Название',
    'PREVIEW_TEXT' => 'Описание для анонса',
    'DETAIL_TEXT' => 'Детальное описание',
);
$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("ACTIVE"=>"Y", "IBLOCK_ID"=>(isset($arCurrentValues["IBLOCK_ID"]) ? $arCurrentValues["IBLOCK_ID"] : $arCurrentValues["ID"])));
while ($arr=$rsProp->Fetch())
{
	$arFields['PROP_' . $arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
}

$arEventType = array();
$rsET = CEventType::GetList(array("LID" => SITE_ID));
while ($arET = $rsET->Fetch())
{
    $arEventType[$arET["EVENT_NAME"]] = "[".$arET["ID"]."] ".$arET["EVENT_NAME"];
}

$arEvent = array();
$arFilter = array("TYPE_ID" => (isset($arCurrentValues["EVENT_NAME"]) ? $arCurrentValues["EVENT_NAME"] : 'ARTIXGROUP_FEEDBACK'), "ACTIVE" => "Y");
$dbType = CEventMessage::GetList($by="ID", $order="DESC", $arFilter);
while($arType = $dbType->GetNext())
	$arEvent[$arType["ID"]] = "[".$arType["ID"]."] ".$arType["SUBJECT"];

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME"=>GetMessage("AG_IBLOCK_TYPE"),
			"TYPE"=>"LIST",
			"VALUES"=>$arTypesEx,
			"DEFAULT"=>"catalog",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME"=>GetMessage("AG_IBLOCK_ID"),
			"TYPE"=>"LIST",
			"VALUES"=>$arIBlocks,
			"DEFAULT"=>'1',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH" => "Y",
		),
        "ELEMENT_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("AG_ELEMENT_ID"),
            "TYPE" => "TEXT",
        ),
		"PAGE_SIZE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("AG_PAGE_SIZE"),
            "DEFAULT" => "10",
            "TYPE" => "TEXT",
        ),
		"EVENT_NAME" => Array(
			"NAME" => GetMessage("AG_EVENT_NAME"), 
			"TYPE"=>"LIST", 
			"VALUES" => $arEventType,
			"DEFAULT"=>"", 
			"MULTIPLE"=>"N", 
			"COLS"=>25, 
			"PARENT" => "BASE",
            "REFRESH" => "Y",
		),        
		"EVENT_MESSAGE_ID" => Array(
			"NAME" => GetMessage("AG_EVENT_MESSAGE_ID"), 
			"TYPE"=>"LIST", 
			"VALUES" => $arEvent,
			"DEFAULT"=>"", 
			"MULTIPLE"=>"Y", 
			"COLS"=>25, 
			"PARENT" => "BASE",
		),  
		"ACTIVE_ELEMENT" => Array(
            "NAME" => GetMessage("AG_ACTIVE_ELEMENT"), 
            "TYPE" => "CHECKBOX",
            "MULTIPLE" => "N",
            "VALUE" => "Y",
            "DEFAULT" =>"N",
            "REFRESH"=> "Y",
		),
        "RESULT_MESSAGE" => Array(
            "NAME" => GetMessage("AG_RESULT_MESSAGE"),
            "TYPE" => "TEXT",
            "DEFAULT" => "Ваше сообщение успешно отправлено",
        ),         
		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
	),
);
?>
