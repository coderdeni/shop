'use strict';
(function (window){
    window.ArtixgroupSetting = function (params)
    {
        this.links = !!params.links ? params.links : [];
        this.rowDisabledClass = !!params.rowDisabledClass ? params.rowDisabledClass : 'form__row_disabled';
        this.rowPrefixClass = !!params.rowPrefixClass ? params.rowPrefixClass : '_row';

        BX.ready(BX.delegate(this.init, this));
    };

    window.ArtixgroupSetting.prototype = {

        init: function ()
        {
            console.log(this.links);
            for (var elementId in this.links)
            {
                var element = document.getElementById(elementId+'_field');

                if (!!element)
                {
                    element.addEventListener('change', BX.delegate(this.onChange, this));
                    element.dispatchEvent(new Event("change", {bubbles : true, cancelable : true}));
                }
            }
        },

        onChange: function ()
        {
            var elementId = event.target.getAttribute('id').replace('_field', '');
            var targets = this.links[elementId]
            event.target.setAttribute('checked', event.target.checked);

            for (var value in targets)
            {
                for (var element in targets[value])
                {
                    var input = document.getElementById(targets[value][element]+'_field'),
                        row = document.getElementById(targets[value][element] + this.rowPrefixClass);

                    if (!!input)
                    {
                        if (event.target.type == 'checkbox' || event.target.type == 'radio')
                            input.disabled = !event.target.disabled ? !event.target.checked : false;
                        else
                            input.disabled = input.value != event.target.value;

                        if (input.disabled)
                            row.classList.add(this.rowDisabledClass);
                        else if (row.classList.contains(this.rowDisabledClass))
                            row.classList.remove(this.rowDisabledClass);
                    }
                }
            }
        },
    };
})(window);
