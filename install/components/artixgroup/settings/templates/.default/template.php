<?php

use Artixgroup\Shop\Setting\Parameters;
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
?>
<form action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="SAVE_SETTING" value="Y">
    <?
    if ($arResult['ERRORS'])
    {
        foreach ($arResult['ERRORS'] as $error)
            ShowError($error);
    }

    foreach ($arResult['GROUPS'] as $group)
    {
        if (empty($group['PARAMETERS']))
            continue;
        ?>
        <div class="group">
            <h2 class="group-name"><?= $group['NAME'] ?></h2>
            <div class="form">
                <?
                foreach ($group['PARAMETERS'] as $code => $parameter)
                {
                    ?>
                    <div class="form__row" id="<?= $code?>_row">
                        <?
                        switch ($parameter['TYPE'])
                        {
                            case Parameters::TYPE_COLOR:
                            case Parameters::TYPE_TEXT:
                                ?>
                                <label for="<?= $code?>_field" class="group-parameters-item-label">
                                    <?= $parameter['NAME']?>
                                </label>
                                <input id="<?= $code?>_field" class="input input_big" type="text" name="<?= $code?>" value="<?= $parameter['VALUE']?>">
                                <?
                                break;

                            case Parameters::TYPE_TEXTAREA:
                                ?>
                                <label for="<?= $code?>_field" for="field_<?= $code?>" class="group-parameters-item-label">
                                    <?= $parameter['NAME']?>
                                </label>
                                <textarea id="field_<?= $code?>" class="textarea textarea_big" name="<?= $code?>"><?= $parameter['VALUE']?></textarea>
                                <?
                                break;

                            case Parameters::TYPE_FILE:
                                ?>
                                <label for="<?= $code?>_field" class="group-parameters-item-label">
                                    <?= $parameter['NAME']?>
                                </label>
                                <?= $parameter['VALUE'];?>
                                <input id="<?= $code?>_field" class="input input_big" type="file" name="<?= $code?>" value="<?= $parameter['VALUE']?>">
                                <?
                                break;

                            case Parameters::TYPE_CHECKBOX:
                                if (count($parameter['VALUES']) > 1)
                                {
                                    ?>
                                    <label class="group-parameters-item-label">
                                        <?= $parameter['NAME']?>
                                    </label>
                                    <?
                                }
                                foreach ($parameter['VALUES'] as $value => $name)
                                {
                                    ?>
                                    <label for="<?= $code?>_<?= $value?>_field" class="group-parameters-item-label">
                                        <input id="<?= $code?>_<?= $value?>_field" class="checkbox__input" type="checkbox" name="<?= $code?>" value="<?= $value?>"<?= $parameter['VALUE'] == $value ? ' checked="checked"' : ''?>>
                                        <?= $name?>
                                    </label>
                                    <?
                                }
                                break;

                            case Parameters::TYPE_RADIO:
                                ?>
                                <label class="group-parameters-item-label">
                                    <?= $parameter['NAME']?>
                                </label>
                                <?
                                foreach ($parameter['VALUES'] as $value => $name)
                                {
                                    ?>
                                    <label for="<?= $code?>_<?= $value?>_field" class="group-parameters-item-label">
                                        <input id="<?= $code?>_<?= $value?>_field" class="radio" type="radio" name="<?= $code?>" value="<?= $value?>"<?= $parameter['VALUE'] === $value ? ' checked="checked"' : ''?>>
                                        <?= $name?>
                                    </label>
                                    <?
                                }
                                break;

                            case Parameters::TYPE_SELECT:
                                ?>
                                <label for="field_<?= $code?>" class="group-parameters-item-label">
                                    <?= $parameter['NAME']?>
                                </label>
                                <div class="select-box">
                                    <div class="select-box__current" tabindex="1">
                                        <?
                                        foreach ($parameter['VALUES'] as $value => $name)
                                        {
                                            ?>
                                            <div class="select-box__value">
                                                <input id="<?= $code?>_<?= $value?>_field" name="<?= $code?>" class="select-box__input" type="radio" value="<?= $value?>"<?= $parameter['VALUE'] == $value ? ' checked' : ''?>>
                                                <p class="select-box__input-text">
                                                    <?= $name?>
                                                </p>
                                            </div>
                                            <?
                                        }
                                        ?>
                                        <img class="select-box__icon" src="<?= $templateFolder?>/images/arrow.svg" alt="Arrow Icon" aria-hidden="true">
                                    </div>
                                    <ul class="select-box__list">
                                        <?
                                        foreach ($parameter['VALUES'] as $value => $name)
                                        {
                                            ?>
                                            <li>
                                                <label for="<?= $code?>_<?= $value?>_field" class="select-box__option">
                                                    <?= $name?>
                                                </label>
                                            </li>
                                            <?
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <?
                                break;
                        }
                        ?>
                    </div>
                    <?
                }
                ?>
                <div class="form__row form__row_sb">
                    <input type="submit" class="button button_bp" value="<?= Loc::getMessage('ARTIXGROUP_SAVE')?>">
                </div>
            </div>
        </div>
        <?
    }
    ?>
</form>
<script>
    new ArtixgroupSetting({
        links: <?= \Bitrix\Main\Web\Json::encode($arResult['JS_PARAMS'])?>
    });
</script>

