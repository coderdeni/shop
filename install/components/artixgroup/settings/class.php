<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Artixgroup\Shop\Setting\Parameters;
use \Bitrix\Main;
use Bitrix\Main\Context;
use \Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Web\Uri;

class ArtixgroupSettings extends \CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $arFilter = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();

    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param $params
     * @return array
     * @internal param array $arParams
     */
    public function onPrepareComponentParams($params)
    {
        return $params;
    }

    /**
     * определяет читать данные из кеша или нет
     * @return bool
     */
    protected function readDataFromCache()
    {
        global $USER;

        if ($this->arParams['CACHE_TYPE'] === 'N')
            return false;

        if (is_array($this->cacheAddon))
            $this->cacheAddon[] = $USER->GetUserGroupArray();
        else
            $this->cacheAddon = [$USER->GetUserGroupArray()];

        return !($this->startResultCache(false, $this->cacheAddon, md5(serialize($this->arParams))));
    }

    /**
     * кеширует ключи массива arResult
     */
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
        {
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    /**
     * завершает кеширование
     * @return bool
     */
    protected function endCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
        {
            return false;
        }

        $this->endResultCache();
        return true;
    }

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Main\Loader::includeModule('artixgroup.shop'))
        {
            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
        }
    }

    /**
     * проверяет заполнение обязательных параметров
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
    }

    /**
     * получение результатов
     */
    protected function getResult()
    {
        if ($this->request->getPost('SAVE_SETTING') === 'Y')
        {
            $postList = $this->request->getPostList();
            foreach (Parameters::getInstance()->getParameters() as $key => $parameter)
            {
                if ($parameter['TYPE'] === Parameters::TYPE_FILE)
                    continue;

                Parameters::getInstance()->setValue($key, isset($postList[$key]) ? $postList[$key] : '');
            }

            foreach ($this->request->getFileList() as $key => $file)
            {
                $file = $this->request->getFile($key);

                if (empty($file['name']))
                    continue;

                $name = basename($file["name"]);
                $param = Parameters::getInstance()->getParameter($key);

                if ($param && (!isset($param['EXT']) || in_array(end(explode(".", $name)), $param['EXT'])))
                {
                    if (move_uploaded_file($file['tmp_name'], $_SERVER['DOCUMENT_ROOT']."/include/$name"))
                    {
                        Parameters::getInstance()->setValue($key, "/include/$name");
                    }
                }
                else
                {
                    $this->arResult['ERRORS'][] = 'Неверный формат файла';
                }
            }

            if (empty($this->arResult['ERRORS']))
            {
                $result = Parameters::getInstance()->save();

                if ($result->isSuccess())
                    LocalRedirect($this->request->getRequestUri());
                else
                    $this->arResult['ERRORS'] = $result->getErrorMessages();
            }
        }

        $groups = Parameters::getInstance()->getGroups();

        foreach ($groups as $code => $group)
        {
            $parameters = Parameters::getInstance()->getGroupParameters($code);

            foreach ($parameters as $id => $parameter)
            {
                if (!isset($parameter['LINK']))
                    continue;

                if (in_array($parameter['TYPE'], [Parameters::TYPE_SELECT, Parameters::TYPE_CHECKBOX,  Parameters::TYPE_RADIO]))
                {
                    foreach ($parameter['LINK'] as $value => $target)
                    {
                        $this->arResult['JS_PARAMS'][$id.'_'.$value][(string)$value] = $target;
                    }
                }
                else
                {
                    foreach ($parameter['LINK'] as $value => $target)
                    {
                        $this->arResult['JS_PARAMS'][$id][(string)$value] = $target;
                    }
                }
            }

            $group['PARAMETERS'] = $parameters;
            $this->arResult['GROUPS'][] = $group;
        }
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->checkParams();
            $this->getResult();
            $this->includeComponentTemplate();
        }
        catch (Exception $e)
        {
            ShowError($e->getMessage());
        }
    }
}
