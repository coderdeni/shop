<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arCurrentValues */

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS" => array(
        "LINK" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTIXGROUP_FAVOURITES_LINK_URL"),
            "TYPE" => "TEXT",
            "DEFAULT" => "/personal/favourites/"
        ),
        "SELECTED_CLASS" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTIXGROUP_FAVOURITES_SELECTED_CLASS"),
            "TYPE" => "TEXT",
            "DEFAULT" => "favorite_added"
        ),
        "SHOW_NAME" => Array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('ARTIXGROUP_FAVOURITES_SHOW_NAME'),
            'TYPE' => 'CHECKBOX',
            'MULTIPLE' => 'N',
            'REFRESH' => 'Y',
            'DEFAULT' => 'Y'
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
    ),
);
