<?php
/** @global CMain $APPLICATION */
define('STOP_STATISTICS', true);
define('PUBLIC_AJAX_MODE', true);
define('NOT_CHECK_PERMISSIONS', true);

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    \Artixgroup\Shop\Favourites\Favourites;

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

Loader::includeModule('artixgroup.shop');
$request = Application::getInstance()->getContext()->getRequest();
if ($request->isAjaxRequest() && $request->get('elementId') > 0)
{
    $elementId = (int)$request->get('elementId');
    $action = null;
    $result = null;

    if (!Favourites::getInstance()->containsItems($elementId))
    {
        $result = Favourites::getInstance()->add($elementId);
        $action = 'add';
    }
    else
    {
        $result = Favourites::getInstance()->remove($elementId);
        $action = 'remove';
    }

    echo json_encode([
        'success' => $result ? $result->isSuccess() : false,
        'action' => $action,
        'items' => Favourites::getInstance()->getItems(),
        'count' => Favourites::getInstance()->getCount(),
        'error' => $result && !$result->isSuccess() ? implode(', ', $result->getErrorMessages()) : ''
    ]);
}