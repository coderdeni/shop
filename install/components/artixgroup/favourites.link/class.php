<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Artixgroup\Shop\Favourites\Favourites;
use \Bitrix\Main;
use Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc as Loc;

class ArtixFavouritesLinkComponent extends CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $arFilter = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $cacheSections = array();


    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param $params
     * @return array
     * @internal param array $arParams
     */
    public function onPrepareComponentParams($params)
    {
        $params['LINK'] = !empty($params['LINK']) ? $params['LINK'] : '/personal/favourites.php';
        $params['SELECTED_CLASS'] = !empty($params['SELECTED_CLASS']) ? $params['SELECTED_CLASS'] : 'favorites_added';

        return $params;
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('iblock'))
            throw new Main\LoaderException(Loc::getMessage('ARTIXGROUP_FAVOURITES_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
        if (!Loader::includeModule('artixgroup.shop'))
            throw new Main\LoaderException(Loc::getMessage('ARTIXGROUP_FAVOURITES_CLASS_FAVOURITES_MODULE_NOT_INSTALLED'));
    }

    /**
     * выполяет действия перед кешированием
     */
    protected function executeProlog()
    {
    }


    /**
     * получение результатов
     */
    protected function getResult()
    {
        $this->arResult = [
            'USER' => Favourites::getInstance()->getUser(),
            'ITEMS' => Favourites::getInstance()->getItems(),
            'COUNT' => Favourites::getInstance()->getCount()
        ];
    }

    /**
     * выполняет действия после выполения компонента, например установка заголовков из кеша
     */
    protected function executeEpilog()
    {
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->executeProlog();
            $this->getResult();
            $this->includeComponentTemplate();
            $this->executeEpilog();
        }
        catch (Exception $e)
        {
            ShowError($e->getMessage());
        }

        return $this->arResult['ITEMS'];
    }
}
