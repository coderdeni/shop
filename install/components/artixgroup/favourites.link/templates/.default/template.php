<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
?>
<a href="<?= $arParams['LINK']?>" class="headerControls" id="artixgroup-favourites-link-container">
    <? $frame = $this->createFrame("artixgroup-favourites-link-container", false)->begin(); ?>
    <span class="headerControls__icon" >
        <span class="headerControls__iconLabel" data-role="favourites-badge"<?= $arResult['COUNT'] > 0 ? '' : ' style="display:none"'?>>
            <?= $arResult['COUNT']?>
        </span>
        <svg class="icon icon-favorite">
            <use xlink:href="#icon-favorite"></use>
        </svg>
    </span>
    <?
    if ($arParams['SHOW_NAME'] === 'Y')
    {
        ?>
        <span class="headerControls__text">
            <?= Loc::getMessage('ARTIXGROUP_FAVOURITES_TEMPLATE_FAVOURITES')?> <?= $arResult['USER'] ? ' ('.$arResult['USER']['LOGIN'].')' : ''?>
        </span>
        <?
    }
    ?>
    <? $frame->beginStub(); ?>
    <span class="headerControls__icon" >
        <span class="headerControls__iconLabel" data-role="favourites-badge" style="display:none"></span>
        <svg class="icon icon-favorite">
            <use xlink:href="#icon-favorite"></use>
        </svg>
    </span>
    <?
    if ($arParams['SHOW_NAME'] === 'Y')
    {
        ?>
        <span class="headerControls__text">
            <?= Loc::getMessage('ARTIXGROUP_FAVOURITES_TEMPLATE_FAVOURITES')?>
        </span>
        <?
    }
    ?>
    <? $frame->end(); ?>
</a>
<script>
    new Favourites({
        'action_url': '<?= $componentPath?>/ajax.php',
        'selected_class': '<?= $arParams['SELECTED_CLASS']?>',
        'items': <?= json_encode($arResult['ITEMS'])?>
    });
</script>