(function (window){
    window.Favourites = function (params) {
        this.actionUrl = params.action_url;
        this.selectedClass = params.selected_class;
        this.items = params.items;
        this.elementLinks = [];
        this.badge = null;
        this.currentLink = null;

        BX.ready(BX.delegate(this.init, this));
    };

    window.Favourites.prototype = {

        init: function()
        {
            this.badge = document.querySelector('[data-role="favourites-badge"]');

            this._bindElements();
            this.updateBadge(this.items.length);

            BX.addCustomEvent('favouritesUpdate', BX.delegate(function () {
                for (var i = 0; i < this.elementLinks.length; i++)
                {
                    if (this.items.includes(parseInt(this.elementLinks[i].getAttribute('data-item-id'))))
                        this.elementLinks[i].classList.remove(this.selectedClass);

                    BX.unbind(this.elementLinks[i], 'click', BX.proxy(this.onClick, this));
                }
                this._bindElements();
            }, this));
        },

        _bindElements: function ()
        {
            this.elementLinks = document.querySelectorAll('[data-role="favourites_link"]');

            if (!!this.elementLinks)
            {
                for (var i = 0; i < this.elementLinks.length; i++)
                {
                    if (this.items.includes(parseInt(this.elementLinks[i].getAttribute('data-item-id'))))
                        this.elementLinks[i].classList.add(this.selectedClass);

                    BX.bind(this.elementLinks[i], 'click', BX.proxy(this.onClick, this));
                }
            }
        },

        updateBadge: function(count)
        {
            if (!!this.badge)
            {
                if (count > 0)
                    BX.show(this.badge);
                else
                    BX.hide(this.badge);

                this.badge.innerHTML = count;
            }
        },

        onClick: function ()
        {
            this.currentLink = BX.proxy_context;
            var elementId = this.currentLink.getAttribute('data-item-id');

            if (!!elementId)
            {
                BX.ajax.loadJSON(
                    this.actionUrl,
                    {
                        elementId: elementId,
                    },
                    BX.delegate(this._onSuccess, this)
                );
            }
        },

        _onSuccess: function (result)
        {
            if (result.success)
            {
                if (result.action == 'add')
                    this.currentLink.classList.add(this.selectedClass);
                else
                    this.currentLink.classList.remove(this.selectedClass);

                this.updateBadge(result.count);
                this.items = result.items;
                this.currentLink = null;
            }
            else
            {
                console.error(result.error);
            }
        }
    };
})(window);