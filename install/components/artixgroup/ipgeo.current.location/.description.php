<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("ARTIXGROUP_GEOIP_CURRENT_COMPONENT_NAME"),
    "DESCRIPTION" => GetMessage("ARTIXGROUP_GEOIP_CURRENT_COMPONENT_DESCRIPTION"),
    "CACHE_PATH" => "Y",
    "SORT" => 30,
    "PATH" => array(
        "ID" => "artixgroup",
        "NAME" => GetMessage("ARTIXGROUP_GEOIP_COMPONENT_GROUP"),
        "SORT" => 20
    ),
);

?>