<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Artixgroup\Shop\Ipgeo\Location;
use Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc as Loc;

class ArtixgroupIpGeoCurrentLocationComponent extends CBitrixComponent
{
    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * проверяет подключение необходиимых модулей
     *
     * @throws \Bitrix\Main\LoaderException
     */
    protected function checkModules()
    {
    }

    /**
     * подготавливает входные параметры
     * @param $params
     * @return array
     * @internal param array $arParams
     */
    public function onPrepareComponentParams($params)
    {
        $params['COOKIE_EXPIRES'] = isset($params['COOKIE_EXPIRES']) ? (int)$params['COOKIE_EXPIRES'] : 3600 * 24 * 7;

        return $params;
    }

    /**
     * получение результатов
     */
    protected function getResult()
    {
        $this->arResult['CURRENT_LOCATION'] = Location::getInstance()->getLocation();
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->getResult();
            $this->includeComponentTemplate();
        }
        catch (Exception $e)
        {
            ShowError($e->getMessage());
        }
    }
}
