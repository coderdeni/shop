<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
?>
<div class="trigger location" data-dialog="locationDialog">
    <svg class="icon icon-place">
        <use xlink:href="#icon-place"></use>
    </svg>
    <span class="location__value"><?= $arResult['CURRENT_LOCATION']['LOCATION_NAME']?></span>
</div>