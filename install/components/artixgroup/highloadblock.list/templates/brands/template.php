<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
	echo $arResult['ERROR'];
	return false;
}

//$GLOBALS['APPLICATION']->SetTitle('Highloadblock List');
?>
<section class="brands">
    <div class="brands__container limiter swiper-container">
        <div class="brands__content swiper-wrapper">
            <?
            foreach ($arResult['rows'] as $row)
            {
                ?>
                <div class="brands__item swiper-slide">
                    <div class="brandsItem swiper-slide">
                        <div class="brandsItem__container">
                            <a href="/brands/<?= $row['UF_XML_ID']?>/" class="brandsItem__content">
                                <div class="brandsItem__photo"><img src="<?= CFile::GetPath($row['UF_FILE'])?>" alt="<?= $row['UF_NAME']?>"  class="brandsItem__img"></div>
                            </a>
                        </div>
                    </div>
                </div>
                <?
            }
            ?>
        </div>
    </div>
</section>

