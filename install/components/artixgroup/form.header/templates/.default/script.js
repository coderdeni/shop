'use strict';
(function (window){
    window.ArtixgroupFormLoader = function ()
    {
        this.popup = null;
        BX.ready(BX.delegate(this.init, this));
    };

    window.ArtixgroupFormLoader.prototype = {

        init: function ()
        {
            var triggerButtons = document.querySelectorAll('.artixgroup_form_trigger');

            if (triggerButtons.length > 0)
            {
                for (var i = 0; i < triggerButtons.length; i++)
                {
                    triggerButtons[i].addEventListener('click', BX.delegate(this.onClickButton, this));

                    if (triggerButtons[i].hasAttribute('data-form-autoload'))
                    {
                        var delay = parseInt(triggerButtons[i].getAttribute('data-form-autoload'));

                        if (delay > 0)
                            setTimeout(BX.delegate(function () {
                                this.showForm(triggerButtons[i]);
                            }, this), delay * 1000);
                        else
                            this.showForm(triggerButtons[i]);
                    }
                }
            }
        },

        onClickButton: function()
        {
            var button = this.findParentByClass(event.target, 'artixgroup_form_trigger');

            if (!!button)
            {
                this.showForm(button);
            }
        },

        findParentByClass: function(element, className)
        {
            if (!element.classList.contains(className) && element.parentNode)
            {
                return this.findParentByClass(element.parentNode, className);
            }

            return element;
        },

        showForm: function (btn)
        {
            //<button class="button artixgroup_form_trigger"
            //  data-form-id="1"
            //  data-form-autoload="0"
            //  data-form-popup="ture">
            //  Обратный звонок
            //</button>

            var formId = btn.getAttribute('data-form-id');
            var popup = btn.hasAttribute('data-form-popup');

            if (formId > 0)
            {
                var data = {
                    'form_id': formId,
                    'sessid': BX.bitrix_sessid(),
                };

                for (var i = 0, len = btn.attributes.length; i < len; i++)
                {
                    var result = btn.attributes[i].name.match(/data-field-([0-9]+)/);

                    if (result !== null && result[1] !== undefined)
                    {
                        data['FIELD_'+result[1]] = btn.attributes[i].nodeValue;
                    }
                }

                BX.ajax({
                    url: '/bitrix/components/artixgroup/form/ajax.php',
                    method: 'POST',
                    dataType: 'html',
                    data: data,
                    processData: false,
                    onsuccess: BX.delegate(function(data) {
                        var div = document.createElement('div');
                        div.innerHTML = data;

                        if (!popup)
                        {
                            btn.parentNode.insertBefore(div, btn);
                            btn.parentNode.removeChild(btn);
                        }
                        else
                        {
                            this.popup = div;
                            document.body.appendChild(this.popup);
                            this.popup.querySelector('.close').addEventListener('click', BX.delegate(this.closePopup, this));
                        }

                        var scripts = div.getElementsByTagName("script");
                        for (var i = 0; i < scripts.length; ++i)
                        {
                            var script = scripts[i];
                            eval(script.innerHTML);
                        }
                    }, this)
                });
            }
        },

        closePopup: function () {
            document.body.removeChild(this.popup);
            this.popup = null;
        }
    };

    window.ArtixgroupForm = function (params)
    {
        this.fields = !!params.fields ? params.fields : [];
        this.onsuccess = !!params.onsuccess ? params.onsuccess : null;
        this.sendButtonId = !!params.sendButtonId ? params.sendButtonId : '';
        this.messageId = !!params.messageId ? params.messageId : '';
        this.actionUrl = !!params.actionUrl ? params.actionUrl : null;
        this.formId = !!params.formId ? params.formId : null;
        this.errors = [];

        BX.ready(BX.delegate(this.init, this));
    };

    window.ArtixgroupForm.prototype = {

        init: function ()
        {
            this.sendButton = document.getElementById(this.sendButtonId);
            this.blockMessage = document.getElementById(this.messageId);

            if (!!this.sendButton)
            {
                this.sendButton.addEventListener('click', BX.delegate(this.onSubmit, this));
            }
        },

        isValidForm: function ()
        {
            var haveError = false

            for (var elementId in this.fields)
            {
                this.fields[elementId].validate();

                if (!this.fields[elementId].valid)
                {
                    haveError = true
                }
            }

            return !haveError;
        },

        onSubmit: function ()
        {
            event.preventDefault();

            if (this.isValidForm())
            {
                this.sendButton.disabled = true;
                this.sendButton.classList.add('loading');

                var data = {
                    'form_id': this.formId,
                    'sessid': BX.bitrix_sessid(),
                    'request_url': window.location.href,
                    'save': 'y',
                };

                for (var i in this.fields)
                {
                    data[this.fields[i].getName()] = this.fields[i].getValue();
                }

                BX.ajax({
                    url: this.actionUrl,
                    method: 'POST',
                    dataType: 'json',
                    data: data,
                    onsuccess: BX.delegate(function(data) {
                        this.sendButton.disabled = false;
                        this.sendButton.classList.remove('loading');
                        this.blockMessage.innerHTML = data.message;
                        this.blockMessage.classList.add(!data.success ? 'error' : 'success');
                        if (data.success)
                        {
                            for (var i in this.fields)
                                this.fields[i].clear();

                            if (this.onsuccess !== null)
                                this.onsuccess();
                        }
                    }, this)
                });
            }
        },
    };

    window.ArtixgroupFormField = function (elementId, params)
    {
        this.maskParams = !!params.maskParams ? params.maskParams : null;
        this.elementId = elementId;
        this.multiple = (typeof params.multiple === 'undefined') ? false : !!params.multiple;
        this.required = (typeof params.required === 'undefined') ? false : !!params.required;
        this.container = null;
        this.fields = [];
        this.valid = false;

        if (!!this.elementId)
        {
            BX.ready(BX.delegate(this.init, this));
        }
    }

    window.ArtixgroupFormField.prototype.init = function ()
    {
        if (this.multiple)
        {
            this.container = document.getElementById(this.elementId);

            if (!!this.container && this.container.tagName !== 'INPUT' && this.container.tagName !== 'SELECT')
            {
                var moreButton = this.container.parentNode.querySelector('.form__button-more'),
                    inputs = this.container.querySelectorAll('input');

                for (var i = 0, len = inputs.length; i < len; i++)
                    this.fields.push(inputs[i]);

                if (!!moreButton)
                {
                    moreButton.addEventListener('click', BX.delegate(this.addField, this));
                }
            }
            else
            {
                this.fields.push(document.getElementById(this.elementId));
            }
        }
        else
        {
            this.fields.push(document.getElementById(this.elementId));
        }

        if (this.maskParams != null)
        {
            for (var i = 0; i < this.fields.length; i++)
            {
		        Inputmask(this.maskParams).mask(this.fields[i]);
            }
        }
    };

    window.ArtixgroupFormField.prototype.getValue = function ()
    {
        var value = this.multiple ? [] : null;

        for (var i = 0; i < this.fields.length; i++)
        {
            var field = this.fields[i];

            switch (field.type)
            {
                case "button":
                case "submit":
                case "reset":
                    break;
                case "select-multiple":
                    var option;
                    for (var k = 0, iLen = field.selectedOptions.length; k < iLen; k++)
                    {
                        option = field.selectedOptions[k];
                        value.push(option.value || option.text);
                    }
                    break;
                case "checkbox":
                    if (field.checked)
                        value.push(field.value);
                    break;
                case "radio":
                    if (field.checked)
                        value = field.value;
                    break;
                default:
                    if (this.multiple)
                    {
                        if (field.value !== '')
                            value.push(field.value);
                    }
                    else
                        value = field.value;
            }
        }

        return value;
    };

    window.ArtixgroupFormField.prototype.getName = function ()
    {
        if (this.fields.length > 0)
        {
            var name = this.fields[0].name;
            return name.replace(/\[|\]/g, '');
        }

        return '';
    };

    window.ArtixgroupFormField.prototype.addField = function ()
    {
        if (this.fields.length > 0)
        {
            var clone = this.fields[0].cloneNode();
            this.container.appendChild(clone);
            this.fields.push(clone);
            clone.value = '';

            if (this.maskParams != null)
            {
                Inputmask(this.maskParams).mask(clone);
            }
        }
    };

    window.ArtixgroupFormField.prototype.validate = function ()
    {
        this.reset();

        for (var i = 0; i < this.fields.length; i++)
        {
            var field = this.fields[i];

            switch (field.type)
            {
                case "button":
                case "submit":
                case "reset":
                    break;
                case "checkbox":
                case "radio":
                    if (!this.required || field.checked)
                    {
                        this.valid = true;
                    }
                    break;
                case "select-one":
                    if (!this.required || field.value !== '')
                    {
                        this.valid = true;
                        field.classList.remove('input_has-error');
                    }
                    else
                        field.classList.add('input_has-error');
                    break;
                case "select-multiple":
                    if (!this.required || field.selectedOptions.length > 0) {
                        field.classList.remove('input_has-error');
                        this.valid = true;
                    } else {
                        field.classList.add('input_has-error');
                    }
                    break;
                default:
                    field.value = field.value.replace(/^\s*/, '').replace(/\s*$/, '');

                    if (!field.value)
                    {
                        field.value = '';
                    }

                    if (!this.required || Inputmask.isValid(field.value, this.maskParams))
                    {
                        this.valid = true;
                        field.classList.remove('input_has-error');
                    }
                    else
                    {
                        this.valid = false;
                        field.classList.add('input_has-error');
                    }
            }
        }
    };

    window.ArtixgroupFormField.prototype.reset = function ()
    {
        this.valid = false;
        return this;
    };

    window.ArtixgroupFormField.prototype.clear = function ()
    {
        for (var i = 0; i < this.fields.length; i++)
        {
            var field = this.fields[i];

            switch (field.type)
            {
                case "button":
                case "submit":
                case "reset":
                    break;
                case "checkbox":
                case "radio":
                    field.checked = false;
                    break;
                default:
                    field.value = '';
            }
        }
    };

    new ArtixgroupFormLoader();
})(window);
