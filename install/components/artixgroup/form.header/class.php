<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Artixgroup\Shop\Form\FieldBase;
use Artixgroup\Shop\FormFieldTable;
use Artixgroup\Shop\Setting\Parameters;
use \Bitrix\Main;
use Bitrix\Main\Context;
use Bitrix\Main\Entity\ExpressionField;
use \Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Web\Uri;

class ArtixgroupFormHeader extends \CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $arFilter = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();

    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param $params
     * @return array
     * @internal param array $arParams
     */
    public function onPrepareComponentParams($params)
    {
        return $params;
    }

    /**
     * определяет читать данные из кеша или нет
     * @return bool
     */
    protected function readDataFromCache()
    {
        global $USER;

        if ($this->arParams['CACHE_TYPE'] === 'N')
            return false;

        if (is_array($this->cacheAddon))
            $this->cacheAddon[] = $USER->GetUserGroupArray();
        else
            $this->cacheAddon = [$USER->GetUserGroupArray()];

        return !($this->startResultCache(false, $this->cacheAddon, md5(serialize($this->arParams))));
    }

    /**
     * кеширует ключи массива arResult
     */
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
        {
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    /**
     * завершает кеширование
     * @return bool
     */
    protected function endCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
        {
            return false;
        }

        $this->endResultCache();
        return true;
    }

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Main\Loader::includeModule('artixgroup.shop'))
        {
            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
        }
    }

    /**
     * проверяет заполнение обязательных параметров
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
    }

    /**
     * получение результатов
     */
    protected function getResult()
    {
        $assets = [
            'css' => [],
            'js' => [],
        ];

        $result = \Artixgroup\Shop\FormTable::getList([
            'select' => ['CNT'],
            'filter' => [
                'ACTIVE' => 'Y',
                'CAPTCHA' => [
                    \Artixgroup\Shop\FormTable::CAPTCHA_GOOGLE_V2,
                    \Artixgroup\Shop\FormTable::CAPTCHA_GOOGLE_V3
                ]
            ],
            'runtime' => [
                new ExpressionField('CNT', 'COUNT(*)')
            ]
        ])->fetch();

        if ($result['CNT'] > 0)
        {
            $secretKey = Main\Config\Option::get('artixgroup.shop', 'recaptcha_v3_public_key');
            $uri = new Uri('https://www.google.com/recaptcha/api.js');

            if ($secretKey)
            {
                $uri->addParams(['render' => $secretKey]);
            }

            $assets['js'][] = $uri->getUri();
        }

        foreach ($assets['js'] as $js)
            Main\Page\Asset::getInstance()->addJs($js);

        foreach ($assets['css'] as $css)
            Main\Page\Asset::getInstance()->addCss($css);

        unset($assets);
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->checkParams();
            $this->getResult();
            $this->includeComponentTemplate();
        }
        catch (Exception $e)
        {
            ShowError($e->getMessage());
        }
    }
}
