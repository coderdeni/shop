<?php

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
$this->addExternalJs('https://api-maps.yandex.ru/2.1/?apikey='.$arParams['PROVIDER_KEY'].'&lang=ru_RU');
?>
<div class="shops">
    <div class="shops__items">
        <div class="shops__search">
            <div class="search">
                <div class="form">
                    <input type="text" class="input input_search" id="map-stores-search" placeholder="<?= Loc::getMessage('SEARCH')?>">
                    <button type="submit" class="searchButton" >
                        <svg class="icon icon-search">
                            <use xlink:href="#icon-search"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
        <div class="shops__list" id="map-stores-list"></div>
    </div>
    <div class="shops__maps">
        <div class="shops__map" id="map-stores"></div>
    </div>
</div>
<script type="application/javascript">
    new MapStores({
        geoData: <?= $arResult['GEO_DATA']?>
    });
</script>

