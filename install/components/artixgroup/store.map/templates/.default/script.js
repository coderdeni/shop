'use strict';
(function (window){
    window.MapStores = function (params)
    {
        this.zoom = !!params.zoom ? params.zoom : 14;
        this.mapId = !!params.mapId ? params.mapId : 'map-stores';
        this.inputSearchId = !!params.inputSearchId ? params.inputSearchId : 'map-stores-search';
        this.listContainerId = !!params.listContainerId ? params.listContainerId : 'map-stores-list';
        this.selectorListItem = !!params.selectorListItem ? params.selectorListItem : '.shops__item';
        this.selectorListActiveItem = !!params.selectorListActiveItem ? params.selectorListActiveItem : '.shops__item_active';
        this.minQueryLength = !!params.minQueryLength ? params.minQueryLength : 2;
        this.geoData = !!params.geoData ? params.geoData : [];
        this.lastRequestSearch = null;

        this.mapItems = {};

        ymaps.ready(BX.delegate(this.createYandexMap, this));
    };

    window.MapStores.prototype = {

        createYandexMap: function ()
        {
            this.map = new ymaps.Map(this.mapId, {
                center: [55.76, 37.64],
                zoom: this.zoom
            });

            this.collection = new ymaps.GeoObjectCollection(null);
            this.map.geoObjects.add(this.collection);
            this.listContainer = document.getElementById(this.listContainerId);
            this.inputSearch = document.getElementById(this.inputSearchId);

            if (!!this.inputSearch)
                BX.bind(this.inputSearch, 'keyup', BX.delegate(this.onSearchKeyUp, this));

            for (var i = 0; i < this.geoData.length; i++)
            {
                this.addPlacemark(this.geoData[i])
            }

            this.map.setBounds(this.map.geoObjects.getBounds(), {
                checkZoomRange: true,
            });
        },

        addPlacemark: function (item)
        {
            var placemark = new ymaps.Placemark(item.coordinates, {
                    balloonHeader: item.title,
                    balloonContent: item
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: item.icon,
                    iconImageSize: [item.iconWidth, item.iconHeight],
                    iconImageOffset: [item.iconOffsetTop, item.iconOffsetLeft],
                    balloonShadow: false,
                    balloonAutoPan: false,
                    balloonLayout: this.getYandexBalloonLayout(),
                    balloonContentLayout: this.getYandexBalloonContentLayout(),
                    balloonPanelMaxMapArea: 0,
                    hideIconOnBalloonOpen: false,
                    balloonOffset: [200, -70]
                }),
                template = this.getLayoutItem(item),
                wrapper = document.createElement('div');

            placemark.events.add('click', function (e) {
                var targetObject = e.get('target');
                var coordinates = placemark.geometry.getCoordinates();
                placemark.getMap().setZoom(14);
                placemark.getMap().panTo([parseFloat(coordinates[0]), parseFloat(coordinates[1])], {
                    flying: 1
                }).then(function () {
                    placemark.balloon.open();
                });
            });
            this.collection.add(placemark);

            if (!!this.listContainer)
            {
                wrapper.innerHTML = template;
                template = wrapper.firstChild;

                this.listContainer.append(template);
                /*
                this.mapItems[item.id] = template;

                BX.bind(template, 'click', BX.delegate(function () {

                    if (!placemark.balloon.isOpen())
                    {
                        var activeItem = document.querySelector(this.selectorListActiveItem);
                        if (!!activeItem)
                            activeItem.classList.remove(this.selectorListActiveItem);

                        this.classList.add(this.selectorListActiveItem);
                        var coordinates = placemark.geometry.getCoordinates();//!TODO: how?

                        this.map.setZoom(14);
                        this.map.panTo([parseFloat(coordinates[0]), parseFloat(coordinates[1])], {
                            flying: 1
                        }).then(function () {
                            placemark.balloon.open();//!TODO: how?
                        });
                    }
                    else
                    {
                        this.classList.remove(this.selectorListActiveItem);
                        placemark.balloon.close();//!TODO: how?
                    }

                }, this));
*/
                (function (placemark) {
                    BX.bind(template, 'click', function() {

                        if (!placemark.balloon.isOpen())
                        {
                            var activeItem = document.querySelector('.shops__item_active');
                            if (!!activeItem)
                                activeItem.classList.remove('shops__item_active');

                            this.classList.add('shops__item_active');
                            var coordinates = placemark.geometry.getCoordinates();
                            placemark.getMap().setZoom(14);
                            placemark.getMap().panTo([parseFloat(coordinates[0]), parseFloat(coordinates[1])], {
                                flying: 1
                            }).then(function () {
                                placemark.balloon.open();
                            });
                        }
                        else
                        {
                            this.classList.remove('shops__item_active');
                            placemark.balloon.close();
                        }
                        return false;
                    });
                })(placemark);
            }
        },

        //TODO: Remove all jquery code
        getYandexBalloonLayout: function ()
        {
            // Создание макета балуна на основе Twitter Bootstrap.
            var MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
                '<div class="shops__popup">' +
                '   <div class="dialog__close">' +
                '       <button class="action close">' +
                '           <svg class="icon icon-close "><use xlink:href="#icon-close"></use></svg>' +
                '       </button>' +
                '   </div>' +
                '   $[[options.contentLayout observeSize minWidth=370 maxWidth=370 maxHeight=350]]' +
                '   <div class="arrow"></div>' +
                '</div>', {
                    /**
                     * Строит экземпляр макета на основе шаблона и добавляет его в родительский HTML-элемент.
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#build
                     * @function
                     * @name build
                     */
                    build: function () {
                        this.constructor.superclass.build.call(this);

                        this._$element = $('.shops__popup', this.getParentElement());

                        this.applyElementOffset();

                        this._$element.find('.dialog__close')
                            .on('click', $.proxy(this.onCloseClick, this));
                    },

                    /**
                     * Удаляет содержимое макета из DOM.
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#clear
                     * @function
                     * @name clear
                     */
                    clear: function () {
                        this._$element.find('.dialog__close')
                            .off('click');

                        this.constructor.superclass.clear.call(this);
                    },

                    /**
                     * Метод будет вызван системой шаблонов АПИ при изменении размеров вложенного макета.
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
                     * @function
                     * @name onSublayoutSizeChange
                     */
                    onSublayoutSizeChange: function () {
                        MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

                        if(!this._isElement(this._$element)) {
                            return;
                        }

                        this.applyElementOffset();

                        this.events.fire('shapechange');
                    },

                    /**
                     * Сдвигаем балун, чтобы "хвостик" указывал на точку привязки.
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
                     * @function
                     * @name applyElementOffset
                     */
                    applyElementOffset: function () {
                        this._$element.css({
                            left: -(this._$element[0].offsetWidth / 2),
                            top: -(this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight)
                        });
                    },

                    /**
                     * Закрывает балун при клике на крестик, кидая событие "userclose" на макете.
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
                     * @function
                     * @name onCloseClick
                     */
                    onCloseClick: function (e) {
                        e.preventDefault();

                        this.events.fire('userclose');
                    },

                    /**
                     * Используется для автопозиционирования (balloonAutoPan).
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/ILayout.xml#getClientBounds
                     * @function
                     * @name getClientBounds
                     * @returns {Number[][]} Координаты левого верхнего и правого нижнего углов шаблона относительно точки привязки.
                     */
                    getShape: function () {
                        var position = this._$element.position();

                        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                            [position.left, position.top], [
                                position.left + this._$element[0].offsetWidth,
                                position.top + this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight
                            ]
                        ]));
                    },

                    /**
                     * Проверяем наличие элемента (в ИЕ и Опере его еще может не быть).
                     * @function
                     * @private
                     * @name _isElement
                     * @param {jQuery} [element] Элемент.
                     * @returns {Boolean} Флаг наличия.
                     */
                    _isElement: function (element) {
                        return element && element[0] && element.find('.arrow')[0];
                    }
                });

            return MyBalloonLayout;
        },

        getYandexBalloonContentLayout: function ()
        {
            return  ymaps.templateLayoutFactory.createClass(
                '<div class="shops__popup-name">$[properties.balloonHeader]</div>' +
                '<div class="shops__popup-info">' +
                '   <div class="shops__popup-info-row">' +
                '       <div class="shops__popup-info-cell">Адрес:</div>' +
                '       <div class="shops__popup-info-cell">$[properties.balloonContent.address]</div>' +
                '   </div>' +
                '   <div class="shops__popup-info-row">' +
                '       <div class="shops__popup-info-cell">Открыто:</div>' +
                '       <div class="shops__popup-info-cell">$[properties.balloonContent.schedule]</div>' +
                '   </div>' +
                '   <div class="shops__popup-info-row">' +
                '       <div class="shops__popup-info-cell">Телефон:</div>' +
                '       <div class="shops__popup-info-cell">$[properties.balloonContent.phone]</div>' +
                '   </div>' +
                '   <div class="shops__popup-info-row">' +
                '       <div class="shops__popup-info-cell">E-mail:</div>' +
                '       <div class="shops__popup-info-cell">$[properties.balloonContent.email]</div>' +
                '   </div>' +
                '</div>'
            );
        },

        searchOnYandexMap: function (request)
        {
            var points = [];

            this.collection.removeAll();

            if (!!this.listContainer)
                this.listContainer.innerHTML = '';

            for (var i = 0, l = this.geoData.length; i < l; i++)
            {
                var point = this.geoData[i];

                if (point.title.toLowerCase().indexOf(request.toLowerCase()) != -1
                    || point.address.toLowerCase().indexOf(request.toLowerCase()) != -1)
                {
                    points.push(point);
                }
            }

            for (var i = 0, l = points.length; i < l; i++)
            {
                this.addPlacemark(points[i]);
            }

            this.map.setBounds(this.map.geoObjects.getBounds(), {
                checkZoomRange: true,
            });
        },

        getLayoutItem: function (item)
        {
            return '<div class="shops__item'+(item.uf_showroom > 0 ? ' shops__item_showroom' : '')+'">'+
                '    <div class="shops__name">'+item.title+'</div>'+
                '    <div class="shops__adress">'+item.address+'</div>'+
                '    <div class="shops__mode">'+item.schedule+'</div>'+
                '</div>';
        },

        onSearchKeyUp: function ()
        {
            if ((this.inputSearch.value.length > this.minQueryLength) || this.lastRequestSearch.length > 0)
            {
                this.searchOnYandexMap(this.inputSearch.value);
            }

            this.lastRequestSearch = this.inputSearch.value;
        },
    };
})(window);

