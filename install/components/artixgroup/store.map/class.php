<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Catalog\StoreTable;
use \Bitrix\Main;
use Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc as Loc;

class ArtixgroupStoreMapComponent extends CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $filter = [];

    protected $limit = null;

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $cacheSections = array();


    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param $params
     * @return array
     * @internal param array $arParams
     */
    public function onPrepareComponentParams($params)
    {
        $params['PROVIDER'] = !empty($params['PROVIDER']) ? $params['PROVIDER'] : 'yandex';
        $params['ICON'] = !empty($params['ICON']) ? trim($params['ICON']) : '';
        $params['ICON_WIDTH'] = !empty($params['ICON_WIDTH']) ? (int)$params['ICON_WIDTH'] : 30;
        $params['ICON_HEIGHT'] = !empty($params['ICON_HEIGHT']) ? (int)$params['ICON_HEIGHT'] : 30;
        $params['ICON_OFFSET_TOP'] = !empty($params['ICON_OFFSET_TOP']) ? (int)$params['ICON_OFFSET_TOP'] : 0;
        $params['ICON_OFFSET_LEFT'] = !empty($params['ICON_OFFSET_LEFT']) ? (int)$params['ICON_OFFSET_LEFT'] : 0;

        return $params;
    }

    /**
     * определяет читать данные из кеша или нет
     * @return bool
     */
    protected function readDataFromCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
            return false;

        return !($this->StartResultCache(false, $this->cacheAddon));
    }

    /**
     * кеширует ключи массива arResult
     */
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
        {
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('catalog'))
            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
    }

    /**
     * проверяет заполнение обязательных параметров
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        if (empty($this->arParams['PROVIDER_KEY']))
            throw new Main\ArgumentNullException('PROVIDER_KEY');
    }

    /**
     * выполяет действия перед кешированием
     */
    protected function executeProlog()
    {
    }

    /**
     * получение результатов
     */
    protected function getResult()
    {
        $this->arResult['STORES'] = StoreTable::getList([
            'select' => ["ID", "TITLE", "ADDRESS", "DESCRIPTION", "GPS_N", "GPS_S", "IMAGE_ID", "PHONE", "EMAIL", "SCHEDULE", "SITE_ID", "UF_SHOWROOM"],
            'filter' => ['ACTIVE' => 'Y', '!GPS_N' => false, '!GPS_S' => false],
            'order' => ['SORT' => 'ASC']
        ])->fetchAll();
    }

    protected function getJson()
    {
        $json = [];

        if ($this->arResult['STORES'])
        {
            foreach ($this->arResult['STORES'] as $store)
            {
                $json[] = [
                    'coordinates' => [$store['GPS_N'], $store['GPS_S']],
                    'title' => $store['TITLE'],
                    'description' => $store['DESCRIPTION'],
                    'address' => $store['ADDRESS'],
                    'schedule' => $store['SCHEDULE'],
                    'phone' => $store['PHONE'],
                    'email' => $store['EMAIL'],
                    'icon' => $this->arParams['ICON'],
                    'iconWidth' => $this->arParams['ICON_WIDTH'],
                    'iconHeight' => $this->arParams['ICON_HEIGHT'],
                    'iconOffsetTop' => $this->arParams['ICON_OFFSET_TOP'],
                    'iconOffsetLeft' => $this->arParams['ICON_OFFSET_LEFT'],
                    'uf_showroom' => $store['UF_SHOWROOM'],
                ];
            }
        }

        $this->arResult['GEO_DATA'] = Main\Web\Json::encode($json);
    }

    /**
     * выполняет действия после выполения компонента, например установка заголовков из кеша
     */
    protected function executeEpilog()
    {
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->checkParams();
            $this->executeProlog();
            $this->getResult();

            if (!$this->readDataFromCache())
            {
                $this->getResult();
                $this->getJson();
                $this->includeComponentTemplate();
                $this->putDataToCache();
            }

            $this->executeEpilog();
        }
        catch (Exception $e)
        {
            $this->abortDataCache();
            ShowError($e->getMessage());
        }
    }
}
