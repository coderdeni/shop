<?php

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arCurrentValues */

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS" => array(
        "PROVIDER" => Array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_STORE_MAP_PROVIDER"),
            "TYPE" => "TEXT",
            "DEFAULT" => "yandex"
        ),
        "PROVIDER_KEY" => Array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_STORE_MAP_PROVIDER_KEY"),
            "TYPE" => "TEXT",
            "DEFAULT" => ""
        ),
        "ICON" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_STORE_MAP_ICON"),
            "TYPE" => "TEXT",
            "DEFAULT" => "yandex"
        ),
        "ICON_WIDTH" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_STORE_MAP_ICON_WIDTH"),
            "TYPE" => "TEXT",
            "DEFAULT" => "30"
        ),
        "ICON_HEIGHT" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_STORE_MAP_ICON_HEIGHT"),
            "TYPE" => "TEXT",
            "DEFAULT" => "30"
        ),
        "ICON_OFFSET_TOP" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_STORE_MAP_ICON_OFFSET_TOP"),
            "TYPE" => "TEXT",
            "DEFAULT" => "0"
        ),
        "ICON_OFFSET_LEFT" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("ARTIXGROUP_STORE_MAP_ICON_OFFSET_LEFT"),
            "TYPE" => "TEXT",
            "DEFAULT" => "0"
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
    ),
);
