<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main,
    Bitrix\Main\Context,
    Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Currency\CurrencyManager,
    Bitrix\Sale\Order,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem;
use Bitrix\Main\Application;

class ArtixBuyClickComponent extends CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $arFilter = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $cacheSections = array();


    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param $params
     * @return array
     * @internal param array $arParams
     */
    public function onPrepareComponentParams($params)
    {
        $params['LINK'] = !empty($params['LINK']) ? $params['LINK'] : '/personal/favourites.php';
        $params['SELECTED_CLASS'] = !empty($params['SELECTED_CLASS']) ? $params['SELECTED_CLASS'] : 'favorites_added';

        return $params;
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('sale'))
            throw new Main\LoaderException(Loc::getMessage('ARTIXGROUP_FAVOURITES_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
        if (!Loader::includeModule('catalog'))
            throw new Main\LoaderException(Loc::getMessage('ARTIXGROUP_FAVOURITES_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
        if (!Loader::includeModule('artixgroup.shop'))
            throw new Main\LoaderException(Loc::getMessage('ARTIXGROUP_FAVOURITES_CLASS_FAVOURITES_MODULE_NOT_INSTALLED'));
    }

    /**
     * выполяет действия перед кешированием
     */
    protected function executeProlog()
    {
    }

    protected function getFinalPrice($productId, $quantity = 1)
    {
        global $USER;

        $renewal = 'N';
        $prices = CCatalogProduct::GetOptimalPrice(
            $productId,
            $quantity,
            $USER->GetUserGroupArray(),
            $renewal
        );

        if (!$prices || count($prices) <= 0)
        {
            if ($nearestQuantity = CCatalogProduct::GetNearestQuantityPrice($productId, $quantity, $USER->GetUserGroupArray()))
            {
                $quantity = $nearestQuantity;
                $prices = CCatalogProduct::GetOptimalPrice($productId, $quantity, $USER->GetUserGroupArray(), $renewal);
            }
        }

        $prices['RESULT_PRICE']['TOTAL_DISCOUNT_PRICE'] = $prices['RESULT_PRICE']['DISCOUNT_PRICE'] * $quantity;
        $prices['RESULT_PRICE']['TOTAL_PRICE'] = $prices['RESULT_PRICE']['BASE_PRICE'] * $quantity;
        $prices['RESULT_PRICE']['TOTAL_DISCOUNT'] = $prices['RESULT_PRICE']['DISCOUNT'] * $quantity;

        $prices['RESULT_PRICE']['PRINT_TOTAL_DISCOUNT_PRICE'] = CCurrencyLang::CurrencyFormat($prices['RESULT_PRICE']['TOTAL_DISCOUNT_PRICE'] , $prices['RESULT_PRICE']['CURRENCY'], true);
        $prices['RESULT_PRICE']['PRINT_TOTAL_PRICE'] = CCurrencyLang::CurrencyFormat($prices['RESULT_PRICE']['TOTAL_PRICE'] , $prices['RESULT_PRICE']['CURRENCY'], true);
        $prices['RESULT_PRICE']['PRINT_TOTAL_DISCOUNT_DIFF'] = CCurrencyLang::CurrencyFormat($prices['RESULT_PRICE']['TOTAL_DISCOUNT'] , $prices['RESULT_PRICE']['CURRENCY'], true);

        $prices['RESULT_PRICE']['PRINT_DISCOUNT_PRICE'] = CCurrencyLang::CurrencyFormat($prices['RESULT_PRICE']['DISCOUNT_PRICE'], $prices['RESULT_PRICE']['CURRENCY'], true);
        $prices['RESULT_PRICE']['PRINT_PRICE'] = CCurrencyLang::CurrencyFormat($prices['RESULT_PRICE']['BASE_PRICE'], $prices['RESULT_PRICE']['CURRENCY'], true);
        $prices['RESULT_PRICE']['PRINT_DISCOUNT_DIFF'] = CCurrencyLang::CurrencyFormat($prices['RESULT_PRICE']['DISCOUNT'], $prices['RESULT_PRICE']['CURRENCY'], true);

        return $prices['RESULT_PRICE'];
    }

    protected function getProductById($productId)
    {
        $select = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'DETAIL_PAGE_URL',
            'DETAIL_PICTURE',
            'PREVIEW_PICTURE',
            'CATALOG_BUNDLE',
            'PROPERTIES_*'
        ];
        $filter = [
            "ID" => $productId,
        ];
        $iterator = CIBlockElement::GetList([], $filter, false, false, $select);

        return $iterator->GetNextElement();
    }

    protected function loadMorePhoto(&$element)
    {
        $iterator = Main\FileTable::getList([
            'filter' => ['ID' => $element['PROPERTIES']['MORE_PHOTO']['VALUE']]
        ]);
        while ($file = $iterator->fetch())
        {
            $file['SRC'] = CFile::GetFileSRC($file);

            foreach ($element['PROPERTIES']['MORE_PHOTO']['VALUE'] as &$id)
            {
                if ($id === $file['ID'])
                {
                    $id = $file;
                }
            }
        }
    }

    protected function addBundlePrice(&$product)
    {
        if (CCatalogProductSet::isProductHaveSet($product['ID'], CCatalogProductSet::TYPE_GROUP))
        {
            $productPrice = $product['ITEM_PRICES'][0];
            $allSets = CCatalogProductSet::getAllSetsByProduct($product['ID'], CCatalogProductSet::TYPE_GROUP);

            foreach ($allSets as $oneSet)
            {
                if ($oneSet['ACTIVE'] == 'Y')
                {
                    $currentSet = $oneSet;
                    break;
                }
            }

            foreach ($currentSet['ITEMS'] as $item)
            {
                if ($item['ITEM_ID'] === $product['ID'])
                    continue;

                $price = $this->getFinalPrice($item['ITEM_ID'], $item['QUANTITY']);

                $productPrice['TOTAL_DISCOUNT_PRICE'] += $price['TOTAL_DISCOUNT_PRICE'];
                $productPrice['TOTAL_PRICE'] += $price['TOTAL_PRICE'];
                $productPrice['TOTAL_DISCOUNT'] += $price['TOTAL_DISCOUNT'];

                $productPrice['DISCOUNT_PRICE'] += $price['DISCOUNT_PRICE'];
                $productPrice['PRICE'] += $price['DISCOUNT_PRICE'];
                $productPrice['DISCOUNT_DIFF'] += $price['DISCOUNT_DIFF'];
            }


            $productPrice['PRINT_TOTAL_DISCOUNT_PRICE'] = CCurrencyLang::CurrencyFormat($productPrice['TOTAL_DISCOUNT_PRICE'], $productPrice['CURRENCY'], true);
            $productPrice['PRINT_TOTAL_PRICE'] = CCurrencyLang::CurrencyFormat($productPrice['TOTAL_PRICE'], $productPrice['CURRENCY'], true);
            $productPrice['PRINT_TOTAL_DISCOUNT_DIFF'] = CCurrencyLang::CurrencyFormat($productPrice['TOTAL_DISCOUNT'], $productPrice['CURRENCY'], true);

            $productPrice['PRINT_DISCOUNT_PRICE'] = CCurrencyLang::CurrencyFormat($productPrice['DISCOUNT_PRICE'], $productPrice['CURRENCY'], true);
            $productPrice['PRINT_PRICE'] = CCurrencyLang::CurrencyFormat($productPrice['PRICE'], $productPrice['CURRENCY'], true);
            $productPrice['PRINT_DISCOUNT_DIFF'] = CCurrencyLang::CurrencyFormat($productPrice['DISCOUNT_DIFF'], $productPrice['CURRENCY'], true);

            $product['ITEM_PRICES'][0] = $productPrice;
            unset($allSets, $currentSet, $productPrice);
        }
    }

    protected function getProduct()
    {
        $product = null;
        $productId = (int)$this->request->get('PRODUCT_ID');

        if ($productId > 0)
        {
            if ($ob = $this->getProductById($productId))
            {
                $fields = $ob->GetFields();
                $fields['PROPERTIES'] = $ob->GetProperties();
                $fields['ITEM_PRICES'][] = $this->getFinalPrice($productId);

                if ($fields['PROPERTIES']['MORE_PHOTO']['VALUE'])
                {
                    $this->loadMorePhoto($fields);
                }

                if ($fields['CATALOG_TYPE'] == CCatalogProduct::TYPE_OFFER && $fields['PROPERTIES']['CML2_LINK']['VALUE'])
                {
                    if ($obParent = $this->getProductById($fields['PROPERTIES']['CML2_LINK']['VALUE']))
                    {
                        $parent = $obParent->GetFields();
                        $parent['PROPERTIES'] = $obParent->GetProperties();

                        if ($parent['PROPERTIES']['MORE_PHOTO']['VALUE'])
                        {
                            $this->loadMorePhoto($parent);
                        }

                        if ($fields['PREVIEW_PICTURE'])
                            $fields['PREVIEW_PICTURE'] = CFile::GetFileArray($fields['PREVIEW_PICTURE']);
                        elseif ($fields['PROPERTIES']['MORE_PHOTO']['VALUE'])
                            $fields['PREVIEW_PICTURE'] = $fields['PROPERTIES']['MORE_PHOTO']['VALUE'][0];
                        elseif ($parent['PREVIEW_PICTURE'])
                            $fields['PREVIEW_PICTURE'] = CFile::GetFileArray($parent['PREVIEW_PICTURE']);
                        elseif ($parent['PROPERTIES']['MORE_PHOTO']['VALUE'])
                            $fields['PREVIEW_PICTURE'] = $parent['PROPERTIES']['MORE_PHOTO']['VALUE'][0];

                        if ($fields['DETAIL_PICTURE'])
                            $fields['DETAIL_PICTURE'] = CFile::GetFileArray($fields['DETAIL_PICTURE']);

                        $parent['OFFERS'][] = $fields;
                        $parent['OFFERS_SELECTED'] = 0;
                        $product = $parent;
                    }
                }
                else
                {
                    $product = $fields;

                    $this->addBundlePrice($product);

                    if ($product['PREVIEW_PICTURE'])
                        $product['PREVIEW_PICTURE'] = CFile::GetFileArray($product['PREVIEW_PICTURE']);
                    elseif ($product['PROPERTIES']['MORE_PHOTO']['VALUE'])
                        $product['PREVIEW_PICTURE'] = $product['PROPERTIES']['MORE_PHOTO']['VALUE'][0];

                    if ($product['DETAIL_PICTURE'])
                        $product['DETAIL_PICTURE'] = CFile::GetFileArray($product['DETAIL_PICTURE']);
                }
            }
        }

        return $product;
    }

    protected function addToBasket()
    {
        global $USER;

        $productId = (int)$this->request->getPost("PRODUCT_ID");
        $phone = $this->request->getPost("PHONE");
        $name = $this->request->getPost("NAME");
        $email = $this->request->getPost("EMAIL");
        $comment = $this->request->getPost("COMMENT");
        $siteId = Context::getCurrent()->getSite();
        $currencyCode = CurrencyManager::getBaseCurrency();

        $order = Order::create($siteId, $USER->isAuthorized() ? $USER->GetID() : 1);
        $order->setPersonTypeId(1);
        $order->setField('CURRENCY', $currencyCode);

        if ($comment)
        {
            $order->setField('USER_DESCRIPTION', $comment); // Устанавливаем поля комментария покупателя
        }

        $basket = Basket::create($siteId);
        $item = $basket->createItem('catalog', $productId);
        $item->setFields(array(
            'QUANTITY' => 1,
            'CURRENCY' => $currencyCode,
            'LID' => $siteId,
            'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
        ));
        $order->setBasket($basket);

        /** @var \Bitrix\Sale\PropertyValueCollection $propertyCollection */
        $propertyCollection = $order->getPropertyCollection();
        if (isset($_SESSION['ARTIXGROUP_LOCATION']) && !empty($_SESSION['ARTIXGROUP_LOCATION']))
        {
            /** @var \Bitrix\Sale\PropertyValue $orderDeliveryLocation */
            $orderDeliveryLocation = $propertyCollection->getDeliveryLocation();
            $orderDeliveryLocation->setValue($_SESSION['ARTIXGROUP_LOCATION']['CODE']); // В какой город "доставляем" (куда доставлять).
        }

        $phoneProp = $propertyCollection->getPhone();
        $phoneProp->setValue($phone);
        $nameProp = $propertyCollection->getPayerName();
        $nameProp->setValue($name);
        $emailProp = $propertyCollection->getUserEmail();
        $emailProp->setValue($email);

        // Создаём одну отгрузку и устанавливаем способ доставки - "Без доставки" (он служебный)
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $service = Delivery\Services\Manager::getById(Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
        $shipment->setFields(array(
            'DELIVERY_ID' => $service['ID'],
            'DELIVERY_NAME' => $service['NAME'],
        ));
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipmentItem = $shipmentItemCollection->createItem($item);
        $shipmentItem->setQuantity($item->getQuantity());

        // Создаём оплату со способом #1
        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem();
        $paySystemService = PaySystem\Manager::getObjectById(1);
        $payment->setFields(array(
            'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
            'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
        ));

        // Сохраняем
        $order->doFinalAction(true);
        $result = $order->save();

        if ($result->isSuccess())
        {
            return true;
        }

        $this->arResult['ERRORS'] = $result->getErrorMessages();
        return false;
    }

    /**
     * выполняет действия после выполения компонента, например установка заголовков из кеша
     */
    protected function executeEpilog()
    {
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->executeProlog();

            if ($this->request->isAjaxRequest() && check_bitrix_sessid())
            {
                if ($this->request->get('action') === 'add2basket')
                {
                    $data = [
                        'success' => true,
                        'message' => $this->arParams['RESULT_MESSAGE']
                    ];

                    if (!$this->addToBasket())
                    {
                        $data['success'] = false;
                        $data['message'] = implode('<br>', $this->arResult['ERRORS']);
                    }

                    echo Main\Web\Json::encode($data);
                }
                elseif ($this->request->get('action') === 'popup')
                {
                    global $USER;
                    $this->arResult = $this->getProduct();
                    $this->arResult['USER'] = [
                        'NAME' => $USER->GetFullName(),
                        'EMAIL' => $USER->GetEmail(),
                        'PHONE' => $USER->GetParam('PERSONAL_PHONE')
                    ];
                    $this->includeComponentTemplate('ajax');
                }

                Application::getInstance()->end(200);
            }

            $this->includeComponentTemplate();
            $this->executeEpilog();
        }
        catch (Exception $e)
        {
            ShowError($e->getMessage());
        }
    }
}
