<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("ARTIXGROUP_BUY_ONE_CLICK_COMPONENT_NAME"),
    "DESCRIPTION" => GetMessage("ARTIXGROUP_BUY_ONE_CLICK_COMPONENT_DESCRIPTION"),
    "CACHE_PATH" => "Y",
    "SORT" => 40,
    "PATH" => array(
        "ID" => "artixgroup",
        "NAME" => GetMessage("ARTIXGROUP_BUY_ONE_CLICK_COMPONENT_GROUP"),
        "SORT" => 10
    ),
);
?>