<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arCurrentValues */

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS" => array(
        "PRODUCT_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTIXGROUP_BUY_ONE_CLICK_PRODUCT_ID"),
            "TYPE" => "TEXT",
            "DEFAULT" => ""
        ),
        "BUTTON_MESSAGE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTIXGROUP_BUY_ONE_CLICK_BUTTON_MESSAGE"),
            "TYPE" => "TEXT",
            "DEFAULT" => GetMessage("ARTIXGROUP_BUY_ONE_CLICK_BUTTON_VALUE")
        ),
        "RESULT_MESSAGE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTIXGROUP_BUY_ONE_CLICK_RESULT_MESSAGE"),
            "TYPE" => "TEXT",
            "DEFAULT" => ""
        ),
    ),
);
