<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

if (isset($arResult['OFFERS']))
    $actualItem = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']];
else
    $actualItem = $arResult;

$prices = $actualItem['ITEM_PRICES'][0];
?>
<div id="productBuyOne" class="dialog dialog--open" data-role="buyclick-popup">
    <div class="dialog__overlay"></div>
    <div class="dialog__noscroll">
        <div class="dialog__container">
            <div class="dialog__title">Купить в 1 клик</div>
            <div class="dialog__content">
                <div class="productBuyOneDialog">
                    <div class="productBuyOneDialog__product" data-role="buyclick-item">
                        <div class="catalogItem">
                            <div class="catalogItem__container">
                                <div class="catalogItem__label">
                                    <?
                                    if ($prices['PERCENT'] > 0)
                                    {
                                        ?>
                                        <div class="saleLabel"><span class="saleLabel__text">-<?= $prices['PERCENT']?>%</span></div>
                                        <?
                                    }
                                    ?>
                                </div>
                                <a href="<?= $actualItem['DETAIL_PAGE_URL']?>" class="catalogItem__content">
                                    <div class="catalogItem__wrapper">
                                        <div class="catalogItem__photo"><img src="<?= $actualItem['PREVIEW_PICTURE']['SRC']?>" alt="" class="catalogItem__img"></div>
                                        <span class="catalogItem__title"><?= $actualItem['NAME']?></span>
                                    </div>
                                    <div class="catalogItem__price">
                                        <div class="prices">
                                            <div class="price"><?= $prices['PRINT_DISCOUNT_PRICE']?></div>
                                            <?
                                            if ($prices['PERCENT'] > 0)
                                            {
                                                ?>
                                                <div class="price-old"><?= $prices['PRINT_PRICE']?></div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="productBuyOneDialog__form">
                        <form action="" class="form" data-role="buyclick-form" method="post">
                            <?= bitrix_sessid_post()?>
                            <input type="hidden" name="PRODUCT_ID" value="" data-role="buyclick-hidden">
                            <div class="form__row">
                                <?
                                foreach ($arResult['ERRORS'] as $error)
                                {
                                    ?>
                                    <div class="authorization__infoText"><?= $error?></div>
                                    <?
                                }
                                ?>
                                <div class="authorization__infoText">Наш менеджер свяжется с вами в ближайшее время</div>
                            </div>
                            <div class="form__row">
                                <input type="text" name="NAME" class="input input_big" value="<?= $arResult['USER']['NAME']?>" placeholder="Имя *" required>
                            </div>
                            <div class="form__row">
                                <input type="tel" name="PHONE" class="input input_big" value="<?= $arResult['USER']['PHONE']?>" placeholder="Телефон *" required>
                            </div>
                            <div class="form__row">
                                <input type="text" name="EMAIL" class="input input_big" value="<?= $arResult['USER']['EMAIL']?>" placeholder="Электронная почта *" required>
                            </div>
                            <div class="form__row">
                                <textarea name="COMMENT" rows="3" class="textarea textarea_big" placeholder="Комментарий"></textarea>
                            </div>
                            <div class="form__row form__row_sb">
                                <input type="submit" class="button button_bp" value="Купить в 1 клик" data-role="buyclick-buy" disabled>
                            </div>
                            <div class="form__row form__row_sb">
                                <label class="checkbox" for="rule-ch">
                                    <input id="rule-ch" type="checkbox" class="checkbox__input" data-role="buyclick-rule">
                                    <span class="checkbox__new-input"></span>
                                    Я согласен на обработку моих персональных данных
                                </label>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="dialog__close">
                <button class="action close" data-dialog-close="">
                    <svg class="icon icon-close ">
                        <use xlink:href="#icon-close"></use>
                    </svg>
                </button>
            </div>
        </div>
        <div class="dialog__event"></div>
    </div>
</div>