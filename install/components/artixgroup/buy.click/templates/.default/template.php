<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

?>
<a id="buy_one_click" href="javascript:void(0)" class="productBuyOne"><?= $arParams['BUTTON_MESSAGE']?></a>

<script>
    new BuyOneClick({
        templateName: '<?= $templateName?>',
        parameters: '<?= serialize($arParams)?>',
        actionUrl: '<?= $componentPath?>/ajax.php',
        productId: <?= $arParams['PRODUCT_ID']?>,
        linkId: 'buy_one_click'
    });
</script>