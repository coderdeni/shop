(function (window){
    window.BuyOneClick = function (params) {
        this.actionUrl = !!params.actionUrl ? params.actionUrl : '/bitrix/components/artixgroup/ajax.comments/ajax.php';
        this.templateName = !!params.templateName ? params.templateName : '.default';
        this.parameters = !!params.parameters ? params.parameters : '';
        this.productId = params.productId;
        this.linkId = !!params.linkId ? params.linkId : 'buy_one_click';
        this.popUpId = !!params.popUpId ? params.popUpId : 'productBuyOne';

        this.inputErrorClass = !!params.input_error_class ? params.input_error_class : 'input_has-error';
        this.popup = null;
        this.inputProductId = null;
        this.buttonBuy = null;
        this.checkboxRule = null;

        BX.ready(BX.delegate(this.init, this));
    };

    window.BuyOneClick.prototype = {

        init: function()
        {
            this.link = document.getElementById(this.linkId);

            if (!!this.link)
                this.link.addEventListener('click', BX.delegate(this.showPopup, this));

            BX.addCustomEvent('onCatalogElementChangeOffer', BX.delegate(function (result) {
                this.productId = result.data.newId;
            }, this));
        },

        isValidField: function (field)
        {
            switch (field.type)
            {
                case 'email':
                    const email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (!email.test(String(field.value).toLowerCase()))
                        return false;
                    break;
                case 'tel':
                    const phone = /^\+?([0-9])\)?[-\s\.]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{2,4}[-\s\.]?[0-9]{2}?$/im;
                    if (!phone.test(String(field.value).toLowerCase()))
                        return false;
                    break;
                case 'text':
                    if (field.value == '')
                        return false;
                    break;
            }

            return true;
        },

        showPopup: function()
        {
            if (this.productId > 0)
            {
                BX.ajax({
                    url: this.actionUrl,
                    method: 'POST',
                    dataType: 'html',
                    data: {
                        action: 'popup',
                        PRODUCT_ID: this.productId,
                        sessid: BX.bitrix_sessid(),
                        parameters: this.parameters,
                        templateName: this.templateName,
                    },
                    onsuccess: BX.delegate(function(data) {
                        var div = document.createElement('div');
                        div.innerHTML = data;
                        this.popup = div.querySelector('#'+this.popUpId);

                        if (!!this.popup)
                        {
                            this.buttonBuy = this.popup.querySelector('[data-role="buyclick-buy"]');
                            var actions = this.popup.querySelectorAll('.action, .dialog__event'),
                                checkboxPolicy = this.popup.querySelector('[data-role="buyclick-rule"]');;

                            if (!!actions)
                            {
                                for (var i = 0; i < actions.length; i++)
                                    actions[i].addEventListener('click', BX.delegate(this.closePopup, this));
                            }

                            if (!!this.buttonBuy)
                                this.buttonBuy.addEventListener('click', BX.delegate(this.onClickButtonBuy, this));

                            if (!!checkboxPolicy)
                                checkboxPolicy.addEventListener('change', BX.delegate(this.onCheckboxPolicyChange, this));

                            document.body.appendChild(this.popup);
                        }
                    }, this)
                });
            }

            event.preventDefault();
        },

        closePopup : function()
        {
            if (!!this.popup)
            {
                this.popup.classList.remove('dialog--open');
                this.popup.classList.add('dialog--close');

                var animationEvents = ['webkitAnimationEnd', 'MSAnimationEnd', 'oAnimationEnd', 'animationend'],
                    container = this.popup.querySelector('.dialog__container');

                if (!!container)
                {
                    for (var i = 0; i < animationEvents.length; i++)
                        container.addEventListener(this.animationEvents[i], BX.proxy(this.endAnimation, this));
                }
            }
        },

        endAnimation : function()
        {
            if (!!this.popup)
            {
                document.body.removeChild(this.popup);
            }
        },

        onClickButtonBuy: function()
        {
            var form = this.popup.querySelector('[data-role="buyclick-form"]'),
                hasError = false,
                data = {},
                i, field;

            event.preventDefault();

            for (i = 0; i < form.elements.length; i++)
            {
                field = form.elements[i];
                data[field.name] = field.value;

                if (field.required && !this.isValidField(field))
                {
                    field.classList.add(this.inputErrorClass);
                    hasError = true;
                }
                else if (field.classList.contains(this.inputErrorClass))
                {
                    field.classList.remove(this.inputErrorClass);
                }
            }

            if (!hasError)
                this.sendAddToBasket(data);
        },

        sendAddToBasket : function(data)
        {
            data['PRODUCT_ID'] = this.productId;
            data['action'] = 'add2basket';
            data['sessid'] = BX.bitrix_sessid();
            data['parameters'] = this.parameters;
            data['templateName'] = this.templateName;

            BX.ajax({
                url: this.actionUrl,
                method: 'POST',
                dataType: 'json',
                data: data,
                onsuccess: BX.delegate(function(data) {
                    if (data.success) {
                        var content = this.popup.querySelector('.dialog__content');
                        content.innerHTML = data.message;
                    }
                }, this)
            });
        },

        onCheckboxPolicyChange: function()
        {
            this.buttonBuy.disabled = !BX.proxy_context.checked;
        }
    };
})(window);