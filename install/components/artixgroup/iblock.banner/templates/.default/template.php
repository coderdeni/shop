<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
?>

<div class="slider">
    <div class="slider__container swiper-container">
        <div class="slider__main slider__swiper swiper-container">
            <div class="slider__main swiper-wrapper">
                <?
                foreach($arResult["BANNERS"] as $banner)
                {
                    $this->AddEditAction($banner['ID'], $banner['EDIT_LINK'], CIBlock::GetArrayByID($banner["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($banner['ID'], $banner['DELETE_LINK'], CIBlock::GetArrayByID($banner["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <<?=$banner["URL"] ? 'a href="'.$banner["URL"].'"' : 'div'?> class="slider__item swiper-slide">
                        <div class="slider__info limiter">
                            <div class="slider__title"><?= $banner['NAME']?></div>
                            <?
                            if ($banner['PREVIEW_TEXT'])
                            {
                                ?>
                                <div class="slider__subtitle"><?= $banner['PREVIEW_TEXT']?></div>
                                <?
                            }
                            if ($banner["FORM_ID"])
                            {
                                $APPLICATION->IncludeComponent(
                                    "artixgroup:form",
                                    "",
                                    Array(
                                        "FORM_ID" => $banner["FORM_ID"],
                                    ),
                                    $component
                                );
                            }
                            ?>
                        </div>
                        <div class="slider__photo" id="<?=$this->GetEditAreaId($banner['ID']);?>">
                            <img src="<?= $banner['DESKTOP_PICTURE']?>" alt="<?= $banner['NAME']?>" class="slider__img swiper-lazy">
                            <?
                            if ($banner['MOBILE_PICTURE'])
                            {
                                ?>
                                <img src="<?= $banner['MOBILE_PICTURE']?>" alt="<?= $banner['NAME']?>" class="slider__img slider__img_mobile swiper-lazy">
                                <?
                            }
                            ?>
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </div>
                    </<?= $banner["URL"] ? 'a' : 'div'?>>
                    <?
                }
                ?>
            </div>
        </div>
        <div class="slider__pagination"></div>
    </div>
</div>
