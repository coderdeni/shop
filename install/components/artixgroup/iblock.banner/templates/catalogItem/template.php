<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
$banner = $arResult['BANNERS'][0];

if ($banner)
{
    ?>
    <a href="<?=$banner["FORM_ID"] ? 'javascript:void(0)' : $banner["URL"]?>" class="catalogItem catalogItem_banner <?= $banner["FORM_ID"]?>" id="<?=$this->GetEditAreaId($banner['ID']);?>">
        <img class="catalogItem__img catalogItem__img_banner" src="<?= $banner['DESKTOP_PICTURE']?>">
    </a>
    <?
}
