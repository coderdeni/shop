<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Artixgroup\Shop\Helpers\Filter;
use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Mail;

class ArtixgroupIblockBanner extends CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = [];

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = [];

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = [];

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $arFilter = [];

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = [];

    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param array $params
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $params['IBLOCK_TYPE'] = trim($params['IBLOCK_TYPE']);
        $params['IBLOCK_ID'] = intval($params['IBLOCK_ID']);
        $params['SECTION'] = trim($params['SECTION']);
        $params['FILTER_NAME'] = trim($params['FILTER_NAME']);
        $params['PAGE_SIZE'] = intval($params['PAGE_SIZE']) > 0 ? intval($params['PAGE_SIZE']) : null;

        return $params;
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     */
    protected function checkModules()
    {
        if (!Main\Loader::includeModule('iblock'))
            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
    }

    /**
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        if ($this->arParams['IBLOCK_ID'] <= 0)
            throw new Main\ArgumentNullException('IBLOCK_ID');

        if (empty($this->arParams['SECTION']))
            throw new Main\ArgumentNullException('SECTION');
    }

    /**
     * определяет читать данные из кеша или нет
     * @return bool
     */
    protected function readDataFromCache()
    {
        global $USER;

        if ($this->arParams['CACHE_TYPE'] === 'N')
            return false;

        if (is_array($this->cacheAddon))
            $this->cacheAddon[] = $USER->GetUserGroupArray();
        else
            $this->cacheAddon = [$USER->GetUserGroupArray()];

        return !($this->startResultCache(false, $this->cacheAddon, md5(serialize($this->arParams))));
    }

    /**
     * кеширует ключи массива arResult
     */
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
        {
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    /**
     * завершает кеширование
     * @return bool
     */
    protected function endCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
        {
            return false;
        }

        $this->endResultCache();
        return true;
    }

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    /**
     * выполяет действия перед кешированием
     */
    protected function executeProlog()
    {
        $filterName = Filter::create([
            'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
            '=SECTION_CODE' => $this->arParams['SECTION'],
            'ACTIVE' => 'Y'
        ]);

        Filter::merge($filterName, $this->arParams['FILTER_NAME']);

        $this->cacheAddon = [
            Filter::getSum($filterName)
        ];

        $this->arFilter = Filter::get($filterName);
        Filter::unset($filterName);

        if ($this->arParams['PAGE_SIZE'] > 0)
        {
            $this->navParams = [
                'nPageSize' => $this->arParams['PAGE_SIZE']
            ];
        }
    }

    protected function getResult()
    {
        $iterator = CIBlockElement::GetList(
            ['SORT' => 'asc'],
            $this->arFilter,
            false,
            $this->navParams,
            ['ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'PROPERTIES_*']
        );

        while ($ob = $iterator->GetNextElement())
        {
            $element = $ob->GetFields();
            $properties = $ob->GetProperties();

            $this->arResult['BANNERS'][] = [
                'ID' => $element['ID'],
                'NAME' => $element['NAME'],
                'MOBILE_PICTURE' => $element['PREVIEW_PICTURE'] ? CFile::GetPath($element['PREVIEW_PICTURE']) : null,
                'DESKTOP_PICTURE' => $element['DETAIL_PICTURE'] ? CFile::GetPath($element['DETAIL_PICTURE']) : null,
                'PREVIEW_TEXT' => $element['PREVIEW_TEXT'],
                'DETAIL_TEXT' => $element['DETAIL_TEXT'],
                'URL' => $properties['LINK']['VALUE'],
                'FORM_ID' => (int)$properties['FORM_ID']['VALUE'],
            ];
        }
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->checkParams();
            $this->executeProlog();

            if (!$this->readDataFromCache())
            {
                $this->getResult();
                $this->putDataToCache();
                $this->includeComponentTemplate();
            }
        }
        catch (Exception $e)
        {
            $this->abortDataCache();
            ShowError($e->getMessage());
        }
    }
}
