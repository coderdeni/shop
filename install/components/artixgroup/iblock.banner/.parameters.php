<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("all"=>" "));

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="all"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
{
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arElementIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["ELEMENT_IBLOCK_TYPE"]!="all"?$arCurrentValues["ELEMENT_IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
{
    $arElementIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME"=>GetMessage("AG_IBLOCK_TYPE"),
			"TYPE"=>"LIST",
			"VALUES"=>$arTypesEx,
			"DEFAULT"=>"catalog",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME"=>GetMessage("AG_IBLOCK_ID"),
			"TYPE"=>"LIST",
			"VALUES"=>$arIBlocks,
			"DEFAULT"=>'1',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH" => "Y",
		),
        "SECTION" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("AG_SECTION"),
            "TYPE" => "TEXT",
        ),
		"FILTER_NAME" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("AG_FILTER_NAME"),
            "DEFAULT" => "10",
            "TYPE" => "TEXT",
        ),
        "PAGE_SIZE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("AG_PAGE_SIZE"),
            "DEFAULT" => "10",
            "TYPE" => "TEXT",
        ),
		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
	),
);
