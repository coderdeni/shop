<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("ARTIXGROUP_FORM_COMPONENT_NAME"),
    "DESCRIPTION" => GetMessage("ARTIXGROUP_FORM_COMPONENT_DESCRIPTION"),
    "CACHE_PATH" => "Y",
    "SORT" => 30,
    "PATH" => array(
        "ID" => "artixgroup",
        "NAME" => GetMessage("ARTIXGROUP_SHOP_COMPONENT_GROUP"),
        "SORT" => 10
    ),
);
