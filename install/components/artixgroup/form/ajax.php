<?php
/** @global CMain $APPLICATION */
define('STOP_STATISTICS', true);
define('PUBLIC_AJAX_MODE', true);
define('NOT_CHECK_PERMISSIONS', true);

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Application::getInstance()->getContext()->getRequest();

if ($request->isAjaxRequest() && check_bitrix_sessid() && $request->getPost('form_id'))
{
    $APPLICATION->IncludeComponent(
        "artixgroup:form",
        '',
        array('FORM_ID' => (int)$request->getPost('form_id')),
        false
    );
}