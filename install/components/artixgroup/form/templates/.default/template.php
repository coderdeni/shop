<?php

use Artixgroup\Shop\FormTable;
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$id = uniqid('ag_form_');

$itemIds = [
    'CAPTCHA' => $id.'_captcha',
    'RESULT_MESSAGE' => $id.'_message',
    'SEND' => $id.'_send',
    'CHECK_AGREEMENT' => $id.'_agreement',
];

$cssPrefix = $arResult['PARAMETERS']['FORM_CSS_PREFIX'];

if ($arResult['SHOW_MODE'] === FormTable::SHOW_MODE_POPUP)
{
    ?>
    <div class="dialog dialog--open">
        <div class="dialog__overlay"></div>
        <div class="dialog__noscroll">
            <div class="dialog__container">
                <div class="dialog__title"><?= $arResult['NAME']?></div>
                <div class="dialog__content">
                    <div class="authorization">
                        <div class="authorization__form">
    <?
}
else
{
    ?>
    <div class="<?= $cssPrefix?>">
        <div class="<?= $cssPrefix?>__container limiter">
            <div class="<?= $cssPrefix?>__title"><?= $arResult['NAME']?></div>
            <div class="<?= $cssPrefix?>__content">
    <?
}
?>
<form class="form__content" action="" method="post" enctype="multipart/form-data">
    <?
    $prevCol = -1;
    $prevRow = -1;

    foreach ($arResult['FIELDS'] as $key => $field)
    {
        if ($prevCol !== $field['COL'])
        {
            if ($prevCol >= 0 && $field['COL'] > 0)
            {
                ?>
                    </div><!-- .form__row -->
                </div><!-- .form__col -->
                <?
            }
            ?>
            <div class="form__col">
                <div class="form__row">
            <?
        }
        elseif ($prevRow !== $field['ROW'])
        {
            if ($prevRow >= 0)
            {
                ?>
                </div><!-- .form__row -->
                <?
            }
            ?>
            <div class="form__row">
            <?
        }
        ?>
        <div class="form__input">
            <label><?= $field['NAME']?></label>
            <?
            $className = isset($field['PARAMETERS']['css_class']) ? ' '.$field['PARAMETERS']['css_class'] : '';

            switch ($field['INPUT_TYPE'])
            {
                case 'file':
                case 'tel':
                case 'email':
                case 'text':
                case 'number':
                    if ($field['MULTIPLE'] === 'Y')
                    {
                        ?>
                        <div class="form__input-container" id="<?= $field['INPUT_ID']?>">
                            <?
                            foreach ($field['VALUE'] as $key => $value)
                            {
                                ?>
                                <input
                                        type="<?= $field['INPUT_TYPE']?>"
                                        value="<?= $value?>"
                                        name="<?= $field['CODE']?>[]"
                                        <?= !empty($field['PARAMETERS']['minlength']) ? 'minlength="'.$field['PARAMETERS']['minlength'].'"' : ''?>
                                        <?= !empty($field['PARAMETERS']['maxlength']) ? 'maxlength="'.$field['PARAMETERS']['maxlength'].'"' : ''?>
                                        <?= !empty($field['PARAMETERS']['size']) ? 'size="'.$field['PARAMETERS']['size'].'"' : ''?>
                                        class="input input_big<?= $className?>">
                                <?
                            }
                            ?>
                        </div>
                        <a href="javascript:void(0)" class="button form__button-more"><?= Loc::getMessage("ARTIXGROUP_FORM_MORE") ?></a>
                        <?
                    }
                    else
                    {
                        ?>
                        <input
                                id="<?= $field['INPUT_ID']?>"
                                type="<?= $field['INPUT_TYPE']?>"
                                value="<?= $field['VALUE']?>"
                                name="<?= $field['CODE']?>"
                                <?= !empty($field['PARAMETERS']['minlength']) ? 'minlength="'.$field['PARAMETERS']['minlength'].'"' : ''?>
                                <?= !empty($field['PARAMETERS']['maxlength']) ? 'maxlength="'.$field['PARAMETERS']['maxlength'].'"' : ''?>
                                <?= !empty($field['PARAMETERS']['size']) ? 'size="'.$field['PARAMETERS']['size'].'"' : ''?>
                                class="input input_big<?= $className?>">
                        <?
                    }
                    break;

                case 'textarea':
                    if ($field['MULTIPLE'] === 'Y')
                    {
                        ?>
                        <div class="form__input-container">
                            <?
                            foreach ($field['VALUE'] as $key => $value)
                            {
                                ?>
                                <textarea
                                    id="<?= $field['INPUT_ID']?>_<?= $key?>"
                                    name="<?= $field['CODE']?>[]"
                                    <?= !empty($field['PARAMETERS']['minlength']) ? 'minlength="'.$field['PARAMETERS']['minlength'].'"' : ''?>
                                    <?= !empty($field['PARAMETERS']['maxlength']) ? 'maxlength="'.$field['PARAMETERS']['maxlength'].'"' : ''?>
                                    <?= !empty($field['PARAMETERS']['rows']) ? 'rows="'.$field['PARAMETERS']['rows'].'"' : ''?>
                                    <?= !empty($field['PARAMETERS']['cols']) ? 'cols="'.$field['PARAMETERS']['cols'].'"' : ''?>
                                    class="textarea textarea_big<?= $className?>"><?= $value?></textarea>
                                <?
                            }
                            ?>
                        </div>
                        <a href="javascript:void(0)" class="button form__button-more"><?= Loc::getMessage("ARTIXGROUP_FORM_MORE") ?></a>
                        <?
                    }
                    else
                    {
                        ?>
                        <textarea
                            id="<?= $field['INPUT_ID']?>"
                            name="<?= $field['CODE']?>"
                            <?= !empty($field['PARAMETERS']['minlength']) ? 'minlength="'.$field['PARAMETERS']['minlength'].'"' : ''?>
                            <?= !empty($field['PARAMETERS']['maxlength']) ? 'maxlength="'.$field['PARAMETERS']['maxlength'].'"' : ''?>
                            <?= !empty($field['PARAMETERS']['rows']) ? 'rows="'.$field['PARAMETERS']['rows'].'"' : ''?>
                            <?= !empty($field['PARAMETERS']['cols']) ? 'cols="'.$field['PARAMETERS']['cols'].'"' : ''?>
                            class="textarea textarea_big<?= $className?>"><?= $field['VALUE']?></textarea>
                        <?
                    }
                    break;

                case 'select':
                    ?>
                    <select id="<?= $field['INPUT_ID']?>" name="<?= $field['CODE']?>" class="select<?= $className?>"<?= ($field['MULTIPLE'] === 'Y') ? ' multiple' : ''?>>
                        <?
                        foreach ($field['VALUES'] as $value)
                        {
                            $selected = (!empty($field['VALUE']) && $value['KEY'] === $field['VALUE']) || $value['DEF'] === 'Y';
                            ?>
                            <option value="<?= $value['KEY']?>"<?= $selected ? ' selected' : ''?>>
                                <?= $value['VALUE']?>
                            </option>
                            <?
                        }
                        ?>
                    </select>
                    <?
                    break;

                case 'radio':
                case 'checkbox':
                    ?>
                    <div class="form__input-container" id="<?= $field['INPUT_ID']?>">
                        <?
                        foreach ($field['VALUES'] as $key => $value)
                        {
                            $checked = (!empty($field['VALUE']) && $value['KEY'] === $field['VALUE']) || $value['DEF'] === 'Y';
                            ?>
                            <label for="<?= $field['INPUT_ID']?>_<?= $key?>">
                                <input
                                        id="<?= $field['INPUT_ID']?>_<?= $key?>"
                                        name="<?= $field['CODE']?>"
                                        class="<?= $field['INPUT_TYPE']?><?= $className?>"
                                        type="<?= $field['INPUT_TYPE']?>"
                                        value="<?= $value['KEY']?>"<?= $checked ? ' checked' : ''?>>
                                <?= $value['VALUE']?>
                            </label>
                            <?
                        }
                        ?>
                    </div>
                    <?
                    break;

                case 'html':
                    echo $field['HTML'];
                    break;
            }
            ?>
            <span class="form__hint"><?= $field['HINT']?></span>
        </div><!-- .form__input -->
        <?
        $prevCol = $field['COL'];
        $prevRow = $field['ROW'];
    }
    ?>
        </div><!-- .form__row -->
        <?
        if ($arResult['CAPTCHA'] !== FormTable::CAPTCHA_NONE)
        {
            switch ($arResult['CAPTCHA'])
            {
                case FormTable::CAPTCHA_BITRIX:
                    ?>
                    <div class="form__row">
                        <div class="form__input">
                            <input name="CAPTCHA_CODE" value="<?= $arResult['CAPTCHA_CODE'];?>" type="hidden">
                            <input id="<?= $itemIds['CAPTCHA']?>" name="captcha_word" type="text">
                        </div>
                        <div class="form__input">
                            <img src="/bitrix/tools/captcha.php?captcha_code=<?= $arResult['CAPTCHA_CODE']?>">
                        </div>
                    </div>
                    <?
                    break;

                case FormTable::CAPTCHA_GOOGLE_V2:
                    ?>
                    <div class="form__row">
                        <div class="form__input">
                            <div class="g-recaptcha" data-sitekey="<?= $arResult['CAPTCHA_CODE']?>"></div>
                        </div>
                    </div>
                    <?
                    break;

                case FormTable::CAPTCHA_GOOGLE_V3:
                    ?>
                    <input type="hidden" name="g-recaptcha-response" id="<?= $itemIds['CAPTCHA']?>">
                    <script>
                        grecaptcha.ready(function () {
                            grecaptcha.execute('<?= $arResult['CAPTCHA_CODE']?>', { action: 'submit' }).then(function (token) {
                                var recaptchaResponse = document.getElementById('<?= $arResult['ID']?>');
                                recaptchaResponse.value = token;
                            });
                        });
                    </script>
                    <?
                    break;
            }
        }
        ?>
        <div class="form__row">
            <??>
            <div id="<?= $itemIds['RESULT_MESSAGE']?>" class="message"></div>
            <input id="<?= $itemIds['SEND']?>" type="submit" class="<?= $arResult['PARAMETERS']['BUTTON_CSS'] ?>" value="<?= $arResult['PARAMETERS']['BUTTON_TEXT'] ?>">
        </div>
        <?
        if ($arResult['CHECK_AGREEMENT'] === 'Y')
        {
            ?>
            <div class="form__row form__row_sb">
                <label class="checkbox">
                    <input id="<?= $itemIds['CHECK_AGREEMENT']?>" name="check_agreement" type="checkbox" class="checkbox__input" value="Y">
                    <span class="checkbox__new-input"></span>
                    <?= $arResult['MESSAGE_AGREEMENT']?>
                </label>
            </div>
            <?
        }
        ?>
    </div><!-- .form__col -->
</form>
<?
if ($arResult['SHOW_MODE'] === FormTable::SHOW_MODE_POPUP)
{
    ?>
                        </div><!-- .authorization__form -->
                    </div><!-- .authorization -->
                </div><!-- .dialog__content -->
                <div class="dialog__close">
                    <button class="action close">
                        <svg class="icon icon-close ">
                            <use xlink:href="#icon-close"></use>
                        </svg>
                    </button>
                </div>
            </div><!-- .dialog__container -->
            <div class="dialog__event"></div>
        </div><!-- .dialog__noscroll -->
    </div><!-- .dialog -->
    <?
}
else
{
    ?>
            </div><!-- .form__content -->
        </div><!-- .form__container -->
    </div><!-- .form -->
    <?
}
?>
<script>
    new ArtixgroupForm({
        actionUrl: '<?= $componentPath.'/ajax.php'?>',
        formId: <?= $arResult['ID']?>,
        sendButtonId: '<?= $itemIds['SEND']?>',
        messageId: '<?= $itemIds['RESULT_MESSAGE']?>',
        showMode: '<?= $arResult['SHOW_MODE']?>',
        <?
        if ($arResult['METRICS'])
        {
            ?>
            onsuccess: function () {
                <?= $arResult['JS_EVENT_HANDLERS']?>
            },
            <?
        }
        ?>
        fields: [
            <?
            foreach ($arResult['FIELDS'] as $key => $field)
            {
                $jsParams = [
                    'required' => $field['REQUIRED'] === 'Y',
                    'multiple' => $field['INPUT_TYPE'] === 'radio' || $field['MULTIPLE'] === 'Y',
                    'mask' => $field['JS_MASK'] !== null ? $field['JS_MASK'] : null
                ];
                ?>
                new ArtixgroupFormField('<?= $field['INPUT_ID']?>', <?= json_encode($jsParams)?>),<?= "\n"?>
                <?
            }
            if ($arResult['CAPTCHA'] !== FormTable::CAPTCHA_NONE)
            {
                $jsParams = [
                    'required' => true,
                ];
                ?>
                new ArtixgroupFormField('<?= $itemIds['CAPTCHA']?>', <?= json_encode($jsParams)?>),<?= "\n"?>
                <?
            }
            if ($arResult['CHECK_AGREEMENT'] === 'Y')
            {
                $jsParams = [
                    'required' => true,
                    'multiple' => true
                ];
                ?>
                new ArtixgroupFormField('<?= $itemIds['CHECK_AGREEMENT']?>', <?= json_encode($jsParams)?>)
                <?
            }
            ?>
        ]
    });
</script>