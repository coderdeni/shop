<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS" => array(
        "FORM_ID" => Array(
            "PARENT" => "BASE",
            "NAME"=> GetMessage("ARTIXGROUP_SHOP_FORM_ID"),
            "TYPE"=>"STRING",
            "DEFAULT"=> "",
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "Y",
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
    ),
);
if ((int)$arCurrentValues["FORM_ID"] > 0 && \Bitrix\Main\Loader::includeModule('artixgroup.shop'))
{
    $fields = \Artixgroup\Shop\FormFieldTable::getList([
        'filter' => ['FORM_ID' => (int)$arCurrentValues["FORM_ID"], 'EXTERNAL' => 'Y']
    ])->fetchAll();

    foreach ($fields as $field)
    {
        $arComponentParameters['PARAMETERS']["FIELD_".$field['ID']] = Array(
            "PARENT" => "BASE",
            "NAME"=> Loc::getMessage("ARTIXGROUP_FIELD_EXTERNAL", ['#FIELD_NAME#' => $field['NAME']]),
            "TYPE"=>"STRING",
            "DEFAULT"=> "",
            "MULTIPLE"=>"N",
            "ADDITIONAL_VALUES"=>"Y",
            "REFRESH" => "N",
        );
    }
}
