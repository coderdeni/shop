<?php
$MESS["ARTXIGROUP_FORM_NOTFOUND"] = "Форма не найдена";
$MESS["ARTIXGROUP_FORM_ERROR_BITRIX_CAPTCHA"] = "Не верно введен код с картинки";
$MESS["ARTIXGROUP_FORM_ERROR_AGREEMENT"] = "Необходимо согласится с пользовательским сооглашением";