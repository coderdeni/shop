<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Artixgroup\Shop\Form\FieldBase;
use Artixgroup\Shop\Form\IblockElementField;
use Artixgroup\Shop\Form\SelectField;
use Artixgroup\Shop\Form\WidgetField;
use Artixgroup\Shop\FormFieldTable;
use Artixgroup\Shop\FormResultFieldTable;
use Artixgroup\Shop\FormResultTable;
use Artixgroup\Shop\FormTable;
use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;

class ArtixgroupForm extends \CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $arFilter = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();

    /** @var array $data */
    protected $data = [];

    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param $params
     * @return array
     * @internal param array $arParams
     */
    public function onPrepareComponentParams($params)
    {
        $params['FORM_ID'] = (int)$params['FORM_ID'];
        return $params;
    }

    /**
     * определяет читать данные из кеша или нет
     * @return bool
     */
    protected function readDataFromCache()
    {
        global $USER;

        if ($this->arParams['CACHE_TYPE'] === 'N')
            return false;

        if (is_array($this->cacheAddon))
            $this->cacheAddon[] = $USER->GetUserGroupArray();
        else
            $this->cacheAddon = [$USER->GetUserGroupArray()];

        return !($this->startResultCache(false, $this->cacheAddon, md5(serialize($this->arParams))));
    }

    /**
     * кеширует ключи массива arResult
     */
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
        {
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    /**
     * завершает кеширование
     * @return bool
     */
    protected function endCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
        {
            return false;
        }

        $this->endResultCache();
        return true;
    }

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Main\Loader::includeModule('artixgroup.shop'))
        {
            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
        }
    }

    /**
     * проверяет заполнение обязательных параметров
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        if ($this->arParams['FORM_ID'] < 1)
            throw new Main\ArgumentNullException('FORM_ID');
    }

    protected function getData()
    {
        $iterator = FormTable::getList([
            'filter' => [
                'ACTIVE' => 'Y',
                'ID' => $this->arParams['FORM_ID'],
                'FORM_SITES.SITE_ID' => SITE_ID
            ]
        ]);

        if (!$this->data['FORM'] = $iterator->fetch())
            throw new Main\ObjectNotFoundException(Loc::getMessage("ARTXIGROUP_FORM_NOTFOUND"));

        $fields = \Artixgroup\Shop\FormFieldTable::getList([
            'filter' => ['FORM_ID' => $this->arParams['FORM_ID']]
        ])->fetchAll();

        foreach ($fields as $key => $field)
        {
            $fieldClassName = FormFieldTable::getFieldByType($field['TYPE']);

            if ($fieldClassName && class_exists($fieldClassName))
            {
                /** @var FieldBase $fieldBase */
                $fieldBase = new $fieldClassName($field, $key);
                $this->data['FIELDS'][] = $fieldBase;
            }
        }
    }

    protected function checkRecaptcha($privateKey)
    {
        if ($recaptcha = $this->request->getPost('g-recaptcha-response'))
        {
            $privateKey = Main\Config\Option::get('artixgroup.shop', 'recaptcha_v2_key');
            $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$privateKey."&response=".$recaptcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

            return $response['success'];
        }

        return false;
    }

    protected function save()
    {
        global $APPLICATION;

        $data = [];
        $errors = [];
        $isSuccess = false;

        switch ($this->data['FORM']['CAPTCHA'])
        {
            case FormTable::CAPTCHA_BITRIX:
                if (!$APPLICATION->CaptchaCheckCode($this->request->getPost('CAPTCHA_WORD'), $this->request->getPost('CAPTCHA_CODE')))
                    $errors[] = Loc::getMessage("ARTIXGROUP_FORM_ERROR_BITRIX_CAPTCHA");
                break;

            case FormTable::CAPTCHA_GOOGLE_V2:
                $secretKey = Main\Config\Option::get('artixgroup.shop', 'recaptcha_v2_secret_key');
                if (!$this->checkRecaptcha($secretKey))
                    $errors[] = Loc::getMessage("ARTIXGROUP_FORM_ERROR_BITRIX_CAPTCHA");
                break;

            case FormTable::CAPTCHA_GOOGLE_V3:
                $secretKey = Main\Config\Option::get('artixgroup.shop', 'recaptcha_v3_secret_key');
                if (!$this->checkRecaptcha($secretKey))
                    $errors[] = Loc::getMessage("ARTIXGROUP_FORM_ERROR_BITRIX_CAPTCHA");
                break;
        }

        if ($this->data['FORM']['CHECK_AGREEMENT'] === 'Y')
        {
            if ($this->request['check_agreement'][0] !== 'Y')
                $errors[] = Loc::getMessage("ARTIXGROUP_FORM_ERROR_AGREEMENT");
        }

        foreach ($this->data['FIELDS'] as $field)
        {
            /** @var FieldBase $field */

            if (!$field->isValid())
            {
                $errors[] = $field->getErrorMessage();
            }

            $data[] = [
                'FIELD_ID' => $field->getId(),
                'RAW_VALUE' => $field->getValue(),
                'VALUE' => $field->getPrepareValue(),
            ];
        }

        if (empty($errors))
        {
            global $USER;

            $result = FormResultTable::add([
                'FORM_ID' => $this->arParams['FORM_ID'],
                'USER_ID' => $USER->IsAuthorized() ? $USER->GetID() : '',
                'PAGE_URL' => $this->request->getPost('request_url'),
            ]);

            if ($result->isSuccess())
            {
                foreach ($data as &$d)
                {
                    $d['RESULT_ID'] = $result->getId();
                }

                $fieldResult = FormResultFieldTable::addMulti($data);

                if (!$fieldResult->isSuccess())
                {
                    $errors = $fieldResult->getErrorMessages();
                }
            }
            else
            {
                $errors = $result->getErrorMessages();
            }

            $isSuccess = empty($errors);
        }

        if ($isSuccess && isset($result))
        {
            $this->data['RESULT_ID'] = $result->getId();
            $this->sendEmail();
        }

        return json_encode([
            'success' => $isSuccess,
            'message' => $isSuccess ? $this->data['FORM']['RESULT_MESSAGE'] : implode('<br>', $errors),
        ]);
    }

    protected function sendEmail()
    {
        /** @var FieldBase $field */

        $serverName = Main\Config\Option::get('main', 'server_name', $_SERVER['SERVER_NAME']);
        $res = Main\SiteTable::getById(SITE_ID);

        if (($siteFields = $res->fetch()))
        {
            if ($siteFields['SERVER_NAME'] <> '')
                $serverName = $siteFields['SERVER_NAME'];
        }

        $protocol = (Main\Context::getCurrent()->getRequest()->isHttps() ? "https" : "http");
        $uri = new Main\Web\Uri($protocol."://".$serverName.'/bitrix/admin/artixgroup_form_result_view.php');
        $uri->addParams('lang', $siteFields['LANGUAGE_ID'] <> '' ? $siteFields['LANGUAGE_ID'] : LANGUAGE_ID)
            ->addParams('ID', $this->data['RESULT_ID']);

        if (!empty($this->data['FORM']["EVENT_MESSAGE_ID"]))
        {
            $fields = [
                'REQUEST_SITE_URL' => $this->request->getPost('request_url'),
                'ADMIN_URL' => $uri->getUri(),
            ];

            $eventMessage = Main\Mail\Internal\EventMessageTable::getById($this->data['FORM']["EVENT_MESSAGE_ID"])->fetch();

            foreach ($this->data['FIELDS'] as $field)
            {
                $fields[$field->getCode()] = $eventMessage['BODY_TYPE'] === 'text'
                    ? $field->getValue()
                    : $field->getHtmlValue();
            }

            Main\Mail\Event::send([
                "EVENT_NAME" => $this->data['FORM']["EVENT_NAME"],
                "MESSAGE_ID" => $this->data['FORM']["EVENT_MESSAGE_ID"],
                "LID" => SITE_ID,
                "C_FIELDS" => $fields
            ]);
        }
        elseif ($this->data['FORM']["EVENT_NAME"])
        {
            $eventMessages = Main\Mail\Internal\EventMessageTable::getList([
                'filter' => [
                    'EVENT_NAME' => $this->data['FORM']["EVENT_NAME"],
                    'LID' => SITE_ID
                ]
            ]);

            while ($eventMessage = $eventMessages->fetch())
            {
                $fields = [
                    'REQUEST_SITE_URL' => $this->request->getPost('request_url'),
                    'ADMIN_URL' => $uri->getUri(),
                ];

                foreach ($this->data['FIELDS'] as $field)
                {
                    $fields[$field->getCode()] = $eventMessage['BODY_TYPE'] === 'text'
                        ? $field->getTextValue()
                        : $field->getHtmlValue();
                }

                Main\Mail\Event::send([
                    "EVENT_NAME" => $this->data['FORM']["EVENT_NAME"],
                    "MESSAGE_ID" => $eventMessage['ID'],
                    "LID" => SITE_ID,
                    "C_FIELDS" => $fields,
                ]);
            }
        }
    }

    /**
     * получение результатов
     */
    protected function getResult()
    {
        $this->arResult = $this->data['FORM'];

        foreach ($this->data['FIELDS'] as $field)
        {
            /** @var FieldBase $field */

            $formField = $field->toArray();

            $formField['CODE'] = $field->getCode();
            $formField['INPUT_TYPE'] = $field->getHtmlType();
            $formField['INPUT_ID'] = $field->getInputId();
            $formField['JS_MASK'] = $field->getJsValidator();
            $formField['VALUE'] = $field->getValue();

            if ($field instanceof SelectField || $field instanceof IblockElementField)
                $formField['VALUES'] = $field->getValues();
            elseif ($field instanceof WidgetField)
                $formField['HTML'] = $field->getHtml();

            $this->arResult['FIELDS'][] = $formField;
        }

        switch ($this->data['FORM']['CAPTCHA'])
        {
            case FormTable::CAPTCHA_BITRIX:
                $cpt = new CCaptcha();
                $captchaPass = Main\Config\Option::get("main", "captcha_password", "");
                if (strlen($captchaPass) <= 0)
                {
                    $captchaPass = randString(10);
                    Main\Config\Option::set("main", "captcha_password", $captchaPass);
                }
                $cpt->SetCodeCrypt($captchaPass);
                $this->arResult['CAPTCHA_CODE'] = htmlspecialchars($cpt->GetCodeCrypt());
                break;

            case FormTable::CAPTCHA_GOOGLE_V2:
                $this->arResult['CAPTCHA_CODE'] = Main\Config\Option::get('artixgroup.shop', 'recaptcha_v2_public_key');
                break;

            case FormTable::CAPTCHA_GOOGLE_V3:
                $this->arResult['CAPTCHA_CODE'] = Main\Config\Option::get('artixgroup.shop', 'recaptcha_v3_public_key');
                break;
        }
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->checkParams();
            $this->getData();

            if ($this->request->getPost('save'))
            {
                global $APPLICATION;
                $APPLICATION->RestartBuffer();
                echo $this->save();
                die();
            }

            $this->getResult();
            $this->includeComponentTemplate();
        }
        catch (Main\ObjectNotFoundException $e)
        {
        }
        catch (Exception $e)
        {
            ShowError($e->getMessage());
        }
    }
}
