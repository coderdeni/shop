<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arCurrentValues */

\Bitrix\Main\Loader::includeModule('sale');

$locationTypes = [];
$res = \Bitrix\Sale\Location\TypeTable::getList([
    'select' => ['ID', 'NAME_'.LANGUAGE_ID => 'NAME.NAME'],
    'filter' => ['=NAME.LANGUAGE_ID' => LANGUAGE_ID]
]);
while ($item = $res->fetch())
{
    $locationTypes[$item['ID']] = $item['NAME_'.LANGUAGE_ID];
}

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS" => array(
        "COOKIE_EXPIRES" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTIXGROUP_GEOIP_COOKIE_EXPIRES"),
            "TYPE" => "TEXT",
            "DEFAULT" => "604800"
        ),
        "LOCATION_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTIXGROUP_GEOIP_LOCATION_TYPE"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $locationTypes,
            "MULTIPLE" => "Y",
            "REFRESH" => "N",
        ),
        "CITY_LIST" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTIXGROUP_GEOIP_CITY_LIST"),
            "MULTIPLE" => "Y",
            "TYPE" => "TEXT",
        ),
        "SEARCH_LIMIT" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTIXGROUP_GEOIP_SEARCH_LIMIT"),
            "TYPE" => "TEXT",
            "DEFAULT" => "10"
        ),
        "MIN_LENGTH" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTIXGROUP_GEOIP_MIN_LENGTH"),
            "TYPE" => "TEXT",
            "DEFAULT" => "2"
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
    ),
);
