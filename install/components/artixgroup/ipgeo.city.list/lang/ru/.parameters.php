<?php
$MESS["ARTIXGROUP_GEOIP_COOKIE_EXPIRES"] = "Времея жизни cookie";
$MESS["ARTIXGROUP_GEOIP_LOCATION_TYPE"] = "Типы местоположения";
$MESS["ARTIXGROUP_GEOIP_SEARCH_LIMIT"] = "Кол-во элементов в результате поиска";
$MESS["ARTIXGROUP_GEOIP_MIN_LENGTH"] = "Минимальная длина запроса для поиска";
$MESS["ARTIXGROUP_GEOIP_CITY_LIST"] = "Список популярных городов";
