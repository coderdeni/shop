<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Artixgroup\Shop\Ipgeo\Location;
use \Bitrix\Main;
use Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;
use Bitrix\Sale\Location\LocationTable;

class ArtixgroupIpGeoCityListComponent extends CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $filter = [];

    protected $limit = null;

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $cacheSections = array();


    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param $params
     * @return array
     * @internal param array $arParams
     */
    public function onPrepareComponentParams($params)
    {
        $params['COOKIE_EXPIRES'] = isset($params['COOKIE_EXPIRES']) ? (int)$params['COOKIE_EXPIRES'] : 3600 * 24 * 7;
        $params['CITY_LIST'] = empty($params['CITY_LIST']) ? [] : array_unique($params['CITY_LIST']);
        $params['LOCATION_TYPE'] = empty($params['LOCATION_TYPE']) ? [] : array_unique($params['LOCATION_TYPE']);
        $params['SEARCH_LIMIT'] = empty($params['SEARCH_LIMIT']) ? 10 : (int)$params['SEARCH_LIMIT'];
        $params['MIN_LENGTH'] = empty($params['MIN_LENGTH']) ? 2 : (int)$params['MIN_LENGTH'];

        return $params;
    }

    /**
     * определяет читать данные из кеша или нет
     * @return bool
     */
    protected function readDataFromCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
            return false;

        return !($this->StartResultCache(false, $this->cacheAddon));
    }

    /**
     * кеширует ключи массива arResult
     */
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
        {
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
//        if (!Loader::includeModule('sale'))
//            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
    }

    /**
     * выполяет действия перед кешированием
     */
    protected function executeProlog()
    {
        if ($this->request->get('change_city') > 0)
        {
            $locationId = $this->request->get('change_city');
            Location::getInstance()->setLocation($locationId);
            $uriString = $this->request->getRequestUri();
            $uri = new Uri($uriString);
            $uri->deleteParams(["change_city"]);

            LocalRedirect($uri->getUri());
        }

        $this->arResult['CURRENT_LOCATION'] = Location::getInstance()->getLocation();

        if ($this->request->isAjaxRequest()
            && $this->request->getPost('sessid') === bitrix_sessid()
            && strlen($this->request->getPost('city')) >= $this->arParams['MIN_LENGTH'])
        {
            $this->filter = [
                '%NAME.NAME' => $this->request->getPost('city'),
                '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
                '=PARENT.NAME.LANGUAGE_ID' => LANGUAGE_ID
            ];

            $this->limit = $this->arParams['SEARCH_LIMIT'];
        }
        elseif ($this->arParams['CITY_LIST'])
        {
            $this->filter = [
                '=NAME.NAME' => $this->arParams['CITY_LIST'],
                '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
                '=PARENT.NAME.LANGUAGE_ID' => LANGUAGE_ID
            ];
        }

        if ($this->arParams['LOCATION_TYPE'])
        {
            $this->filter['=TYPE_ID'] = $this->arParams['LOCATION_TYPE'];
        }

        $this->cacheAddon['filter'] = md5(serialize($this->filter));
    }

    /**
     * получение результатов
     */
    protected function getResult()
    {
        if (Loader::includeModule('sale'))
        {
            $this->arResult['CITIES'] = LocationTable::getList([
                'filter' => $this->filter,
                'select' => ['ID', 'LOCATION_NAME' => 'NAME.NAME', 'PARENT_LOCATION_NAME' => 'PARENT.NAME.NAME'],
                'limit' => $this->limit
            ])->fetchAll();
        }
        else
        {
            foreach ($this->arParams['CITY_LIST'] as $city)
            {
                $this->arResult['CITIES'][] = [
                    'ID' => $city,
                    'LOCATION_NAME' => $city
                ];
            }
        }
    }

    /**
     * выполняет действия после выполения компонента, например установка заголовков из кеша
     */
    protected function executeEpilog()
    {
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->executeProlog();

            if (!$this->readDataFromCache())
            {
                $this->getResult();
                $this->putDataToCache();
            }

            if ($this->request->isAjaxRequest()
                && $this->request->getPost('sessid') === bitrix_sessid()
                && strlen($this->request->getPost('city')) >= $this->arParams['MIN_LENGTH'])
            {
                global $APPLICATION;
                $APPLICATION->RestartBuffer();
                $this->includeComponentTemplate('ajax');
                Application::getInstance()->end();
            }
            else
                $this->includeComponentTemplate();

            $this->executeEpilog();
        }
        catch (Exception $e)
        {
            $this->abortDataCache();
            ShowError($e->getMessage());
        }
    }
}
