'use strict';
(function (window){
    window.IpGeoCityList = function (params)
    {
        this.siteId = !!params.siteId ? params.siteId : 's1';
        this.windowId = !!params.windowId ? params.windowId : 'locationDialog';
        this.suggestId = !!params.suggestId ? params.suggestId : 'locationSuggest';
        this.inputSearchId = !!params.inputSearchId ? params.inputSearchId : 'locationSearch';
        this.inputValueId = !!params.inputValueId ? params.inputValueId : 'locationCityId';
        this.ajaxPath = !!params.ajaxPath ? params.ajaxPath : '/';
        this.minLength = !!params.minLength ? params.minLength : 2;
        this.limit = !!params.limit ? params.limit : 40;

        BX.ready(BX.delegate(this.init, this));
    };

    window.IpGeoCityList.prototype = {

        init: function ()
        {
            this.popupWindow = BX(this.windowId);
            this.suggest = BX(this.suggestId);
            this.inputSearch = BX(this.inputSearchId);
            this.inputValue = BX(this.inputValueId);

            if (this.popupWindow)
            {
                if (this.inputSearch)
                {
                    BX.bind(this.inputSearch, 'keyup', this.closure('onKeyUp', {}));
                    BX.bind(this.inputSearch, 'blur', this.closure('onBlur'));
                }
            }
        },

        closure: function (fname, data)
        {
            var obj = this;
            return data
                ? function(){obj[fname](data)}
                : function(arg1){obj[fname](arg1)};
        },

        setCity: function ()
        {
            this.inputSearch.value = BX.proxy_context.innerText;
            this.inputValue.value = BX.proxy_context.getAttribute('data-city-id');
        },

        onBlur: function ()
        {
            setTimeout(BX.delegate(function() {
                BX.style(this.suggest, 'display', 'none');
            }, this), 250);
        },

        onKeyUp: function (data)
        {
            if (this.inputSearch.value.length >= this.minLength)
            {
                BX.style(this.suggest, 'display', 'block');
                data.sessid = BX.bitrix_sessid();
                data.siteId = this.siteId;
                data.city = this.inputSearch.value;
                data.limit = this.limit;
                BX.ajax({
                    url: this.ajaxPath,
                    method: 'POST',
                    dataType: 'html',
                    data: data,
                    onsuccess: BX.delegate(function(data){
                        this.suggest.innerHTML = data;
                    }, this)
                });
            }
        },
    };
})(window);
