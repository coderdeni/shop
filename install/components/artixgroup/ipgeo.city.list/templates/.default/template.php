<?php

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
?>
<div id="locationDialog" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__container">
        <div class="dialog__title"><?= Loc::getMessage('ARTIXGROUP_CHOOSE_REGION')?></div>
        <div class="dialog__content">
            <div class="locationSelect">
                <div class="locationSelect__infoText"><?= Loc::getMessage('ARTIXGROUP_INFO_TEXT')?></div>
                <div class="locationSelect__city">
                    <svg class="icon icon-checked ">
                        <use xlink:href="#icon-checked"></use>
                    </svg>
                    <?= $arResult['CURRENT_LOCATION']['LOCATION_NAME']?>
                </div>
                <div class="locationSelect__form">
                    <?
                    if (\Bitrix\Main\Loader::includeModule('sale'))
                    {
                        ?>
                        <div class="headerSearch headerSearch_location">
                            <div action="" class="form">
                                <input id="locationSearch" type="text" class="input input_headerSearch" name="city_name" placeholder="<?= Loc::getMessage('ARTIXGROUP_SEARCH')?>">
                                <input id="locationCityId" type="hidden" class="input input_headerSearch" name="change_city">
                                <input type="submit" class="searchButton" value="">
                            </div>
                            <div id="locationSuggest" class="suggest"></div>
                        </div>
                        <?
                    }
                    ?>
                    <div class="locationSelect__regions">
                        <div class="locationSelect__subTitle"><?= Loc::getMessage('ARTIXGROUP_POPULAR')?></div>
                        <div class="locationSelect__regionWrap">
						<?
						$countInColumn = ceil(count($arResult['CITIES']) / 7);
						$count = 0;
						foreach ($arResult['CITIES'] as $city)
						{
							if ($count === 0)
							{
								?><ul class="locationSelect__region"><?
							}
							?>
							<li class="locationSelect__regionItem">
								<a 
									class="locationSelect__regionItemText<?= $city['ID'] === $arResult['CURRENT_LOCATION']['ID'] ? ' locationSelect__regionItemText_active' : ''?>"
									href="?change_city=<?= $city['ID']?>" rel="nofollow"><?=$city['LOCATION_NAME']?></a>
							</li>
							<?
							$count++;
							if ($count == $countInColumn)
							{
								$count = 0;
								?>
								</ul>
								<?
							}
						}
					
						if ($count != 0)
						{
							$count = 0;
							?>
							</ul>
							<?
						}
						?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog__close">
            <button class="action close" data-dialog-close="">
                <svg class="icon icon-close ">
                    <use xlink:href="#icon-close"></use>
                </svg>
            </button>
        </div>
    </div>
</div>

<script>
	new IpGeoCityList({
        "siteId": '<?=SITE_ID?>',
        "windowId": 'locationDialog',
        "suggestId": 'locationSuggest',
        "inputSearchId": 'locationSearch',
        "inputValueId": 'locationCityId',
        "ajaxPath": '/',
        "minLength": <?= $arParams['MIN_LENGTH']?>,
        "limit": <?= $arParams['SEARCH_LIMIT']?>
    });
</script>