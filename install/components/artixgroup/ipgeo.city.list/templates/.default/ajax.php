<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
?>
<ul>
<?
foreach ($arResult['CITIES'] as $city)
{
	?>
	<li class="locationSelect__regionItem">
		<a class="locaitonSelect__regionItemText" href="?change_city=<?=$city['ID']?>" rel="nofollow">
            <?= $city['LOCATION_NAME']?>, <?= $city['PARENT_LOCATION_NAME']?>
        </a>
	</li>
	<?
}
?>
</ul>