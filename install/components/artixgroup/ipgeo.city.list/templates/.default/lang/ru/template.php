<?php
$MESS['ARTIXGROUP_CHOOSE_REGION'] = 'Выбор региона';
$MESS['ARTIXGROUP_INFO_TEXT'] = 'От выбранного региона зависят доступные способы доставки, их стоимость и наличие товаров';
$MESS['ARTIXGROUP_SEARCH'] = 'Поиск';
$MESS['ARTIXGROUP_POPULAR'] = 'Популярные регионы';