<?php
$MESS ['AG_IBLOCK_TYPE'] = "Тип информационного блока";
$MESS ['AG_IBLOCK_ID'] = "Код информационного блока";
$MESS ['AG_ELEMENT_ID'] = "ID елемента";
$MESS ['AG_ACTIVE_ELEMENT'] = "Показвать только активные елементы";
$MESS ['AG_FORM_ID'] = "ID формы";
$MESS ['AG_LINK_PROPERTY'] = "Код свойства привязки к товару";
$MESS ['AG_PHOTOS_PROPERTY'] = "Код свойства фотографий пользователя";
$MESS ['AG_THUMB_WIDTH'] = "Ширина превью фотографии в px";
$MESS ['AG_THUMB_HEIGHT'] = "Высота превью фотографии в px";
