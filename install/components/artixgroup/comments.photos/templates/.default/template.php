<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

if (!empty($arResult['PHOTOS']))
{
    ?>
    <div class="photo-users">
        <div class="photo-users__title">Фотографии пользователей</div>
        <div class="photo-slider__users swiper-container swiper-container-users">
            <div class="photo-slider__users-content swiper-wrapper">
                <?
                foreach ($arResult['PHOTOS'] as $item)
                {
                    ?>
                    <div class="photo-slider__users-item swiper-slide">
                        <a href="<?= $item['SRC']?>" class="photo-slider__users-img">
                            <img src="<?= $item['THUMB_SRC']?>">
                        </a>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
    </div>
    <?
}


