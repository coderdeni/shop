new Swiper('.photo-slider__users', {
    spaceBetween: 10,
    slidesPerView: 'auto',
    freeMode: false,
    observer: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});