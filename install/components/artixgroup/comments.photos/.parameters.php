<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("all"=>" "));

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="all"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
{
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arElementIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["ELEMENT_IBLOCK_TYPE"]!="all"?$arCurrentValues["ELEMENT_IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
{
    $arElementIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arFields = array(
    'NAME' => 'Название',
    'PREVIEW_TEXT' => 'Описание для анонса',
    'DETAIL_TEXT' => 'Детальное описание',
);
$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("ACTIVE"=>"Y", "IBLOCK_ID"=>(isset($arCurrentValues["IBLOCK_ID"]) ? $arCurrentValues["IBLOCK_ID"] : $arCurrentValues["ID"])));
while ($arr=$rsProp->Fetch())
{
	$arFields['PROP_' . $arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
}

$arEventType = array();
$rsET = CEventType::GetList(array("LID" => SITE_ID));
while ($arET = $rsET->Fetch())
{
    $arEventType[$arET["EVENT_NAME"]] = "[".$arET["ID"]."] ".$arET["EVENT_NAME"];
}

$properties = [];
if ($arCurrentValues["IBLOCK_ID"])
{
	$propertyIterator = \Bitrix\Iblock\PropertyTable::getList([
		'filter' => [
			'IBLOCK_ID' => $arCurrentValues["IBLOCK_ID"]
		]
	]);
	while ($property = $propertyIterator->fetch())
	{
		$properties[$property['CODE']] = "[{$property['CODE']}] {$property['NAME']}";
	}
}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME"=>GetMessage("AG_IBLOCK_TYPE"),
			"TYPE"=>"LIST",
			"VALUES"=>$arTypesEx,
			"DEFAULT"=>"catalog",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME"=>GetMessage("AG_IBLOCK_ID"),
			"TYPE"=>"LIST",
			"VALUES"=>$arIBlocks,
			"DEFAULT"=>'1',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH" => "Y",
		),
        "ELEMENT_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("AG_ELEMENT_ID"),
            "TYPE" => "TEXT",
        ),
		"LINK_PROPERTY" => Array(
			"PARENT" => "BASE",
			"NAME"=>GetMessage("AG_LINK_PROPERTY"),
			"TYPE"=>"LIST",
			"VALUES"=>$properties,
			"DEFAULT"=>'PRODUCT_ID',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH" => "Y",
		),
		"PHOTOS_PROPERTY" => Array(
			"PARENT" => "BASE",
			"NAME"=>GetMessage("AG_PHOTOS_PROPERTY"),
			"TYPE"=>"LIST",
			"VALUES"=>$properties,
			"DEFAULT"=>'PHOTOS',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH" => "Y",
		),
		"ONLY_ACTIVE_ELEMENT" => Array(
            "NAME" => GetMessage("AG_ACTIVE_ELEMENT"), 
            "TYPE" => "CHECKBOX",
            "MULTIPLE" => "N",
            "VALUE" => "Y",
            "DEFAULT" =>"N",
            "REFRESH"=> "Y",
		),
		"THUMB_WIDTH" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("AG_THUMB_WIDTH"),
			"TYPE" => "TEXT",
		),
		"THUMB_HEIGHT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("AG_THUMB_HEIGHT"),
			"TYPE" => "TEXT",
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
	),
);
?>
