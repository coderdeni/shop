<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Mail;

class ArtixgroupCommentsPhotos extends CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     * @var array
     */
    protected $navParams = array();

    /**
     * Дополнительный поля фильтрации
     * @var array
     */
    protected $arFilter = array();

    /**
     * Дополнительный поля выборки
     * @var array
     */
    protected $arSelect = array();


    protected $fields = array();
    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param array $params
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $params['IBLOCK_TYPE'] = trim($params['IBLOCK_TYPE']);
        $params['IBLOCK_ID'] = intval($params['IBLOCK_ID']);
        $params['ELEMENT_ID'] = intval($params['ELEMENT_ID']);
        $params['LINK_PROPERTY'] = trim($params['LINK_PROPERTY']);
        $params['PHOTOS_PROPERTY'] = trim($params['PHOTOS_PROPERTY']);
        $params['THUMB_WIDTH'] = intval($params['THUMB_WIDTH']);
        $params['THUMB_HEIGHT'] = intval($params['THUMB_HEIGHT']);
        $params['ACTIVE_ELEMENT'] = trim($params['ACTIVE_ELEMENT']) ? $params['ACTIVE_ELEMENT'] : 'Y';


        return $params;
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     */
    protected function checkModules()
    {
        if (!Main\Loader::includeModule('iblock'))
            throw new Main\LoaderException(Loc::getMessage('STANDARD_ELEMENTS_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED'));
    }

    /**
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        if ($this->arParams['IBLOCK_ID'] <= 0)
            throw new Main\ArgumentNullException('IBLOCK_ID');

        if (empty($this->arParams['ELEMENT_ID']))
            throw new Main\ArgumentNullException('ELEMENT_ID');

        if (empty($this->arParams['LINK_PROPERTY']))
            throw new Main\ArgumentNullException('LINK_PROPERTY');

        if (empty($this->arParams['PHOTOS_PROPERTY']))
            throw new Main\ArgumentNullException('PHOTOS_PROPERTY');
    }

    protected function getElementPropertyValues($field, array $filter) : array
    {
        $values = [];
        $elementPropertyIterator = \Bitrix\Iblock\ElementPropertyTable::getList([
            'select' => [$field],
            'filter' => $filter
        ]);
        while ($elementProperty = $elementPropertyIterator->fetch())
        {
            $values[] = (int)$elementProperty[$field];
        }

        return $values;
    }

    protected function getResult()
    {
        $photosIds = [];
        $linkPropertyId = null;
        $photosPropertyId = null;

        $propertyIterator = \Bitrix\Iblock\PropertyTable::getList([
            'select' => ['ID', 'CODE'],
            'filter' => [
                'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                'CODE' => [
                    $this->arParams['LINK_PROPERTY'],
                    $this->arParams['PHOTOS_PROPERTY']
                ]
            ]
        ]);
        while ($property = $propertyIterator->fetch())
        {
            if ($property['CODE'] === $this->arParams['LINK_PROPERTY'])
                $linkPropertyId = $property['ID'];
            elseif ($property['CODE'] === $this->arParams['PHOTOS_PROPERTY'])
                $photosPropertyId = $property['ID'];
        }

        if (!$linkPropertyId)
            throw new Main\ObjectNotFoundException('PROPERTY '.$this->arParams['LINK_PROPERTY'].' not found');

        if (!$photosPropertyId)
            throw new Main\ObjectNotFoundException('PROPERTY '.$this->arParams['PHOTOS_PROPERTY'].' not found');

        $filter = [
            'IBLOCK_PROPERTY_ID' => $linkPropertyId,
            'VALUE' => $this->arParams['ELEMENT_ID']
        ];

        if ($this->arParams['ONLY_ACTIVE_ELEMENT'] === 'Y')
            $filter['ELEMENT.ACTIVE'] = 'Y';

        $elementsIds = $this->getElementPropertyValues('IBLOCK_ELEMENT_ID', $filter);

        if (count($elementsIds) > 0)
        {
            $photosIds = $this->getElementPropertyValues('VALUE', [
                'IBLOCK_PROPERTY_ID' => $photosPropertyId,
                'IBLOCK_ELEMENT_ID' => $elementsIds
            ]);
        }

        if (count($photosIds) > 0)
        {
            if ($this->arParams['THUMB_WIDTH'] > 0 && $this->arParams['THUMB_HEIGHT'] > 0)
            {
                $thumbSize = [
                    'width' => $this->arParams['THUMB_WIDTH'],
                    'height' => $this->arParams['THUMB_HEIGHT']
                ];
            }

            $uploadDir = Main\Config\Option::get("main", "upload_dir", "upload");
            $fileIterator = Main\FileTable::getList([
                'filter' => ['ID' => $photosIds]
            ]);
            while ($file = $fileIterator->fetch())
            {
                $src = "/" . $uploadDir . "/" . $file["SUBDIR"] . "/" . $file["FILE_NAME"];
                $src = str_replace("//", "/", $src);
                if (defined("BX_IMG_SERVER"))
                    $src = BX_IMG_SERVER . $src;

                $thumbSrc = $src;

                if (isset($thumbSize))
                {
                    $thumb = CFile::ResizeImageGet($file, $thumbSize);
                    $thumbSrc = $thumb['src'];
                }

                $this->arResult['PHOTOS'][] = [
                    'SRC' => $src,
                    'THUMB_SRC' => $thumbSrc
                ];
            }
        }

        unset($elementsIds, $photosIds, $linkPropertyId, $photosPropertyId);
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();
            $this->checkParams();
            $this->getResult();
            $this->includeComponentTemplate();
        }
        catch (Exception $e)
        {
            ShowError($e->getMessage());
        }
    }
}
