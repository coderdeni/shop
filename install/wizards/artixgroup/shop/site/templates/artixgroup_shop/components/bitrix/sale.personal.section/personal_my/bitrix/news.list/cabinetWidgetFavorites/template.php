<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="cabinetWidget">
    <div class="cabinetWidget__header">
        <div class="cabinetWidget__title"><i class="icon icon_orders"></i>Мои избранные</div>
        <a href="/personal/favorites/" class="cabinetWidget__button">Посмотреть</a>
    </div>
    <div class="cabinetWidget__content">
        <div class="cabinetWidgetFavorites">
            <?php
            foreach ($arResult['ITEMS'] as $item)
            {
                ?>
                <a href="<?= $item['DETAIL_PAGE_URL']?>" class="cabinetWidgetFavorites__item"><?= $item['NAME']?></a>
                <?
            }
            ?>
        </div>
    </div>
</div>
