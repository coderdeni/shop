<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	
if ($arParams['SHOW_ACCOUNT_PAGE'] !== 'Y')
{
	LocalRedirect($arParams['SEF_FOLDER']);
}

use Bitrix\Main\Localization\Loc;
if ($arParams['SET_TITLE'] == 'Y')
{
	$APPLICATION->SetTitle(Loc::getMessage("SPS_TITLE_ACCOUNT"));
}

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_ACCOUNT"));
?>
<div class="cabinet">
    <div class="cabinet__left">
        <?include_once 'sidebar.php'?>
    </div>
    <div class="cabinet__right">
        <div class="cabinetBonus">
            <?
            if ($arParams['SHOW_ACCOUNT_COMPONENT'] !== 'N')
            {
                $APPLICATION->IncludeComponent(
                    "bitrix:sale.personal.account",
                    "cabinetWidgetBonusDetail",
                    Array(
                        "SET_TITLE" => "N"
                    ),
                    $component
                );
            }
            ?>
        </div>
    </div>
</div>