<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$prevLevel = 0;
?>
<ul class="my-menu">
    <li class="slider-menu__item_header"><span class="slider-menu__text"></span></li>
    <?
    foreach ($arResult as $item)
    {
        if ($prevLevel > 0 && $item["DEPTH_LEVEL"] < $prevLevel)
        {
            echo str_repeat("</ul></li>", ($prevLevel - $item["DEPTH_LEVEL"]));
        }

        if ($item['IS_PARENT'])
        {
            ?>
            <li>
                <a href="<?= $item["LINK"] ?>">
                    <?= htmlspecialcharsbx($item["TEXT"]) ?>
                </a>
                <ul>
                    <li class="slider-menu__item_header"><span class="slider-menu__text"><?= htmlspecialcharsbx($item["TEXT"]) ?></span></li>
            <?
        }
        else
        {
            ?>
            <li>
                <a href="<?= $item["LINK"] ?>">
                    <?= htmlspecialcharsbx($item["TEXT"]) ?>
                </a>
            </li>
            <?
        }

        $prevLevel = $item['DEPTH_LEVEL'];
    }

    if ($prevLevel > 1)
    {
        echo str_repeat("</ul></li>", ($prevLevel - 1));
    }
    ?>
</ul>
