<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters['DISPLAY_FAVORITES_LINK'] = array(
    'PARENT' => 'ADDITIONAL_SETTINGS',
    'NAME' => GetMessage('ARTIXGROUP_AUTH_DISPLAY_FAVORITES_LINK'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
);
$arTemplateParameters['DISPLAY_BONUS_LINK'] = array(
    'PARENT' => 'ADDITIONAL_SETTINGS',
    'NAME' => GetMessage('ARTIXGROUP_AUTH_DISPLAY_BONUS_LINK'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
);