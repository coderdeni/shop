<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult))
	return;
?>
<div class="footer__box">
    <div class="footer__title"><?= $arParams['TITLE'] ?></div>
    <?
    foreach ($arResult as $item)
    {
        if ($item["DEPTH_LEVEL"] === 1)
        {
            ?>
            <div class="footer__item">
                <a class="footer__link" href="<?= $item["LINK"]?>"><?= htmlspecialcharsbx($item["TEXT"]) ?></a>
            </div>
            <?
        }
    }
    ?>
</div>