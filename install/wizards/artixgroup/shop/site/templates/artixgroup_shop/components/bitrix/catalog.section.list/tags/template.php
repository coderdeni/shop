<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="tags">
    <div class="tags__container swiper-container">
        <ul class="tags-list swiper-wrapper">
        <?
        foreach ($arResult['SECTIONS'] as $arSection)
        {
            ?>
            <li class="tags-list__item swiper-slide">
                <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>" class="tags-list__link<?= $arSection['SELECTED'] ? ' tags-list__link_active' : ''?>">
					<? echo $arSection['NAME'] ?><?= $arSection['UF_HIT_CATEGORY'] ? ' hit' : ''?><?= $arSection['UF_NEW_CATEGORY'] ? ' new' : ''?>
                </a>
            </li>
            <?
        }
        ?>
        </ul>
    </div>
</div>