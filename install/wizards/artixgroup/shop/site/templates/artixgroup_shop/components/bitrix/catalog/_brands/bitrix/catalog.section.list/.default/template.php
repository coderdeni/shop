<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="categoryListBoxWrap">
    <button class="button button_big js-toggle-class" data-target=".categoryListBox" data-class="visible">Категории</button>
    <div class="categoryListBox">
        <?php
        if ('Y' == $arParams['SHOW_PARENT_NAME'] && 0 < $arResult['SECTION']['ID'])
        {
            ?>
            <a href="maincatalog.html" class="categoryListBox__mainLink"><?= $arResult['SECTION']['NAME'] ?></a>
            <?
        }
        ?>
        <ul class="categoryList">
            <?
            $prevLevel = 1;
            foreach ($arResult['SECTIONS'] as $key => $arSection)
            {
                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                if ($prevLevel > (int)$arSection['DEPTH_LEVEL'])
                {
                    str_repeat('</ul>', $prevLevel - (int)$arSection['DEPTH_LEVEL']);
                }

                if ((int)$arSection['DEPTH_LEVEL'] === 1)
                {
                    ?>
                    <li class="categoryList__item" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                        <div class="categoryList__parent categoryList__parent_has-child<?= $arSection['IS_PARENT'] ? ' categoryList__parent_open' : ''?><?= $arSection['IS_ACTIVE'] ? ' categoryList__parent_active' : ''?>">
                            <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>" class="categoryList__link"><? echo $arSection['NAME'];?></a>
                        </div>
                        <?
                        if ($arSection['IS_PARENT'])
                        {
                            ?>
                            <ul class="categoryListSub">
                            <?
                        }
                        ?>
                    </li>
                    <?
                }
                else
                {
                    if (!$arSection['IS_ACTIVE'])
                    {
                        ?>
                        <li class="categoryListSub__item" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                            <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>" class="categoryListSub__link"><? echo $arSection['NAME']; ?></a>
                        </li>
                        <?
                    }
                    else
                    {
                        ?>
                        <li class="categoryListSub__item" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                            <span class="categoryListSub__link categoryListSub__link_active"><? echo $arSection['NAME']; ?></span>
                        </li>
                        <?
                    }
                }

                $prevLevel = (int)$arSection['DEPTH_LEVEL'];
            }
            str_repeat('</ul>', $prevLevel - 1);
            ?>
        </ul>
    </div>
</div>