<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$files = get_included_files();
$activePage = end(explode('\\', trim($files[count($files) - 2], '.php')));

$groups = [
    'personal' => [
        'name' => Loc::getMessage('SPS_PERSONAL_PAGE_TITLE'),
        'items' => []
    ],
    'orders' => [
        'name' => Loc::getMessage('SPS_ORDER_PAGE_TITLE'),
        'items' => []
    ],
    'subscribe' => [
        'name' => Loc::getMessage('SPS_SUBSCRIBE_PAGE_NAME'),
        'items' => []
    ]
];

if ($arParams['SHOW_PRIVATE_PAGE'] === 'Y')
{
    $groups['personal']['items'][] = array(
        "path" => $arResult['PATH_TO_PRIVATE'],
        "name" => Loc::getMessage("SPS_PERSONAL_PAGE_NAME"),
        "icon" => 'icon-personal',
        'active' => $activePage === 'private',
    );
}

if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
{
    $groups['orders']['items'][] = array(
        "path" => $arResult['PATH_TO_ORDERS'],
        "name" => Loc::getMessage("SPS_ORDER_PAGE_NAME"),
        "icon" => 'icon-orders',
        'active' => in_array($activePage, ['orders', 'order_detail']),
    );
}

if ($arParams['SHOW_ACCOUNT_PAGE'] === 'Y')
{
    $groups['orders']['items'][] = array(
        "path" => $arResult['PATH_TO_ACCOUNT'],
        "name" => Loc::getMessage("SPS_ACCOUNT_PAGE_NAME"),
        "icon" => 'icon-bonus',
        'active' => $activePage == 'account',
    );
}

if ($arParams['SHOW_PROFILE_PAGE'] === 'Y')
{
    $groups['personal']['items'][] = array(
        "path" => $arResult['PATH_TO_PROFILE'],
        "name" => Loc::getMessage("SPS_PROFILE_PAGE_NAME"),
        "icon" => 'icon-profiles',
        'active' => in_array($activePage, ['profile', 'profile_detail']),
    );
}

if ($arParams['SHOW_BASKET_PAGE'] === 'Y')
{
    $groups['orders']['items'][] = array(
        "path" => $arParams['PATH_TO_BASKET'],
        "name" => Loc::getMessage("SPS_BASKET_PAGE_NAME"),
        "icon" => 'icon_notice'
    );
}

$groups['subscribe']['items'][] = array(
    "path" => '/personal/favorites/',
    "name" => Loc::getMessage("SPS_FAVORITES_PAGE_NAME"),
    "icon" => 'icon_delayed',
    'active' => $activePage === 'favorites',
);

if ($arParams['SHOW_SUBSCRIBE_PAGE'] === 'Y')
{
    $groups['subscribe']['items'][] = array(
        "path" => $arResult['PATH_TO_SUBSCRIBE'],
        "name" => Loc::getMessage("SPS_SUBSCRIBE_PAGE_NAME"),
        "icon" => 'icon_notice',
        'active' => $activePage === 'subscribe',
    );
}

if ($arParams['SHOW_CONTACT_PAGE'] === 'Y')
{
    $groups['personal']['items'][] = array(
        "path" => $arParams['PATH_TO_CONTACT'],
        "name" => Loc::getMessage("SPS_CONTACT_PAGE_NAME"),
        "icon" => 'icon_notice',
    );
}

Loc::loadMessages(__FILE__);
?>
<div class="cabinetMenu">
    <?
    foreach ($groups as $group)
    {
        if (count($group['items']) > 0)
        {
            ?>
            <div class="cabinetMenu__title"><?= $group['name'] ?></div>
            <?
            foreach ($group['items'] as $item)
            {
                ?>
                <div class="cabinetMenu__item<?= $item['active'] ? ' cabinetMenu__item_active' : ''?>">
                    <a href="<?= $item['path']?>" class="cabinetMenu__link">
                        <svg class="icon <?= $item['icon']?>">
                            <use xlink:href="#<?= $item['icon']?>"></use>
                        </svg>
                        <?= $item['name']?>
                    </a>
                </div>
                <?
            }
        }
    }
    ?>
    <div class="cabinetMenu__item cabinetMenu__item_exit">
        <a href="/personal/account/index.php?logout=yes" class="cabinetMenu__link">
            <svg class="icon icon-exit">
                <use xlink:href="#icon-exit"></use>
            </svg>
            <?= Loc::getMessage("SPS_EXIT")?>
        </a>
    </div>
</div>