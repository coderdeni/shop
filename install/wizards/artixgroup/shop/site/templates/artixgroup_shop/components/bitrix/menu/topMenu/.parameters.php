<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters['TITLE'] = array(
    'PARENT' => 'ADDITIONAL_SETTINGS',
    'NAME' => GetMessage('CP_BC_TPL_TITLE'),
    'TYPE' => 'TEXT',
    'DEFAULT' => ''
);