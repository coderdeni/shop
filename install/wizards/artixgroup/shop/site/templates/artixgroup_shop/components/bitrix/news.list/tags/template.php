<?php

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="contentTags">
    <div class="contentTags__title"><?= Loc::getMessage('TAGS')?></div>
    <div class="contentTags__content">
        <? 
        foreach ($arResult['TAGS'] as $tag)
        {
            ?>
            <a href="<?= $arResult['LIST_PAGE_URL']?>?tag=<?= $tag['URL']?>" class="contentTags__button"><?= $tag['NAME']?></a>
            <?
        }  
        ?>
    </div>
</div>
