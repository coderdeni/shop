<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="cabinetWidget cabinetWidget_bonus">
    <div class="cabinetWidget__header">
        <div class="cabinetWidget__title"><i class="icon icon_bonus"></i>Мои бонусы</div>
        <a href="/personal/account/" class="cabinetWidget__button">Посмотреть</a>
    </div>
    <div class="cabinetWidget__content">
        <?
        foreach ($arResult["ACCOUNT_LIST"] as $accountValue)
        {
            ?>
            <div class="cabinetWidgetBonus">
                <div class="cabinetWidgetBonus__title">На вашем счету</div>
                <div class="cabinetWidgetBonus__count"><?=$accountValue['SUM']?></div>
            </div>
            <?
        }
        ?>
    </div>
</div>
