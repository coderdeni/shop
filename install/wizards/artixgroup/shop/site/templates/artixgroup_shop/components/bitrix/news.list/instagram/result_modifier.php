<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$index = 1;
$isGroup = false;

foreach ($arResult['ITEMS'] as &$item)
{
    $size = ['width'=> 332, 'height'=> 436];

    if (($item['PROPERTIES']['GROUP']['VALUE'] === 'Y' && isset($arResult['ITEMS'][$index])) || $isGroup)
    {
        $isGroup = $item['PROPERTIES']['GROUP']['VALUE'] === 'Y';
        $size['height'] = 214;
    }

    $pic = CFile::ResizeImageGet($item['PREVIEW_PICTURE']['ID'], $size, BX_RESIZE_IMAGE_EXACT, true);
    $item['PREVIEW_PICTURE'] = array_change_key_case($pic, CASE_UPPER);
    $index++;
}
