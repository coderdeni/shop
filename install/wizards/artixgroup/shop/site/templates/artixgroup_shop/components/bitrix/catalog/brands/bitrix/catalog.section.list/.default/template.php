<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>

<div class="categoryListBoxWrap">
    <button class="button button_big js-toggle-class" data-target=".categoryListBox" data-class="visible">Категории</button>
    <div class="categoryListBox">
        <?php
        if ('Y' == $arParams['SHOW_PARENT_NAME'] && 0 < $arResult['SECTION']['ID'])
        {
            ?>
            <a href="maincatalog.html" class="categoryListBox__mainLink"><?= $arResult['SECTION']['NAME'] ?></a>
            <?
        }
        ?>

        <ul class="categoryList">
            <?
            foreach ($arResult['SECTIONS'] as &$arSection)
            {
                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                ?>
                <li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>" class="categoryList__item">
                    <div class="categoryList__parent categoryList__parent_has-child">
                        <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>" class="categoryListSub__link"><? echo $arSection['NAME']; ?></a>
                    </div>
                </li>
                <?
            }
            ?>
        </ul>
    </div>
</div>

