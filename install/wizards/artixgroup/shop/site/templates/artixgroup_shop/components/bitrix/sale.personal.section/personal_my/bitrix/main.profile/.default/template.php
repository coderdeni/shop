<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;

?>
<div class="cabinetPersonal">
    <div class="cabinetForm">
        <?
        ShowError($arResult["strProfileError"]);

        if ($arResult['DATA_SAVED'] == 'Y')
        {
            ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
        }

        ?>
        <form method="post" name="form1" action="<?=$APPLICATION->GetCurUri()?>" enctype="multipart/form-data" role="form" class="cabinetForm__container">
            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="ID" value="<?=$arResult["ID"]?>" />
            <input type="hidden" name="LOGIN" value="<?=$arResult["arUser"]["LOGIN"]?>" />
            <div class="cabinetForm__row">
                <div class="cabinetForm__title">Учетная запись</div>
            </div>
            <div class="cabinetForm__row">
                <label for="main-profile-email" class="cabinetForm__label"><?=Loc::getMessage('EMAIL')?>:</label>
                <div class="cabinetForm__iwrap">
                    <input type="text" name="EMAIL" class="cabinetForm__input" id="main-profile-email" value="<?=$arResult["arUser"]["EMAIL"]?>">
                </div>
            </div>
            <div class="cabinetForm__row">
                <label for="main-profile-mobile" class="cabinetForm__label">Телефон:</label>
                <div class="cabinetForm__iwrap">
                    <input type="text" name="PERSONAL_MOBILE" class="cabinetForm__input input_phone" id="main-profile-mobile" value="<?=$arResult["arUser"]["PERSONAL_MOBILE"]?>" placeholder="+7 (999) 999-99-99">
                </div>
            </div>
            <?
            if($arResult["arUser"]["EXTERNAL_AUTH_ID"] == '')
            {
                ?>
                <div class="cabinetForm__row">
                    <label for="main-profile-password" class="cabinetForm__label"><?=Loc::getMessage('NEW_PASSWORD_REQ')?>:</label>
                    <div class="cabinetForm__iwrap">
                        <input type="password" name="NEW_PASSWORD" class="cabinetForm__input" value="" id="main-profile-password" autocomplete="off">
                    </div>
                </div>
                <div class="cabinetForm__row">
                    <label for="main-profile-password-confirm" class="cabinetForm__label"><?=Loc::getMessage('NEW_PASSWORD_CONFIRM')?>:</label>
                    <div class="cabinetForm__iwrap">
                        <input type="password" name="NEW_PASSWORD_CONFIRM" class="cabinetForm__input" value="" id="main-profile-password-confirm" autocomplete="off">
                    </div>
                </div>
                <div class="cabinetForm__row">
                    <div class="cabinetForm__text"><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></div>
                </div>
                <?
            }
            ?>
            <div class="cabinetForm__row">
                <div class="cabinetForm__title">Личные данные</div>
            </div>
            <div class="cabinetForm__row">
                <label for="main-profile-name" class="cabinetForm__label"><?=Loc::getMessage('NAME')?>:</label>
                <div class="cabinetForm__iwrap">
                    <input type="text" name="NAME" class="cabinetForm__input" id="main-profile-name" value="<?=$arResult["arUser"]["NAME"]?>">
                </div>
            </div>
            <div class="cabinetForm__row">
                <label for="main-profile-last-name" class="cabinetForm__label"><?=Loc::getMessage('LAST_NAME')?>:</label>
                <div class="cabinetForm__iwrap">
                    <input type="text" name="LAST_NAME"  class="cabinetForm__input" maxlength="50" id="main-profile-last-name" value="<?=$arResult["arUser"]["LAST_NAME"]?>">
                </div>
            </div>
            <div class="cabinetForm__row">
                <label for="main-profile-second-name" class="cabinetForm__label"><?=Loc::getMessage('SECOND_NAME')?>:</label>
                <div class="cabinetForm__iwrap">
                    <input type="text" class="cabinetForm__input" name="SECOND_NAME" maxlength="50" id="main-profile-second-name" value="<?=$arResult["arUser"]["SECOND_NAME"]?>">
                </div>
            </div>
            <div class="cabinetForm__controls">
                <input type="submit" name="save" class="button" value="<?=(($arResult["ID"]>0) ? Loc::getMessage("MAIN_SAVE") : Loc::getMessage("MAIN_ADD"))?>">
            </div>
        </form>
        <div class="cabinetSocial">
            <div class="cabinetSocial__icons">
                <?
                if ($arResult["SOCSERV_ENABLED"])
                {
                    $APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
                        "SHOW_PROFILES" => "Y",
                        "ALLOW_DELETE" => "Y"
                    ),
                        false
                    );
                }
                ?>
            </div>
        </div>
    </div>
</div>