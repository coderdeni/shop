<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

if ($arParams['GUEST_MODE'] !== 'Y')
{
	Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
	Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
}
$this->addExternalCss("/bitrix/css/main/bootstrap.css");

CJSCore::Init(array('clipboard', 'fx'));

$APPLICATION->SetTitle("");

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach ($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}

	$component = $this->__component;

	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}
}
else
{
	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach ($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	?>
    <div class="cabinetOrder__header">
        <div class="cabinetOrder__back"><a href="<?= $arResult["URL_TO_LIST"] ?>" class="link link_back">Вернуться</a></div>
        <div class="cabinetOrder__headerText">
            <div class="cabinetOrder__title">
                <?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
                    "#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
                    "#DATE_ORDER_CREATE#"=> $arResult["DATE_INSERT_FORMATED"]
                ))?>
            </div>
            <div class="cabinetOrder__date"><?= $arResult["DATE_INSERT_FORMATED"] ?></div>
        </div>
        <?
        if ($arParams['GUEST_MODE'] !== 'Y')
        {
            ?>
            <div class="cabinetOrder__headerControls">
                <a href="<?=$arResult["URL_TO_COPY"]?>" class="cabinetOrder__cancel">
                    <?= Loc::getMessage('SPOD_ORDER_REPEAT') ?>
                </a>
                <?
                if ($arResult["CAN_CANCEL"] === "Y")
                {
                    ?>
                    <a href="<?=$arResult["URL_TO_CANCEL"]?>" class="cabinetOrder__cancel">
                        <?= Loc::getMessage('SPOD_ORDER_CANCEL') ?>
                    </a>
                    <?
                }
                ?>
            </div>
            <?
        }
        ?>
    </div>
    <div class="cabinetOrder__content">
        <div class="cabinetOrder__contentLeft">
            <div class="cabinetOrder__info">
                <div class="cabinetOrder__subtitle"><i class="icon icon_cabinethome"></i> Получатель</div>
                <div class="cabinetOrder__text"><?= $userName = $arResult["USER"]["NAME"] ." ". $arResult["USER"]["SECOND_NAME"] ." ". $arResult["USER"]["LAST_NAME"];?></div>
                <div class="cabinetOrder__text"><?= $arResult["USER"]["EMAIL"] ?></div>
                <div class="cabinetOrder__text">79184633256</div>
            </div>
            <?
            if (count($arResult['SHIPMENT']))
            {
                ?>
                <div class="cabinetOrder__info">
                    <div class="cabinetOrder__subtitle"><i class="icon icon_cabinetuser"></i>Доставка</div>
                    <?
                    foreach ($arResult['SHIPMENT'] as $shipment)
                    {
                        ?>
                        <div class="cabinetOrder__text"><?= htmlspecialcharsbx($shipment["DELIVERY_NAME"])?></div>
                        <?
                        $store = $arResult['DELIVERY']['STORE_LIST'][$shipment['STORE_ID']];
                        if (isset($store))
                        {
                            if (strlen($store['ADDRESS']))
                            {
                                ?>
                                <div class="cabinetOrder__text">Адрес доставки: <?= htmlspecialcharsbx($store['ADDRESS'])?></div>
                                <?
                            }
                        }
                    }
                    ?>
                    <div class="cabinetOrder__text">Статус отгрузки: <?= htmlspecialcharsbx($shipment['STATUS_NAME'])?></div>
                    <?
                    if (strlen($shipment['TRACKING_NUMBER']))
                    {
                        ?>
                        <div class="cabinetOrder__text"><?= Loc::getMessage('SPOD_ORDER_TRACKING_NUMBER')?>: <?= htmlspecialcharsbx($shipment['TRACKING_NUMBER'])?></div>
                        <?
                    }
                    ?>
                    <?
                    if (strlen($shipment['TRACKING_URL']))
                    {
                        ?>
                        <div class="cabinetOrder__text">
                            <a class="sale-order-detail-payment-options-shipment-button-element" href="<?=$shipment['TRACKING_URL']?>">
                                <?= Loc::getMessage('SPOD_ORDER_CHECK_TRACKING')?>
                            </a>
                        </div>
                        <?
                    }
                    ?>
                </div>
                <?
            }
            ?>
        </div>
        <div class="cabinetOrder__contentRight">
            <div class="cabinetOrderPayInfo">
                <div class="cabinetOrderPayInfo__title"><i class="icon icon_cabinetpay"></i>Сумма <?=$arResult['PRICE_FORMATED']?></div>
                <?
                foreach ($arResult['PAYMENT'] as $payment)
                {
                    ?>
                    <div class="cabinetOrderPayInfo__row">
                        <div class="cabinetOrderPayInfo__item"><?=$payment['PAY_SYSTEM_NAME']?></div>
                        <div class="cabinetOrderPayInfo__item">
                            <?
                            if ($payment['PAID'] === 'Y')
                            {
                                ?>
                                <div class="orderStatus"><?=Loc::getMessage('SPOD_PAYMENT_PAID')?></div>
                                <?
                            }
                            elseif ($arResult['IS_ALLOW_PAY'] == 'N')
                            {
                                ?>
                                <div class="orderStatus"><?=Loc::getMessage('SPOD_TPL_RESTRICTED_PAID')?></div>
                                <?
                            }
                            else
                            {
                                ?>
                                <div class="orderStatus"><?=Loc::getMessage('SPOD_PAYMENT_UNPAID')?></div>
                                <?
                            }
                            ?>
                        </div>
                    </div>

                    <div class="cabinetOrderPayInfo__row cabinetOrderPayInfo__row_changePay">
                        <?
                        if (
                            $payment['PAID'] !== 'Y'
                            && $arResult['CANCELED'] !== 'Y'
                            && $arParams['GUEST_MODE'] !== 'Y'
                            && $arResult['LOCK_CHANGE_PAYSYSTEM'] !== 'Y'
                        )
                        {
                            ?>
                            <div class="cabinetOrderPayInfo__item">
                                <a href="#" id="<?=$payment['ACCOUNT_NUMBER']?>" class="cabinetOrderPayInfo__item"><?=Loc::getMessage('SPOD_CHANGE_PAYMENT_TYPE')?></a>
                            </div>
                            <?
                        }
                        ?>
                        <?
                        if ($payment['PAY_SYSTEM']["IS_CASH"] !== "Y")
                        {
                            ?>
                            <div class="cabinetOrderPayInfo__item">
                                <?
                                if ($payment['PAY_SYSTEM']['PSA_NEW_WINDOW'] === 'Y' && $arResult["IS_ALLOW_PAY"] !== "N")
                                {
                                    ?>
                                    <a class="cabinetOrderPayInfo__pay"
                                       target="_blank"
                                       href="<?=htmlspecialcharsbx($payment['PAY_SYSTEM']['PSA_ACTION_FILE'])?>">
                                        <?= Loc::getMessage('SPOD_ORDER_PAY') ?>
                                    </a>
                                    <?
                                }
                                else
                                {
                                    if ($payment["PAID"] === "Y" || $arResult["CANCELED"] === "Y" || $arResult["IS_ALLOW_PAY"] === "N")
                                    {
                                        ?>
                                        <?
                                    }
                                    else
                                    {
                                        ?>
                                        <a class="cabinetOrderPayInfo__pay"><?= Loc::getMessage('SPOD_ORDER_PAY') ?></a>
                                        <?
                                    }
                                }
                                ?>
                            </div>
                            <?
                        }
                        ?>
                    </div>

                    <?
                }
                ?>

                <?
                if (floatval($arResult["ORDER_WEIGHT"]))
                {
                    ?>
                    <div class="cabinetOrderPayInfo__row">
                        <div class="cabinetOrderPayInfo__item">Вес</div>
                        <div class="cabinetOrderPayInfo__item"><?= $arResult['ORDER_WEIGHT_FORMATED'] ?></div>
                    </div>
                    <?
                }

                if (!empty($arResult['PRODUCT_SUM_FORMATED']))
                {
                    ?>
                    <div class="cabinetOrderPayInfo__row">
                        <div class="cabinetOrderPayInfo__item">Товары</div>
                        <div class="cabinetOrderPayInfo__item"><?=$arResult['PRODUCT_SUM_FORMATED']?></div>
                    </div>
                    <?
                }

                if (strlen($arResult["PRICE_DELIVERY_FORMATED"]))
                {
                    ?>
                    <div class="cabinetOrderPayInfo__row">
                        <div class="cabinetOrderPayInfo__item">Доставка</div>
                        <div class="cabinetOrderPayInfo__item"><?=$arResult['PRICE_DELIVERY_FORMATED']?></div>
                    </div>
                    <?
                }

                if ((float)$arResult["TAX_VALUE"] > 0)
                {
                    ?>
                    <div class="cabinetOrderPayInfo__row">
                        <div class="cabinetOrderPayInfo__item">Налоги</div>
                        <div class="cabinetOrderPayInfo__item"><?=$arResult['TAX_VALUE_FORMATED']?></div>
                    </div>
                    <?
                }
                ?>

                <div class="cabinetOrderPayInfo__row cabinetOrderPayInfo__row_itog">
                    <div class="cabinetOrderPayInfo__item">Итого </div>
                    <div class="cabinetOrderPayInfo__item"><?=$arResult['PRICE_FORMATED']?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="cabinetOrder__products">
        <div class="orderProducts">
            <div class="orderProducts__header">
                <div class="orderProducts__headerTitle">Фото</div>
                <div class="orderProducts__headerTitle">Наименование</div>
                <div class="orderProducts__headerTitle">Артикул</div>
                <div class="orderProducts__headerTitle">Цена</div>
                <div class="orderProducts__headerTitle">Скидка</div>
                <div class="orderProducts__headerTitle">Количество</div>
                <div class="orderProducts__headerTitle">Сумма</div>
            </div>
            <?
            foreach ($arResult['BASKET'] as $basketItem)
            {
                ?>
                <div class="orderProducts__row">
                    <div class="orderProducts__photo">
                        <a href="<?=$basketItem['DETAIL_PAGE_URL']?>" class="orderProducts__imgLink">
                            <?
                            if (strlen($basketItem['PICTURE']['SRC']))
                            {
                                $imageSrc = $basketItem['PICTURE']['SRC'];
                            }
                            else
                            {
                                $imageSrc = $this->GetFolder().'/images/no_photo.png';
                            }
                            ?>
                            <img src="<?= $imageSrc ?>" alt="" class="orderProducts__img">
                        </a>
                    </div>
                    <div class="orderProducts__info">
                        <div class="orderProducts__titleWrap"><a href="<?=$basketItem['DETAIL_PAGE_URL']?>" class="orderProducts__title"><?=htmlspecialcharsbx($basketItem['NAME'])?></a></div>
                        <?
                        if (isset($basketItem['PROPS']) && is_array($basketItem['PROPS']))
                        {
                            ?>
                            <div class="orderProducts__props">
                                <?
                                foreach ($basketItem['PROPS'] as $itemProps)
                                {
                                    ?>
                                    <div class="orderProducts__prop"><span class="orderProducts__propName"><?=htmlspecialcharsbx($itemProps['NAME'])?>:</span> ?=htmlspecialcharsbx($itemProps['VALUE'])?></div>
                                    <?
                                }
                                ?>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                    <div class="orderProducts__article"><span class="orderProducts__mobileTitle">Артикул</span>2155546456-BM</div>
                    <div class="orderProducts__price"><span class="orderProducts__mobileTitle">Цена</span> <?=$basketItem['BASE_PRICE_FORMATED']?></div>
                    <div class="orderProducts__discont"><span class="orderProducts__mobileTitle">Скидка</span><?= $basketItem['DISCOUNT_PRICE_PERCENT_FORMATED'] ?></div>
                    <div class="orderProducts__count"><span class="orderProducts__mobileTitle">Количество</span><?=$basketItem['QUANTITY']?></div>
                    <div class="orderProducts__cost"><span class="orderProducts__mobileTitle">Сумма</span><?=$basketItem['FORMATED_SUM']?></div>
                </div>
                <?
            }
            ?>
        </div>
    </div>
	<?
	$javascriptParams = array(
	"url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
	"templateFolder" => CUtil::JSEscape($templateFolder),
	"paymentList" => $paymentData
	);
	$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
	?>
	<script>
		BX.Sale.PersonalOrderComponent.PersonalOrderDetail.init(<?=$javascriptParams?>);
	</script>
<?
}
?>

