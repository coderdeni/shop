<div class="cabinetProfile">
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

if(strlen($arResult["ERROR_MESSAGE"])>0)
{
	ShowError($arResult["ERROR_MESSAGE"]);
}
if(strlen($arResult["NAV_STRING"]) > 0)
{
	?>
	<p><?=$arResult["NAV_STRING"]?></p>
	<?
}
if (count($arResult["PROFILES"]))
{
    foreach($arResult["PROFILES"] as $val)
    {
        $profileData = \Bitrix\Sale\OrderUserProperties::getProfileValues((int)($val['ID']));
        if (empty($profileData))
        {
            continue;
        }
        ?>
        <div class="cabinetProfileItem">
            <div class="cabinetProfile__adress"><?= $profileData[7] ?></div>
            <div class="cabinetProfile__controls">
                <a href="<?= $val["URL_TO_DETAIL"] ?>" class="edit-link"><i class="icon icon_edit"></i></a>
                <a href="javascript:if(confirm('<?= Loc::getMessage("STPPL_DELETE_CONFIRM") ?>')) window.location='<?= $val["URL_TO_DETELE"] ?>'" class="delete-link"><i class="icon icon_delete"></i></a>
            </div>
        </div>
        <?
    }

	if(strlen($arResult["NAV_STRING"]) > 0)
	{
		?>
		<p><?=$arResult["NAV_STRING"]?></p>
		<?
	}
}
else
{
	?>
	<h3><?=Loc::getMessage("STPPL_EMPTY_PROFILE_LIST") ?></h3>
	<?
}
?>
</div>