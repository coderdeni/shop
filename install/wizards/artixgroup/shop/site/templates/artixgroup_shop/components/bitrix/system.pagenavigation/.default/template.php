<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->createFrame()->begin("Загрузка навигации");
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<div class="pagination">
	<div class="pagination__container">
		<?
		if ($arResult["NavPageNomer"] > 1):
			if($arResult["bSavePage"]):
				?>
				<div class="pagination__prev">
					<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" class="pagination__left">
						<svg class="icon icon-arrow ">
							<use xlink:href="#icon-arrow"></use>
						</svg>
					</a>
				</div>
				<?
			else:
				if ($arResult["NavPageNomer"] > 2):
					?>
					<div class="pagination__prev">
						<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" class="pagination__left">
							<svg class="icon icon-arrow ">
								<use xlink:href="#icon-arrow"></use>
							</svg>
						</a>
					</div>
					<?
				else:
					?>
					<div class="pagination__prev">
						<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="pagination__left">
							<svg class="icon icon-arrow ">
								<use xlink:href="#icon-arrow"></use>
							</svg>
						</a>
					</div>
					<?
				endif;
			endif;
		else:
			?>
			<div class="pagination__prev">
				<span class="pagination__left disabled">
					<svg class="icon icon-arrow ">
						<use xlink:href="#icon-arrow"></use>
					</svg>
				</span>
			</div>
			<?
		endif;
		?>
		<div class="pagination__text">Страница 1 из 20</div>
		<div class="pagination__links">
			<?
			if ($arResult["NavPageNomer"] > 4)
			{
				?>
				<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1" class="pagination__link"><span class="pagination__linkText">1</span></a>
				<a class="pagination__link"><span class="pagination__linkText">...</span></a>
				<?
			}

			do
			{
				if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
					?>
					<a  class="pagination__link pagination__link_current">
						<span class="pagination__linkText"><?=$arResult["nStartPage"]?></span>
					</a>
					<?
				elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
					?>
					<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="pagination__link">
						<span class="pagination__linkText"><?=$arResult["nStartPage"]?></span>
					</a>
					<?
				else:

					?>
					<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>" class="pagination__link">
						<span class="pagination__linkText"><?=$arResult["nStartPage"]?></span>
					</a>
					<?
				endif;
				$arResult["nStartPage"]++;
			}

			while($arResult["nStartPage"] <= $arResult["nEndPage"]);

			if ($arResult["NavPageCount"] > 5 && ($arResult["NavPageCount"] - 5) >= $arResult['NavPageNomer'])
			{
				?>
				<a class="pagination__link"><span class="pagination__linkText">...</span></a>
				<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>" class="pagination__link"><span class="pagination__linkText"><?= $arResult["NavPageCount"]?></span></a>
				<?
			}
			?>
		</div>
		<?
		if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
			?>
			<div class="pagination__next">
				<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>" class="pagination__right">
					<svg class="icon icon-arrow ">
						<use xlink:href="#icon-arrow"></use>
					</svg>
				</a>
			</div>
			<?
		else:
			?>
			<div class="pagination__next">
				<a class="pagination__right disabled">
					<svg class="icon icon-arrow ">
						<use xlink:href="#icon-arrow"></use>
					</svg>
				</a>
			</div>
			<?
		endif;
		?>
	</div>
</div>
