BX.namespace("BX.Iblock.Catalog");

BX.Iblock.Catalog.CompareClass = (function()
{
	var CompareClass = function(wrapObjId)
	{
		this.wrapObjId = wrapObjId;
		this.lastAction = null;
	};

	CompareClass.prototype.MakeAjaxAction = function(url)
	{
		BX.showWait(BX(this.wrapObjId));
		BX.ajax.post(
			url,
			{
				ajax_action: 'Y'
			},
			BX.proxy(function(result)
			{
				BX.closeWait();
				BX(this.wrapObjId).innerHTML = result;
				if (this.lastAction == "DELETE")
				{
					SiteHelper.compare.remove();
                    this.lastAction = null;
				}
			}, this)
		);
	};

    CompareClass.prototype.DeleteAction = function(url)
    {
		this.lastAction = "DELETE";
		this.MakeAjaxAction(url);
    };

	return CompareClass;
})();