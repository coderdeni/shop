<?
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CJSCore::Init();
?>
<div class="authorization">
    <div class="authorization__form">
        <?
        if ($arResult["FORM_TYPE"] === "login")
        {
            ?>
            <form class="form" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                <?
                if ($arResult["BACKURL"] <> '')
                {
                    ?>
                    <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                    <?
                }
                foreach ($arResult["POST"] as $key => $value)
                {
                    ?>
                    <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
                    <?
                }
                ?>
                <div class="form__row">
                    <input type="text" name="USER_LOGIN" maxlength="50" class="input input_big" placeholder="<?= Loc::getMessage("AUTH_LOGIN")?>">
                    <script>
                        BX.ready(function() {
                            var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                            if (loginCookie)
                            {
                                var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                                var loginInput = form.elements["USER_LOGIN"];
                                loginInput.value = loginCookie;
                            }
                        });
                    </script>
                </div>
                <div class="form__row">
                    <input type="password" class="input input_big" name="USER_PASSWORD" maxlength="255" autocomplete="off" placeholder="<?= Loc::getMessage("AUTH_PASSWORD")?>">
                    <?
                    if ($arResult["SECURE_AUTH"])
                    {
                        ?>
                        <span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo Loc::getMessage("AUTH_SECURE_NOTE")?>" style="display:none">
                                        <div class="bx-auth-secure-icon"></div>
                                    </span>
                        <noscript>
                                        <span class="bx-auth-secure" title="<?echo Loc::getMessage("AUTH_NONSECURE_NOTE")?>">
                                            <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                        </span>
                        </noscript>
                        <script type="text/javascript">
                            document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
                        </script>
                        <?
                    }
                    ?>
                </div>
                <?
                if ($arResult["STORE_PASSWORD"] === "Y")
                {
                    ?>
                    <div class="form__row form__row_sb">
                        <label class="checkbox">
                            <input type="checkbox" id="USER_REMEMBER_frm" class="checkbox__input" name="USER_REMEMBER" value="Y">
                            <span class="checkbox__new-input"></span>
                            <?echo Loc::getMessage("AUTH_REMEMBER_SHORT")?>
                        </label>
                    </div>
                    <?
                }
                ?>
                <?
                if ($arResult["CAPTCHA_CODE"])
                {
                    ?>
                    <div class="form__row form__row_sb">
                        <input type="text" class="input input_midle" placeholder="<?echo Loc::getMessage("AUTH_CAPTCHA_PROMT")?>">
                        <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                        <div class="captcha">
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" alt="CAPTCHA">
                        </div>
                    </div>
                    <?
                }
                ?>
                <div class="form__row form__row_sb">
                    <input type="submit" class="button button_bp" name="Login" value="<?= Loc::getMessage("AUTH_LOGIN_BUTTON")?>">
                    <a class="link link_blue" href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow">
                        <?=Loc::getMessage("AUTH_FORGOT_PASSWORD_2")?>
                    </a>
                </div>
                <?
                if ($arResult["AUTH_SERVICES"])
                {
                    ?>
                    <div class="form__row form__row_sb">
                        <?= Loc::getMessage("AUTH_FORGOT_PASSWORD_2")?>
                        <?
                        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons",
                            array(
                                "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
                                "SUFFIX"=>"form",
                            ),
                            $component,
                            array("HIDE_ICONS"=>"Y")
                        );
                        ?>
                    </div>
                    <?
                }
                ?>
                <div class="form__row form__row_ac">
                    <a href="javascript:void(0)" class="link link_big link_next trigger" data-dialog="registrationDialog"><?= Loc::getMessage("AUTH_REGISTER")?></a>
                </div>
                <div class="form__row form__row_ac">
                    <div class="authorization__infoText"><?= Loc::getMessage("AUTH_INFO_TEXT")?></div>
                </div>
            </form>
            <?
            if ($arResult["AUTH_SERVICES"])
            {
                $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
                    array(
                        "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                        "AUTH_URL" => $arResult["AUTH_URL"],
                        "POST" => $arResult["POST"],
                        "POPUP" => "Y",
                        "SUFFIX" => "form",
                    ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                );
            }
        }
        elseif ($arResult["FORM_TYPE"] === "otp")
        {
            ?>
            <form class="form" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                <?
                if ($arResult["BACKURL"] <> '')
                {
                    ?>
                    <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                    <?
                }
                ?>
                <input type="hidden" name="AUTH_FORM" value="Y" />
                <input type="hidden" name="TYPE" value="OTP" />
                <div class="form__row">
                    <input type="text" name="USER_OTP" maxlength="50" class="input input_big" autocomplete="off" placeholder="<?= Loc::getMessage("auth_form_comp_otp")?>">
                </div>
                <?
                if ($arResult["CAPTCHA_CODE"])
                {
                    ?>
                    <div class="form__row form__row_sb">
                        <input type="text" class="input input_midle" placeholder="<?echo Loc::getMessage("AUTH_CAPTCHA_PROMT")?>">
                        <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                        <div class="captcha">
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" alt="CAPTCHA">
                        </div>
                    </div>
                    <?
                }
                ?>
                <?
                if ($arResult["REMEMBER_OTP"] == "Y")
                {
                    ?>
                    <div class="form__row form__row_sb">
                        <label class="checkbox" title="<?= Loc::getMessage("auth_form_comp_otp_remember_title")?>">
                            <input type="checkbox" id="OTP_REMEMBER_frm" class="checkbox__input" name="OTP_REMEMBER" value="Y">
                            <span class="checkbox__new-input"></span>
                            <?echo Loc::getMessage("auth_form_comp_otp_remember")?>
                        </label>
                    </div>
                    <?
                }
                ?>
                <div class="form__row form__row_sb">
                    <input type="submit" class="button button_bp" name="Login" value="<?= Loc::getMessage("AUTH_LOGIN_BUTTON")?>">
                </div>
                <div class="form__row form__row_ac">
                    <a href="<?=$arResult["AUTH_LOGIN_URL"]?>" class="link link_big" data-dialog="registrationDialog"><?= Loc::getMessage("auth_form_comp_auth")?></a>
                </div>
                <div class="form__row form__row_ac">
                    <a href="javascript:void(0)" class="link link_big link_next trigger" data-dialog="registrationDialog"><?= Loc::getMessage("AUTH_REGISTER")?></a>
                </div>
                <div class="form__row form__row_ac">
                    <div class="authorization__infoText"><?= Loc::getMessage("AUTH_INFO_TEXT")?></div>
                </div>
            </form>
            <?
        }
        else
        {
            ?>
            <form class="form" action="<?=$arResult["AUTH_URL"]?>">
                <div class="form__row">
                    <?=$arResult["USER_NAME"]?><br />
                    [<?=$arResult["USER_LOGIN"]?>]<br />
                    <a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE")?>"><?=GetMessage("AUTH_PROFILE")?></a><br />
                </div>
                <div class="form__row form__row_sb">
                    <?
                    foreach ($arResult["GET"] as $key => $value)
                    {
                        ?>
                        <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
                        <?
                    }
                    ?>
                    <input type="hidden" name="logout" value="yes" />
                    <input type="submit" class="button button_bp" value="<?= Loc::getMessage("AUTH_LOGOUT_BUTTON")?>">
                </div>
            </form>
            <?
        }
        ?>
    </div>
</div>