<?php
$MESS ['subscr_form_title_desc'] = "Выберите рассылку";
$MESS ['subscr_form_button'] = "Подписаться";
$MESS ['subscr_form_response_ERROR'] = "Что-то пошло не так";
$MESS ['subscr_form_response_NOTE'] = "Поздравляем!";
$MESS ['subscr_form_button_sent'] = "ГОТОВО";
$MESS ['SUBSCRIBE_FORM_EMAIL_TITLE'] = "Введите ваш e-mail";
$MESS ['SUBSCRIBE_FORM_TITLE'] = "Всегда узнавайте первым о наших скидках и акциях";
$MESS ['SUBSCRIBE_FORM_DESC'] = "Подписывайтесь на нашу новостную рассылку и получайте эксклюзивные рекламные акции, предложения и полезную информацию от нашей компании.";
