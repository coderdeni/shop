<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

if (!empty($arResult['ERROR']))
{
    echo $arResult['ERROR'];
    return false;
}
?>

<div class="choose-glasses__content js-tab-content<?= $arParams['IS_ACTIVE'] === 'Y' ? ' active' : ''?>" data-tab="<?= $arParams['TAB_ID']?>">
    <?
    foreach ($arResult['rows'] as $row)
    {
        ?>
        <a href="<?= $row['UF_LINK']?>" class="choose-glasses__item">
            <span class="span choose-glasses__photo">
                <img src="<?= CFile::GetPath($row['UF_FILE'])?>" alt="<?= $row['UF_NAME']?>" class="choose-glasses__img">
            </span>
            <span class="choose-glasses__type"><?= $row['UF_NAME']?></span>
        </a>
        <?
    }
    ?>
</div>
