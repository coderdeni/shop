<div class="cabinetWidget">
    <div class="cabinetWidget__header">
        <div class="cabinetWidget__title"><i class="icon icon_profiles"></i>Мои адреса</div>
        <a href="/personal/profiles/" class="cabinetWidget__button">Изменить</a>
    </div>
    <div class="cabinetWidget__content">
    <?
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

    use Bitrix\Main\Localization\Loc;
    use Bitrix\Sale\OrderUserProperties;

    if(strlen($arResult["ERROR_MESSAGE"])>0)
    {
        ShowError($arResult["ERROR_MESSAGE"]);
    }

    if (count($arResult["PROFILES"]))
    {
        ?>
        <div class="cabinetWidgetAdress">
            <?
            foreach($arResult["PROFILES"] as $val)
            {
                $profileData = OrderUserProperties::getProfileValues((int)($val['ID']));
                if (empty($profileData))
                {
                    continue;
                }

                ?>

                <div class="cabinetWidgetAdress__item">
                    <div class="cabinetWidgetAdress__title">Адрес</div>
                    <div class="cabinetWidgetAdress__prop"><?= $profileData[7] ?></div>
                </div>
                <?
            }?>
        </div>
        <?
    }
    else
    {
        ?>
        <h3><?=Loc::getMessage("STPPL_EMPTY_PROFILE_LIST") ?></h3>
        <?
    }
    ?>
    </div>
</div>