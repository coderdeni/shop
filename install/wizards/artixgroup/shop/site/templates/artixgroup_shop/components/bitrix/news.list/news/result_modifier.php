<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$sectionIds = [];

foreach ($arResult["ITEMS"] as $item)
{
    $sectionIds[] = $item['IBLOCK_SECTION_ID'];
}
$sections = [];
$result = \Bitrix\Iblock\SectionTable::getList([
    'filter' => ['IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $sectionIds]
]);
while ($section = $result->fetch())
{
    $sections[$section['ID']] = $section;
}

foreach ($arResult["ITEMS"] as &$item)
{
    $item['SECTION'] = [
        'NAME' => $sections[$item['IBLOCK_SECTION_ID']]['NAME'],
        'CLASS' => '',
    ];
}