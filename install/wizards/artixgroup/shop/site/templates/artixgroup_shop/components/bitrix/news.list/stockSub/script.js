(function (window){
    window.AgActionsTimers = function (params) {
        this.selectorTimerContainer = !!params.selectorTimerContainer ? params.selectorTimerContainer : '[data-role="timer"]';
        this.format = !!params.format ? params.format : '{0}д : {1}ч : {2}м : {3}с';

        BX.ready(BX.delegate(this.init, this));
    };

    window.AgActionsTimers.prototype = {
        init: function()
        {
            if (!String.format)
            {
                String.format = function(format) {
                    var args = Array.prototype.slice.call(arguments, 1);
                    return format.replace(/{(\d+)}/g, function(match, number) {
                        return typeof args[number] != 'undefined'
                            ? args[number]
                            : match
                            ;
                    });
                };
            }

            this.timers = document.querySelectorAll(this.selectorTimerContainer);

            if (!!this.timers.length > 0)
            {
                setInterval(BX.delegate(this.updateTimers, this), 1000);
            }
        },
        updateTimers: function ()
        {
            var now = new Date(), i;

            for (i = 0; i < this.timers.length; i++)
            {
                console.log(now.getDate());
                var interval = this.getInterval(now.getTime(), parseInt(this.timers[i].getAttribute('data-timer')));
                this.timers[i].innerText = String.format(this.format, interval.days, interval.hours, interval.minutes, interval.seconds);
            }
        },
        getInterval: function (from, to)
        {
            var t = to - from;
            var seconds = Math.floor((t / 1000) % 60);
            var minutes = Math.floor((t / 1000 / 60) % 60);
            var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));

            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        },
    };
})(window);