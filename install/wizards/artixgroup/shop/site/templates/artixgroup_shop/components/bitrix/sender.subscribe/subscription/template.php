<?

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$buttonId = $this->randString();
?>
<div class="bx-subscribe"  id="sender-subscribe">
    <?
    $frame = $this->createFrame("sender-subscribe", false)->begin();

    if (isset($arResult['MESSAGE']))
    {
        ?>
        <div id="subscribeDialog" class="dialog">
            <div class="dialog__overlay"></div>
            <div class="dialog__noscroll">
                <div class="dialog__container">
                    <div class="dialog__title">Заказать звонок</div>
                    <div class="dialog__content">
                        <table>
                            <tr>
                                <td style="padding-right: 40px; padding-bottom: 0px;"><img src="<?=($this->GetFolder().'/images/'.($arResult['MESSAGE']['TYPE']=='ERROR' ? 'icon-alert.png' : 'icon-ok.png'))?>" alt=""></td>
                                <td>
                                    <div style="font-size: 22px;"><?=GetMessage('subscr_form_response_'.$arResult['MESSAGE']['TYPE'])?></div>
                                    <div style="font-size: 16px;"><?=htmlspecialcharsbx($arResult['MESSAGE']['TEXT'])?></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="dialog__close">
                        <button class="action close" data-dialog-close="">
                            <svg class="icon icon-close ">
                                <use xlink:href="#icon-close"></use>
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="dialog__event"></div>
            </div>
        </div>
        <script>
            BX.ready(function() {
                var subscribeDialog = document.querySelector('#subscribeDialog');
                document.body.append(subscribeDialog);
                subscribeDialog.classList.add('dialog--open');
            });
        </script>
        <?
    }
    ?>
    <script>
        (function () {
            var btn = document.querySelector('#bx_subscribe_btn_<?=$buttonId?>');
            var form = document.querySelector('#bx_subscribe_subform_<?=$buttonId?>');

            if (!btn) return;

            function mailSender()
            {
                setTimeout(function() {
                    if (!btn) return;
                    btn.classList.add('subscriptionButton__send');
                }, 400);
            }

            BX.ready(function() {
                BX.bind(btn, 'click', function() {
                    setTimeout(mailSender, 250);
                    return false;
                });
            });

            BX.bind(form, 'submit', function () {
                btn.disabled = true;
                setTimeout(function () {
                    btn.disabled = false;
                }, 2000);
                return true;
            });
        })();
    </script>

    <section class="subscription">
        <div class="subscription__container limiter">
            <div class="subscription__content">
                <div class="subscription__info">
                    <div class="subscription__title"><?= Loc::getMessage('SUBSCRIBE_FORM_TITLE')?></div>
                    <div class="subscription__text"><?= Loc::getMessage('SUBSCRIBE_FORM_DESC')?></div>
                </div>
                <div class="subscription__form">
                    <form id="bx_subscribe_subform_<?= $buttonId?>" role="form" method="post" action="<?= $arResult["FORM_ACTION"]?>" class="form form_subscription">
                        <?=bitrix_sessid_post()?>
                        <input type="email" name="SENDER_SUBSCRIBE_EMAIL" value="<?=$arResult["EMAIL"]?>" class="input input_subscription" placeholder="<?=htmlspecialcharsbx(Loc::getMessage('SUBSCRIBE_FORM_EMAIL_TITLE'))?>">
                        <button id="bx_subscribe_btn_<?= $buttonId?>" class="subscriptionButton">
                            <svg class="icon icon-mail">
                                <use xlink:href="#icon-mail"></use>
                            </svg>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <?
    $frame->beginStub();

    if (isset($arResult['MESSAGE']))
    {
        ?>
        <div id="subscribeDialog" class="dialog">
            <div class="dialog__overlay"></div>
            <div class="dialog__noscroll">
                <div class="dialog__container">
                    <div class="dialog__title">Заказать звонок</div>
                    <div class="dialog__content">
                        <table>
                            <tr>
                                <td style="padding-right: 40px; padding-bottom: 0px;"><img src="<?=($this->GetFolder().'/images/'.($arResult['MESSAGE']['TYPE']=='ERROR' ? 'icon-alert.png' : 'icon-ok.png'))?>" alt=""></td>
                                <td>
                                    <div style="font-size: 22px;"><?=GetMessage('subscr_form_response_'.$arResult['MESSAGE']['TYPE'])?></div>
                                    <div style="font-size: 16px;"><?=htmlspecialcharsbx($arResult['MESSAGE']['TEXT'])?></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="dialog__close">
                        <button class="action close" data-dialog-close="">
                            <svg class="icon icon-close ">
                                <use xlink:href="#icon-close"></use>
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="dialog__event"></div>
            </div>
        </div>
        <script>
            BX.ready(function() {
                var subscribeDialog = document.querySelector('#subscribeDialog');
                document.body.append(subscribeDialog);
                subscribeDialog.classList.add('dialog--open');
            });
        </script>
        <?
    }
    ?>

    <script>
        (function () {
            var btn = document.querySelector('#bx_subscribe_btn_<?=$buttonId?>');
            var form = document.querySelector('#bx_subscribe_subform_<?=$buttonId?>');

            if (!btn) return;

            function mailSender()
            {
                setTimeout(function() {
                    if (!btn) return;
                    btn.classList.add('subscriptionButton__send');
                }, 400);
            }

            BX.ready(function() {
                BX.bind(btn, 'click', function() {
                    setTimeout(mailSender, 250);
                    return false;
                });
            });

            BX.bind(form, 'submit', function () {
                btn.disabled = true;
                setTimeout(function () {
                    btn.disabled = false;
                }, 2000);
                return true;
            });
        })();
    </script>
    <section class="subscription">
        <div class="subscription__container limiter">
            <div class="subscription__content">
                <div class="subscription__info">
                    <div class="subscription__title"><?= Loc::getMessage('SUBSCRIBE_FORM_TITLE')?></div>
                    <div class="subscription__text"><?= Loc::getMessage('SUBSCRIBE_FORM_DESC')?></div>
                </div>
                <div class="subscription__form">
                    <form id="bx_subscribe_subform_<?=$buttonId?>" role="form" method="post" action="<?=$arResult["FORM_ACTION"]?>" class="form form_subscription">
                        <?=bitrix_sessid_post()?>
                        <input type="email" name="SENDER_SUBSCRIBE_EMAIL" value="<?=$arResult["EMAIL"]?>" class="input input_subscription" placeholder="<?=htmlspecialcharsbx(Loc::getMessage('SUBSCRIBE_FORM_EMAIL_TITLE'))?>">
                        <button id="bx_subscribe_btn_<?=$buttonId?>" class="subscriptionButton">
                            <svg class="icon icon-mail">
                                <use xlink:href="#icon-mail"></use>
                            </svg>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <?
    $frame->end();
    ?>
</div>