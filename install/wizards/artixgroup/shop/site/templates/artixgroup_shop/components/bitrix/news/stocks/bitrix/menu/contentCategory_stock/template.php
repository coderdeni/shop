<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult))
	return;
?>
<div class="contentCategory contentCategory_stock">
    <div class="contentCategory__content">
        <?
        foreach ($arResult as $item)
        {
            if ($item["DEPTH_LEVEL"] === 1)
            {
                ?>
                <div class="contentCategory__item">
                    <a href="<?= $item["LINK"]?>" class="contentCategory__link<?= $item['SELECTED'] ? ' contentCategory__link_active' : ''?>">
                        <?= htmlspecialcharsbx($item["TEXT"]) ?>
                    </a>
                </div>
                <?
            }
        }
        ?>
    </div>
</div>
