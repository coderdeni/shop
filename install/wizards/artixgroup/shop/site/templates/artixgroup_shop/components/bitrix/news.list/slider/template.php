<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="slider">
    <div class="slider__container limiter swiper-container">
        <div class="slider__main swiper-wrapper">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="slider__item swiper-slide">
                    <div class="slider__photo" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?= $arItem['NAME']?>" class="slider__img swiper-lazy">
                        <?
                        if ($arItem["DISPLAY_PROPERTIES"]["IMG_MOBILE"]["FILE_VALUE"])
                        {
                            ?>
                            <img src="<?= $arItem["DISPLAY_PROPERTIES"]["IMG_MOBILE"]["FILE_VALUE"]['SRC']?>" alt="<?= $arItem['NAME']?>" class="slider__img slider__img_mobile swiper-lazy">
                            <?
                        }
                        ?>
                        <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                    </div>
                </a>
            <?endforeach;?>
        </div>
        <div class="slider__pagination limiter"></div>
    </div>
</div>