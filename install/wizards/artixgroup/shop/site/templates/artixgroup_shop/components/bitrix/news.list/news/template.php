<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0)
{
    ?>
    <div class="b-news">
        <?
        foreach ($arResult["ITEMS"] as $key => $arItem)
        {
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="newsItem" id="<? echo $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="newsItem__category"><?= $arItem['SECTION']['NAME'] ?></div>
                <div class="newsItem__photo">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="newsItem__photoLink">
                        <img
                            alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                            title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                            src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" class="newsItem__img">
                    </a>
                </div>
                <div class="newsItem__content">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="newsItem__title"><?= $arItem["NAME"] ?></a>
                    <div class="newsItem__descr"><?= $arItem["PREVIEW_TEXT"] ?></div>
                    <div class="newsItem__info">
                        <div class="newsItem__date"><?= $arItem["TIMESTAMP_X"] ?></div>
                    </div>
                </div>
            </div>
            <?
        }
        ?>
    </div>
    <?
    if ($arParams["DISPLAY_BOTTOM_PAGER"])
    {
        echo $arResult["NAV_STRING"];
    }
}