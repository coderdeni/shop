<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
/** @global CMain $APPLICATION */

use Bitrix\Main\Localization\Loc;
?>
<div class="cabinetWidget">
    <div class="cabinetWidget__header">
        <div class="cabinetWidget__title"><i class="icon icon_orders"></i>Подписки</div>
        <a href="/personal/subscribe/" class="cabinetWidget__button">Посмотреть</a>
    </div>
    <div class="cabinetWidget__content">
        <div class="cabinetWidgetSubscriptions">
            <?
            if (!empty($arResult['ITEMS']))
            {
                foreach ($arResult['ITEMS'] as $key => $arItem)
                {
                    ?>
                    <a href="<?=$arItem['DETAIL_PAGE_URL'];?>" title="<?=$arItem['NAME'];?>" class="cabinetWidgetSubscriptions__item"><?=$arItem['NAME'];?></a>
                    <?
                }
            }
            else
            {
                if(isset($arParams['GUEST_ACCESS'])):
                    echo '<h3>'.Loc::getMessage('CPSL_SUBSCRIBE_NOT_FOUND').'</h3>';
                endif;
            }
            ?>
        </div>
    </div>
</div>