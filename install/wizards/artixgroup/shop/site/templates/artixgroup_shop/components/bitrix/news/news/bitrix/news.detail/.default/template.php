<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="articlePage">
	<div class="articlePage__container">
		<div class="articlePage__content">
			<div class="articlePage__header">
				<div class="articlePage__date"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></div>
				<div class="articlePage__category"><?= $arResult['SECTION']['PATH'][0]['NAME']?></div>
			</div>
			<div class="articlePage__photo">
				<img
					class="articlePage__img"
					src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
					alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
					title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>">
			</div>
			<div class="articlePage__text">
				<?echo $arResult["DETAIL_TEXT"];?>
			</div>
			<div class="share">
				<script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
				<script src="https://yastatic.net/share2/share.js"></script>
				<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,viber,whatsapp,skype,telegram"></div>
			</div>
			<?php
            if ($arResult['TAGS'])
            {
                $tags = explode(',', $arResult['TAGS']);
                ?>
                <div class="articlePage__tags">
                    <?php
                    foreach($tags as $tag)
                    {
                        ?><a href="/news/?tag=<?= urlencode(trim($tag)) ?>" class="articlePage__tag"><?= trim($tag)?></a><?
                    }
                    ?>
                </div>
                <?
            }
            ?>
			<div class="articlePage__close">
				<a href="<?= $arResult['SECTION_URL']?>" class="close">
                    <svg class="icon icon-close">
                        <use xlink:href="#icon-close"></use>
                    </svg>
                </a>
			</div>
		</div>
	</div>
</div>