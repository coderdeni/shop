<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

if ($arParams['SHOW_PROFILE_PAGE'] !== 'Y')
{
	LocalRedirect($arParams['SEF_FOLDER']);
}


if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_PROFILE"));
?>
<div class="cabinet">
	<div class="cabinet__left">
		<?require_once 'sidebar.php'?>
	</div>
	<div class="cabinet__right">
		<?
		$APPLICATION->IncludeComponent(
			"bitrix:sale.personal.profile.list",
			"",
			array(
				"PATH_TO_DETAIL" => $arResult['PATH_TO_PROFILE_DETAIL'],
				"PATH_TO_DELETE" => $arResult['PATH_TO_PROFILE_DELETE'],
				"PER_PAGE" => 20,
				"SET_TITLE" =>$arParams["SET_TITLE"],
			),
			$component
		);
		?>
	</div>
</div>