<?php
$MESS["subscr_form_title_desc"] = "Select Subscription";
$MESS["subscr_form_button"] = "Subscribe";
$MESS["subscr_form_response_ERROR"] = "Something's gone wrong.";
$MESS["subscr_form_response_NOTE"] = "Congratulations!";
$MESS["subscr_form_button_sent"] = "DONE";
$MESS['SUBSCRIBE_FORM_EMAIL_TITLE'] = "Enter your e-mail address";
$MESS['SUBSCRIBE_FORM_TITLE'] = "Always be the first to know about our discounts and promotions";
$MESS['SUBSCRIBE_FORM_DESC'] = "Subscribe to our newsletter and get exclusive promotions, offers and useful information from our company.";
