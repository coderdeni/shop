(function (window){
    window.AgActionTimer = function (params) {
        this.dateTo = params.dateTo;
        this.selectorTimerContainer = !!params.selectorTimerContainer ? params.selectorTimerContainer : 'timer_container';
        this.format = !!params.format ? params.format : '{0}д : {1}ч : {2}м : {3}с';

        BX.ready(BX.delegate(this.init, this));
    };

    window.AgActionTimer.prototype = {
        init: function()
        {
            if (!String.format)
            {
                String.format = function(format) {
                    var args = Array.prototype.slice.call(arguments, 1);
                    return format.replace(/{(\d+)}/g, function(match, number) {
                        return typeof args[number] != 'undefined'
                            ? args[number]
                            : match
                            ;
                    });
                };
            }

            this.timerContainer = document.querySelector(this.selectorTimerContainer);

            if (!!this.timerContainer && this.dateTo > 0)
            {
                setInterval(BX.delegate(this.updateTimer, this), 1000);
            }
        },
        updateTimer: function ()
        {
            var now = new Date();
            var interval = this.getInterval(now.getTime(), this.dateTo);
            this.timerContainer.innerText = String.format(this.format, interval.days, interval.hours, interval.minutes, interval.seconds);
        },
        getInterval: function (from, to)
        {
            var t = to - from;
            var seconds = Math.floor((t / 1000) % 60);
            var minutes = Math.floor((t / 1000 / 60) % 60);
            var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));

            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        },
    };
})(window);