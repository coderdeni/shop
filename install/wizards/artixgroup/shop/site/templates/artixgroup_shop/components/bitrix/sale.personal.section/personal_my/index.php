<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;


if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

$theme = Bitrix\Main\Config\Option::get("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);

$availablePages = array();

if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ORDERS'],
		"name" => Loc::getMessage("SPS_ORDER_PAGE_NAME"),
		"icon" => '<i class="fa fa-calculator"></i>'
	);
}

if ($arParams['SHOW_ACCOUNT_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ACCOUNT'],
		"name" => Loc::getMessage("SPS_ACCOUNT_PAGE_NAME"),
		"icon" => '<i class="fa fa-credit-card"></i>'
	);
}

if ($arParams['SHOW_PRIVATE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_PRIVATE'],
		"name" => Loc::getMessage("SPS_PERSONAL_PAGE_NAME"),
		"icon" => '<i class="fa fa-user-secret"></i>'
	);
}

if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
{

	$delimeter = ($arParams['SEF_MODE'] === 'Y') ? "?" : "&";
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ORDERS'].$delimeter."filter_history=Y",
		"name" => Loc::getMessage("SPS_ORDER_PAGE_HISTORY"),
		"icon" => '<i class="fa fa-list-alt"></i>'
	);
}

if ($arParams['SHOW_PROFILE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_PROFILE'],
		"name" => Loc::getMessage("SPS_PROFILE_PAGE_NAME"),
		"icon" => '<i class="fa fa-list-ol"></i>'
	);
}

if ($arParams['SHOW_BASKET_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arParams['PATH_TO_BASKET'],
		"name" => Loc::getMessage("SPS_BASKET_PAGE_NAME"),
		"icon" => '<i class="fa fa-shopping-cart"></i>'
	);
}

if ($arParams['SHOW_SUBSCRIBE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_SUBSCRIBE'],
		"name" => Loc::getMessage("SPS_SUBSCRIBE_PAGE_NAME"),
		"icon" => '<i class="fa fa-envelope"></i>'
	);
}

if ($arParams['SHOW_CONTACT_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arParams['PATH_TO_CONTACT'],
		"name" => Loc::getMessage("SPS_CONTACT_PAGE_NAME"),
		"icon" => '<i class="fa fa-info-circle"></i>'
	);
}

$customPagesList = CUtil::JsObjectToPhp($arParams['~CUSTOM_PAGES']);
if ($customPagesList)
{
	foreach ($customPagesList as $page)
	{
		$availablePages[] = array(
			"path" => $page[0],
			"name" => $page[1],
			"icon" => (strlen($page[2])) ? '<i class="fa '.htmlspecialcharsbx($page[2]).'"></i>' : ""
		);
	}
}

if (empty($availablePages))
{
	ShowError(Loc::getMessage("SPS_ERROR_NOT_CHOSEN_ELEMENT"));
}
else
{
	?>
    <div class="cabinet">
        <div class="cabinet__left">
            <div class="cabinetMenu">
                <div class="cabinetMenu__title">Личная информация</div>
                <?
                foreach ($availablePages as $blockElement)
                {
                    ?>
                    <?
                }
                ?>
                <div class="cabinetMenu__title">Личная информация</div>
                <div class="cabinetMenu__item"><a href="cabinetPersonal.html" class="cabinetMenu__link"><i class="icon icon_personal"></i>Личные данные</a></div>
                <div class="cabinetMenu__item"><a href="cabinetProfile.html" class="cabinetMenu__link"><i class="icon icon_profiles"></i>Мои адреса доставки</a></div>
                <div class="cabinetMenu__title">Заказы </div>
                <div class="cabinetMenu__item"><a href="cabinetOrders.html" class="cabinetMenu__link"><i class="icon icon_orders"></i>Мои заказы</a></div>
                <div class="cabinetMenu__item"><a href="cabinetBonus.html" class="cabinetMenu__link"><i class="icon icon_bonusCabinetDrop"></i>Мои бонусы</a></div>
                <div class="cabinetMenu__title">Подписки </div>
                <div class="cabinetMenu__item"><a href="favorites.html" class="cabinetMenu__link"><i class="icon icon_delayed"></i>Мои избранные</a></div>
                <div class="cabinetMenu__item"><a href="cabinetSubscribes.html" class="cabinetMenu__link"><i class="icon icon_notice"></i>Подписки</a></div>
                <div class="cabinetMenu__item cabinetMenu__item_exit"><a href="#" class="cabinetMenu__link"><i class="icon icon_exit"></i>Выход</a></div>
            </div>
        </div>
        <div class="cabinet__right">
            <div class="cabinetWidgets">
                <div class="cabinetWidget">
                    <div class="cabinetWidget__header">
                        <div class="cabinetWidget__title"><i class="icon icon_personal"></i>Личные данные</div>
                        <a href="cabinetOrders.html" class="cabinetWidget__button">Изменить</a>
                    </div>
                    <div class="cabinetWidget__content">
                        <div class="cabinetWidgetPersonal">
                            <div class="cabinetWidgetPersonal__title">Сергей Солодков</div>
                            <div class="cabinetWidgetPersonal__prop"><span class="cabinetWidgetPersonal__propTitle">Почта:</span> maxumus@mail.ru</div>
                            <div class="cabinetWidgetPersonal__prop"><span class="cabinetWidgetPersonal__propTitle">Телефон:</span> +7 918-****256</div>
                        </div>
                    </div>
                </div>
                <div class="cabinetWidget cabinetWidget_bonus">
                    <div class="cabinetWidget__header">
                        <div class="cabinetWidget__title"><i class="icon icon_bonus"></i>Мои бонусы</div>
                        <a href="cabinetOrders.html" class="cabinetWidget__button">Посмотреть</a>
                    </div>
                    <div class="cabinetWidget__content">
                        <div class="cabinetWidgetBonus">
                            <div class="cabinetWidgetBonus__title">На вашем счету</div>
                            <div class="cabinetWidgetBonus__count">0 бонусов</div>
                        </div>
                    </div>
                </div>
                <div class="cabinetWidget">
                    <div class="cabinetWidget__header">
                        <div class="cabinetWidget__title"><i class="icon icon_profiles"></i>Мои адреса</div>
                        <a href="cabinetOrders.html" class="cabinetWidget__button">Изменить</a>
                    </div>
                    <div class="cabinetWidget__content">
                        <div class="cabinetWidgetAdress">
                            <div class="cabinetWidgetAdress__item">
                                <div class="cabinetWidgetAdress__title">Адрес</div>
                                <div class="cabinetWidgetAdress__prop">г.Краснодар ул им Академика Лукьяненко П.П., дом 28, квартира 25</div>
                            </div>
                            <div class="cabinetWidgetAdress__item">
                                <div class="cabinetWidgetAdress__title">Адрес</div>
                                <div class="cabinetWidgetAdress__prop">г.Краснодар ул Красных Партизан, дом 1/3, корпус 25</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cabinetWidget">
                    <div class="cabinetWidget__header">
                        <div class="cabinetWidget__title"><i class="icon icon_orders"></i>Мои заказы</div>
                        <a href="cabinetOrders.html" class="cabinetWidget__button">Все заказы</a>
                    </div>
                    <div class="cabinetWidget__content">
                        <div class="cabinetWidgetOrders">
                            <div class="cabinetWidgetOrders__item">
                                <div class="cabinetWidgetOrders__itemLeft">
                                    <a href="cabinetOrderItem.html" class="cabinetWidgetOrders__title">Заказ № 1085</a>
                                    <div class="cabinetWidgetOrders__number">от 18 декабря 2018 года</div>
                                </div>
                                <div class="cabinetWidgetOrders__itemRight">
                                    <div class="cabinetWidgetOrders__price">520 <span class="ruble">руб.</span></div>
                                    <div class="cabinetWidgetOrders__status">Не оплачено</div>
                                </div>
                            </div>
                            <div class="cabinetWidgetOrders__item">
                                <div class="cabinetWidgetOrders__itemLeft">
                                    <a href="cabinetOrderItem.html" class="cabinetWidgetOrders__title">Заказ № 1084 </a>
                                    <div class="cabinetWidgetOrders__number">от 18 декабря 2018 года</div>
                                </div>
                                <div class="cabinetWidgetOrders__itemRight">
                                    <div class="cabinetWidgetOrders__price">900 <span class="ruble">руб.</span></div>
                                    <div class="cabinetWidgetOrders__status cabinetWidgetOrders__status_paid">Оплачено</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cabinetWidget">
                    <div class="cabinetWidget__header">
                        <div class="cabinetWidget__title"><i class="icon icon_orders"></i>Мои избранные</div>
                        <a href="cabinetOrders.html" class="cabinetWidget__button">Посмотреть</a>
                    </div>
                    <div class="cabinetWidget__content">
                        <div class="cabinetWidgetFavorites">
                            <a href="product.html" class="cabinetWidgetFavorites__item">Набор кукол Steffi Love Веселая прогулка Штеффи</a>
                            <a href="product.html" class="cabinetWidgetFavorites__item">Кукла-загадка Hairdorables Стильные подружки Серия 1</a>
                        </div>
                    </div>
                </div>
                <div class="cabinetWidget">
                    <div class="cabinetWidget__header">
                        <div class="cabinetWidget__title"><i class="icon icon_orders"></i>Подписки</div>
                        <a href="cabinetOrders.html" class="cabinetWidget__button">Посмотреть</a>
                    </div>
                    <div class="cabinetWidget__content">
                        <div class="cabinetWidgetSubscriptions">
                            <a href="product.html" class="cabinetWidgetSubscriptions__item">Набор кукол Steffi Love Веселая прогулка Штеффи</a>
                            <a href="product.html" class="cabinetWidgetSubscriptions__item">Кукла-загадка Hairdorables Стильные подружки Серия 1</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


	<div class="row">
		<div class="col-md-12 sale-personal-section-index">
			<div class="row sale-personal-section-row-flex">
				<?
				foreach ($availablePages as $blockElement)
				{
					?>
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="sale-personal-section-index-block bx-theme-<?=$theme?>">
							<a class="sale-personal-section-index-block-link" href="<?=htmlspecialcharsbx($blockElement['path'])?>">
								<span class="sale-personal-section-index-block-ico">
									<?=$blockElement['icon']?>
								</span>
								<h2 class="sale-personal-section-index-block-name">
									<?=htmlspecialcharsbx($blockElement['name'])?>
								</h2>
							</a>
						</div>
					</div>
					<?
				}
				?>
			</div>
		</div>
	</div>
	<?
}
?>
