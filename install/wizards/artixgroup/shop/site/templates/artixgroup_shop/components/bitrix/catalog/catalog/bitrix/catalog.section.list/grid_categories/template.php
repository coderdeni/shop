<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="grid grid_categories">
    <div class="grid__wrapper">
        <div class="grid__inner">
            <div class="grid__row">
                <?php
                foreach ($arResult['SECTIONS'] as &$arSection)
                {
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                    ?>
                    <div class="grid__item" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                        <div class="categories">
                            <div class="categories__photo">
                                <a class="link link_img" href="<? echo $arSection['SECTION_PAGE_URL']; ?>">
                                    <img class="categories__img" alt="<? echo $arSection['NAME']; ?>" src="<? echo $arSection['PICTURE']['SRC']; ?>">
                                </a>
                            </div>
                            <div class="categories__title">
                                <a class="link link_categories" href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>