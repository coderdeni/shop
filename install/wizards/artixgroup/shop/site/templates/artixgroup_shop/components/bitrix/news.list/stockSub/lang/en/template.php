<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS['DATE'] = 'from #DATE_FROM#';
$MESS['PERIOD'] = '#DATE_FROM# to #DATE_TO#';
$MESS['FORMAT_TIMER'] = '{0}d : {1}h : {2}m : {3}s';
?>