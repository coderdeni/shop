<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"HASHTAG" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_HASHTAG"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"DESCRIPTION" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DESCRIPTION"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
    "INSTAGRAM_URL" => Array(
        "NAME" => GetMessage("T_IBLOCK_DESC_NEWS_INSTAGRAM_URL"),
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
);
