<?

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)).'/top_template.php');

if ($arParams["SHOW_PRODUCTS"] == "Y" && $arResult['NUM_PRODUCTS'] > 0)
{
    ?>
	<div data-role="basket-item-list" class="dropdown dropdown_cartDropdown">
        <div class="dropdown__content">
            <div id="<?=$cartId?>products" class="cartDropdown">
                <div class="close"></div>
                <div class="title title_small"><?= Loc::getMessage('TSB1_CART')?></div>
                <?
                foreach ($arResult["CATEGORIES"] as $category => $items)
                {
                    if (empty($items))
                        continue;

                    foreach ($items as $v)
                    {
                        ?>
                        <div class="cartDropdown__item">
                            <div class="cartDropdown__photo">
                                <?
                                if ($arParams["SHOW_IMAGE"] == "Y" && $v["PICTURE_SRC"])
                                {
                                    if ($v["DETAIL_PAGE_URL"])
                                    {
                                        ?>
                                        <a class="link link_img" href="<?= $v["DETAIL_PAGE_URL"] ?>">
                                            <img src="<?= $v["PICTURE_SRC"] ?>" alt="<?= $v["NAME"] ?>" class="cartDropdown__img">
                                        </a>
                                        <?
                                    }
                                    else
                                    {
                                        ?>
                                        <img src="<?=$v["PICTURE_SRC"]?>" alt="<?= $v["NAME"] ?>" class="cartDropdown__img" />
                                        <?
                                    }
                                }
                                ?>
                            </div>
                            <div class="cartDropdown__info">
                                <div class="cartDropdown__title">
                                    <?
                                    if ($v["DETAIL_PAGE_URL"])
                                    {
                                        ?>
                                        <a class="link link_cartDropdownName" href="<?=$v["DETAIL_PAGE_URL"]?>">
                                            <?=$v["NAME"]?>
                                        </a>
                                        <?
                                    }
                                    else
                                    {
                                        echo $v["NAME"];
                                    }
                                    ?>
                                </div>
                                <?php if (true):/*$category != "SUBSCRIBE") TODO */?>
                                    <?php if ($arParams["SHOW_PRICE"] == "Y"): ?>
                                        <div class="cartDropdown__price"><?= $v["SUM"] ?></div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?
                    }
                }
                ?>
                <div class="cartDropdown__button">
                    <div class="cartDropdown__buttonCell">
                        <div class="cartDropdown__info_itog"><?= Loc::getMessage('TSB1_RESULT')?></div>
                        <div class="cartDropdown__price cartDropdown__price_big">
                            <?= $arResult['TOTAL_PRICE'] ?>
                        </div>
                    </div>
                    <div class="cartDropdown__buttonCell">
                        <a href="<?=$arParams["PATH_TO_BASKET"]?>" class="button button_issue">
                            <?= Loc::getMessage('TSB1_2CART')?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
	</div>

	<script>
		BX.ready(function(){
			<?=$cartId?>.fixCart();
		});
	</script>
    <?
}