<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$arWaterMark = Array(
    array(
        "name" => "watermark",
        "position" => "center", // Положение
        "type" => "image",
        "size" => "real",
        "file" => $_SERVER["DOCUMENT_ROOT"].'/upload/watermark.png', // Путь к картинке
        "fill" => "exact",
    )
);

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$sections = \Bitrix\Iblock\SectionTable::getList(array(
    'filter' => array('<=LEFT_MARGIN' => $arResult['SECTION']['LEFT_MARGIN'], '>=RIGHT_MARGIN' => $arResult['SECTION']['LEFT_MARGIN']),
    'order' => array("LEFT_MARGIN")
))->fetchAll();

$arResult['BRAND'] = [
    'XML_ID' => $arResult['DISPLAY_PROPERTIES']['BRAND_REF']['VALUE'],
    'NAME' => $arResult['DISPLAY_PROPERTIES']['BRAND_REF']['DISPLAY_VALUE'],
    'PIC' => ''
];

$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(3)->fetch(); // получаем объект вашего HL блока
$entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);  // получаем рабочую сущность
$entity_data_class = $entity->getDataClass(); // получаем экземпляр класса
$brand = $entity_data_class::getRow(array(
    "filter" => ['UF_XML_ID' => $arResult['BRAND']['XML_ID']],
));
if ($brand['UF_FILE'])
{
    $file = CFile::ResizeImageGet($brand['UF_FILE'], array("width" => 80, "height" => 43), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    $arResult['BRAND']['PIC'] = array_change_key_case($file, CASE_UPPER);;

    //$arResult['DISPLAY_PROPERTIES']['BREND']['SRC'] = '/brands/'.$brand['UF_XML_ID'].'/';
}

unset($morePhoto);