<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Artixgroup\Shop\Favourites\Favourites;
use Artixgroup\Shop\Setting\Parameters;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;


if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}
?>
<div class="cabinet">
    <div class="cabinet__left">
        <?include_once 'sidebar.php'?>
    </div>
    <div class="cabinet__right">
        <div class="cabinetWidgets">
            <div class="cabinetWidget">
                <div class="cabinetWidget__header">
                    <div class="cabinetWidget__title">
                        <svg class="icon icon-personal">
                            <use xlink:href="#icon-personal"></use>
                        </svg>
                        <?= Loc::getMessage('SPS_PERSONAL_PAGE_NAME')?>
                    </div>
                    <a href="<?= $arResult['PATH_TO_PRIVATE']?>" class="cabinetWidget__button"><?= Loc::getMessage('SPS_EDIT')?></a>
                </div>
                <div class="cabinetWidget__content">
                    <div class="cabinetWidgetPersonal">
                        <div class="cabinetWidgetPersonal__title"><?= $USER->GetFullName()?></div>
                        <div class="cabinetWidgetPersonal__prop">
                            <span class="cabinetWidgetPersonal__propTitle"><?= Loc::getMessage('SPS_EMAIL')?>:</span>
                            <?= $USER->GetEmail()?>
                        </div>
                        <div class="cabinetWidgetPersonal__prop">
                            <span class="cabinetWidgetPersonal__propTitle"><?= Loc::getMessage('SPS_PHONE')?>:</span>
                            <?= $USER->GetParam('PERSONAL_MOBILE')?>
                        </div>
                    </div>
                </div>
            </div>
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:sale.personal.account",
                "cabinetWidgetBonus",
                Array(
                    "SET_TITLE" => "N"
                ),
                $component
            );?>
            <div class="cabinetWidget">
                <div class="cabinetWidget__header">
                    <div class="cabinetWidget__title">
                        <svg class="icon icon-profiles">
                            <use xlink:href="#icon-profiles"></use>
                        </svg>
                        <?= Loc::getMessage('SPS_PROFILE_PAGE_NAME')?>
                    </div>
                    <a href="<?= $arResult['PATH_TO_PROFILE']?>" class="cabinetWidget__button"><?= Loc::getMessage('SPS_EDIT')?></a>
                </div>
            </div>
            <?
            if (Loader::includeModule('sale'))
            {
                $orders = \Bitrix\Sale\OrderTable::getList([
                    'select' => ['ID', 'DATE_INSERT', 'PAYED', 'DATE_PAYED', 'CANCELED', 'DATE_CANCELED', 'PRICE', 'CURRENCY', "STATUS_ID", "DATE_STATUS", "LID"],
                    'filter' => ["USER_ID" => $USER->GetID()],
                    'limit' => 2,
                    'order' => ['DATE_INSERT' => 'DESC']
                ])->fetchAll();
            }
            ?>
            <div class="cabinetWidget">
                <div class="cabinetWidget__header">
                    <div class="cabinetWidget__title">
                        <svg class="icon icon-orders">
                            <use xlink:href="#icon-orders"></use>
                        </svg>
                        <?= Loc::getMessage('SPS_ORDER_PAGE_NAME')?>
                    </div>
                    <a href="<?= $arResult['PATH_TO_ORDERS']?>" class="cabinetWidget__button"><?= Loc::getMessage('SPS_ALL_ORDERS')?></a>
                </div>
                <div class="cabinetWidget__content">
                    <div class="cabinetWidgetOrders">
                        <?
                        foreach ($orders as $order)
                        {
                            ?>
                            <div class="cabinetWidgetOrders__item">
                                <div class="cabinetWidgetOrders__itemLeft">
                                    <a href="/personal/orders/<?= $order['ID']?>/" class="cabinetWidgetOrders__title">Заказ № <?= $order['ID']?></a>
                                    <div class="cabinetWidgetOrders__number"><?= strtolower(FormatDate('от j F Y года')) ?></div>
                                </div>
                                <div class="cabinetWidgetOrders__itemRight">
                                    <div class="cabinetWidgetOrders__price"><?= CurrencyFormat($order['PRICE'], $order['CURRENCY'])?></div>
                                    <div class="cabinetWidgetOrders__status"><?= $order['PAYED'] ? 'Оплачено' : 'Не оплачено'?></div>
                                </div>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?
            $favorites = Favourites::getInstance()->getItems();

            if ($favorites)
            {
                $GLOBALS['favoritesFilter'] = ['ID' => $favorites];
                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "cabinetWidgetFavorites",
                    Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array("", ""),
                        "FILTER_NAME" => "favoritesFilter",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => Parameters::getInstance()->getInt('catalog_id'),
                        "IBLOCK_TYPE" => "catalog",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "2",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array("", ""),
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N"
                    ),
                    $component
                );
            }
            ?>
            <?
            $APPLICATION->IncludeComponent(
                'bitrix:catalog.product.subscribe.list',
                '',
                array('SET_TITLE' => $arParams['SET_TITLE'])
                ,
                $component
            );
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:sale.personal.profile.list",
                "widget",
                array(
                    "PATH_TO_DETAIL" => $arResult['PATH_TO_PROFILE_DETAIL'],
                    "PATH_TO_DELETE" => $arResult['PATH_TO_PROFILE_DELETE'],
                    "PER_PAGE" => $arParams["PROFILES_PER_PAGE"],
                    "SET_TITLE" =>$arParams["SET_TITLE"],
                ),
                $component
            );
            ?>
        </div>
    </div>
</div>
<?
