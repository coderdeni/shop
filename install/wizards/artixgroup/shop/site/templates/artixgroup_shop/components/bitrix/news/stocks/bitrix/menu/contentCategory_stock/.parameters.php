<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters['IS_LEFT'] = array(
    'PARENT' => 'ADDITIONAL_SETTINGS',
    'NAME' => GetMessage('CP_BC_TPL_IS_LEFT'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
);