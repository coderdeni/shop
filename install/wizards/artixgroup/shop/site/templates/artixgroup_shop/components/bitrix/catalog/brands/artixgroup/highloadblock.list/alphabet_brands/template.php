<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
	echo $arResult['ERROR'];
	return false;
}

$letters = [
    "a" => "en--a",
    "b" => "en--b",
    "c" => "en--c",
    "d" => "en--d",
    "e" => "en--e",
    "f" => "en--f",
    "g" => "en--g",
    "h" => "en--h",
    "i" => "en--i",
    "j" => "en--j",
    "k" => "en--k",
    "l" => "en--l",
    "m" => "en--m",
    "n" => "en--n",
    "o" => "en--o",
    "p" => "en--p",
    "q" => "en--q",
    "r" => "en--r",
    "s" => "en--s",
    "t" => "en--t",
    "u" => "en--u",
    "v" => "en--v",
    "w" => "en--w",
    "x" => "en--x",
    "y" => "en--y",
    "z" => "en--z",
    "0" => "nums--digits",
    "1" => "nums--digits",
    "2" => "nums--digits",
    "3" => "nums--digits",
    "4" => "nums--digits",
    "5" => "nums--digits",
    "6" => "nums--digits",
    "7" => "nums--digits",
    "8" => "nums--digits",
    "9" => "nums--digits",
    "а" => "ru--a",
    "б" => "ru--b",
    "в" => "ru--v",
    "г" => "ru--g",
    "д" => "ru--d",
    "е" => "ru--e",
    "ё" => "ru--ie",
    "ж" => "ru--zh",
    "з" => "ru--z",
    "и" => "ru--i",
    "й" => "ru--y",
    "к" => "ru--k",
    "л" => "ru--l",
    "м" => "ru--m",
    "н" => "ru--n",
    "о" => "ru--o",
    "п" => "ru--p",
    "р" => "ru--r",
    "с" => "ru--s",
    "т" => "ru--t",
    "у" => "ru--u",
    "ф" => "ru--f",
    "х" => "ru--h",
    "ц" => "ru--ts",
    "ч" => "ru--ch",
    "ш" => "ru--sh",
    "щ" => "ru--shch",
    "ъ" => "ru--thd",
    "ы" => "ru--iy",
    "ь" => "ru--tsf",
    "э" => "ru--ee",
    "ю" => "ru--yu",
    "я" => "ru--ya",
];
?>
<div class="brands__letter">
    <div class="brandsLetter">
        <div class="title">Все бренды</div>
        <div class="brandsLetter__item">
            <a href="/brands/" class="brandsLetter__link"> Все </a>
            <a href="/brands/letter/en--a/" class="brandsLetter__link"> A </a>
            <a href="/brands/letter/en--b/" class="brandsLetter__link"> B </a>
            <a href="/brands/letter/en--c/" class="brandsLetter__link"> C </a>
            <a href="/brands/letter/en--d/" class="brandsLetter__link"> D </a>
            <a href="/brands/letter/en--e/" class="brandsLetter__link"> E </a>
            <a href="/brands/letter/en--f/" class="brandsLetter__link"> F </a>
            <a href="/brands/letter/en--g/" class="brandsLetter__link"> G </a>
            <a href="/brands/letter/en--h/" class="brandsLetter__link"> H </a>
            <a href="/brands/letter/en--i/" class="brandsLetter__link"> I </a>
            <a href="/brands/letter/en--j/" class="brandsLetter__link"> J </a>
            <a href="/brands/letter/en--k/" class="brandsLetter__link"> K </a>
            <a href="/brands/letter/en--l/" class="brandsLetter__link"> L </a>
            <a href="/brands/letter/en--m/" class="brandsLetter__link"> M </a>
            <a href="/brands/letter/en--n/" class="brandsLetter__link"> N </a>
            <a href="/brands/letter/en--o/" class="brandsLetter__link"> O </a>
            <a href="/brands/letter/en--p/" class="brandsLetter__link"> P </a>
            <a href="/brands/letter/en--q/" class="brandsLetter__link"> Q </a>
            <a href="/brands/letter/en--r/" class="brandsLetter__link"> R </a>
            <a href="/brands/letter/en--s/" class="brandsLetter__link"> S </a>
            <a href="/brands/letter/en--t/" class="brandsLetter__link"> T </a>
            <a href="/brands/letter/en--u/" class="brandsLetter__link"> U </a>
            <a href="/brands/letter/en--v/" class="brandsLetter__link"> V </a>
            <a href="/brands/letter/en--w/" class="brandsLetter__link"> W </a>
            <a href="/brands/letter/en--x/" class="brandsLetter__link"> X </a>
            <a href="/brands/letter/en--y/" class="brandsLetter__link"> Y </a>
            <a href="/brands/letter/en--z/" class="brandsLetter__link"> Z </a>
            <a href="/brands/letter/nums--digits/" class="brandsLetter__link"> 0-9 </a>
        </div>
        <div class="brandsLetter__item">
            <a href="/brands/letter/ru--a/" class="brandsLetter__link"> А </a>
            <a href="/brands/letter/ru--b/" class="brandsLetter__link"> Б </a>
            <a href="/brands/letter/ru--v/" class="brandsLetter__link"> В </a>
            <a href="/brands/letter/ru--g/" class="brandsLetter__link"> Г </a>
            <a href="/brands/letter/ru--d/" class="brandsLetter__link"> Д </a>
            <a href="/brands/letter/ru--e/" class="brandsLetter__link"> Е </a>
            <a href="/brands/letter/ru--ie/" class="brandsLetter__link"> Ё </a>
            <a href="/brands/letter/ru--zh/" class="brandsLetter__link"> Ж </a>
            <a href="/brands/letter/ru--z/" class="brandsLetter__link"> З </a>
            <a href="/brands/letter/ru--i/" class="brandsLetter__link"> И </a>
            <a href="/brands/letter/ru--y/" class="brandsLetter__link"> Й </a>
            <a href="/brands/letter/ru--k/" class="brandsLetter__link"> К </a>
            <a href="/brands/letter/ru--l/" class="brandsLetter__link"> Л </a>
            <a href="/brands/letter/ru--m/" class="brandsLetter__link"> М </a>
            <a href="/brands/letter/ru--n/" class="brandsLetter__link"> Н </a>
            <a href="/brands/letter/ru--o/" class="brandsLetter__link"> О </a>
            <a href="/brands/letter/ru--p/" class="brandsLetter__link"> П </a>
            <a href="/brands/letter/ru--r/" class="brandsLetter__link"> Р </a>
            <a href="/brands/letter/ru--s/" class="brandsLetter__link"> С </a>
            <a href="/brands/letter/ru--t/" class="brandsLetter__link"> Т </a>
            <a href="/brands/letter/ru--u/" class="brandsLetter__link"> У </a>
            <a href="/brands/letter/ru--f/" class="brandsLetter__link"> Ф </a>
            <a href="/brands/letter/ru--h/" class="brandsLetter__link"> Х </a>
            <a href="/brands/letter/ru--ts/" class="brandsLetter__link"> Ц </a>
            <a href="/brands/letter/ru--ch/" class="brandsLetter__link"> Ч </a>
            <a href="/brands/letter/ru--sh/" class="brandsLetter__link"> Ш </a>
            <a href="/brands/letter/ru--shch/" class="brandsLetter__link"> Щ </a>
            <a href="/brands/letter/ru--thd/" class="brandsLetter__link"> Ъ </a>
            <a href="/brands/letter/ru--iy/" class="brandsLetter__link"> Ы </a>
            <a href="/brands/letter/ru--tsf/" class="brandsLetter__link"> Ь </a>
            <a href="/brands/letter/ru--ee/" class="brandsLetter__link"> Э </a>
            <a href="/brands/letter/ru--yu/" class="brandsLetter__link"> Ю </a>
            <a href="/brands/letter/ru--ya/" class="brandsLetter__link"> Я </a>
        </div>
    </div>
    <?
    $lastLatter = null;
    foreach ($arResult['rows'] as $row)
    {
        $letter = strtolower(mb_substr($row['UF_NAME'],0,1,"UTF-8"));

        if ($letter !== $lastLatter)
        {
            if ($lastLatter !== null)
            {
                ?></div></div><?
            }

            $lastLatter = $letter;
            ?>
            <div class="brandsItems">
                <a href="/brands/letter/<?= $letters[$letter]?>/" class="brandsItems__title"><?= strtoupper($letter)?></a>
                <div class="brandsItems__content">
            <?
        }
        ?>
        <a href="/brands/<?= $row['UF_XML_ID']?>/" class="brandsItems__link"><?= $row['UF_NAME']?></a>
        <?
    }
    ?>
    </div></div>
</div>