<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$this->addExternalJs(SITE_TEMPLATE_PATH . '/js/nouislider/nouislider.min.js');
$this->addExternalCss(SITE_TEMPLATE_PATH . '/js/nouislider/nouislider.min.css');
?>
<div class="smartfilter">
    <div class="smartfilter__buttons">
        <button class="smartfilter__button js-toggle-class" data-target=".smartfilter__content, body" data-class="visible">
            <i class="smartfilter__button-icon">
                <svg class="icon icon-filter">
                    <use xlink:href="#icon-filter"></use>
                </svg>
            </i>
            Подбор по параметрам
        </button>
    </div>
    <div class="smartfilter__content">
        <div class="smartfilter__title">Фильтры</div>
        <?/*<button class="close close_smartfilter js-toggle-class" data-target=".smartfilter__content, body" data-class="visible">
            <svg class="icon icon-close">
                <use xlink:href="#icon-close"></use>
            </svg>
        </button>*/?>
        <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter__row">
            <?foreach($arResult["HIDDEN"] as $arItem):?>
                <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
            <?endforeach;?>
            <?
            foreach ($arResult["ITEMS"] as $key => $arItem)//prices
            {
                $key = $arItem["ENCODED_ID"];
                if(isset($arItem["PRICE"])):
                    if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                        continue;

                    $step_num = 4;
                    $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
                    $prices = array();
                    if (Bitrix\Main\Loader::includeModule("currency"))
                    {
                        for ($i = 0; $i < $step_num; $i++)
                        {
                            $prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
                        }
                        $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
                    }
                    else
                    {
                        $precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
                        for ($i = 0; $i < $step_num; $i++)
                        {
                            $prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $precision, ".", "");
                        }
                        $prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                    }
                    ?>
                    <div class="smartfilter-select">
                        <div class="smartfilter-select__placeholder" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
                            <div class="smartfilter-select__placeholder-value"><?= $arItem['NAME']?></div>
                            <svg class="smartfilter-select__placeholder-icon" width="12" height="12" viewBox="0 0 18 10" xmlns="http://www.w3.org/2000/svg"><path d="M0.692347 0.666666C1.07469 0.298477 1.6946 0.298477 2.07694 0.666667L10.3845 8.66666L8.99991 10L0.692348 2C0.310003 1.63181 0.310002 1.03486 0.692347 0.666666Z"></path><path d="M7.6156 8.66699L15.9232 0.666997C16.3055 0.298807 16.9254 0.298806 17.3078 0.666996C17.6901 1.03519 17.6901 1.63214 17.3078 2.00033L9.00019 10.0003L7.6156 8.66699Z"></path></svg>
                        </div>
                        <div class="smartfilter-select__dropdown">
                            <div class="smartfilter-select__option">
                                <div id="price-slider-<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>" style="width:100%!important;"></div>
                            </div>
                            <div class="smartfilter-select__option">
                                <input
                                        class="input input_midle"
                                        type="text"
                                        name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                        id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                        value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
                                        size="5"
                                        onkeyup="smartFilter.keyup(this)"
                                />
                                <input
                                        class="input input_midle"
                                        type="text"
                                        name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                        id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                        value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
                                        size="5"
                                        onkeyup="smartFilter.keyup(this)"
                                />
                                <script type="text/javascript">
                                    <?
                                    $sliderVar = 'slider' . $arItem["VALUES"]["MIN"]["CONTROL_ID"];
                                    $inputMinVar = 'input' . $arItem["VALUES"]["MIN"]["CONTROL_ID"];
                                    $inputMaxVar = 'input' . $arItem["VALUES"]["MAX"]["CONTROL_ID"];
                                    ?>
                                    var <?= $sliderVar?> = document.getElementById('price-slider-<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>');
                                    noUiSlider.create(slider<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>, {
                                        start: [
                                            <?= intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"]?>,
                                            <?= intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"]?>
                                        ],
                                        connect: true,
                                        range: {
                                            'min': <?= $arItem["VALUES"]["MIN"]["VALUE"]?>,
                                            'max': <?= $arItem["VALUES"]["MAX"]["VALUE"]?>
                                        }
                                    });

                                    var <?= $inputMinVar?> = document.getElementById('<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>');
                                    var <?= $inputMaxVar?> = document.getElementById('<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>');
                                    <?= $sliderVar?>.noUiSlider.on('update', function (values, handle) {
                                        var value = values[handle];
                                        if (handle) {
                                            <?= $inputMaxVar?>.value = value;
                                            if (typeof(smartFilter) != "undefined")
                                                smartFilter.keyup(<?= $inputMaxVar?>);
                                        } else {
                                            <?= $inputMinVar?>.value = Math.round(value);
                                            if (typeof(smartFilter) != "undefined")
                                                smartFilter.keyup(<?= $inputMinVar?>);
                                        }
                                    });
                                    <?= $inputMinVar?>.addEventListener('change', function () {
                                        <?= $sliderVar?>.noUiSlider.set([this.value, null]);
                                    });
                                    <?= $inputMaxVar?>.addEventListener('change', function () {
                                        <?= $sliderVar?>.noUiSlider.set([null, this.value]);
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                <?endif;
            }

            //not prices
            foreach ($arResult["ITEMS"] as $key=>$arItem)
            {
                if(
                    empty($arItem["VALUES"])
                    || isset($arItem["PRICE"])
                )
                    continue;

                if (
                    $arItem["DISPLAY_TYPE"] == "A"
                    && (
                        $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                    )
                )
                    continue;
                ?>

                <?
                $arCur = current($arItem["VALUES"]);
                switch ($arItem["DISPLAY_TYPE"])
                {
                    case "A"://NUMBERS_WITH_SLIDER
                        ?>
                        <div class="smartfilter-select">
                            <div class="smartfilter-select__placeholder">
                                <div class="smartfilter-select__placeholder-value"><?= $arItem['NAME']?></div>
                                <svg class="smartfilter-select__placeholder-icon" width="12" height="12" viewBox="0 0 18 10" xmlns="http://www.w3.org/2000/svg"><path d="M0.692347 0.666666C1.07469 0.298477 1.6946 0.298477 2.07694 0.666667L10.3845 8.66666L8.99991 10L0.692348 2C0.310003 1.63181 0.310002 1.03486 0.692347 0.666666Z"></path><path d="M7.6156 8.66699L15.9232 0.666997C16.3055 0.298807 16.9254 0.298806 17.3078 0.666996C17.6901 1.03519 17.6901 1.63214 17.3078 2.00033L9.00019 10.0003L7.6156 8.66699Z"></path></svg>
                            </div>
                            <div class="smartfilter-select__dropdown">
                                <div class="smartfilter-select__option">
                                    <div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
                                        <i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
                                        <div class="bx-filter-input-container">
                                            <input
                                                class="min-price"
                                                type="text"
                                                name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                                id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                                value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
                                                size="5"
                                                onkeyup="smartFilter.keyup(this)"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
                                        <i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
                                        <div class="bx-filter-input-container">
                                            <input
                                                class="max-price"
                                                type="text"
                                                name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                                id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                                value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
                                                size="5"
                                                onkeyup="smartFilter.keyup(this)"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-xs-10 col-xs-offset-1 bx-ui-slider-track-container">
                                        <div class="bx-ui-slider-track" id="drag_track_<?=$key?>">
                                            <?
                                            $precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
                                            $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
                                            $value1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
                                            $value2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
                                            $value3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
                                            $value4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
                                            $value5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                                            ?>
                                            <div class="bx-ui-slider-part p1"><span><?=$value1?></span></div>
                                            <div class="bx-ui-slider-part p2"><span><?=$value2?></span></div>
                                            <div class="bx-ui-slider-part p3"><span><?=$value3?></span></div>
                                            <div class="bx-ui-slider-part p4"><span><?=$value4?></span></div>
                                            <div class="bx-ui-slider-part p5"><span><?=$value5?></span></div>

                                            <div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
                                            <div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
                                            <div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
                                            <div class="bx-ui-slider-range" 	id="drag_tracker_<?=$key?>"  style="left: 0;right: 0;">
                                                <a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
                                                <a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                    $arJsParams = array(
                                        "leftSlider" => 'left_slider_'.$key,
                                        "rightSlider" => 'right_slider_'.$key,
                                        "tracker" => "drag_tracker_".$key,
                                        "trackerWrap" => "drag_track_".$key,
                                        "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                                        "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                                        "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                                        "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                                        "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                                        "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                                        "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
                                        "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                                        "precision" => $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0,
                                        "colorUnavailableActive" => 'colorUnavailableActive_'.$key,
                                        "colorAvailableActive" => 'colorAvailableActive_'.$key,
                                        "colorAvailableInactive" => 'colorAvailableInactive_'.$key,
                                    );
                                    ?>
                                    <script type="text/javascript">
                                        BX.ready(function(){
                                            window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <?
                        break;
                    case "B"://NUMBERS
                        ?>
                        <div class="smartfilter-select">
                            <div class="smartfilter-select__placeholder">
                                <div class="smartfilter-select__placeholder-value"><?= $arItem['NAME']?></div>
                                <svg class="smartfilter-select__placeholder-icon" width="12" height="12" viewBox="0 0 18 10" xmlns="http://www.w3.org/2000/svg"><path d="M0.692347 0.666666C1.07469 0.298477 1.6946 0.298477 2.07694 0.666667L10.3845 8.66666L8.99991 10L0.692348 2C0.310003 1.63181 0.310002 1.03486 0.692347 0.666666Z"></path><path d="M7.6156 8.66699L15.9232 0.666997C16.3055 0.298807 16.9254 0.298806 17.3078 0.666996C17.6901 1.03519 17.6901 1.63214 17.3078 2.00033L9.00019 10.0003L7.6156 8.66699Z"></path></svg>
                            </div>
                            <div class="smartfilter-select__dropdown">
                                <div class="smartfilter-select__option">
                                    <div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
                                        <i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
                                        <div class="bx-filter-input-container">
                                            <input
                                                class="min-price"
                                                type="text"
                                                name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                                id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                                value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
                                                size="5"
                                                onkeyup="smartFilter.keyup(this)"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
                                        <i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
                                        <div class="bx-filter-input-container">
                                            <input
                                                class="max-price"
                                                type="text"
                                                name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                                id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                                value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
                                                size="5"
                                                onkeyup="smartFilter.keyup(this)"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?
                        break;
                    case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
                    case "P"://DROPDOWN
                        $checkedItemExist = false;
                        ?>
                        <div class="smartfilter-select">
                            <div class="smartfilter-select__placeholder" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
                                <input
                                        style="display: none"
                                        type="radio"
                                        name="<?=$arCur["CONTROL_NAME_ALT"]?>"
                                        id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
                                        class="select-box__input"
                                        value=""
                                />
                                <div class="smartfilter-select__placeholder-value">
                                    <?
                                    foreach ($arItem["VALUES"] as $val => $ar)
                                    {
                                        if ($ar["CHECKED"])
                                        {
                                            echo $ar["VALUE"];
                                            $checkedItemExist = true;
                                        }
                                    }
                                    if (!$checkedItemExist)
                                    {
                                        echo GetMessage("CT_BCSF_FILTER_ALL");
                                    }
                                    ?>
                                </div>
                                <svg class="smartfilter-select__placeholder-icon" width="12" height="12" viewBox="0 0 18 10" xmlns="http://www.w3.org/2000/svg"><path d="M0.692347 0.666666C1.07469 0.298477 1.6946 0.298477 2.07694 0.666667L10.3845 8.66666L8.99991 10L0.692348 2C0.310003 1.63181 0.310002 1.03486 0.692347 0.666666Z"></path><path d="M7.6156 8.66699L15.9232 0.666997C16.3055 0.298807 16.9254 0.298806 17.3078 0.666996C17.6901 1.03519 17.6901 1.63214 17.3078 2.00033L9.00019 10.0003L7.6156 8.66699Z"></path></svg>
                            </div>
                            <div class="smartfilter-select__dropdown">
                            <?
                            foreach ($arItem["VALUES"] as $val => $ar):
                                $class = "";
                                if ($ar["CHECKED"])
                                    $class.= " selected";
                                if ($ar["DISABLED"])
                                    $class.= " disabled";
                                ?>
                                <div class="smartfilter-select__option">
                                    <input
                                        style="display: none"
                                        type="radio"
                                        name="<?=$ar["CONTROL_NAME_ALT"]?>"
                                        id="<?=$ar["CONTROL_ID"]?>"
                                        value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                    />
                                    <label for="<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
                                </div>
                            <?endforeach?>
                            </div>
                        </div>
                        <?
                        break;
                    case "K"://RADIO_BUTTONS
                        ?>
                        <div class="smartfilter-select">
                            <div class="smartfilter-select__placeholder" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
                                <div class="smartfilter-select__placeholder-value"><?= $arItem['NAME']?></div>
                                <svg class="smartfilter-select__placeholder-icon" width="12" height="12" viewBox="0 0 18 10" xmlns="http://www.w3.org/2000/svg"><path d="M0.692347 0.666666C1.07469 0.298477 1.6946 0.298477 2.07694 0.666667L10.3845 8.66666L8.99991 10L0.692348 2C0.310003 1.63181 0.310002 1.03486 0.692347 0.666666Z"></path><path d="M7.6156 8.66699L15.9232 0.666997C16.3055 0.298807 16.9254 0.298806 17.3078 0.666996C17.6901 1.03519 17.6901 1.63214 17.3078 2.00033L9.00019 10.0003L7.6156 8.66699Z"></path></svg>
                            </div>
                            <div class="smartfilter-select__dropdown">
                                <div class="smartfilter-select__option">
                                    <label class="bx-filter-param-label" for="<? echo "all_".$arCur["CONTROL_ID"] ?>">
                                        <span class="bx-filter-input-checkbox">
                                            <input
                                                type="radio"
                                                value=""
                                                name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
                                                id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
                                                onclick="smartFilter.changeRadio(this)"
                                            />
                                            <span class="bx-filter-param-text"><? echo GetMessage("CT_BCSF_FILTER_ALL"); ?></span>
                                        </span>
                                    </label>
                                </div>
                                <?foreach($arItem["VALUES"] as $val => $ar):?>
                                    <div class="smartfilter-select__option">
                                        <label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
                                            <span class="bx-filter-input-checkbox <? echo $ar["DISABLED"] ? 'disabled': '' ?>">
                                                <input
                                                    type="radio"
                                                    value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                                    name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
                                                    id="<? echo $ar["CONTROL_ID"] ?>"
                                                    <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                    onclick="smartFilter.click(this)"
                                                />
                                                <span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
                                                    if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
                                                        ?>&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
                                                    endif;?></span>
                                            </span>
                                        </label>
                                    </div>
                                <?endforeach;?>
                            </div>
                        </div>
                    <?
                    break;
                    case "U"://CALENDAR
                        ?>
                        <div class="col-xs-12">
                            <div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
                                    <?$APPLICATION->IncludeComponent(
                                        'bitrix:main.calendar',
                                        '',
                                        array(
                                            'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
                                            'SHOW_INPUT' => 'Y',
                                            'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
                                            'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
                                            'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                                            'SHOW_TIME' => 'N',
                                            'HIDE_TIMEBAR' => 'Y',
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );?>
                                </div></div>
                            <div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
                                    <?$APPLICATION->IncludeComponent(
                                        'bitrix:main.calendar',
                                        '',
                                        array(
                                            'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
                                            'SHOW_INPUT' => 'Y',
                                            'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
                                            'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
                                            'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                                            'SHOW_TIME' => 'N',
                                            'HIDE_TIMEBAR' => 'Y',
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );?>
                                </div></div>
                        </div>
                        <?
                        break;
                    case "G"://CHECKBOXES_WITH_PICTURES
                    case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
                    default://CHECKBOXES
                        ?>
                        <div class="smartfilter-select">
                            <div class="smartfilter-select__placeholder" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
                                <div class="smartfilter-select__placeholder-value">
                                    <?= $arItem['NAME']?>
                                    <span data-role="selected_count_<?=$arItem["ID"]?>"></span>
                                </div>
                                <svg class="smartfilter-select__placeholder-icon" width="12" height="12" viewBox="0 0 18 10" xmlns="http://www.w3.org/2000/svg"><path d="M0.692347 0.666666C1.07469 0.298477 1.6946 0.298477 2.07694 0.666667L10.3845 8.66666L8.99991 10L0.692348 2C0.310003 1.63181 0.310002 1.03486 0.692347 0.666666Z"></path><path d="M7.6156 8.66699L15.9232 0.666997C16.3055 0.298807 16.9254 0.298806 17.3078 0.666996C17.6901 1.03519 17.6901 1.63214 17.3078 2.00033L9.00019 10.0003L7.6156 8.66699Z"></path></svg>
                            </div>
                            <div class="smartfilter-select__dropdown">
                                <?foreach($arItem["VALUES"] as $val => $ar):?>
                                    <label class="smartfilter-select__option<?= $ar["CHECKED"]? ' selected' : '' ?><?= $ar["DISABLED"] ? ' disabled' : '' ?>" data-role="label_<?=$ar["CONTROL_ID"]?>" for="<? echo $ar["CONTROL_ID"] ?>">
                                        <span class="smartfilter-select__option-icon">
                                            <svg class="smartfilter-select__option-arrow" width="12" height="9" viewBox="0 0 12 9" xmlns="http://www.w3.org/2000/svg"><path d="M4.28553 9C4.05822 8.99995 3.84023 8.9051 3.67952 8.7363L0.250892 5.13632C0.0901684 4.96745 -8.03205e-05 4.73845 5.36389e-08 4.49971C8.04278e-05 4.26096 0.0904831 4.03203 0.251321 3.86328C0.412158 3.69452 0.630256 3.59976 0.857634 3.59985C1.08501 3.59993 1.30305 3.69485 1.46377 3.86373L4.28553 6.82651L10.5359 0.263749C10.6968 0.0948737 10.9149 -3.55879e-09 11.1424 0C11.3698 3.55879e-09 11.588 0.0948737 11.7488 0.263749C11.9096 0.432625 12 0.661669 12 0.900495C12 1.13932 11.9096 1.36837 11.7488 1.53724L4.89154 8.7372C4.73069 8.90566 4.51272 9.00019 4.28553 9Z"></path>
                                            </svg>
                                        </span>
                                        <span class="smartfilter-select__option-value" title="<?=$ar["VALUE"];?>">
                                            <?= $ar["VALUE"];?>
                                            <?
                                            if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"]))
                                            {
                                                ?>
                                                (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)
                                                <?
                                            }
                                            ?>&nbsp;
                                        </span>
                                        <input
                                                style="display: none"
                                                type="checkbox"
                                                value="<? echo $ar["HTML_VALUE"] ?>"
                                                name="<? echo $ar["CONTROL_NAME"] ?>"
                                                id="<? echo $ar["CONTROL_ID"] ?>"
                                            <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                onclick="smartFilter.click(this)"
                                        />
                                    </label>
                                <?endforeach;?>
                            </div>
                        </div>
                    <?
                }
            }
            ?>
            <div class="bx-filter-popup-result <?if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"]?>" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
                <?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
                <span class="arrow"></span>
                <br/>
                <a href="<?echo $arResult["FILTER_URL"]?>" target=""><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
            </div>
			<div class="smartfilter__controls">
				<input 
					class="smartfilter__clear"
					type="submit"
					id="del_filter"
					name="del_filter"
					value="Очистить:">
				<input 
					class="smartfilter__apply"
					type="submit"
					id="set_filter"
					name="set_filter"
					value="Показать">
			</div>
        </form>
    </div>
</div>

<script type="text/javascript">
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>