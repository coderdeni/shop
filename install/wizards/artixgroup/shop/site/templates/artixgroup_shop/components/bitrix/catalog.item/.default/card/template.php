<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>
<?
if (!empty($item['BRAND']['PIC']))
{
    ?>
    <div class="brandLabel"><img src="<?= $item['BRAND']['PIC']['SRC']?>" alt="<?= $item['BRAND']['NAME']?>" title="<?= $item['BRAND']['NAME']?>" class="brandLabel__img"></div>
    <?
}
?>
<div class="catalogItem__label" id="<?=$itemIds['STICKER_ID']?>">
	<?php
	if ($price['PERCENT'] > 0)
	{
		if ($haveOffers)
		{
			?>
			<div class="saleLabel">
				<span class="saleLabel__text">-<?= $price['PERCENT'] ?>%</span>
			</div>
			<?php
		}
		else
		{
			if ($price['DISCOUNT'] > 0)
			{
				?>
				<div class="saleLabel">
					<span class="saleLabel__text">-<?= $price['PERCENT'] ?>%</span>
				</div>
				<?
			}
		}
	}
	?>
	<?
	if (!empty($item['LABEL_ARRAY_VALUE']))
	{
		foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value)
		{
			?>
			<div class="newLabel"><?=$value?></div>
			<?
		}
	}
	?>
</div>
<a href="<?= $item['DETAIL_PAGE_URL']?>" class="catalogItem__photo">
    <img src="<?= !empty($actualItem['PREVIEW_PICTURE']) ? $actualItem['PREVIEW_PICTURE']['SRC'] : '/images/no-photo.png'?>" alt="<?=$productTitle?>" class="catalogItem__img" id="<?=$itemIds['PICT']?>">
</a>
<?
if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $haveOffers && !empty($item['OFFERS_PROP']))
{
    ?>
    <div id="<?=$itemIds['PROP_DIV']?>">
        <?
        foreach ($arParams['SKU_PROPS'] as $skuProperty)
        {
            $propertyId = $skuProperty['ID'];
            $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
            if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
                continue;
            ?>
            <div class="catalogItem__colors" data-entity="sku-line-block">
                <ul class="skuPropertyList">
                    <?
                    foreach ($skuProperty['VALUES'] as $value)
                    {
                        if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                            continue;

                        $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                        if ($skuProperty['SHOW_MODE'] === 'PICT')
                        {
                            ?>
                            <li class="skuPropertyList__value">
                                <a href="#" class="skuPropertyList__link" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
                                    <img src="<?=$value['PICT']['SRC']?>" alt="<?=$value['NAME']?>" class="skuPropertyList__img">
                                </a>
                            </li>
                            <?
                        }
                        else
                        {
                            ?>
                            <li class="skuPropertyList__value">
                                <a href="#" class="skuPropertyList__link" title="<?=$value['NAME']?>" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
                                    <?=$value['NAME']?>
                                </a>
                            </li>
                            <?
                        }
                    }
                    ?>
                </ul>
            </div>
            <?
        }
        ?>
    </div>
    <?
    foreach ($arParams['SKU_PROPS'] as $skuProperty)
    {
        if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
            continue;

        $skuProps[] = array(
            'ID' => $skuProperty['ID'],
            'SHOW_MODE' => $skuProperty['SHOW_MODE'],
            'VALUES' => $skuProperty['VALUES'],
            'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
        );
    }

    unset($skuProperty, $value);

    if ($item['OFFERS_PROPS_DISPLAY'])
    {
        foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer)
        {
            $strProps = '';

            if (!empty($jsOffer['DISPLAY_PROPERTIES']))
            {
                foreach($jsOffer['DISPLAY_PROPERTIES'] as $prop)
                {
                    $strProps .= '<div class="catalogItem__size">
						<svg class="icon icon-width">
							<use xlink:href="#icon-'.strtolower($prop['CODE']).'"></use>
						</svg>
						'.$prop['VALUE'].' мм
					</div>';
                }
            }

            $item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
        }
        unset($jsOffer, $strProps);
    }
}
?>
<div class="catalogItem__title">
    <a href="<?= $item['DETAIL_PAGE_URL']?>" class="catalogItem__name"><?=$productTitle?></a>
    <?
    if ($arParams['DISPLAY_FAVORITES'] === 'Y')
    {
        ?>
        <a href="javascript:void(0)" class="favoriteButton" data-role="favourites_link" data-item-id="<?= $item['ID']?>">
            <svg class="icon icon-favorite">
                <use xlink:href="#icon-favorite"></use>
            </svg>
        </a>
        <?
    }
    ?>
</div>
<div class="prices" data-entity="price-block">
    <div class="price" id="<?=$itemIds['PRICE']?>">
        <?
        if (!empty($price))
        {
            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
            {
                echo Loc::getMessage(
                    'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                    array(
                        '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                        '#VALUE#' => $measureRatio,
                        '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                    )
                );
            }
            else
            {
                echo $price['PRINT_RATIO_PRICE'];
            }
        }
        ?>
    </div>
    <?
    if ($arParams['SHOW_OLD_PRICE'] === 'Y')
    {
        ?>
        <div class="price-old" id="<?=$itemIds['PRICE_OLD']?>"<?=($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? ' style="display: none;"' : '')?>>
            <?=$price['PRINT_RATIO_BASE_PRICE']?>
        </div>
        <?
    }
    ?>
</div>


<div id="<?=$itemIds['PICT_SLIDER']?>"></div>
<div id="<?=$itemIds['DISPLAY_PROP_DIV']?>"></div>
