<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	'TEXT_LINK_ALL' => array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CPT_BCSL_TEXT_LINK_ALL'),
		'TYPE' => 'TEXT',
		'DEFAULT' => ''
	),
);