<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Sale;

/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */

global $APPLICATION;

if (isset($templateData['TEMPLATE_THEME']))
{
    $APPLICATION->SetAdditionalCSS($templateFolder.'/themes/'.$templateData['TEMPLATE_THEME'].'/style.css');
    $APPLICATION->SetAdditionalCSS('/bitrix/css/main/themes/'.$templateData['TEMPLATE_THEME'].'/style.css', true);
}

if (!empty($templateData['TEMPLATE_LIBRARY']))
{
    $loadCurrency = false;

    if (!empty($templateData['CURRENCIES']))
    {
        $loadCurrency = Loader::includeModule('currency');
    }

    CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
    if ($loadCurrency)
    {
        ?>
        <script>
            BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
        </script>
        <?
    }
}

if (isset($templateData['JS_OBJ']))
{
    ?>
    <script>
        BX.ready(BX.defer(function(){
            if (!!window.<?=$templateData['JS_OBJ']?>)
            {
                window.<?=$templateData['JS_OBJ']?>.allowViewedCount(true);
            }
        }));
    </script>

    <?
    $compared = false;
    $inBasket = false;
    $comparedIds = array();
    $inBasketIds = array();
    $item = $templateData['ITEM'];
    // check compared state
    if ($arParams['DISPLAY_COMPARE'])
    {
        if (!empty($_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]))
        {
            if (!empty($item['JS_OFFERS']))
            {
                foreach ($item['JS_OFFERS'] as $key => $offer)
                {
                    if (array_key_exists($offer['ID'], $_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']))
                    {
                        if ($key == $item['OFFERS_SELECTED'])
                        {
                            $compared = true;
                        }

                        $comparedIds[] = $offer['ID'];
                    }
                }
            }
            elseif (array_key_exists($item['ID'], $_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']))
            {
                $compared = true;
            }
        }
    }

    $basketItems = [];
    $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

    foreach ($basket->getBasketItems() as $basketItem)
        $basketItems[] = $basketItem->getProductId();

    if (!empty($item['JS_OFFERS']))
    {
        foreach ($item['JS_OFFERS'] as $key => $offer)
        {
            if (in_array($offer['ID'], $basketItems))
            {
                if ($key == $item['OFFERS_SELECTED'])
                {
                    $inBasket = true;
                }

                $inBasketIds[] = $offer['ID'];
            }
        }
    }
    elseif (in_array((int)$item['ID'], $basketItems))
    {
        $inBasket = true;
    }

    if ($templateData['JS_OBJ'])
    {
        ?>
        <script>
            BX.ready(BX.defer(function(){

                if (!!window.<?=$templateData['JS_OBJ']?>)
                {
                    <? if (!empty($comparedIds)): ?>
                    window.<?=$templateData['JS_OBJ']?>.setCompared('<?=$compared?>');
                    <? endif ?>
                    window.<?=$templateData['JS_OBJ']?>.setBasketAdded('<?=$inBasket?>');

                    <? if (!empty($comparedIds)): ?>
                    window.<?=$templateData['JS_OBJ']?>.setCompareInfo(<?=CUtil::PhpToJSObject($comparedIds, false, true)?>);
                    <? endif ?>

                    <? if (!empty($inBasketIds)): ?>
                    window.<?=$templateData['JS_OBJ']?>.setBasketInfo(<?=CUtil::PhpToJSObject($inBasketIds, false, true)?>);
                    <? endif ?>
                }
            }));
        </script>
        <?
    }


    // select target offer
    $request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
    $offerNum = false;
    $offerId = (int)$this->request->get('OFFER_ID');
    $offerCode = $this->request->get('OFFER_CODE');

    if ($offerId > 0 && !empty($templateData['OFFER_IDS']) && is_array($templateData['OFFER_IDS']))
    {
        $offerNum = array_search($offerId, $templateData['OFFER_IDS']);
    }
    elseif (!empty($offerCode) && !empty($templateData['OFFER_CODES']) && is_array($templateData['OFFER_CODES']))
    {
        $offerNum = array_search($offerCode, $templateData['OFFER_CODES']);
    }

    if (!empty($offerNum))
    {
        ?>
        <script>
            BX.ready(function(){
                if (!!window.<?=$templateData['JS_OBJ']?>)
                {
                    window.<?=$templateData['JS_OBJ']?>.setOffer(<?=$offerNum?>);
                }
            });
        </script>
        <?
    }
}


$productsInBasket = array();
$resBasket = CSaleBasket::GetList(
    array(),
    array(
        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
        "LID" => SITE_ID,
        "ORDER_ID" => "NULL"
    )
);
while ($arBasketProd = $resBasket->GetNext())
{
    $arProductsInBasket[] = $arBasketProd['PRODUCT_ID'];
}

if (in_array($templateData['ITEM']['ID'], $arProductsInBasket))
{
    ?>
    <script>
        console.log(detailCatalog);
        BX.addClass(detailCatalog.obAddToBasketBtn, "button_in");
    </script>
    <?
}