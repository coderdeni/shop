<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$this->addExternalCss("/bitrix/css/main/bootstrap.css");

?>
<div class="brands brands_page">
    <div class="brands__container">
        <?php $GLOBALS['brandFilter'] = ['UF_POPULAR' => true]?>

        <?$APPLICATION->IncludeComponent(
            "artix:highloadblock.list",
            "popular_brands",
            array(
                "BLOCK_ID" => "2",
                "CHECK_PERMISSIONS" => "N",
                "DETAIL_URL" => "",
                "FILTER_NAME" => "brandFilter",
                "PAGEN_ID" => "page",
                "ROWS_PER_PAGE" => "12",
                "SORT_FIELD" => "ID",
                "SORT_ORDER" => "DESC",
                "COMPONENT_TEMPLATE" => "brands"
            ),
            false
        );?>

        <?$APPLICATION->IncludeComponent(
            "artix:highloadblock.list",
            "alphabet_brands",
            array(
                "BLOCK_ID" => "2",
                "CHECK_PERMISSIONS" => "N",
                "DETAIL_URL" => "",
                "FILTER_NAME" => "",
                "PAGEN_ID" => "page",
                "ROWS_PER_PAGE" => "12",
                "SORT_FIELD" => "UF_NAME",
                "SORT_ORDER" => "DESC",
                "COMPONENT_TEMPLATE" => "brands"
            ),
            false
        );?>
    </div>
</div>