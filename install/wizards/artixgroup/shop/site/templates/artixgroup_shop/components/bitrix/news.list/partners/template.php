<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="partners-stock">
    <div class="partners-stock__container limiter">
        <div class="partners-stock__content">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="partner-stock-item">
                    <a href="#" class="partner-stock-item__photo">
                        <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?= $arItem['NAME']?>" class="partner-stock-item__img">
                    </a>
                    <div class="partner-stock-item__title">
                        <a href="#" class="partner-stock-item__name"><?= $arItem['NAME']?></a>
                    </div>
                    <div class="partner-stock-item__info"><?= $arItem['PREVIEW_TEXT']?></div>
                    <div class="partner-stock-item__controls">
                        <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="button button_line">Купить сейчас</a>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    </div>
</div>