<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}
	$component = $this->__component;
	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}

}
else {
    if (!empty($arResult['ERRORS']['NONFATAL'])) {
        foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
            ShowError($error);
        }
    }
    if (!count($arResult['ORDERS'])) {
        if ($_REQUEST["filter_history"] == 'Y') {
            if ($_REQUEST["show_canceled"] == 'Y') {
                ?>
                <h3 class="orders-list__title"><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER') ?></h3>
                <?
            } else {
                ?>
                <h3 class="orders-list__title"><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST') ?></h3>
                <?
            }
        } else {
            ?>
            <h3 class="orders-list__title"><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST') ?></h3>
            <?
        }
    }
    ?>

    <div class="cabinetOrders">
        <div class="cabinetOrders__sort">
            <?
            $nothing = !isset($_REQUEST["filter_history"]) && !isset($_REQUEST["show_all"]);
            $clearFromLink = array("filter_history","filter_status","show_all", "show_canceled");
            ?>
            <a class="ordersSortButton<?= $_REQUEST['show_all'] === 'Y' ? ' ordersSortButton_active' : '' ?>"
               href="<?= $APPLICATION->GetCurPageParam("show_all=Y", $clearFromLink, false) ?>"><? echo Loc::getMessage("SPOL_TPL_VIEW_ALL_ORDERS") ?></a>
            <a class="ordersSortButton<?= $_REQUEST['show_all'] !== 'Y' && $_REQUEST['filter_history'] !== 'Y' ? ' ordersSortButton_active' : '' ?>"
               href="<?= $APPLICATION->GetCurPageParam("", $clearFromLink, false) ?>"><? echo Loc::getMessage("SPOL_TPL_CUR_ORDERS") ?></a>
            <a class="ordersSortButton<?= $_REQUEST['filter_history'] === 'Y' && $_REQUEST['show_canceled'] !== 'Y' ? ' ordersSortButton_active' : '' ?>"
               href="<?= $APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false) ?>"><? echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_HISTORY") ?></a>
            <a class="ordersSortButton<?= $_REQUEST['filter_history'] === 'Y' && $_REQUEST['show_canceled'] === 'Y' ? ' ordersSortButton_active' : '' ?>"
               href="<?= $APPLICATION->GetCurPageParam("filter_history=Y&show_canceled=Y", $clearFromLink, false) ?>"><? echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_CANCELED") ?></a>
        </div>
        <div class="cabinetOrders__list">
            <div class="cabinetTable">
                <div class="cabinetTable__header">
                    <div class="cabinetTableHeader__item">Заказ</div>
                    <div class="cabinetTableHeader__item">Дата заказа</div>
                    <div class="cabinetTableHeader__item">Сумма</div>
                    <div class="cabinetTableHeader__item">Способ оплаты</div>
                    <div class="cabinetTableHeader__item">Статус</div>
                </div>
                <?
                $orderHeaderStatus = null;
                $statusClass = [
                    'N' => '',
                    'OC' => ' orderStatus_cancel',
                    'P' => ' orderStatus_paid',
                    'F' => ' orderStatus_completed'
                ];

                foreach ($arResult['ORDERS'] as $key => $order)
                {
                    if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID'])
                    {
                        $orderHeaderStatus = $order['ORDER']['STATUS_ID'];
                        $orderHeaderStatusName = htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME']);
                        $statusClass = $statusClass[$orderHeaderStatus];
                    }
                    ?>
                    <div class="cabinetTable__row">
                        <div class="cabinetTable__item cabinetTable__item_name">
                            <a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"])?>" class="cabinetOrders__order">
                                <span class="cabinetTableHeader__itemMobile">Заказ №</span><?= $order['ORDER']['ACCOUNT_NUMBER'] ?>
                            </a>
                        </div>
                        <div class="cabinetTable__item cabinetTable__item_date">
                            <span class="cabinetTableHeader__itemMobile">Дата заказа:</span><?= strtolower(FormatDate('j F Y', $order['ORDER']['DATE_INSERT'])) ?>
                        </div>
                        <div class="cabinetTable__item cabinetTable__item_price">
                            <span class="cabinetTableHeader__itemMobile">Сумма заказа</span><?= $order['ORDER']['FORMATED_PRICE'] ?>
                        </div>
                        <div class="cabinetTable__item cabinetTable__item_pay">
                            <?
                            foreach ($order['PAYMENT'] as $payment) {
                                ?>
                                <span class="cabinetTableHeader__itemMobile">Оплата</span>
                                <div class="orderPay"><?= $payment['PAY_SYSTEM_NAME'] ?></div>
                                <?
                            }
                            ?>
                        </div>
                        <div class="cabinetTable__item cabinetTable__item_status">
                            <span class="cabinetTableHeader__itemMobile">Статус</span>
                            <div class="orderStatus<?= $statusClass?>"><?= $orderHeaderStatusName?></div>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
    </div>
    <?php
}