<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

$letters = [
    "en--a" => "a",
    "en--b" => "b",
    "en--c" => "c",
    "en--d" => "d",
    "en--e" => "e",
    "en--f" => "f",
    "en--g" => "g",
    "en--h" => "h",
    "en--i" => "i",
    "en--j" => "j",
    "en--k" => "k",
    "en--l" => "l",
    "en--m" => "m",
    "en--n" => "n",
    "en--o" => "o",
    "en--p" => "p",
    "en--q" => "q",
    "en--r" => "r",
    "en--s" => "s",
    "en--t" => "t",
    "en--u" => "u",
    "en--v" => "v",
    "en--w" => "w",
    "en--x" => "x",
    "en--y" => "y",
    "en--z" => "z",
    "nums--digits" => "9",
    "ru--a" => "а",
    "ru--b" => "б",
    "ru--v" => "в",
    "ru--g" => "г",
    "ru--d" => "д",
    "ru--e" => "е",
    "ru--ie" => "ё",
    "ru--zh" => "ж",
    "ru--z" => "з",
    "ru--i" => "и",
    "ru--y" => "й",
    "ru--k" => "к",
    "ru--l" => "л",
    "ru--m" => "м",
    "ru--n" => "н",
    "ru--o" => "о",
    "ru--p" => "п",
    "ru--r" => "р",
    "ru--s" => "с",
    "ru--t" => "т",
    "ru--u" => "у",
    "ru--f" => "ф",
    "ru--h" => "х",
    "ru--ts" => "ц",
    "ru--ch" => "ч",
    "ru--sh" => "ш",
    "ru--shch" => "щ",
    "ru--thd" => "ъ",
    "ru--iy" => "ы",
    "ru--tsf" => "ь",
    "ru--ee" => "э",
    "ru--yu" => "ю",
    "ru--ya" => "я",
];

?>
<div class="brands brands_page">
    <div class="brands__container">
        <?php
        $letter = $letters[$arResult['VARIABLES']['LETTER_CODE']];

        if ($arResult['VARIABLES']['LETTER_CODE'] !== 'nums--digits')
            $GLOBALS['brandFilter'] = ['%=UF_NAME' => $letter . '%'];
        else
            $GLOBALS['brandFilter'] = ['<=UF_NAME' => '9%'];
        ?>

        <?$APPLICATION->IncludeComponent(
            "artix:highloadblock.list",
            "popular_brands",
            array(
                "BLOCK_ID" => "2",
                "CHECK_PERMISSIONS" => "N",
                "DETAIL_URL" => "",
                "FILTER_NAME" => "brandFilter",
                "PAGEN_ID" => "page",
                "ROWS_PER_PAGE" => "1000000",
                "SORT_FIELD" => "ID",
                "SORT_ORDER" => "DESC",
                "COMPONENT_TEMPLATE" => "brands"
            ),
            false
        );?>

    </div>
</div>