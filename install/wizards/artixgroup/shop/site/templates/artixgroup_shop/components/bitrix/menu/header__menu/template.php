<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

$prev = 0;
$numLevelOne = 1;
?>
<nav class="header__menu">
    <ul class="menu limiter">
    <?
    foreach ($arResult as $i => $arItem)
    {
        if ($prev > 1 && $arItem["DEPTH_LEVEL"] == 1)
        {
            ?>
                        </ul>
                    </div>
                </div>
            </li>
            <?
        }
        elseif ($arItem["DEPTH_LEVEL"] == 2 && $prev > 1)
        {
            ?>
            </ul>
            <?
        }

        if ($arItem["DEPTH_LEVEL"] == 1)
        {
            if ($arItem['IS_PARENT'])
            {
                ?>
                <li class="menu__item js-toggle-class-hover" data-target=".overlay, .menu" data-class="visible">
                    <a href="<?= $arItem["LINK"] ?>" class="menu__link"><?= htmlspecialcharsbx($arItem["TEXT"]) ?></a>
                    <div class="menuSubDrop">
                        <div class="menuSubDrop__container limiter">
                <?
            }
            else
            {
                ?>
                <li class="menu__item">
                    <a href="<?= $arItem["LINK"] ?>" class="menu__link">
                        <?= htmlspecialcharsbx($arItem["TEXT"]) ?>
                    </a>
                </li>
                <?
            }
        }
        elseif ($arItem["DEPTH_LEVEL"] == 2)
        {
            ?>
            <ul class="menuSub">
                <li class="menuSub__photo"><a href="<?= $arItem["LINK"] ?>" class="menuSub__linkImg"><img src="" alt="" class="menuSub__photo"></a></li>
                <li class="menuSub__title"><a href="<?= $arItem["LINK"] ?>" class="menuSub__linkCatalog"><?= htmlspecialcharsbx($arItem["TEXT"]) ?></a></li>
            <?
        }
        else
        {
            ?>
            <li class="menuSub__item"><a href="<?= $arItem["LINK"] ?>" class="menuSub__link"><?= htmlspecialcharsbx($arItem["TEXT"]) ?></a></li>
            <?
        }

        $prev = $arItem['DEPTH_LEVEL'];
    }

    if ($prev && $prev > 1)
    {
        ?>
                    </ul>
                </div>
            </div>
        </li>
        <?
    }
    ?>
    </ul>
</nav>
