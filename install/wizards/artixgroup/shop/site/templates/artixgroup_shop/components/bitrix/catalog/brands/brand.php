<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

$obCache = new CPHPCache();

if ($obCache->InitCache(36000, serialize($arResult["VARIABLES"]), "/iblock/catalog"))
{
    $resultCache = $obCache->GetVars();
}
elseif ($obCache->StartDataCache())
{
    $resultCache = array();

    if (Loader::includeModule("iblock"))
    {
        // Получаем список всех разделов
        $tree = [];
        $sections = CIBlockSection::GetList(['LEFT_MARGIN' => 'ASC'], ['IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y']);
        while ($section = $sections->Fetch())
        {
            $tree[] = $section;
        }

        // Получаем список элементов текущего бренда
        $foundSections = [];
        $elementIds = [];
        $elements = CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'PROPERTY_BRAND_REF' => $arResult["VARIABLES"]["BRAND_CODE"]],
            false,
            false,
            ['ID']
        );
        while ($ob = $elements->GetNextElement())
        {
            $element = $ob->GetFields();
            $elementIds[] = $element['ID'];
        }

        $sections = CIBlockElement::GetElementGroups($elementIds, ['ACTIVE' => 'Y'], ['ID', 'LEFT_MARGIN', 'RIGHT_MARGIN']);
        while ($section = $sections->Fetch())
            $foundSections[] = $section;

        // Находим все вектки
        $sectionIds = [];
        foreach ($foundSections as $foundSection)
        {
            foreach ($tree as $section)
            {
                if ($section['LEFT_MARGIN'] <= $foundSection['LEFT_MARGIN'] && $section['RIGHT_MARGIN'] >= $foundSection['RIGHT_MARGIN'])
                {
                    $sectionIds[] = (int)$section['ID'];
                }
            }
        }

        $sectionIds = array_unique($sectionIds);

        $filter = [];
        $curSection = [];

        if ($arResult['VARIABLES']['SECTION_CODE'])
        {
            foreach ($tree as $section)
            {
                if ($section['CODE'] === $arResult['VARIABLES']['SECTION_CODE'])
                {
                    $curSection = $section;
                    break;
                }
            }

            foreach ($tree as $section)
            {
                if ((in_array((int)$section['ID'], $sectionIds)))
                {
                    $filter[] = $section['ID'];
                }
            }
        }

        $filter = count($filter) > 0 ? $filter : $sectionIds;

        if (defined("BX_COMP_MANAGED_CACHE"))
        {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache("/iblock/catalog");
            $resultCache = [
                'curSectionId' => $curSection['ID'],
                'curSection' => $curSection,
                'tree' => $tree,
                'filter' => $filter,
            ];
            $CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"] . '_brands');
            $CACHE_MANAGER->EndTagCache();
        }
        else
        {
            $resultCache = [];

            if ($tree)
                $resultCache['tree'] = $tree;

            if ($filter)
                $resultCache['filter'] = $filter;
        }

    }

    if (Loader::includeModule('highloadblock'))
    {
        $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(3)->fetch(); // получаем объект вашего HL блока
        $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);  // получаем рабочую сущность
        $entity_data_class = $entity->getDataClass(); // получаем экземпляр класса
        $resultCache['brand'] = $entity_data_class::getRow(array(
            "filter" => ['UF_XML_ID' => $arResult["VARIABLES"]["BRAND_CODE"]],
        ));
    }

    $obCache->EndDataCache($resultCache);
}

foreach ($arResult['URL_TEMPLATES'] as &$url)
{
    $url = str_replace("#BRAND_CODE#", $arResult["VARIABLES"]["BRAND_CODE"], $url);
}

if ($resultCache && count($resultCache['filter']))
{
    $GLOBALS['sectionFilter'] = ['ID' => $resultCache['filter']];
}

$GLOBALS['brandFilter'] = ['PROPERTY_BRAND_REF' => $arResult["VARIABLES"]["BRAND_CODE"]];
?>
<div class="headers">
    <div class="headers__container limiter">
        <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "breadcrumb",
            array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "-"
            ),
            false,
            array('HIDE_ICONS' => 'Y')
        );?>
        <h1 class="title title_big"><?$APPLICATION->ShowTitle(false)?></h1>
        <?$APPLICATION->IncludeComponent(
			"bitrix:catalog.section.list",
			"tags",
			array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"SECTION_ID" => $sectionId,
				"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                "FILTER_NAME" => "sectionFilter",
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"SECTION_USER_FIELDS" => array(
					0 => "UF_NEW_CATEGORY",
					1 => "UF_HIT_CATEGORY",
					2 => "",
				),
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
				"TOP_DEPTH" => 3,
				"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
				"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
				"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
				"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
				"ADD_SECTIONS_CHAIN" => 'N'
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter",
            "",
            array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => $resultCache["curSectionId"],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "PREFILTER_NAME" => 'brandFilter',
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SAVE_IN_SESSION" => "N",
                "POPUP_POSITION" => "right",
                "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                "XML_EXPORT" => "Y",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                "SEF_MODE" => $arParams["SEF_MODE"],
                "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
                "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
            ),
            false,
            array('HIDE_ICONS' => 'Y')
        );?>
    </div>
</div>
<div class="catalog">
    <div class="catalog__wrapper limiter">
        <div class="catalog__container">
            <?$sort = $APPLICATION->IncludeComponent(
                "artixgroup:elements.sort",
                ".default",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "DEFAULT_ELEMENT_COUNT" => "15",
                    "ELEMENT_COUNT_LIST" => array(
                        0 => "15",
                        1 => "25",
                        2 => "30",
                        3 => "40",
                        4 => "50",
                        5 => "",
                    ),
                    "TEMPLATE_ELEMENT_COUNT" => "%d",
                    "DISPLAY_ELEMENT_COUNT" => "N",
                    "INVERT_ORDER" => "Y",
                    "DEFAULT_SORT_FIELD" => "timestamp_x",
                    "SORT_FIELDS" => array(
                        0 => "timestamp_x",
                        1 => "show_counter",
                        2 => "catalog_PRICE_1",
                    ),
                    "UNILATERAL" => array(
                        0 => "timestamp_x",
                        1 => "show_counter",
                    ),
                    "SORT_TIMESTAMP_X_NAME" => "новизна",
                    "SORT_SHOW_COUNTER_NAME" => "популярность",
                    "SORT_CATALOG_PRICE_SCALE_1_NAME" => "цена",
                    "VIEW_MODE" => "ONE_LIST",
                    "COMPOSITE_FRAME_MODE" => "A",
                    "COMPOSITE_FRAME_TYPE" => "AUTO",
                    "DISPLAY_ELEMENT_COUNT_LIST" => "Y",
                    "ORDER_NAME_DESC" => "По убыванию",
                    "ORDER_NAME_ASC" => "По возрастанию",
                    "SORT_timestamp_x_desc_NAME" => "По новизне",
                    "DEFAULT_ORDER_timestamp_x" => "desc",
                    "SORT_timestamp_x_CODE" => "new",
                    "SORT_catalog_PRICE_1_CODE" => "price",
                    "DEFAULT_ORDER" => "desc",
                    "SORT_show_counter_desc_NAME" => "По популярности",
                    "SORT_show_counter_CODE" => "popular",
                    "DEFAULT_ORDER_show_counter" => "desc",
                    "SORT_catalog_PRICE_1_desc_NAME" => "Сначала дороже",
                    "SORT_catalog_PRICE_1_asc_NAME" => "Сначала дешевле",
                ),
                false
            );?>
            <?
            $intSectionID = $APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "catalog__content",
                array(
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "ELEMENT_SORT_FIELD" => $sort["SORT"],
                    "ELEMENT_SORT_ORDER" => $sort["ORDER"],
                    "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                    "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                    "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                    "PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
                    "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                    "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                    "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                    "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                    "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                    "BASKET_URL" => $arParams["BASKET_URL"],
                    "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                    "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                    "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                    "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                    "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                    "FILTER_NAME" => $arParams["FILTER_NAME"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "SET_TITLE" => $arParams["SET_TITLE"],
                    "MESSAGE_404" => $arParams["~MESSAGE_404"],
                    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                    "SHOW_404" => $arParams["SHOW_404"],
                    "FILE_404" => $arParams["FILE_404"],
                    "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                    "PAGE_ELEMENT_COUNT" => $sort["PER_PAGE"],
                    "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                    "PRICE_CODE" => $arParams["PRICE_CODE"],
                    "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                    "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                    "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                    "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                    "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                    "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                    "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                    "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                    "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                    "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                    "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                    "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                    "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                    "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                    "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                    "LAZY_LOAD" => $arParams["LAZY_LOAD"],
                    "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
                    "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

                    "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                    "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                    "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                    "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                    "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                    "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                    "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                    "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                    "SECTION_ID" => $resultCache["curSectionId"],
                    "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                    "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                    "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                    'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                    'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                    'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

                    'LABEL_PROP' => $arParams['LABEL_PROP'],
                    'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                    'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                    'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                    'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
                    'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                    'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
                    'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
                    'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
                    'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                    'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                    'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

                    'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                    'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                    'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                    'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                    'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
                    'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                    'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                    'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                    'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                    'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                    'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                    'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                    'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                    'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                    'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                    'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                    'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

                    'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                    'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                    'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

                    'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                    "ADD_SECTIONS_CHAIN" => "N",
                    'ADD_TO_BASKET_ACTION' => $basketAction,
                    'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                    'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                    'COMPARE_NAME' => $arParams['COMPARE_NAME'],
                    'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                    'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
                    'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                    'HIDE_SECTION_DESCRIPTION' => 'Y',
                ),
                $component
            );
            ?>
        </div>
    </div>
</div>
<?
$APPLICATION->SetTitle($resultCache['brand']['UF_NAME']);
$APPLICATION->AddChainItem($resultCache['brand']['UF_NAME'], $arResult["FOLDER"] . $arResult['URL_TEMPLATES']['brand']);

if ($resultCache["curSectionId"] > 0)
{
    $APPLICATION->SetTitle($resultCache['curSection']['NAME'].' от '.$resultCache['brand']['UF_NAME']);
    $APPLICATION->AddChainItem($resultCache['curSection']['NAME'].' от '.$resultCache['brand']['UF_NAME'], $arResult["FOLDER"] . $arResult['URL_TEMPLATES']['section']);
}
