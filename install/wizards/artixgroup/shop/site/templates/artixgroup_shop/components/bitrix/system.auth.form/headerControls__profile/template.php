<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

CJSCore::Init();
?>
<div class="headerControls headerControls__profile<?= $arResult["FORM_TYPE"] === 'login' ? ' trigger' : ''?> js-toggle-class-hover" data-target=".drop_profile" data-class="drop_open" data-dialog="loginDialog">
    <span class="headerControls__icon">
        <svg class="icon icon-user ">
            <use xlink:href="#icon-user"></use>
        </svg>
    </span>
    <?
    if ($arResult["FORM_TYPE"] !== "login")
    {
        ?>
        <div class="drop drop_profile">
            <div class="drop__close js-toggle-class" data-target=".drop_profile" data-class="drop_open"></div>
            <div class="drop__content">
                <div class="dropProfile">
                    <div class="dropProfile__user">
                        <div class="dropProfile__name"><?=$arResult["USER_NAME"]?></div>
                        <div class="dropProfile__email"><?=$arResult["USER_LOGIN"]?></div>
                    </div>
                    <div class="dropProfile__menu">
                        <form action="<?=$arResult["AUTH_URL"]?>">
                            <div class="dropProfile__item">
                                <a href="/personal/orders/" class="dropProfile__link">
                                    <svg class="icon icon-orders">
                                        <use xlink:href="#icon-orders"></use>
                                    </svg>
                                    Мои заказы
                                </a>
                            </div>
                            <?
                            if ($arResult['DISPLAY_FAVORITES_LINK'])
                            {
                                ?>
                                <div class="dropProfile__item">
                                    <a href="/personal/favorites/" class="dropProfile__link">
                                        <svg class="icon icon-favorite">
                                            <use xlink:href="#icon-favorite"></use>
                                        </svg>
                                        Мои избранные
                                    </a>
                                </div>
                                <?
                            }
                            if ($arResult['DISPLAY_BONUS_LINK'])
                            {
                                ?>
                                <div class="dropProfile__item">
                                    <a href="/personal/account/" class="dropProfile__link">
                                        <svg class="icon icon-bonus">
                                            <use xlink:href="#icon-bonus"></use>
                                        </svg>
                                        Мои бонусы
                                    </a>
                                </div>
                                <?
                            }
                            ?>
                            <div class="dropProfile__item dropProfile__item_line"></div>
                            <div class="dropProfile__item">
                                <a href="/personal/private/" class="dropProfile__link">
                                    <svg class="icon icon-personal">
                                        <use xlink:href="#icon-personal"></use>
                                    </svg>
                                    Личные данные
                                </a>
                            </div>
                            <div class="dropProfile__item">
                                <a href="/personal/profiles/" class="dropProfile__link">
                                    <svg class="icon icon-profiles">
                                        <use xlink:href="#icon-profiles"></use>
                                    </svg>
                                    Мои адреса доставки
                                </a>
                            </div>
                            <div class="dropProfile__item dropProfile__item_line"></div>
                            <?foreach ($arResult["GET"] as $key => $value):?>
                                <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
                            <?endforeach?>
                            <input type="hidden" name="logout" value="yes" />
                            <div class="dropProfile__item">
                                <button type="submit" name="logout_butt" class="dropProfile__link">
                                    <svg class="icon icon-exit">
                                        <use xlink:href="#icon-exit"></use>
                                    </svg>
                                    <?=GetMessage("AUTH_LOGOUT_BUTTON")?>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?
    }
    ?>
</div>