<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ACCOUNT_LIST"] as $accountValue)
{
    ?>
    <div class="cabinetWidget cabinetWidget_bonus">
        <div class="cabinetWidget__header">
            <div class="cabinetWidget__title"><i class="icon icon_bonus"></i>Мои бонусы</div>
            <a href="/personal/account/" class="cabinetWidget__button">Посмотреть</a>
        </div>
        <div class="cabinetWidget__content">
            <div class="cabinetWidgetBonus">
                <div class="cabinetWidgetBonus__title">На вашем счету</div>
                <div class="cabinetWidgetBonus__count"><?=$accountValue['SUM']?></div>
            </div>
        </div>
    </div>
    <?
    $GLOBALS['account_'.$accountValue['ACCOUNT_LIST']['ID'].'_filter'] = ['UF_ACCOUNT_ID' => $accountValue['ACCOUNT_LIST']['ID']];
    $APPLICATION->IncludeComponent(
        "bitrix:highloadblock.list",
        "cabinetBonus__list",
        Array(
            "BLOCK_ID" => "5",
            "CHECK_PERMISSIONS" => "N",
            "DETAIL_URL" => "",
            "FILTER_NAME" => 'account_'.$accountValue['ID'].'_filter',
            "PAGEN_ID" => "page",
            "ROWS_PER_PAGE" => "",
            "SORT_FIELD" => "ID",
            "SORT_ORDER" => "DESC"
        )
    );
}
?>
