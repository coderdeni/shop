<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="topMenu<?= $arParams['IS_LEFT'] == 'Y' ? ' topMenu_left' : ''?>">
    <?
    foreach ($arResult as $item)
    {
        if ($item["DEPTH_LEVEL"] === 1)
        {
            ?>
            <div class="topMenu__item<?= $item["SELECTED"] ? ' topMenu__item_selected' : '' ;?>">
                <a class="topMenu__link" href="<?= $item["LINK"]?>"><?= htmlspecialcharsbx($item["TEXT"]) ?></a>
            </div>
            <?
        }
    }
    ?>
</div>