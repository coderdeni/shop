<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="headers">
    <div class="headers__container limiter">
        <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "breadcrumb",
            array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "-"
            ),
            false,
            array('HIDE_ICONS' => 'Y')
        );?>
        <h1 class="title title_big"><?$APPLICATION->ShowTitle(false)?></h1>
    </div>
</div>
<div class="brands brands_page">
    <div class="brands__container">
        <?$APPLICATION->IncludeComponent(
            "artixgroup:highloadblock.list",
            "popular_brands",
            array(
                "BLOCK_ID" => "3",
                "CHECK_PERMISSIONS" => "N",
                "DETAIL_URL" => "",
                "FILTER_NAME" => "brandFilter",
                "PAGEN_ID" => "page",
                "ROWS_PER_PAGE" => "2000",
                "SORT_FIELD" => "ID",
                "SORT_ORDER" => "DESC",
                "COMPONENT_TEMPLATE" => "brands"
            ),
            $component
        );?>

        <?/*$APPLICATION->IncludeComponent(
            "artix:highloadblock.list",
            "alphabet_brands",
            array(
                "BLOCK_ID" => "3",
                "CHECK_PERMISSIONS" => "N",
                "DETAIL_URL" => "",
                "FILTER_NAME" => "",
                "PAGEN_ID" => "page",
                "ROWS_PER_PAGE" => "12",
                "SORT_FIELD" => "UF_NAME",
                "SORT_ORDER" => "DESC",
                "COMPONENT_TEMPLATE" => "brands"
            ),
            $component
        );*/?>
    </div>
</div>