<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "TAB_ID" => Array(
        "NAME" => "ID таба",
        "TYPE" => "STRING"
    ),
	"IS_ACTIVE" => Array(
		"NAME" => "Активный таб",
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
);
?>
