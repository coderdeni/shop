<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
	CJSCore::Init('phone_auth');
}

if ($arResult["USE_EMAIL_CONFIRMATION"] === "Y")
{
    ?>
    <div class="dialog dialog--open">
        <div class="dialog__overlay"></div>
        <div class="dialog__noscroll">
            <div class="dialog__container">
                <div class="dialog__title"><?= Loc::getMessage("AUTH_REGISTER")?></div>
                <div class="dialog__content">
                    <div class="authorization">
                        <div class="authorization__form">
                            <div class="form">
                                <div class="form__row form__row_ac">
                                    <div class="authorization__infoText"><?echo Loc::getMessage("REGISTER_EMAIL_WILL_BE_SENT")?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dialog__close">
                    <button class="action close" data-dialog-close="">
                        <svg class="icon icon-close ">
                            <use xlink:href="#icon-close"></use>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="dialog__event"></div>
        </div>
    </div>
    <?
}
?>

<div class="authorization">
    <div class="authorization__form">
        <?
        if ($USER->IsAuthorized())
        {
            ?>
            <div class="form">
                <div class="form__row form__row_ac">
                    <div class="authorization__infoText"><?echo GetMessage("MAIN_REGISTER_AUTH")?></div>
                </div>
            </div>
        <?
        }
        elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y")
        {
            ?>
            <div class="form">
                <div class="form__row form__row_ac">
                    <div class="authorization__infoText"><?echo Loc::getMessage("REGISTER_EMAIL_WILL_BE_SENT")?></div>
                </div>
            </div>
            <?
        }
        elseif ($arResult["SHOW_SMS_FIELD"])
        {
            ?>
            <form class="form" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform">
                <?
                if ($arResult["BACKURL"] <> '')
                {
                    ?>
                    <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                    <?
                }
                ?>
                <input type="hidden" name="SIGNED_DATA" value="<?=htmlspecialcharsbx($arResult["SIGNED_DATA"])?>" />
                <div class="form__row form__row_ac">
                    <div class="authorization__infoText"><?echo Loc::getMessage("REGISTER_INFO_TEXT")?></div>
                </div>
                <?
                if (count($arResult["ERRORS"]) > 0)
                {
                    ?>
                    <div class="form__row">
                        <?
                        foreach ($arResult["ERRORS"] as $key => $error)
                        {
                            if (intval($key) == 0 && $key !== 0)
                                $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".Loc::getMessage("REGISTER_FIELD_".$key)."&quot;", $error);
                        }

                        ShowError(implode("<br />", $arResult["ERRORS"]));
                        ?>
                    </div>
                    <?
                }
                ?>
                <div class="form__row">
                    <input class="input input_big" type="text" name="SMS_CODE" value="<?=htmlspecialcharsbx($arResult["SMS_CODE"])?>" autocomplete="off" placeholder="<?echo Loc::getMessage("main_register_sms")?>"/>
                </div>
                <div class="form__row form__row_sb">
                    <input type="submit" name="code_submit_button" class="button button_bp" value="<?echo Loc::getMessage("main_register_sms_send")?>" />
                </div>
                <div class="form__row form__row_sb">
                    <label class="checkbox">
                        <input type="checkbox" class="checkbox__input">
                        <span class="checkbox__new-input"></span>
                        Я согласен на обработку моих персональных данных
                    </label>
                </div>
                <div class="form__row form__row_ac">
                    <a href="javascript::void(0)" class="link link_big link_back">Вернуться</a>
                </div>
            </form>
            <script>
                new BX.PhoneAuth({
                    containerId: 'bx_register_resend',
                    errorContainerId: 'bx_register_error',
                    interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
                    data:
                        <?=CUtil::PhpToJSObject([
                            'signedData' => $arResult["SIGNED_DATA"],
                        ])?>,
                    onError:
                        function(response)
                        {
                            var errorDiv = BX('bx_register_error');
                            var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
                            errorNode.innerHTML = '';
                            for(var i = 0; i < response.errors.length; i++)
                            {
                                errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br>';
                            }
                            errorDiv.style.display = '';
                        }
                });
            </script>
            <div id="bx_register_error" style="display:none"><?ShowError("error")?></div>
            <div id="bx_register_resend"></div>
            <?
        }
        else
        {
            ?>
            <form class="form" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
                <?
                if ($arResult["BACKURL"] <> '')
                {
                    ?>
                    <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                    <?
                }
                ?>
                <div class="form__row form__row_ac">
                    <div class="authorization__infoText">Вы сможете отслеживать статус своих заказов и получать индивидуальные рекомендации</div>
                </div>
                <?
                if (count($arResult["ERRORS"]) > 0)
                {
                    ?>
                    <div class="form__row">
                        <?
                        foreach ($arResult["ERRORS"] as $key => $error)
                        {
                            if (intval($key) == 0 && $key !== 0)
                                $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".Loc::getMessage("REGISTER_FIELD_".$key)."&quot;", $error);
                        }

                        ShowError(implode("<br />", $arResult["ERRORS"]));
                        ?>
                    </div>
                    <?
                }

                foreach ($arResult["SHOW_FIELDS"] as $FIELD)
                {
                    if ($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"])
                    {
                        ?>
                        <tr>
                            <td><?echo GetMessage("main_profile_time_zones_auto")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><span class="starrequired">*</span><?endif?></td>
                            <td>
                                <select name="REGISTER[AUTO_TIME_ZONE]" onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled=(this.value != 'N')">
                                    <option value=""><?echo GetMessage("main_profile_time_zones_auto_def")?></option>
                                    <option value="Y"<?=$arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_yes")?></option>
                                    <option value="N"<?=$arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_no")?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?echo GetMessage("main_profile_time_zones_zones")?></td>
                            <td>
                                <select name="REGISTER[TIME_ZONE]"<?if(!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"'?>>
                                    <?foreach($arResult["TIME_ZONE_LIST"] as $tz=>$tz_name):?>
                                        <option value="<?=htmlspecialcharsbx($tz)?>"<?=$arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : ""?>><?=htmlspecialcharsbx($tz_name)?></option>
                                    <?endforeach?>
                                </select>
                            </td>
                        </tr>
                        <?
                    }
                    else
                    {
                        ?>
                        <div class="form__row">
                            <?
                            switch ($FIELD)
                            {
                            case "PASSWORD":
                                ?>
                            <input type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" class="input input_big" placeholder="<?= Loc::getMessage("REGISTER_FIELD_".$FIELD)?><?= ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] === "Y" ? ' *' : '')?>">
                            <?
                            if ($arResult["SECURE_AUTH"])
                            {
                            ?>
                                <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
                                                            <div class="bx-auth-secure-icon"></div>
                                                        </span>
                                <noscript>
                                                            <span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
                                                                <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                                            </span>
                                </noscript>
                                <script type="text/javascript">
                                    document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                </script>
                            <?
                            }
                            break;

                            case "CONFIRM_PASSWORD":
                            ?>
                            <input class="input input_big" type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" placeholder="<?= Loc::getMessage("REGISTER_FIELD_".$FIELD)?><?= ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] === "Y" ? ' *' : '')?>">
                            <?
                            break;

                            case "PERSONAL_GENDER":
                            ?>
                                <select name="REGISTER[<?=$FIELD?>]">
                                    <option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
                                    <option value="M"<?=$arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_MALE")?></option>
                                    <option value="F"<?=$arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
                                </select>
                            <?
                            break;

                            case "PERSONAL_COUNTRY":
                            case "WORK_COUNTRY":
                            ?>
                                <select name="REGISTER[<?=$FIELD?>]">
                                    <?
                                    foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value)
                                    {
                                        ?>
                                        <option value="<?=$value?>"<?if ($value == $arResult["VALUES"][$FIELD]):?> selected="selected"<?endif?>><?=$arResult["COUNTRIES"]["reference"][$key]?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            <?
                            break;

                            case "PERSONAL_PHOTO":
                            case "WORK_LOGO":
                            ?>
                            <input type="file" class="input input_big" name="REGISTER_FILES_<?=$FIELD?>" placeholder="<?= Loc::getMessage("REGISTER_FIELD_".$FIELD)?><?= ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] === "Y" ? ' *' : '')?>">
                            <?
                            break;

                            case "PERSONAL_NOTES":
                            case "WORK_NOTES":
                            ?>
                                <textarea rows="3" class="textarea textarea_big" name="REGISTER[<?=$FIELD?>]"><?=$arResult["VALUES"][$FIELD]?></textarea>
                                <?
                                break;
                                default:
                                    if ($FIELD == "PERSONAL_BIRTHDAY"):?><small><?=$arResult["DATE_FORMAT"]?></small><br /><?endif;
                                    ?>
                                    <input class="input input_big" type="text" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" />
                                    <?
                                    if ($FIELD == "PERSONAL_BIRTHDAY")
                                    {
                                        $APPLICATION->IncludeComponent(
                                            'bitrix:main.calendar',
                                            '',
                                            array(
                                                'SHOW_INPUT' => 'N',
                                                'FORM_NAME' => 'regform',
                                                'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
                                                'SHOW_TIME' => 'N'
                                            ),
                                            null,
                                            array("HIDE_ICONS" => "Y")
                                        );
                                    }
                                    ?><?
                            }
                            ?>
                        </div>
                        <?
                    }
                }
                if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y")
                {
                    foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField)
                    {
                        ?>
                        <div class="form__row">
                            <?= $arUserField["EDIT_FORM_LABEL"]?>:<?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;?>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:system.field.edit",
                                $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                array(
                                    "bVarsFromForm" => $arResult["bVarsFromForm"],
                                    "arUserField" => $arUserField,
                                    "form_name" => "regform"
                                ),
                                null,
                                array("HIDE_ICONS"=>"Y")
                            );?>
                        </div>
                        <?
                    }
                }
                if ($arResult["USE_CAPTCHA"] == "Y")
                {
                    ?>
                    <div class="form__row form__row_sb">
                        <input type="text" class="input input_midle" name="captcha_word" maxlength="50" value="" autocomplete="off" placeholder="<?= Loc::getMessage("REGISTER_CAPTCHA_TITLE")?>">
                        <div class="captcha">
                            <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" alt="CAPTCHA" />
                        </div>
                    </div>
                    <?
                }
                ?>
                <div class="form__row form__row_sb">
                    <input type="submit" class="button button_bp" name="register_submit_button" value="<?=Loc::getMessage("AUTH_REGISTER")?>" />
                </div>
                <div class="form__row form__row_sb">
                    <label class="checkbox">
                        <input type="checkbox" class="checkbox__input">
                        <span class="checkbox__new-input"></span>
                        Я согласен на обработку моих персональных данных
                    </label>
                </div>
                <div class="form__row form__row_ac">
                    <a href="javascript::void(0)" class="link link_big link_back">Вернуться</a>
                </div>
            </form>
            <?
        }
        ?>
    </div>
</div>