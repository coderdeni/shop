<?

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => Loc::getMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>
<div class="contentCategory">
    <div class="contentCategory__title"><?= Loc::getMessage('CATEGORY')?></div>
    <div class="contentCategory__content">
        <div class="contentCategory__item">
            <a href="<?= $arResult['LIST_PAGE_URL']?>" class="contentCategory__link<?= '' == $arParams['PARENT_SECTION_CODE'] && '' == $arParams['PARENT_SECTION_ID'] ? ' contentCategory__link_active' : ''?>">
                <?= $arParams['TEXT_LINK_ALL']?>
            </a>
        </div>
        <?
        foreach ($arResult['SECTIONS'] as $key => $arSection)
        {
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            ?>            
            <div class="contentCategory__item" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>" class="contentCategory__link<?= $arSection['CODE'] === $arParams['PARENT_SECTION_CODE'] || $arSection['ID'] === $arParams['PARENT_SECTION_ID'] ? ' contentCategory__link_active' : ''?>">
                    <? echo $arSection['NAME']; ?>
                </a>
            </div>
            <?
        }
        ?> 
    </div>
</div>