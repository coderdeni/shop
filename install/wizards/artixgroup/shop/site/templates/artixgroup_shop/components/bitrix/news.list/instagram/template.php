<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="instagram">
    <div class="instagram__wrapper limiter">
        <div class="instagram__container swiper-container">
            <div class="instagram__title"><?= $arParams['HASHTAG']?></div>
            <div class="instagram__subtitle"><?= $arParams['DESCRIPTION']?></div>
            <div class="instagram__content swiper-wrapper">
                <?
                $index = 1;
                $isGroup = false;
                foreach($arResult["ITEMS"] as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                    if (!$isGroup && $arItem['PROPERTIES']['GROUP']['VALUE'] === 'Y' && isset($arResult['ITEMS'][$index]))
                    {
                        $isGroup = true;
                        ?>
                        <div class="instagram__items instagram__items_2 swiper-slide">
                            <div class="instagram__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?= $arItem['NAME']?>" class="instagram__img">
                            </div>
                        <?
                    }
                    elseif ($isGroup)
                    {
                        $isGroup = false;
                        ?>
                            <div class="instagram__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?= $arItem['NAME']?>" class="instagram__img">
                            </div>
                        </div>
                        <?
                    }
                    else
                    {
                        ?>
                        <div class="instagram__items swiper-slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                            <div class="instagram__item"><img src="<?= $arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?= $arItem['NAME']?>" class="instagram__img"></div>
                        </div>
                        <?
                    }
                    $index++;
                    ?>
                <?endforeach;?>
            </div>
            <div class="instagram__pagination"></div>
            <div class="instagram-arrow">
                <button class="instagram__arrow-left"></button>
                <button class="instagram__arrow-right"></button>
            </div>
            <div class="instagram__controls">
                <a href="<?= $arParams['INSTAGRAM_URL']?>" target="_blank" class="button trigger">Подписаться</a>
            </div>
        </div>
    </div>
</div>