<?
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="stockPage">
	<div class="stockPage__container">
		<div class="stockPage__content">
			<div class="stockPage__category stockPage__category_<?= $arResult['PROPERTIES']['TYPE']['VALUE_XML_ID'] ?>">
                <?= $arResult['PROPERTIES']['TYPE']['VALUE'] ?>
            </div>
			<div class="stockPage__header">
				<div class="stockPage__date">
					<div class="stockPage__period">
                        <?= (!$arResult['ACTIVE_TO'])
                            ? Loc::getMessage('DATE', [
                                '#DATE_FROM#' => FormatDateFromDB($arResult['ACTIVE_FROM'], 'DD MMMM')
                            ])
                            : Loc::getMessage('PERIOD', [
                                '#DATE_FROM#' => FormatDateFromDB($arResult['ACTIVE_FROM'], 'DD MMMM'),
                                '#DATE_TO#' => FormatDateFromDB($arResult['ACTIVE_TO'], 'DD MMMM'),
                            ])
                        ?>
                    </div>
                    <div class="stockPage__end"></div>
				</div>
			</div>
            <div class="stockPage__info">
                <div class="stockPage__photo">
                    <img
                        class="stockPage__img"
                        src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
                        alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
                        title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>">
                </div>
                <div class="stockPage__text">
                    <?echo $arResult["DETAIL_TEXT"];?>
                </div>
                <?
                if ($arParams['USE_SHARE'] === 'Y')
                {
                    ?>
                    <div class="share">
                        <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="https://yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,viber,whatsapp,skype,telegram"></div>
                    </div>
                    <?
                }
                ?>
            </div>
			<div class="stockPagePage__close">
				<a href="<?= $arResult['LIST_PAGE_URL']?>" class="close"></a>
			</div>
		</div>
	</div>
</div>
<?
if ($arResult['ACTIVE_TO'])
{
    ?>
    <script type="application/javascript">
        new AgActionTimer({
            dateTo: <?= strtotime($arResult['ACTIVE_TO']) * 1000?>,
            selectorTimerContainer: '.stockPage__end',
            format: '<?= Loc::getMessage('FORMAT_TIMER')?>'
        });
    </script>
    <?
}
