<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Artixgroup\Shop\Services\Bonus;
use Artixgroup\Shop\Services\Installment;
use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList,
    'ITEM' => array(
        'ID' => $arResult['ID'],
        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
        'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
        'JS_OFFERS' => $arResult['JS_OFFERS']
    )
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
    'ID' => $mainId,
    'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
    'STICKER_ID' => $mainId.'_sticker',
    'BIG_SLIDER_ID' => $mainId.'_big_slider',
    'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
    'SLIDER_CONT_ID' => $mainId.'_slider_cont',
    'OLD_PRICE_ID' => $mainId.'_old_price',
    'PRICE_ID' => $mainId.'_price',
    'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
    'PRICE_TOTAL' => $mainId.'_price_total',
    'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
    'QUANTITY_ID' => $mainId.'_quantity',
    'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
    'QUANTITY_UP_ID' => $mainId.'_quant_up',
    'QUANTITY_MEASURE' => $mainId.'_quant_measure',
    'QUANTITY_LIMIT' => $mainId.'_quant_limit',
    'BUY_LINK' => $mainId.'_buy_link',
    'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
    'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
    'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
    'COMPARE_LINK' => $mainId.'_compare_link',
    'TREE_ID' => $mainId.'_skudiv',
    'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
    'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
    'OFFER_GROUP' => $mainId.'_set_group_',
    'BASKET_PROP_DIV' => $mainId.'_basket_prop',
    'SUBSCRIBE_LINK' => $mainId.'_subscribe',
    'TABS_ID' => $mainId.'_tabs',
    'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
    'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
    'TABS_PANEL_ID' => $mainId.'_tabs_panel',
    'INSTALLMENT_ID' => $mainId.'_installment',
    'BONUS_ID' => $mainId.'_bonus',
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
    : $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
    : $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
    : $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
    $actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
        ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
        : reset($arResult['OFFERS']);
    $showSliderControls = false;

    foreach ($arResult['OFFERS'] as $key => $offer)
    {
        if ($offer['MORE_PHOTO_COUNT'] > 1)
        {
            $showSliderControls = true;
            break;
        }
    }
}
else
{
    $actualItem = $arResult;
    $showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;
$discountClass = " catalogItem__label_green";
if ($price['PERCENT'] >= 25 && $price['PERCENT'] < 50)
    $discountClass = "";
elseif ($price['PERCENT'] >= 50)
    $discountClass = " catalogItem__label_red";

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
    'left' => 'product-item-label-left',
    'center' => 'product-item-label-center',
    'right' => 'product-item-label-right',
    'bottom' => 'product-item-label-bottom',
    'middle' => 'product-item-label-middle',
    'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
    foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
    {
        $discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
    }
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
    foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
    {
        $labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
    }
}
?>

<div class="product" id="<?=$itemIds['ID']?>" itemscope itemtype="http://schema.org/Product">
    <meta itemprop="name" content="<?=$name?>" />
    <meta itemprop="category" content="<?=$arResult['CATEGORY_PATH']?>" />
    <div class="product__container limiter">
        <div class="product__main">
            <div class="product__photo">
                <div class="brandLabel"><img src="<?= $arResult['BRAND']['PIC']['SRC']?>" alt="<?= $arResult['BRAND']['NAME']?>" class="brandLabel__img"></div>
                <div class="product__label" date-role="item-label">
                    <?php
                    if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && $price['PERCENT'] > 0)
                    {
                        if ($haveOffers)
                        {
                            ?>
                            <div class="saleLabel" id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>">
                                <span class="saleLabel__text">-<?= $price['PERCENT'] ?>%</span>
                            </div>
                            <?php
                        }
                        else
                        {
                            if ($price['DISCOUNT'] > 0)
                            {
                                ?>
                                <div class="saleLabel" id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>">
                                    <span class="saleLabel__text">-<?= $price['PERCENT'] ?>%</span>
                                </div>
                                <?
                            }
                        }
                    }
                    ?>
                </div>
                <?
                if ($arParams['DISPLAY_FAVORITES'] === 'Y')
                {
                    ?>
                    <button class="productFavorute" data-role="favourites_link" data-item-id="<?= $arResult['ID'] ?>">
                        <svg class="icon icon-favorite">
                            <use xlink:href="#icon-favorite"></use>
                        </svg>
                    </button>
                    <?
                }
                ?>
                <div class="photo-slider">
                    <div class="photo-slider__inner">
                        <div class="photo-slider__main swiper-container">
                            <div class="photo-slider__content swiper-wrapper">
                                <?
                                if (!empty($actualItem['MORE_PHOTO']))
                                {
                                    foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
                                    {
                                        ?>
                                        <div class="photo-slider__item swiper-slide" data-entity="image" data-id="<?=$photo['ID']?>">
                                            <div class="photo-slider__img">
                                                <img date-role="item-img" src="<?=$photo['SRC']?>" alt="<?=$alt?>" title="<?=$title?>"<?=($key == 0 ? ' itemprop="image"' : '')?>>
                                            </div>
                                        </div>
                                        <?
                                    }
                                }
                                if ($arResult['HAV_3D_MODELS'])
                                {
                                    ?>
                                    <div class="photo-slider__item swiper-slide 3d-models" data-entity="slider-control">
                                        <div class="photo-slider__img" id="3d-models"></div>
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                            <div class="photo-slider__arrow_left"></div>
                            <div class="photo-slider__arrow_right"></div>
                            <div class="swiper-pagination swiper-pagination-bullets"></div>
                        </div>

                        <div class="photo-slider__thumbs swiper-container swiper-container-thumbs">
                            <div class="photo-slider__thumbs-content swiper-wrapper">
                                <?
                                if (!empty($actualItem['MORE_PHOTO']))
                                {
                                    foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
                                    {
                                        ?>
                                        <div class="photo-slider__thumbs-item swiper-slide" data-entity="slider-control" data-value="<?=$photo['ID']?>">
                                            <div class="photo-slider__thumbs-img"> <img src="<?=$photo['SRC']?>" alt=""> </div>
                                        </div>
                                        <?
                                    }
                                }

                                if ($arResult['HAV_3D_MODELS'])
                                {
                                    ?>
                                    <div class="photo-slider__thumbs-item swiper-slide 3d-models" data-entity="slider-control" onclick="load3d()">
                                        <div class="photo-slider__thumbs-img"><img src="/images/3d-models.png" alt=""></div>
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                        </div>

                        <div class="photo-slider__zoom"></div>
                    </div>
                </div>
            </div>
            <div class="product__info">
            </div>
            <div class="product__price">
                <div class="product__header">
                    <div class="product__title" date-role="item-title"><?=$name?></div>
                    <?
                    if ($arResult['PROPERTIES']['ARTNUMBER']['VALUE'])
                    {
                        ?>
                        <div class="product__code">Артикул: <?= $arResult['PROPERTIES']['ARTNUMBER']['VALUE']?></div>
                        <?
                    }
                    ?>
                </div>
                <div class="productPrice">
                    <div class="prices" date-role="item-price">
                        <div class="price" id="<?=$itemIds['PRICE_ID']?>"><?=$price['PRINT_RATIO_PRICE']?></div>
                        <?php
                        if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                        {
                            ?>
                            <div class="price-old" id="<?=$itemIds['OLD_PRICE_ID']?>"
                                 style="display: <?=($showDiscount ? '' : 'none')?>;">
                                <?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                    <?
                    if ($arParams['DISPLAY_INSTALLMENT'] === 'Y')
                    {
                        $installment = Installment::getPayMonth($price['BASE_PRICE']);
                        if ($installment > 0)
                        {
                            ?>
                            <a href="javascript:void(0)" class="price-installment" id="<?= $itemIds['INSTALLMENT_ID']?>">
                                Рассрочка от <?= CurrencyFormat($installment, $price['CURRENCY']); ?>/месяц
                            </a>
                            <?
                        }
                    }

                    if ($arParams['DISPLAY_BONUS'] === 'Y')
                    {
                        $bonus = Bonus::getProductBonus($actualItem['ID'], $price['BASE_PRICE'], $price['CURRENCY']);

                        if ($bonus > 0)
                        {
                            ?>
                            <div class="productPriceLink" id="<?= $itemIds['BONUS_ID'].'_container'?>">
                                <svg class="icon icon-bonus">
                                    <use xlink:href="#icon-bonus"></use>
                                </svg>
                                <span class="productPriceLink__text" id="<?= $itemIds['BONUS_ID']?>">
                                    +<?= $bonus?> бонусов
                                </span>
                            </div>
                            <?
                        }
                    }
                    ?>
                </div>
                <?
                if ($haveOffers && !empty($arResult['OFFERS_PROP']))
                {
                    ?>
                    <div id="<?=$itemIds['TREE_ID']?>">
                        <?
                        foreach ($arResult['SKU_PROPS'] as $skuProperty)
                        {
                            if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
                                continue;

                            $propertyId = $skuProperty['ID'];
                            $skuProps[] = array(
                                'ID' => $propertyId,
                                'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                                'VALUES' => $skuProperty['VALUES'],
                                'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                            );
                            ?>
                            <div class="productSku<?= $skuProperty['SHOW_MODE'] === 'TEXT' ? ' productSku_size' : ''?>" data-entity="sku-line-block">
                                <div class="productSku__title"><?=htmlspecialcharsEx($skuProperty['NAME'])?></div>
                                <ul class="productSku__list">
                                    <?
                                    foreach ($skuProperty['VALUES'] as &$value)
                                    {
                                        $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                        if ($skuProperty['SHOW_MODE'] === 'PICT')
                                        {
                                            ?>
                                            <li class="productSku__list-item" title="<?=$value['NAME']?>"
                                                data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"
                                                data-onevalue="<?=$value['ID']?>">
                                                <div class="productSku__item">
                                                    <img src="<?=$value['PICT']['SRC']?>" alt="<?=$value['NAME']?>" title="<?=$value['NAME']?>" class="productSku__img">
                                                </div>
                                            </li>
                                            <?
                                        }
                                        else
                                        {
                                            ?>
                                            <li class="productSku__list-item" title="<?=$value['NAME']?>"
                                                data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"
                                                data-onevalue="<?=$value['ID']?>">
                                                <div class="productSku__item">
                                                    <?=$value['NAME']?>
                                                </div>
                                            </li>
                                            <?
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                    <?
                }
                ?>
                <div class="productButtons" id="<?=$itemIds['BASKET_ACTIONS_ID']?>" style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;">
                    <?
                    if ($showAddBtn)
                    {
                        ?>
                        <a class="productBuy" id="<?=$itemIds['ADD_BASKET_LINK']?>" href="javascript:void(0);">
                            <svg class="icon icon-cart">
                                <use xlink:href="#icon-cart"></use>
                            </svg>
                            <?=$arParams['MESS_BTN_ADD_TO_BASKET']?>
                        </a>
                        <?
                    }
                    if ($showBuyBtn)
                    {
                        ?>
                        <a class="productBuy" id="<?=$itemIds['BUY_LINK']?>" href="javascript:void(0);">
                            <svg class="icon icon-cart">
                                <use xlink:href="#icon-cart"></use>
                            </svg>
                            <?=$arParams['MESS_BTN_BUY']?>
                        </a>
                        <?
                    }
                    ?>
                    <?$APPLICATION->IncludeComponent(
                        "artixgroup:buy.click",
                        "",
                        Array(
                            "PRODUCT_ID" => $actualItem['ID'],
                            "RESULT_MESSAGE" => "<p>Ваш заказ успешно создан. Ваш менеджер свяжется с вами в ближайшее время.</p>"
                        ),
                        $component
                    );?>
                </div>
                <?
                if ($showSubscribe && (!$showAddBtn && !$showBuyBtn))
                {
                    ?>
                    <div class="productButtons">
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:catalog.product.subscribe',
                            '',
                            array(
                                'PRODUCT_ID' => $arResult['ID'],
                                'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                'BUTTON_CLASS' => 'productBuy',
                                'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                            ),
                            $component,
                            array('HIDE_ICONS' => 'Y')
                        );
                        ?>
                    </div>
                    <?
                }

                if ($showDescription)
                {
                    ?>
                    <div class="productDescription productDescription_descript" itemprop="description">
                        <h2 class="productDescription__title">Описание</h2>
                        <div class="productDescription__content">
                            <?
                            if (
                                $arResult['PREVIEW_TEXT'] != ''
                                && (
                                    $arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'S'
                                    || ($arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'E' && $arResult['DETAIL_TEXT'] == '')
                                )
                            )
                            {
                                echo $arResult['PREVIEW_TEXT_TYPE'] === 'html' ? $arResult['PREVIEW_TEXT'] : '<p>'.$arResult['PREVIEW_TEXT'].'</p>';
                            }

                            if ($arResult['DETAIL_TEXT'] != '')
                            {
                                echo $arResult['DETAIL_TEXT_TYPE'] === 'html' ? $arResult['DETAIL_TEXT'] : '<p>'.$arResult['DETAIL_TEXT'].'</p>';
                            }
                            ?>
                        </div>
                    </div>
                    <?
                }

                if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
                {
                    ?>
                    <div class="productDescription productDescription_properties">
                        <h2 class="productDescription__title">Характеристики</h2>
                        <div class="productDescription__content">
                            <div class="properties">
                                <div class="properties__container" id="<?= $mainId.'_main_sku_prop'?>">
                                    <?
                                    foreach ($arResult['DISPLAY_PROPERTIES'] as $property)
                                    {
                                        ?>
                                        <div class="properties__item">
                                            <div class="properties__name"><?= $property['NAME']?>:</div>
                                            <div class="properties__line"></div>
                                            <div class="properties__value">
                                                <?=(is_array($property['DISPLAY_VALUE'])
                                                    ? implode(' / ', $property['DISPLAY_VALUE'])
                                                    : $property['DISPLAY_VALUE']
                                                )?>
                                            </div>
                                        </div>
                                        <?
                                    }
                                    ?>
                                </div>
                                <div class="properties__container" id="<?= $mainId.'_sku_prop'?>"></div>
                            </div>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?
if ($haveOffers)
{
    $offerIds = array();
    $offerCodes = array();

    $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

    foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer)
    {
        $offerIds[] = (int)$jsOffer['ID'];
        $offerCodes[] = $jsOffer['CODE'];

        $fullOffer = $arResult['OFFERS'][$ind];
        $measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

        $strAllProps = '';
        $strMainProps = '';
        $strPriceRangesRatio = '';
        $strPriceRanges = '';

        if ($arParams['DISPLAY_BONUS'] === 'Y')
        {
            foreach ($jsOffer['ITEM_PRICES'] as $priceIndex => $offerPrice)
            {
                $offerPrice['BONUS'] = Bonus::getProductBonus((int)$jsOffer['ID'], $offerPrice['BASE_PRICE'], $offerPrice['CURRENCY']);
                $jsOffer['ITEM_PRICES'][$priceIndex] = $offerPrice;
            }
        }

        if ($arParams['DISPLAY_INSTALLMENT'] === 'Y')
        {
            foreach ($jsOffer['ITEM_PRICES'] as $priceIndex => $offerPrice)
            {
                $offerPrice['INSTALLMENT'] = Installment::getPayMonth($offerPrice['BASE_PRICE']);
                $jsOffer['ITEM_PRICES'][$priceIndex] = $offerPrice;
            }
        }

        if ($arResult['SHOW_OFFERS_PROPS'])
        {
            if (!empty($jsOffer['DISPLAY_PROPERTIES']))
            {
                foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property)
                {
                    $current =
                        '<div class="properties__item"><div class="properties__name">'.$property['NAME'].'</div><div class="properties__line"></div><div class="properties__value">'.(
                        is_array($property['VALUE'])
                            ? implode(' / ', $property['VALUE'])
                            : $property['VALUE']
                        ).'</div></div>';

                    $strAllProps .= $current;

                    if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']]))
                    {
                        $strMainProps .= $current;
                    }
                }

                unset($current);
            }
        }

        $strTemplateSlide = '';
        $strTemplateThumb = '';

        foreach ($jsOffer['SLIDER'] as $keyPhoto => $photo)
        {
            $strTemplateSlide .= '<div class="photo-slider__item swiper-slide" data-offer-id="'.$offer['ID'].'" data-entity="image" data-id="'.$photo['ID'].'">';
            $strTemplateSlide .= '    <div class="photo-slider__img">';
            $strTemplateSlide .= '        <img date-role="item-img" src="'.$photo['SRC'].'" alt="'.$alt.'" title="'.$title.'"'.($keyPhoto == 0 ? ' itemprop="image"' : '').'>';
            $strTemplateSlide .= '    </div>';
            $strTemplateSlide .= '</div>';

            $strTemplateThumb .= '<div class="photo-slider__thumbs-item swiper-slide" data-offer-id="'.$offer['ID'].'" data-id="'.$photo['ID'].'">';
            $strTemplateThumb .= '    <div class="photo-slider__thumbs-img">';
            $strTemplateThumb .= '        <img date-role="item-img" src="'.$photo['SRC'].'" alt="'.$alt.'" title="'.$title.'">';
            $strTemplateThumb .= '    </div>';
            $strTemplateThumb .= '</div>';
        }

        if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1)
        {
            $strPriceRangesRatio = '('.Loc::getMessage(
                    'CT_BCE_CATALOG_RATIO_PRICE',
                    array('#RATIO#' => ($useRatio
                            ? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']
                            : '1'
                        ).' '.$measureName)
                ).')';

            foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range)
            {
                if ($range['HASH'] !== 'ZERO-INF')
                {
                    $itemPrice = false;

                    foreach ($jsOffer['ITEM_PRICES'] as $itemPrice)
                    {
                        if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
                        {
                            break;
                        }
                    }

                    if ($itemPrice)
                    {
                        $strPriceRanges .= '<dt>'.Loc::getMessage(
                                'CT_BCE_CATALOG_RANGE_FROM',
                                array('#FROM#' => $range['SORT_FROM'].' '.$measureName)
                            ).' ';

                        if (is_infinite($range['SORT_TO']))
                        {
                            $strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
                        }
                        else
                        {
                            $strPriceRanges .= Loc::getMessage(
                                'CT_BCE_CATALOG_RANGE_TO',
                                array('#TO#' => $range['SORT_TO'].' '.$measureName)
                            );
                        }

                        $strPriceRanges .= '</dt><dd>'.($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']).'</dd>';
                    }
                }
            }

            unset($range, $itemPrice);
        }

        $jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
        $jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
        $jsOffer['PRICE_RANGES_RATIO_HTML'] = Ratio;
        $jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;
        $jsOffer['SLIDER_IMG'] = $strTemplateSlide;
        $jsOffer['SLIDER_THUMBS'] = $strTemplateThumb;
    }

    $templateData['OFFER_IDS'] = $offerIds;
    $templateData['OFFER_CODES'] = $offerCodes;
    unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

    $jsParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => true,
            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
            'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
            'OFFER_GROUP' => $arResult['OFFER_GROUP'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
            'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
            'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
            'USE_STICKERS' => true,
            'USE_SUBSCRIBE' => $showSubscribe,
            'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
            'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
            'ALT' => $alt,
            'TITLE' => $title,
            'MAGNIFIER_ZOOM_PERCENT' => 200,
            'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
            'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
            'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
                ? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
                : null
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'VISUAL' => $itemIds,
        'DEFAULT_PICTURE' => array(
            'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
            'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
        ),
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'ACTIVE' => $arResult['ACTIVE'],
            'NAME' => $arResult['~NAME'],
            'CATEGORY' => $arResult['CATEGORY_PATH']
        ),
        'BASKET' => array(
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'BASKET_URL' => $arParams['BASKET_URL'],
            'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        ),
        'OFFERS' => $arResult['JS_OFFERS'],
        'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
        'TREE_PROPS' => $skuProps
    );
}
else
{
    $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
    if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties)
    {
        ?>
        <div id="<?=$itemIds['BASKET_PROP_DIV']?>" style="display: none;">
            <?
            if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
            {
                foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo)
                {
                    ?>
                    <input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=htmlspecialcharsbx($propInfo['ID'])?>">
                    <?
                    unset($arResult['PRODUCT_PROPERTIES'][$propId]);
                }
            }

            $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
            if (!$emptyProductProperties)
            {
                ?>
                <table>
                    <?
                    foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo)
                    {
                        ?>
                        <tr>
                            <td><?=$arResult['PROPERTIES'][$propId]['NAME']?></td>
                            <td>
                                <?
                                if (
                                    $arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L'
                                    && $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
                                )
                                {
                                    foreach ($propInfo['VALUES'] as $valueId => $value)
                                    {
                                        ?>
                                        <label>
                                            <input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]"
                                                   value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"checked"' : '')?>>
                                            <?=$value?>
                                        </label>
                                        <br>
                                        <?
                                    }
                                }
                                else
                                {
                                    ?>
                                    <select name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]">
                                        <?
                                        foreach ($propInfo['VALUES'] as $valueId => $value)
                                        {
                                            ?>
                                            <option value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"selected"' : '')?>>
                                                <?=$value?>
                                            </option>
                                            <?
                                        }
                                        ?>
                                    </select>
                                    <?
                                }
                                ?>
                            </td>
                        </tr>
                        <?
                    }
                    ?>
                </table>
                <?
            }
            ?>
        </div>
        <?
    }

    $jsParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
            'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
            'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
            'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
            'USE_STICKERS' => true,
            'USE_SUBSCRIBE' => $showSubscribe,
            'ALT' => $alt,
            'TITLE' => $title,
            'MAGNIFIER_ZOOM_PERCENT' => 200,
            'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
            'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
            'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
                ? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
                : null
        ),
        'VISUAL' => $itemIds,
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'ACTIVE' => $arResult['ACTIVE'],
            'PICT' => reset($arResult['MORE_PHOTO']),
            'NAME' => $arResult['~NAME'],
            'SUBSCRIPTION' => true,
            'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
            'ITEM_PRICES' => $arResult['ITEM_PRICES'],
            'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
            'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
            'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
            'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
            'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
            'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
            'SLIDER' => $arResult['MORE_PHOTO'],
            'CAN_BUY' => $arResult['CAN_BUY'],
            'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
            'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
            'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
            'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
            'CATEGORY' => $arResult['CATEGORY_PATH']
        ),
        'BASKET' => array(
            'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'EMPTY_PROPS' => $emptyProductProperties,
            'BASKET_URL' => $arParams['BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        )
    );
    unset($emptyProductProperties);
}

if ($arParams['DISPLAY_COMPARE'])
{
    $jsParams['COMPARE'] = array(
        'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
        'COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
        'COMPARE_PATH' => $arParams['COMPARE_PATH']
    );
}
?>
    <script>
        BX.message({
            ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
            TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
            TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
            BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
            BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
            BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
            BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
            BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
            TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
            COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
            COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
            COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
            BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
            PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
            PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
            RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
            RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
            SITE_ID: '<?=SITE_ID?>'
        });
        var <?=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
    </script>
<?
unset($actualItem, $itemIds, $jsParams);