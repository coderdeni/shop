<?php

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="stocksSub">
    <div class="stocksSub__container">
        <div class="stocksSub__content">
        <?
        foreach($arResult["ITEMS"] as $arItem)
        {
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="stockSub" id="<?= $this->GetEditAreaId($arItem['ID']) ?>">
                <div class="stockSub__category stockSub__category_<?= $arItem['PROPERTIES']['TYPE']['VALUE_XML_ID'] ?>"><?= $arItem['PROPERTIES']['TYPE']['VALUE'] ?></div>
                <div class="stockSub__photo"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="stockSub__photoLink"><img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="" class="stockSub__img"></a></div>
                <div class="stockSub__date">
                    <div class="stockSub__period">
                        <?= (!$arItem['ACTIVE_TO'])
                            ? Loc::getMessage('DATE', [
                                '#DATE_FROM#' => FormatDateFromDB($arItem['ACTIVE_FROM'], 'DD MMMM')
                            ])
                            : Loc::getMessage('PERIOD', [
                                '#DATE_FROM#' => FormatDateFromDB($arItem['ACTIVE_FROM'], 'DD MMMM'),
                                '#DATE_TO#' => FormatDateFromDB($arItem['ACTIVE_TO'], 'DD MMMM'),
                            ])
                        ?>
                    </div>
                    <?
                    if ($arItem['ACTIVE_TO'])
                    {
                        ?>
                        <div data-role="timer" data-timer="<?= strtotime($arItem['ACTIVE_TO'])*1000?>" class="stockSub__end"></div>
                        <?
                    }
                    ?>
                </div>
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="stockSub__title"><?= $arItem['NAME'] ?></a>
            </div>
            <?
        }
        ?>
        </div>
    </div>
</div>
<?
if ($arParams["DISPLAY_BOTTOM_PAGER"])
{
    echo $arResult["NAV_STRING"];
}
?>
<script type="application/javascript">
    new AgActionsTimers({
        selectorTimerContainer: '[data-role="timer"]',
        format: '<?= Loc::getMessage('FORMAT_TIMER')?>'
    });
</script>
