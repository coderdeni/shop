<?php

use Artixgroup\Shop\Setting\Parameters;
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:sender.subscribe",
        "subscription",
        Array()
    );?>
    <footer class="footer">
        <div class="footer__container limiter">
            <div class="footer__content">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footer__box",
                    array(
                        "ROOT_MENU_TYPE" => "catalog_bottom",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_THEME" => "site",
                        "CACHE_SELECTED_ITEMS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "catalog_bottom",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "footer__box",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "TITLE" => Loc::getMessage("MENU_CATALOG_BOTTOM"),
                        "IS_LEFT" => "N"
                    ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footer__box",
                    array(
                        "ROOT_MENU_TYPE" => "online_shop",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_THEME" => "site",
                        "CACHE_SELECTED_ITEMS" => "N",
                        "MENU_CACHE_GET_VARS" => "",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "footer__box",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "TITLE" => Loc::getMessage('MENU_INTERNET_SHOP')
                    ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footer__box",
                    array(
                        "ROOT_MENU_TYPE" => "buyers",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_THEME" => "site",
                        "CACHE_SELECTED_ITEMS" => "N",
                        "MENU_CACHE_GET_VARS" => "",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "footer__box",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "TITLE" => Loc::getMessage('MENU_BUYERS')
                    ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footer__box",
                    array(
                        "ROOT_MENU_TYPE" => "personal",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_THEME" => "site",
                        "CACHE_SELECTED_ITEMS" => "N",
                        "MENU_CACHE_GET_VARS" => "",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "footer__box",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "TITLE" => Loc::getMessage('MENU_PERSONAL')
                    ),
                    false
                );?>
                <div class="footer__box">
                    <div class="footer__title"><?= Loc::getMessage('CONTACT_CENTER')?></div>
                    <div class="footer__phone">
                        <?= Parameters::getInstance()->getText('phone')?>
                    </div>
                    <?
                    if (Parameters::getInstance()->getBool('show_callback'))
                    {
                        ?>
                        <div class="footer__button">
                            <a href="javascript:void(0)" class="button button_black-line trigger" data-dialog="requestCall">
                                <?= Parameters::getInstance()->getText('callback_name')?>
                            </a>
                        </div>
                        <?
                    }
                    ?>
                    <div class="social">
                        <?
                        if (Parameters::getInstance()->notEmpty('facebook_url'))
                        {
                            ?>
                            <a href="<?= Parameters::getInstance()->getText('facebook_url')?>" class="social__item">
                                <svg class="icon icon-fb">
                                    <use xlink:href="#icon-fb"></use>
                                </svg>
                            </a>
                            <?
                        }
                        if (Parameters::getInstance()->notEmpty('ok_url'))
                        {
                            ?>
                            <a href="<?= Parameters::getInstance()->getText('ok_url')?>" class="social__item">
                                <svg class="icon icon-ok">
                                    <use xlink:href="#icon-ok"></use>
                                </svg>
                            </a>
                            <?
                        }
                        if (Parameters::getInstance()->notEmpty('vk_url'))
                        {
                            ?>
                            <a href="<?= Parameters::getInstance()->getText('vk_url')?>" class="social__item">
                                <svg class="icon icon-vk">
                                    <use xlink:href="#icon-vk"></use>
                                </svg>
                            </a>
                            <?
                        }
                        if (Parameters::getInstance()->notEmpty('twitter_url'))
                        {
                            ?>
                            <a href="<?= Parameters::getInstance()->getText('twitter_url')?>" class="social__item">
                                <svg class="icon icon-twitter">
                                    <use xlink:href="#icon-twitter"></use>
                                </svg>
                            </a>
                            <?
                        }
                        if (Parameters::getInstance()->notEmpty('instagram_url'))
                        {
                            ?>
                            <a href="<?= Parameters::getInstance()->getText('instagram_url')?>" class="social__item">
                                <svg class="icon icon-ig">
                                    <use xlink:href="#icon-ig"></use>
                                </svg>
                            </a>
                            <?
                        }
                        if (Parameters::getInstance()->notEmpty('youtube_url'))
                        {
                            ?>
                            <a href="<?= Parameters::getInstance()->getText('youtube_url')?>" class="social__item">
                                <svg class="icon icon-youtube">
                                    <use xlink:href="#icon-youtube"></use>
                                </svg>
                            </a>
                            <?
                        }
                        ?>
                    </div>
                </div>
                <div class="footer__box footer__box_cr">
                    <div class="footer__cr">
                        © <?= Parameters::getInstance()->getText('site_name')?>, <?= date('Y')?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    </div>
</div>
<?
if (Parameters::getInstance()->getBool('show_geoip'))
{
    ?>
    <?$APPLICATION->IncludeComponent(
        "artixgroup:ipgeo.city.list",
        ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
            "COOKIE_EXPIRES" => "604800",
            "CITY_LIST" => array(
                0 => "Астрахань",
                1 => "Барнаул",
                2 => "Владивосток",
                3 => "Волгоград",
                4 => "Воронеж",
                5 => "Екатеринбург",
                6 => "Ижевск",
                7 => "Иркутск",
                8 => "Казань",
                9 => "Кемерово",
                10 => "Киров",
                11 => "Краснодар",
                12 => "Красноярск",
                13 => "Липецк",
                14 => "Москва",
                15 => "Махачкала",
                16 => "Набережные Челны",
                17 => "Нижний Новгород",
                18 => "Новокузнецк",
                19 => "Новосибирск",
                20 => "Омск",
                21 => "Оренбург",
                22 => "Пенза",
                23 => "Пермь",
                24 => "Ростов-на-Дону",
                25 => "Самара",
                26 => "Саратов",
                27 => "Санкт-Петербург",
                28 => "Тольятти",
            ),
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "36000000",
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO",
            "LOCATION_TYPE" => array(
                0 => "5",
                1 => "6",
                2 => "",
            ),
            "SEARCH_LIMIT" => "10",
            "MIN_LENGTH" => "2"
        ),
        false
    );?>
    <?
}
?>
<div id="requestCall" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__noscroll">
        <div class="dialog__container">
            <div class="dialog__title">Заказать звонок</div>
            <div class="dialog__content">
                <div class="authorization">
                    <div class="authorization__form">
                        <form action="" class="form">
                            <div class="form__row">
                                <input type="text" class="input input_big" placeholder="Имя *">
                            </div>
                            <div class="form__row">
                                <input type="tel" class="input input_big" placeholder="Телефон *">
                            </div>
                            <div class="form__row">
                                <textarea name="" id="" rows="3" class="textarea textarea_big" placeholder="Комментарий"></textarea>
                            </div>
                            <div class="form__row form__row_sb">
                                <input type="submit" class="button button_bp" value="Отправить">
                            </div>
                            <div class="form__row form__row_sb">
                                <label class="checkbox">
                                    <input type="checkbox" class="checkbox__input">
                                    <span class="checkbox__new-input"></span>
                                    Я согласен на обработку моих персональных данных
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="dialog__close">
                <button class="action close" data-dialog-close="">
                    <svg class="icon icon-close ">
                        <use xlink:href="#icon-close"></use>
                    </svg>
                </button>
            </div>
        </div>
        <div class="dialog__event"></div>
    </div>
</div>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.register",
    "regdialog",
    array(
        "AUTH" => "N",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "REQUIRED_FIELDS" => array(
            0 => "NAME",
        ),
        "SET_TITLE" => "N",
        "SHOW_FIELDS" => array(
            0 => "NAME",
            1 => "LAST_NAME",
        ),
        "SUCCESS_PAGE" => "",
        "USER_PROPERTY" => array(
        ),
        "USER_PROPERTY_NAME" => "",
        "USE_BACKURL" => "Y",
        "COMPONENT_TEMPLATE" => "regDialog"
    ),
    false
);?>
<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "logindialog",
    array(
        "REGISTER_URL" => "/login/",
        "PROFILE_URL" => "/personal/",
        "SHOW_ERRORS" => "N"
    ),
    false,
    array()
);?>
<div id="productInstallment" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__noscroll">
        <div class="dialog__container">
            <div class="dialog__title">Оформить в рассрочку</div>
            <div class="dialog__content">
                <div class="productBuyOneDialog">
                    <div class="productBuyOneDialog__product">
                        <div class="catalogItem">
                            <div class="catalogItem__container">
                                <div class="catalogItem__label">
                                    <div class="saleLabel"><span class="saleLabel__text">-35%</span></div>
                                </div>
                                <a href="product.html" class="catalogItem__content">
                                    <div class="catalogItem__wrapper">
                                        <div class="catalogItem__photo"><img src="files/product_1.jpg" alt="" class="catalogItem__img"></div>
                                        <span class="catalogItem__title">Набор кукол Steffi Love Веселая прогулка Штеффи, Еви, Тимми, 29, 12 и 12 см, 5733229</span>
                                    </div>
                                    <div class="catalogItem__price">
                                        <div class="prices">
                                            <div class="price">29 854 <span class="ruble">руб.</span></div>
                                            <div class="price-old">39 854 <span class="ruble">руб.</span></div>
                                        </div>
                                        <a href="#" class="price-installment">Рассрочка от 5800 <span class="ruble">руб.</span>/месяц</a>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="productBuyOneDialog__form">
                        <form action="" class="form">
                            <div class="form__row">
                                <div class="authorization__infoText">Наш менеджер свяжется с вами в ближайшее время</div>
                            </div>
                            <div class="form__row">
                                <input type="text" class="input input_big" placeholder="Имя *">
                            </div>
                            <div class="form__row">
                                <input type="tel" class="input input_big" placeholder="Телефон *">
                            </div>
                            <div class="form__row">
                                <input type="text" class="input input_big" placeholder="Электронная почта *">
                            </div>
                            <div class="form__row form__row_sb">
                                <input type="submit" class="button button_bp" value="Отправить">
                            </div>
                            <div class="form__row form__row_sb">
                                <label class="checkbox">
                                    <input type="checkbox" class="checkbox__input">
                                    <span class="checkbox__new-input"></span>
                                    Я согласен на обработку моих персональных данных
                                </label>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="dialog__close">
                <button class="action close" data-dialog-close="">
                    <svg class="icon icon-close ">
                        <use xlink:href="#icon-close"></use>
                    </svg>
                </button>
            </div>
        </div>
        <div class="dialog__event"></div>
    </div>
</div>
<?$APPLICATION->IncludeComponent(
    "artixgroup:settings",
    "",
    array(
    ),
    false
);?>
</body>
</html>