    $(document).ready(function () {
        $(document).on('click', '.js-toggle-class', function (e) {
            e.preventDefault();
            $($(this).attr('data-target')).toggleClass($(this).attr('data-class'));
        });
		$( ".js-toggle-class-hover" ).hover(
			function() {
				$($(this).attr('data-target')).addClass($(this).attr('data-class'));
			}, function() {
				$($(this).attr('data-target')).removeClass($(this).attr('data-class'));
			}
		);
    });
/*
* <div class="other-class hide">Скрытый текст</div>
* <a href="#" class="js-toggle-class" data-target=".other-class" data-class="hide">Click me!</a>
*/
