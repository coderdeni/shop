// Popular products
var swiper1 = new Swiper('.slider__container', {
  loop: true,
  lazy: true,
  observer: true,
  observeParents: true,
  paginationClickable: true,
  autoplay: {
    delay: 4500,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.slider__pagination',
    clickable: true,
    renderBullet: function (index, className) {
      return '<span class="' + className + '"></span>';
    },
  },
  navigation: {
    nextEl: '.slider__arrow-left',
    prevEl: '.slider__arrow-right',
  },
});


var swiper2 = new Swiper('.instagram__container', {
  loop: true,
  lazy: true,
  slidesPerView: 4,
  paginationClickable: true,
  spaceBetween: 10,
  observer: true,
  observeParents: true,
  pagination: {
    el: '.instagram__pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.instagram__arrow-right',
    prevEl: '.instagram__arrow-left',
  },
  breakpoints: {
    0: {
      slidesPerView: 1,
    },
    480: {
      slidesPerView: 2,
    },
    640: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 3,
    },
    1200: {
      slidesPerView: 3,
    },
    1201: {
      slidesPerView: 4,
    },
  }
});



var galleryThumbs = new Swiper('.photo-slider__thumbs', {
  spaceBetween: 10,
  slidesPerView: 'auto',
  touchRatio: 0.2,
});

var galleryTop = new Swiper('.photo-slider__main', {
  spaceBetween: 10,
  loop: true,
  loopedSlides: 5, //looped slides should be the same
  observer: true,
  observeParents: true,
  navigation: {
    nextEl: '.photo-slider__arrow_right',
    prevEl: '.photo-slider__arrow_left',
  },
  thumbs: {
    swiper: galleryThumbs,
  },
});


var swiper3 = new Swiper('.catalog-similar__container', {
  slidesPerView: 3,
  paginationClickable: true,
  spaceBetween: 50,
  observer: true,
  observeParents: true,
  loop: true,
  loopedSlides: 5, //looped slides should be the same
  pagination: {
    el: '.catalog-similar__pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.catalog-similar__arrow-right',
    prevEl: '.catalog-similar__arrow-left',
  },
  breakpoints: {
    0: {
      slidesPerView: 1,
    },
    480: {
      slidesPerView: 1,
    },
    640: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 2,
    },
    1024: {
      slidesPerView: 3,
    },
    1280: {
      slidesPerView: 3,
    },
    1281: {
      slidesPerView: 3,
    },
  }
});
