(function (window) {
	window.ArtixGroupDialog = function (params)
	{
		this.dialogOpenClass = !!params.dialogOpenClass ? params.dialogOpenClass : 'dialog--open';
		this.dialogCloseClass = !!params.dialogOpenClass ? params.dialogOpenClass : 'dialog--close';
		this.dialogOpenSelector = !!params.dialogOpenClass ? '.'+params.dialogOpenClass : '.dialog--open';
		this.bodyOpenClass = !!params.bodyOpenClass ? '.'+params.bodyOpenClass : 'noscroll';
		this.triggerSelector = !!params.triggerSelector ? params.triggerSelector : '.trigger';
		this.actionSelector = !!params.actionSelector ? params.actionSelector : '.action, .dialog__event';
		this.dialogContainerSelector = !!params.dialogContainerSelector ? params.dialogContainerSelector : '.dialog__container';
		this.animationEvents = ['webkitAnimationEnd', 'MSAnimationEnd', 'oAnimationEnd', 'animationend'];
		this.dialog = null;

		BX.ready(BX.delegate(this.init, this));
	};

	window.ArtixGroupDialog.prototype = {

		init : function ()
		{
			this.triggers = document.querySelectorAll(this.triggerSelector);
			this.actions = document.querySelectorAll(this.actionSelector);

			if (!!this.triggers)
			{
				for (var i = 0; i < this.triggers.length; i++)
					this.triggers[i].addEventListener('click', BX.delegate(this.openDialog, this));
			}

			if (!!this.actions)
			{
				for (var i = 0; i < this.actions.length; i++)
					this.actions[i].addEventListener('click', BX.delegate(this.closeDialog, this));
			}
		},

		openDialog : function ()
		{
			if (!!this.dialog) this.closeDialog();
			this.dialog = document.getElementById(BX.proxy_context.getAttribute('data-dialog'));//TODO:!!!

			if (!!this.dialog)
			{
				this.dialog.classList.add(this.dialogOpenClass);
				document.body.classList.add(this.bodyOpenClass);
			}
		},

		closeDialog : function ()
		{
			if (!this.dialog)
				this.dialog = document.querySelector(this.dialogOpenSelector);

			if (!!this.dialog)
			{
				this.dialog.classList.remove(this.dialogOpenClass);
				this.dialog.classList.add(this.dialogCloseClass);
				document.body.classList.remove(this.bodyOpenClass);

				var container = this.dialog.querySelector(this.dialogContainerSelector);
				if (!!container)
				{
					for (var i = 0; i < this.animationEvents.length; i++)
						container.addEventListener(this.animationEvents[i], BX.proxy(this.endAnimation, this));
				}
				else
				{
					this.dialog.classList.remove(this.dialogCloseClass);
				}

				this.dialog = null;
			}
		},

		endAnimation : function ()
		{
			for (var i = 0; i < this.animationEvents.length; i++)
				BX.proxy_context.removeEventListener(this.animationEvents[i], BX.proxy(this.endAnimation, this));

			BX.proxy_context.parentNode.parentNode.classList.remove(this.dialogCloseClass);
		}
	};
	var currentDialog = new ArtixGroupDialog({});
})(window);