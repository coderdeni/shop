<?php
$MESS['MENU_CATALOG_BOTTOM'] = 'Product catalog';
$MESS['MENU_INTERNET_SHOP'] = 'Online store';
$MESS['MENU_BUYERS'] = 'Buyers';
$MESS['MENU_PERSONAL'] = 'Personal account';
$MESS['CONTACT_CENTER'] = 'Contact center';
$MESS['PAYMENT_METHOD'] = 'Payment method';
$MESS['PAYMENT_INFO'] = 'You can pay for your purchases in cash at the store, or choose another payment method that is convenient for you.';