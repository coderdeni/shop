<?php
$MESS['MENU_CATALOG_BOTTOM'] = 'Каталог товаров';
$MESS['MENU_INTERNET_SHOP'] = 'Интернет-магазин';
$MESS['MENU_BUYERS'] = 'Покупателям';
$MESS['MENU_PERSONAL'] = 'Личный кабинет';
$MESS['CONTACT_CENTER'] = 'Контактный центр';
$MESS['PAYMENT_METHOD'] = 'Способы оплаты';
$MESS['PAYMENT_INFO'] = 'Вы можете оплатить покупки наличными в салоне, либо выбрать другой удобный для вас способ оплаты.';