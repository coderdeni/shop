<?php

use Artixgroup\Shop\Services\Bonus;
use Artixgroup\Shop\Setting\Parameters;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

return [
    'site_name' => [
        'NAME' => Loc::getMessage('PARAM_SITE_NAME'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_BASE,
        'VALUE' => Loc::getMessage('PARAM_SITE_NAME_VALUE')
    ],
    'logotype' => [
        'NAME' => Loc::getMessage('PARAM_LOGOTYPE'),
        'TYPE' => Parameters::TYPE_FILE,
        'EXT' => ['gif','jpg','jpe','jpeg','png','svg'],
        'GROUP' => Parameters::GROUP_BASE,
        'VALUE' => '/include/logo.png'
    ],
    'show_favourites' => [
        'NAME' => Loc::getMessage('PARAM_SHOW_FAVOURITES'),
        'TYPE' => Parameters::TYPE_CHECKBOX,
        'GROUP' => Parameters::GROUP_BASE,
        'VALUE' => 1,
        'VALUES' => [
            1 => Loc::getMessage('PARAM_SHOW_FAVOURITES')
        ]
    ],
    'show_geoip' => [
        'NAME' => Loc::getMessage('PARAM_SHOW_GEOIP'),
        'TYPE' => Parameters::TYPE_CHECKBOX,
        'GROUP' => Parameters::GROUP_BASE,
        'VALUE' => 1,
        'VALUES' => [
            1 => Loc::getMessage('PARAM_SHOW_GEOIP')
        ]
    ],
    'show_callback' => [
        'NAME' => Loc::getMessage('PARAM_SHOW_CALLBACK'),
        'TYPE' => Parameters::TYPE_CHECKBOX,
        'GROUP' => Parameters::GROUP_BASE,
        'VALUE' => 1,
        'VALUES' => [
            1 => Loc::getMessage('PARAM_SHOW_CALLBACK')
        ]
    ],
    'callback_name' => [
        'NAME' => Loc::getMessage('PARAM_CALLBACK_NAME'),
        'TYPE' => Parameters::TYPE_TEXT,
        'LINK' => 'show_callback',
        'GROUP' => Parameters::GROUP_BASE,
        'VALUE' => Loc::getMessage('PARAM_CALLBACK_NAME_VALUE')
    ],
    'catalog_id' => [
        'NAME' => Loc::getMessage('PARAM_CATALOG_ID'),
        'TYPE' => Parameters::TYPE_SELECT,
        'GROUP' => Parameters::GROUP_CATALOG,
        'VALUE' => 2,
        'VALUES' => function() {
            $iblocks = [];
            if (Loader::includeModule('iblock'))
            {
                $iterator = \Bitrix\Iblock\IblockTable::getList(['order' => 'IBLOCK_TYPE_ID']);
                while ($iblock = $iterator->fetch())
                    $iblocks[$iblock['ID']] = '['.$iblock['IBLOCK_TYPE_ID'].'] '.$iblock['NAME'];
            }

            return $iblocks;
        }
    ],
    'price' => [
        'NAME' => Loc::getMessage('PARAM_PRICE'),
        'TYPE' => Parameters::TYPE_SELECT,
        'GROUP' => Parameters::GROUP_CATALOG,
        'VALUE' => 'BASE',
        'VALUES' => function() {
            $prices = [];
            if (Loader::includeModule('catalog'))
            {
                $dbResultList = CCatalogGroup::GetList(['SORT' => 'ASC'], [], false, false, ['NAME']);
                while ($price = $dbResultList->Fetch())
                    $prices[$price['NAME']] = $price['NAME'];
            }

            return $prices;
        }
    ],
    'show_brands' => [
        'NAME' => Loc::getMessage('PARAM_SHOW_BRANDS'),
        'TYPE' => Parameters::TYPE_CHECKBOX,
        'GROUP' => Parameters::GROUP_CATALOG,
        'VALUE' => 1,
        'VALUES' => [
            1 => Loc::getMessage('PARAM_SHOW_BRANDS')
        ]
    ],
    'show_buy_one_click' => [
        'NAME' => Loc::getMessage('PARAM_SHOW_BUY_ONE_CLICK'),
        'TYPE' => Parameters::TYPE_CHECKBOX,
        'GROUP' => Parameters::GROUP_CATALOG,
        'VALUE' => 1,
        'VALUES' => [
            1 => Loc::getMessage('PARAM_SHOW_BUY_ONE_CLICK')
        ]
    ],
    'show_installment' => [
        'NAME' => Loc::getMessage('PARAM_SHOW_INSTALLMENT'),
        'TYPE' => Parameters::TYPE_CHECKBOX,
        'GROUP' => Parameters::GROUP_CATALOG,
        'LINK' => [1 => ['max_installment_month']],
        'VALUE' => 1,
        'VALUES' => [
            1 => Loc::getMessage('PARAM_SHOW_INSTALLMENT')
        ]
    ],
    'max_installment_month' => [
        'NAME' => Loc::getMessage('PARAM_MAX_INSTALLMENT_MONTH'),
        'TYPE' => Parameters::TYPE_TEXT,
        'LINK' => 'show_installment',
        'GROUP' => Parameters::GROUP_CATALOG,
        'VALUE' => 12
    ],
    'bonus_enabled' => [
        'NAME' => Loc::getMessage('PARAM_BONUS_ENABLED'),
        'TYPE' => Parameters::TYPE_CHECKBOX,
        'GROUP' => Parameters::GROUP_BONUS,
        'LINK' => [1 => ['bonus_mode', 'bonus_course', 'bonus_currency', 'bonus_property']],
        'VALUE' => 1,
        'VALUES' => [
            1 => Loc::getMessage('PARAM_BONUS_ENABLED')
        ]
    ],
    'bonus_mode' => [
        'NAME' => Loc::getMessage('PARAM_BONUS_MODE'),
        'TYPE' => Parameters::TYPE_SELECT,
        'GROUP' => Parameters::GROUP_BONUS,
        'LINK' => [
            Bonus::BONUS_MODE_BOTH => ['bonus_course', 'bonus_property'],
            Bonus::BONUS_MODE_COURSE => ['bonus_course'],
            Bonus::BONUS_MODE_PROPERTY => ['bonus_property']
        ],
        'VALUE' => Bonus::BONUS_MODE_BOTH,
        'VALUES' => [
            Bonus::BONUS_MODE_BOTH => Loc::getMessage('PARAM_BONUS_MODE_BOTH'),
            Bonus::BONUS_MODE_COURSE => Loc::getMessage('PARAM_BONUS_MODE_COURSE'),
            Bonus::BONUS_MODE_PROPERTY => Loc::getMessage('PARAM_BONUS_MODE_PROPERTY')
        ]
    ],
    'bonus_course' => [
        'NAME' => Loc::getMessage('PARAM_BONUS_COURSE'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_BONUS,
        'VALUE' => 10
    ],
    'bonus_currency' => [
        'NAME' => Loc::getMessage('PARAM_BONUS_CURRENCY'),
        'TYPE' => Parameters::TYPE_SELECT,
        'GROUP' => Parameters::GROUP_BONUS,
        'VALUE' => 'RUB',
        'VALUES' => function() {
            $currencies = [];
            if (Loader::includeModule('currency'))
            {
                $iterator = \Bitrix\Currency\CurrencyTable::getList([
                    'select' => ['CURRENCY', 'NAME' => 'CURRENT_LANG_FORMAT.FULL_NAME']
                ]);
                while ($price = $iterator->fetch())
                    $currencies[$price['CURRENCY']] = $price['NAME'];
            }

            return $currencies;
        }
    ],
    'bonus_property' => [
        'NAME' => Loc::getMessage('PARAM_BONUS_PROPERTY'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_BONUS,
        'VALUE' => 'BONUS',
    ],
    'phone' => [
        'NAME' => Loc::getMessage('PARAM_PHONE'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_CONTACTS,
        'VALUE' => '8 800 2000 600'
    ],
    'email' => [
        'NAME' => Loc::getMessage('PARAM_EMAIL'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_CONTACTS,
        'VALUE' => 'info@site.ru'
    ],
    'vk_url' => [
        'NAME' => Loc::getMessage('PARAM_VK_URL'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_CONTACTS,
        'VALUE' => 'https://vk.com/'
    ],
    'facebook_url' => [
        'NAME' => Loc::getMessage('PARAM_FACEBOOK_URL'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_CONTACTS,
        'VALUE' => 'https://facebook.com/'
    ],
    'twitter_url' => [
        'NAME' => Loc::getMessage('PARAM_TWITTER_URL'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_CONTACTS,
        'VALUE' => 'https://twitter.com/'
    ],
    'ok_url' => [
        'NAME' => Loc::getMessage('PARAM_OK_URL'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_CONTACTS,
        'VALUE' => 'https://ok.ru/'
    ],
    'instagram_url' => [
        'NAME' => Loc::getMessage('PARAM_INSTAGRAM_URL'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_CONTACTS,
        'VALUE' => 'https://instagram.com/'
    ],
    'youtube_url' => [
        'NAME' => Loc::getMessage('PARAM_YOUTUBE_URL'),
        'TYPE' => Parameters::TYPE_TEXT,
        'GROUP' => Parameters::GROUP_CONTACTS,
        'VALUE' => 'https://youtube.com/'
    ],
];
