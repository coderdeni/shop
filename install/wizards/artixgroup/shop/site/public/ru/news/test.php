<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");

use Bitrix\Main;
use Bitrix\Sale;

if (Main\Loader::includeModule('sale'))
{
    $deliveryDate = "";
    $order = Sale\Order::load(70);

    if ($order)
    {
        $propertyCollection = $order->getPropertyCollection();
        $deliveryId = $order->getField("DELIVERY_ID");
        $locationId = $propertyCollection->getDeliveryLocation()->getValue();

        if ($deliveryId == 18 && $locationId == "0000073738")
        {
            $dateInsert = $order->getField("DATE_INSERT");
            $deliveryDate = "Дата доставки: ";
            $deliveryDate .= $dateInsert->add("+7 days")->format("d.m.Y H:i:s");
        }

        echo '<pre>'; print_r($deliveryDate); echo '</pre>';
    }
}


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
