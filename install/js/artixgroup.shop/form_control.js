(function (window){
    window.FormControl = function (params)
    {
        this.actionUrl = params.actionUrl;
        this.fieldTypes = params.fieldTypes;
        this.lastIndex = !!params.countFields ? params.countFields : 0;
        this.dragged = null;
        this.dragInputRow = null;
        this.dragInputHelper = null;
        this.popup = null;
        this.blockParameters = null;
        this.currentParameters = null;


        BX.ready(BX.delegate(this.init, this));
    };

    window.FormControl.prototype = {

        init: function()
        {
            this.form = document.querySelector('.form');
            this.inputs = this.form.querySelectorAll('.form__input');
            this.btnContainer = document.getElementById('btn-container');
            this.showModeInput = document.getElementById('show_mode');
            this.autoloadRow = document.getElementById('autoload_row');
            this.autoloadInput = document.getElementById('autoload');
            this.autoloadTimerRow = document.getElementById('autoload_timer_row');
            this.cols = this.form.querySelectorAll('.form__col');
            this.currentCol = 1;

            if (!!this.inputs)
            {
                for (var i = 0; i < this.inputs.length; i++)
                    this.bindInput(this.inputs[i]);
            }

            this.showModeInput.addEventListener("change", BX.delegate(this.onShowModeChange, this), false);
            this.autoloadInput.addEventListener("change", BX.delegate(this.onAutoloadChange, this), false);

            document.addEventListener("drag", function( event ) {

            }, false);

            document.addEventListener("dragstart", BX.delegate(this.dragStart, this), false);

            document.addEventListener("dragend", BX.delegate(this.dragEnd, this), false);

            /* events fired on the drop targets */
            document.addEventListener("dragover", function( event ) {
                // prevent default to allow drop
                event.preventDefault();
            }, false);

            document.addEventListener("dragenter", function( event ) {

                if (event.target.nodeType > 1)
                    return;

                if ( event.target.classList.contains('form__row-drag-helper') ) {
                    event.target.classList.add('form__row-drag-helper_over');
                } else if ( event.target.classList.contains('form__sort-drag-helper') ) {
                    event.target.classList.add('form__sort-drag-helper_over');
                }

            }, false);

            document.addEventListener("dragleave", function( event ) {

                if (event.target.nodeType > 1)
                    return;

                if ( event.target.classList.contains('form__row-drag-helper') ) {
                    event.target.classList.remove('form__row-drag-helper_over');
                } else if ( event.target.classList.contains('form__sort-drag-helper') ) {
                    event.target.classList.remove('form__sort-drag-helper_over');
                }

            }, false);

            document.addEventListener("drop", BX.delegate(this.drop, this), false);
            var items = [];

            for (var type in this.fieldTypes)
            {
                items.push({
                    text: this.fieldTypes[type],
                    href: this.actionUrl+'?type='+type,
                    onclick: BX.delegate(function(event, item) {
                        this.addInput(item.href);
                        item.getMenuWindow().close();
                        event.preventDefault();
                    }, this),
                });
            }

            var button = new BX.UI.Button({
                text: "Добавить поле",
                color: BX.UI.Button.Color.LIGHT_BORDER,
                icon: BX.UI.Button.Icon.ADD,
                dropdown: true,
                menu: {
                    items: items,
                    closeByEsc: true,
                    offsetTop: 5,
                },
                events: {
                    click: function(button, event) {
                        event.preventDefault();
                    },
                }
            });
            button.renderTo(this.btnContainer);
        },

        recalculateOrder: function()
        {
            var cols = this.form.querySelectorAll('.form__col'),
                inputs, c, r, s, rows, idx = 0;

            for (c = 0; c < cols.length; c++)
            {
                rows = cols[c].querySelectorAll('.form__row');

                for (r = 0; r < rows.length; r++)
                {
                    inputs = rows[r].querySelectorAll('.form__input');

                    for (s = 0; s < inputs.length; s++, idx++)
                    {
                        var parametersInput = inputs[s].querySelector('.form__input-parameters');
                        var parameters = JSON.parse(parametersInput.value);

                        parameters['COL'] = c;
                        parameters['ROW'] = r;
                        parameters['SORT'] = idx;

                        parametersInput.innerHTML = JSON.stringify(parameters);
                    }
                }
            }
        },

        onShowModeChange: function(input)
        {
            if (this.showModeInput.value == 'S')
            {
                this.autoloadRow.style.display = 'none';
                this.autoloadInput.checked = false;
                this.onAutoloadChange();
            }
            else
            {
                this.autoloadRow.style.display = null;
            }
        },

        onAutoloadChange: function(input)
        {
            this.autoloadTimerRow.style.display = this.autoloadInput.checked ? null : 'none';
        },

        bindInput: function(input)
        {
            var deleteLink = input.querySelector('.form__input-delete-link'),
                settingLink = input.querySelector('.form__input-setting-link');

            BX.bind(deleteLink, 'click', BX.delegate(this.deleteInput, this));
            BX.bind(settingLink, 'click', BX.delegate(this.showParameters, this));
        },

        showParameters: function()
        {
            var input = this.getParentByClass(event.target, 'form__input');
            this.blockParameters = input.querySelector('.form__input-parameters');

            this.popup = new BX.CAdminDialog({
                'title': 'Параметры поля',
                'content_url': this.actionUrl + '?action=parameters&bxpublic=Y',
                'content_post': JSON.parse(this.blockParameters.value),
                'draggable': true,
                'resizable': true,
                'buttons': [BX.CAdminDialog.btnSave, BX.CAdminDialog.btnCancel]
            });

            this.popup.Show();
        },

        setParameters: function(value)
        {
            var parameters = JSON.parse(value),
                container = this.blockParameters.parentNode.querySelector('.form__input-container');

            container.innerHTML = parameters.NAME;

            if (parameters.REQUIRED == 'Y')
            {
                var div = document.createElement('span');
                div.classList.add('form__input-required');
                div.innerText = '*';
                container.appendChild(div);
            }

            this.blockParameters.innerHTML = value;
            this.popup = null;
            this.blockParameters = null;
        },

        addInput(url)
        {
            var oUrl = new URL(url, window.location.origin);
            oUrl.searchParams.set('action', 'new');
            oUrl.searchParams.set('key', this.lastIndex);

            BX.ajax({
                url: oUrl.href,
                method: 'POST',
                dataType: 'html',
                processData: false,
                onsuccess: BX.delegate(function(data) {
                    var col = this.getCurrentCol(),
                        row = this.createRowBefore(col.children[col.children.length - 1]),
                        input;

                    row.insertAdjacentHTML('beforeEnd', data);
                    input = row.children[row.children.length - 2];

                    var scripts = input.getElementsByTagName("script");
                    for (var i = 0; i < scripts.length; ++i) {
                        var script = scripts[i];
                        eval(script.innerHTML);
                    }

                    this.bindInput(input);
                    this.recalculateOrder();
                    this.lastIndex++;
                }, this)
            });
        },

        deleteInput: function()
        {
            if (confirm("Вы действительно хотите удалить поле?"))
            {
                var input = this.getParentByClass(event.target, 'form__input'),
                    row = input.parentNode,
                    helper = this.getNextHelper(input);

                row.removeChild(input);

                if (!row.querySelector('.form__input'))
                {
                    helper = this.getNextHelper(row);
                    row.parentNode.removeChild(helper);
                    row.parentNode.removeChild(row);
                }
                else
                {
                    row.removeChild(helper);
                }

                this.recalculateOrder();
            }
        },

        dragStart: function(e)
        {
            this.dragged = event.target;
            this.form.classList.add('form_dragged');
            event.target.style.opacity = .5;
        },

        dragMove: function(e)
        {
            if (!!this.dragInput)
            {
                this.dragInput.style.left = e.pageX - this.shiftX + 'px';
                this.dragInput.style.top = e.pageY - this.shiftY + 'px';
            }
        },

        dragEnd: function(e)
        {
            if (!this.dragged)
                return;

            console.log(e);
            event.target.style.opacity = "";
            this.form.classList.remove('form_dragged');
        },

        drop: function( event )
        {
            // prevent default action (open as link for some elements)
            event.preventDefault();
            // move dragged elem to the selected drop target
            if (event.target.classList.contains('form__row-drag-helper'))
            {
                event.target.classList.remove('form__row-drag-helper_over');
                var row = this.createRowBefore(event.target),
                    oldRow = this.dragged.parentNode,
                    helper = this.getNextHelper(this.dragged);

                oldRow.removeChild( this.dragged );
                row.appendChild(helper);
                row.insertBefore(this.dragged, helper);

                if (!oldRow.querySelector('.form__input'))
                {
                    helper = this.getNextHelper(oldRow);
                    oldRow.parentNode.removeChild(helper);
                    oldRow.parentNode.removeChild(oldRow);
                }
                this.recalculateOrder();
            }
            else if (event.target.classList.contains('form__sort-drag-helper'))
            {
                event.target.classList.remove('form__sort-drag-helper_over');
                var row = event.target.parentNode,
                    oldRow = this.dragged.parentNode,
                    helper = this.getNextHelper(this.dragged);

                oldRow.removeChild( this.dragged );
                row.insertBefore(helper, event.target);
                row.insertBefore(this.dragged, event.target);

                if (!oldRow.querySelector('.form__input'))
                {
                    helper = this.getNextHelper(oldRow);
                    oldRow.parentNode.removeChild(helper);
                    oldRow.parentNode.removeChild(oldRow);
                }
                this.recalculateOrder();
            }
        },

        getCurrentCol: function ()
        {
            if (this.cols.length < 1)
            {
                for (var i = 0; i < 3; i++)
                {
                    this.createCol();
                }

                this.currentCol = 1;
            }

            return this.cols[this.currentCol];
        },

        createCol: function ()
        {
            var col = document.createElement('div'),
                helper = document.createElement('div');

            col.classList.add('form__col');
            helper.classList.add('form__row-drag-helper');

            this.form.appendChild(col);
            col.appendChild(helper);
            this.cols = this.form.querySelectorAll('.form__col');
        },

        createRowBefore: function (element)
        {
            var col = element.parentNode;
            var row = document.createElement('div');
            row.classList.add('form__row');
            col.insertBefore(row, element);

            var sortHelper = document.createElement('div');
            sortHelper.classList.add('form__sort-drag-helper');
            row.appendChild(sortHelper);

            var rowHelper = document.createElement('div');
            rowHelper.classList.add('form__row-drag-helper');

            col.insertBefore(rowHelper, row);

            return row;
        },

        getCoords: function (elem)
        {
            var box = elem.getBoundingClientRect();

            return {
                top: box.top + pageYOffset,
                left: box.left + pageXOffset
            };
        },

        getNextHelper: function (element)
        {
            for (var i = 0; i < element.parentNode.children.length; i++)
            {
                if (element.parentNode.children[i] == element)
                {
                    return element.parentNode.children[i + 1];
                }
            }

            return null;
        },

        getParentByClass: function (el, className)
        {
            if (!!el && el.classList.contains(className))
                return el;
            else if (el.parentNode)
                return this.getParentByClass(el.parentElement, className);

            return null;
        }
    };
})(window);