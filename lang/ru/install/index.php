<?
$MESS ['ARTIXGROUP_SHOP_MODULE_NAME'] = "Интернет-магазин от Artix group";
$MESS ['ARTIXGROUP_SHOP_MODULE_DESCRIPTION'] = "Современный, адаптивный шаблон интернет-магазина";
$MESS ['ARTIXGROUP_SHOP_INSTALL_ERROR_VERSION'] = "Версия главного модуля должна быть не ниже #VERSION#";
$MESS ['ARTIXGROUP_SHOP_DENIED'] = "доступ закрыт";
$MESS ['ARTIXGROUP_SHOP_OPENED'] = "доступ открыт";
$MESS ['ARTIXGROUP_SHOP_FULL'] = "полный доступ";
$MESS ['ARTIXGROUP_SHOP_INSTALL_TITLE'] = 'Установка модуля "Интернет-магазин от Artix group"';
$MESS ['ARTIXGROUP_SHOP_UNINSTALL_TITLE'] = 'Удаление модуля "Интернет-магазин от Artix group"';
?>