<?php
$MESS['ARTIXGROUP_ENTITY_ID'] = 'ID';
$MESS['ELEMENT_ENTITY_ACTIVE'] = 'Активность';
$MESS['ARTIXGROUP_ENTITY_NAME'] = 'Название';
$MESS['ARTIXGROUP_ENTITY_SHOW_MODE'] = 'Рижим отображения';
$MESS['ELEMENT_ENTITY_CAPTCHA'] = 'Captcha';
$MESS['ELEMENT_ENTITY_AUTOLOAD'] = 'Автозагрузка';
$MESS['ELEMENT_ENTITY_AUTOLOAD_TIMER'] = 'Таймер автозагрузки';
$MESS['ARTIXGROUP_ENTITY_HANDLER'] = 'Обработчик';
$MESS['ARTIXGROUP_ENTITY_RESULT_MESSAGE'] = 'Сообщение об успешной отправки';
$MESS['ARTIXGROUP_ENTITY_DESCRIPTION'] = 'Описание';