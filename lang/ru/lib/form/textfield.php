<?
$MESS["ARTIXGROUP_FORM_MIN_LENGTH"] = "Минимальная длина значения";
$MESS["ARTIXGROUP_FORM_MAX_LENGTH"] = "Минимальная длина значения";
$MESS["ARTIXGROUP_FORM_SIZE"] = "Размер поля";
$MESS["ARTIXGROUP_FORM_MASK"] = "Маска:";
$MESS["ARTIXGROUP_FORM_WITHOUT_MASK"] = "Не использовать";
$MESS["ARTIXGROUP_FORM_ALIAS"] = "Алиас";
$MESS["ARTIXGROUP_FORM_TYPE_MASK"] = "Маска";
$MESS["ARTIXGROUP_FORM_REGEX"] = "Регулярное вырожение";
$MESS["ARTIXGROUP_FORM_DATE"] = "Дата";
$MESS["ARTIXGROUP_FORM_DATETIME"] = "Дата и время";
$MESS["ARTIXGROUP_FORM_EMAIL"] = "Email адрес";
$MESS["ARTIXGROUP_FORM_DECIMAL"] = "Дробное число";
$MESS["ARTIXGROUP_FORM_INTEGER"] = "Целое число";
$MESS["ARTIXGROUP_FORM_CURRENCY"] = "Валюта";
$MESS["ARTIXGROUP_FORM_MIN_LENGTH_ERROR"] = "Значение должно быть больше либо равно ";
$MESS["ARTIXGROUP_FORM_MAX_LENGTH_ERROR"] = "Значение должно быть меньше либо равно";
$MESS["ARTIXGROUP_FORM_STRING"] = "Строка";
