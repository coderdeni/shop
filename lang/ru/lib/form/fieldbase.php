<?php
$MESS['ARTIXGROUP_SHOP_FORM_ERROR_EMPTY'] = 'Поле "#NAME#" обязательно для заполнения';
$MESS['ARTIXGROUP_SHOP_FROM_NAME'] = 'Название поля';
$MESS['ARTIXGROUP_SHOP_FROM_REQUIRED'] = 'Обезательное';
$MESS['ARTIXGROUP_SHOP_FROM_MULTIPLE'] = 'Множественное';
$MESS['ARTIXGROUP_SHOP_FROM_EXTERNAL'] = 'Внешний';
$MESS['ARTIXGROUP_SHOP_FROM_HINT'] = 'Подсказка';
$MESS['ARTIXGROUP_SHOP_FROM_CSS_CLASS'] = 'CSS стиль';