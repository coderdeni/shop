<?php
$MESS['USER_ACCOUNT_INFO_DEBIT'] = 'Начисление бонуса за оплату заказа №#ORDER_ID#';
$MESS['USER_ACCOUNT_INFO_CREDIT'] = 'Снятие денег со счета в связи с отменой оплаты заказа №#ORDER_ID#';
$MESS['USER_ACCOUNT_INFO_CREDIT_CANCEL'] = 'Снятие денег со счета в связи с отменой заказ №#ORDER_ID#';
$MESS['USER_ACCOUNT_INFO_DEBIT_CANCEL'] = 'Возвращение денег в связи с отменой отмены заказа №#ORDER_ID#';