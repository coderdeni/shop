<?php
$MESS['GROUP_BASE_NAME'] = 'Основные настройки';
$MESS['GROUP_CATALOG_NAME'] = 'Настройки каталога';
$MESS['GROUP_BONUS_NAME'] = 'Настройки бонусной системы';
$MESS['GROUP_TEMPLATE_NAME'] = 'Настройки шаблона';
$MESS['GROUP_CONTACTS_NAME'] = 'Контактные данные';