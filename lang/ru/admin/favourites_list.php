<?php
$MESS["ARTIXGROUP_FAVOURITES_LIST_MESS_TITLE"] = "Избранное пользователей";
$MESS["ARTIXGROUP_FAVOURITES_EDIT_LINK"] = "Просмотр";
$MESS["ARTIXGROUP_FAVOURITES_DELETE_LINK"] = "Удалить";
$MESS["ARTIXGROUP_FAVOURITES_CONFIRM_DELETE"] = "Вы действительно хотите удалить избранное пользователя?";