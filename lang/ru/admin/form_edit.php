<?php
$MESS["ARTIXGROUP_FAVOURITES_TAB_COMMON"] = "Форма";
$MESS["ARTIXGROUP_FAVOURITES_TAB_COMMON_TITLE"] = "Форма";
$MESS["ARTIXGROUP_FAVOURITES_2_LIST"] = "Назад к списку";
$MESS["ARTIXGROUP_FAVOURITES_SHOW_MODE"] = "Режим отображения";
$MESS["ARTIXGROUP_FAVOURITES_SHOW_MODE_SIMPLE"] = "Обычный";
$MESS["ARTIXGROUP_FAVOURITES_SHOW_MODE_AJAX"] = "Ajax загрузка";
$MESS["ARTIXGROUP_FAVOURITES_SHOW_MODE_POPUP"] = "Всплавающее окно";
$MESS["ARTIXGROUP_FAVOURITES_RESULT_MESSAGE"] = "Сообщение об успешной отправке";
$MESS["ARTIXGROUP_FAVOURITES_DESCRIPTION"] = "Описание формы";
$MESS["ARTIXGROUP_FAVOURITES_FORM"] = "Настройка формы";
$MESS["ARTIXGROUP_FAVOURITES_ITEMS_EMPTY"] = "Список элементов пуст";
$MESS["ARTIXGROUP_FAVOURITES_NAME"] = "Название";

$MESS["ARTIXGROUP_FAVOURITES_LANGUAGE"] = "Язык";
$MESS["ARTIXGROUP_FAVOURITES_ERROR_LID"] = "Не введена привязка к сайту.";
$MESS["ARTIXGROUP_FAVOURITES_ACTIVE"] = "Активная";
$MESS["ARTIXGROUP_FAVOURITES_LID"] = "Привязка к сайтам";
$MESS["ARTIXGROUP_FAVOURITES_AUTOLOAD"] = "Автозагрузка формы";
$MESS["ARTIXGROUP_FAVOURITES_AUTOLOAD_TIMER"] = "Таймер автозагрузки (сек)";
$MESS["ARTIXGROUP_FAVOURITES_CAPTCHA"] = "Каптча";
$MESS["ARTIXGROUP_FAVOURITES_CAPTCHA_NONE"] = "Без каптчи";
$MESS["ARTIXGROUP_FAVOURITES_CAPTCHA_BITRIX"] = "Bitrix каптча";
$MESS["ARTIXGROUP_FAVOURITES_CAPTCHA_GOOGLE_V2"] = "Google reCaptcha v2";
$MESS["ARTIXGROUP_FAVOURITES_CAPTCHA_GOOGLE_V2"] = "Google reCaptcha v3";

$MESS["ARTIXGROUP_FAVOURITES_ADD_EVENT_TYPE"] = "Создать тип и шаблон почтового события";
$MESS["ARTIXGROUP_FAVOURITES_UPDATE_EVENT_TYPE"] = "Обновить тип почтового события";
$MESS["ARTIXGROUP_FAVOURITES_UPDATE_TEMPLATE_EVENT_TYPE"] = "Обновить шаблон почтового события";

$MESS["ARTIXGROUP_FAVOURITES_COPY_CODE"] = 'Скопировать <a href="javascript:void(0)" id="copy_code">код</a> для встваки формы';
$MESS["ARTIXGROUP_FORM_RESULT_MESSAGE"] = "Сообщение успешно отправлено.";
$MESS["ARTIXGROUP_FORM_JS_EVENT_HANDLERS"] = "JS обработчики";
$MESS["ARTIXGROUP_FORM_MESSAGE_AGREEMENT_VALUE"] = "Я согласен на обработку моих персональных данных";
$MESS["ARTIXGROUP_FORM_CHECK_AGREEMENT"] = "Запрашивать согласие на обработку персональных данных";
$MESS["ARTIXGROUP_FORM_MESSAGE_AGREEMENT"] = "Текст соглашения:";
$MESS["ARTIXGROUP_FORM_COPY_CODE_COMPLETE"] = "Код скопирован!";
$MESS["ARTIXGROUP_FORM_BUTTON_TEXT"] = "Отправить";
