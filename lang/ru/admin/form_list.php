<?php
$MESS["ARTIXGROUP_FAVOURITES_LIST_MESS_TITLE"] = "Формы";
$MESS["ARTIXGROUP_FAVOURITES_EDIT_LINK"] = "Просмотр";
$MESS["ARTIXGROUP_FAVOURITES_DELETE_LINK"] = "Удалить";
$MESS["ARTIXGROUP_FAVOURITES_CONFIRM_DELETE"] = "Вы действительно хотите удалить форму?";
$MESS["ARTIXGROUP_FAVOURITES_LIST_FILTER_NAME"] = "Название";
$MESS["ARTIXGROUP_FAVOURITES_ADM_TITLE_NAME"] = "Название";
$MESS["ARTIXGROUP_FAVOURITES_ADM_DSC_HEADER_TITLE_NAME"] = "Название";
$MESS["ARTIXGROUP_FAVOURITES_ADD_FORM"] = "Добавить форму";