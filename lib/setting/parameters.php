<?php

namespace Artixgroup\Shop\Setting;

use Artixgroup\Shop\SettingTable;
use Bitrix\Main\Application;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Result;

class Parameters
{
    const GROUP_BASE = 'base';
    const GROUP_TEMPLATE = 'template';
    const GROUP_CATALOG = 'catalog';
    const GROUP_BONUS = 'bonus';
    const GROUP_CONTACTS = 'contacts';

    const TYPE_TEXT = 'text';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_FILE = 'file';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_RADIO = 'radio';
    const TYPE_SELECT = 'select';
    const TYPE_COLOR = 'color';

    protected static $instance = null;
    protected $parameters = [];
    protected $siteId;

    public static function getInstance()
    {
        if (self::$instance === null)
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct($siteId = SITE_ID)
    {
        $this->siteId = $siteId;
        $this->loadParameters();
    }

    public function loadParameters()
    {
        $cache = Cache::createInstance();

        if ($cache->initCache(80600, $this->siteId, 'artixgroup_shop/settings'))
        {
            $this->parameters = $cache->getVars();
        }
        elseif ($cache->startDataCache())
        {
            $defaultValues = $this->getDefaultValues();
            $dbParameters = [];

            $iterator = SettingTable::getList([
                'filter' => ['=SITE_ID' => $this->siteId]
            ]);
            while ($parameter = $iterator->fetch())
            {
                $parameter['ID'] = $parameter['CODE'].'_'.$parameter['SITE_ID'];
                $dbParameters[$parameter['CODE']] = $parameter;
            }

            $needUpdate = false;
            foreach ($defaultValues as $code => $value)
            {
                $this->parameters[$code] = isset($dbParameters[$code]) ? array_merge($value, $dbParameters[$code]) : $value;

                if (isset($value['VALUES']))
                {
                    $value['VALUES'] = is_callable($value['VALUES'])
                        ? call_user_func($value['VALUES'])
                        : $value['VALUES'];
                }

                if (!isset($dbParameters[$code]))
                {
                    $value['SITE_ID'] = $this->siteId;
                    $value['TEMPLATE'] = 'artixgroup_shop';
                    $value['CODE'] = $code;

                    $this->parameters[$code] = $value;
                    $needUpdate = true;
                }
                else
                {
                    $this->parameters[$code] = array_merge($value, $dbParameters[$code]);
                }
            }

            if ($needUpdate)
            {
                $cache->abortDataCache();
                $this->save();
            }

            $cache->endDataCache($this->parameters);
        }

        $cache->clean($this->siteId, 'artixgroup_shop/settings');
    }

    public function setDefaultValues()
    {
        $defaultValues = $this->getDefaultValues();

        if ($defaultValues)
        {
            foreach ($defaultValues as $key => $parameter)
            {
                if (!$this->isParameterExists($key))
                {
                    $parameter['SITE_ID'] = $this->siteId;
                    $this->parameters[$key] = $parameter;
                }
                else
                {
                    $this->setValue($key, $parameter['VALUE']);
                }
            }

            return $this->save();
        }

        return false;
    }

    public function setValue($parameter, $value)
    {
        if ($this->isParameterExists($parameter))
        {
            $this->parameters[$parameter]['VALUE'] = $value;
        }
    }

    public function save()
    {
        $saveResult = new Result();
        $dataAdd = [];
        $connection = Application::getConnection();
        $connection->startTransaction();

        foreach ($this->parameters as $code => $parameter)
        {
            if ($parameter['ID'])
            {
                $result = SettingTable::update(null, $parameter);

                if (!$result->isSuccess())
                {
                    $connection->rollbackTransaction();
                    $saveResult->addErrors($result->getErrors());

                    return $saveResult;
                }
            }
            else
            {
                $dataAdd[] = $parameter;
            }
        }

        if (count($dataAdd) > 0)
        {
            $result = SettingTable::addMulti($dataAdd);

            if (!$result->isSuccess())
            {
                $connection->rollbackTransaction();
                $saveResult->addErrors($result->getErrors());

                return $saveResult;
            }
        }

        $connection->commitTransaction();

        $cache = Cache::createInstance();
        $cache->clean($this->siteId, 'artixgroup_shop/settings');

        return $saveResult;
    }

    public function getDefaultValues()
    {
        $file = $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/config.php';

        if (file_exists($file))
        {
            $config = include $file;

            if ($config && is_array($config))
            {
                return $config;
            }
        }

        return null;
    }

    public function getGroups()
    {
        return [
            self::GROUP_BASE => [
                'NAME' => Loc::getMessage('GROUP_BASE_NAME'),
            ],
            self::GROUP_CATALOG => [
                'NAME' => Loc::getMessage('GROUP_CATALOG_NAME'),
            ],
            self::GROUP_BONUS => [
                'NAME' => Loc::getMessage('GROUP_BONUS_NAME'),
            ],
            self::GROUP_TEMPLATE => [
                'NAME' => Loc::getMessage('GROUP_TEMPLATE_NAME'),
            ],
            self::GROUP_CONTACTS => [
                'NAME' => Loc::getMessage('GROUP_CONTACTS_NAME'),
            ],
        ];
    }

    public function getGroupParameters($group)
    {
        $parameters = [];

        foreach ($this->parameters as $code => $parameter)
        {
            if ($parameter['GROUP'] === $group)
                $parameters[$code] = $parameter;
        }

        return $parameters;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function isParameterExists($parameter)
    {
        return isset($this->parameters[$parameter]);
    }

    public function getParameter($parameter)
    {
        if ($this->isParameterExists($parameter))
        {
            return $this->parameters[$parameter];
        }

        return null;
    }

    public function notEmpty($parameter)
    {
        $params = $this->getParameter($parameter);

        if ($params)
        {
            return !empty($params['VALUE']);
        }

        return false;
    }

    public function getBool($parameter)
    {
        $params = $this->getParameter($parameter);

        if ($params)
        {
            return (bool)$params['VALUE'];
        }

        return false;
    }

    public function getInt($parameter)
    {
        $params = $this->getParameter($parameter);

        if ($params)
        {
            return (int)$params['VALUE'];
        }

        return null;
    }

    public function getText($parameter, $format = null)
    {
        $params = $this->getParameter($parameter);

        if ($params)
        {
            if ($params['TYPE'] === self::TYPE_CHECKBOX)
                return (bool)$params['VALUE'] ? 'Y' : 'N';

            return $params['VALUE'];
        }

        return null;
    }
}