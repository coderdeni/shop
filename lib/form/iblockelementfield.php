<?php

namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\FormFieldTable;
use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\TypeLanguageTable;
use Bitrix\Iblock\TypeTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Data\Cache;

class IblockElementField extends SelectField
{
    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_ELEMENT_LIST");
        $config['TYPE'] = FormFieldTable::TYPE_IBLOCK_ELEMENT;

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_ELEMENT_LIST");
    }

    /**
     * @inheritDoc
     */
    public function showHtmlParameters()
    {
        ?>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_IBLOCK") ?></td>
            <td class="adm-detail-content-cell-r">
                <select name="PARAMETERS[iblock_id]">
                    <?
                    $iblocks = $this->getIblocks();
                    foreach ($iblocks as $group)
                    {
                        ?>
                        <optgroup label="<?= $group['NAME']?>">
                        <?
                        foreach ($group['IBLOCKS'] as $iblock)
                        {
                            ?>
                            <option value="<?= $iblock['ID']?>"<?= $iblock['ID'] === (int)$this->config['PARAMETERS']['iblock_id'] ? ' selected' : ''?>>
                                <?= $iblock['NAME']?>
                            </option>
                            <?
                        }
                        ?>
                        </optgroup>
                        <?
                    }
                    ?>
                </select>
            </td>
        </tr>
        <?
    }

    /**
     * @inheritDoc
     */
    public function getValues(): array
    {
        $cache = Cache::createInstance(); // получаем экземпляр класса

        $values = [];
        $iblockId = (int)$this->config['PARAMETERS']['iblock_id'];

        if ($iblockId > 0 && Loader::includeModule('iblock'))
        {
            if ($cache->initCache(7200, "artixgroup_iblock_element_field_$iblockId"))
            {
                $values = $cache->getVars();
            }
            elseif ($cache->startDataCache())
            {
                $select = ['ID', 'NAME'];
                $filter = ['IBLOCK_ID' => $iblockId, 'ACTIVE' => 'Y'];
                $iterator = \CIBlockElement::GetList(['SORT'], $filter, false, false, $select);

                while ($element = $iterator->Fetch())
                {
                    $values[] = [
                        'KEY' => $element['ID'],
                        'VALUE' => $element['NAME'],
                        'DEF' => ''
                    ];
                }

                $cache->endDataCache($values);
            }
        }

        return $values;
    }

    /**
     * @inheritDoc
     */
    protected function prepareValue($value): int
    {
        return (int)$value;
    }

    /**
     * @inheritDoc
     */
    protected function getHtmlTemplateValue($value): string
    {
        $iblockId = (int)$this->config['PARAMETERS']['iblock_id'];
        $values = $this->getValues();
        $url = "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=$iblockId&lang=".LANGUAGE_ID."&ID=$value";
        $elementName = '';

        foreach ($values as $item)
        {
            if ((int)$item['KEY'] === (int)$value)
            {
                $elementName = $item['VALUE'];
                break;
            }
        }

        return '[<a href="'.$url.'">'.$value.'</a>] <a href="'.$url.'">'.$elementName."</a>";
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function getIblocks(): array
    {
        Loader::includeModule('iblock');
        $result = [];

        $iblockTypes = TypeTable::getList([
            'select' => ['ID']
        ]);
        while ($iblockType = $iblockTypes->fetch())
        {
            $language = TypeLanguageTable::getList([
                'select' => ['NAME'],
                'filter' => ['LANGUAGE_ID' => LANGUAGE_ID, 'IBLOCK_TYPE_ID' => $iblockType['ID']],
            ])->fetch();

            $iblockType['NAME'] = $language['NAME'];
            $iblockType['IBLOCKS'] = [];

            $iterator = IblockTable::getList([
                'select' => ['ID', 'NAME'],
                'filter' => ['ACTIVE' => 'Y', 'IBLOCK_TYPE_ID' => $iblockType['ID']],
            ]);

            while ($iblock = $iterator->fetch())
            {
                $iblockType['IBLOCKS'][] = $iblock;
            }

            if (count($iblockType['IBLOCKS']) > 0)
                $result[] = $iblockType;
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getJson()
    {
        return json_encode($this->config);
    }
}
