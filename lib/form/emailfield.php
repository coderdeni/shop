<?php

namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Localization\Loc;

class EmailField extends TextField
{
    protected $defaultType = 'text';

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_EMAIL");
        $config['TYPE'] = FormFieldTable::TYPE_EMAIL;
        $config['PARAMETERS']['type_mask'] = 'alias';
        $config['PARAMETERS']['alias'] = 'email';

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_EMAIL");
    }

    /**
     * @inheritDoc
     */
    protected function validate($value): bool
    {
        if (parent::validate($value))
        {
            if (filter_var($value, FILTER_VALIDATE_EMAIL))
            {
                return true;
            }

            $this->errorMessage = Loc::getMessage("ARTIXGROUP_FORM_INVALID_EMAIL");
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    protected function getHtmlTemplateValue($value): string
    {
        return '<a href="mailto:'.$value.'">'.$value."</a>";
    }
}
