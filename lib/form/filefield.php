<?php


namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;

class FileField extends FieldBase
{
    protected $defaultType = 'file';

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_FIELD_FILE");
        $config['TYPE'] = FormFieldTable::TYPE_FILE;
        $config['PARAMETERS']['filesize'] = self::_getMaxAllowedUploadSize();

        return $config;
    }

    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_FIELD_TYPE_FILE");
    }

    /**
     * @inheritDoc
     */
    public function loadValue()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        return $request->getFile($this->getCode());
    }

    /**
     * @inheritDoc
     */
    public function showHtmlParameters()
    {
        ?>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_PARAM_EXT") ?></td>
            <td class="adm-detail-content-cell-r">
                <input type="number" name="PARAMETERS[extension]" value="<?= $this->config['PARAMETERS']['extension']?>">
            </td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_PARAM_MAX_SIZE") ?></td>
            <td class="adm-detail-content-cell-r">
                <input type="number" name="PARAMETERS[filesize]" value="<?= $this->config['PARAMETERS']['filesize']?>">
            </td>
        </tr>
        <?
    }

    /**
     * @return mixed
     */
    public static function _getMaxAllowedUploadSize()
    {
        $sizes = array();
        $sizes[] = ini_get('upload_max_filesize');
        $sizes[] = ini_get('post_max_size');
        $sizes[] = ini_get('memory_limit');

        for ($x = 0; $x < count($sizes); $x++)
        {
            $last = strtolower($sizes[$x][strlen($sizes[$x])-1]);

            if ($last == 'k')
            {
                $sizes[$x] *= 1024;
            }
            elseif ($last == 'm')
            {
                $sizes[$x] *= 1024;
                $sizes[$x] *= 1024;
            }
            elseif ($last == 'g')
            {
                $sizes[$x] *= 1024;
                $sizes[$x] *= 1024;
                $sizes[$x] *= 1024;
            }
            elseif ($last == 't')
            {
                $sizes[$x] *= 1024;
                $sizes[$x] *= 1024;
                $sizes[$x] *= 1024;
                $sizes[$x] *= 1024;
            }
        }

        return min($sizes);
    }

    /**
     * @inheritDoc
     */
    protected function prepareValue($value)
    {
        return \CFile::MakeFileArra($value);
    }

    /**
     * @inheritDoc
     */
    protected function validate($value): bool
    {
        if (parent::validate($value))
        {
            $filesize = !empty($this->config['PARAMETERS']['filesize']) ? $this->config['PARAMETERS']['filesize'] : false;
            $extension = !empty($this->config['PARAMETERS']['extension']) ? $this->config['PARAMETERS']['extension'] : false;

            if ($error = \CFile::CheckFile($value, $filesize, false, $extension))
            {
                return true;
            }

            $this->errorMessage = $error;
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getJsValidator()
    {
        return null;
    }
}
