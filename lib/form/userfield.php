<?php

namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

class UserField extends SelectField
{
    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_FIELD_USER");
        $config['TYPE'] = FormFieldTable::TYPE_USER;

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_FIELD_TYPE_USER");
    }

    /**
     * @inheritDoc
     */
    public function showHtmlParameters()
    {
    }

    /**
     * @inheritDoc
     */
    public function getValues(): array
    {
        $values = [];
        $iterator = UserTable::getList([
            'select' => ['ID', 'LOGIN', 'NAME', 'SECOND_NAME', 'LAST_NAME'],
            'filter' => ['ACTIVE' => 'Y']
        ]);

        while ($user = $iterator->Fetch())
        {
            $name = [];

            if ($user['LAST_NAME'])
                $name[] = $user['LAST_NAME'];

            if ($user['NAME'])
                $name[] = $user['NAME'];

            if ($user['SECOND_NAME'])
                $name[] = $user['SECOND_NAME'];

            $values[] = [
                'KEY' => $user['ID'],
                'VALUE' => count($name) > 0 ? implode(' ', $name) : $user['LOGIN'],
                'DEF' => ''
            ];
        }

        return $values;
    }

    /**
     * @inheritDoc
     */
    protected function prepareValue($value)
    {
        return (int)$value;
    }

    /**
     * @inheritDoc
     */
    public function getJsValidator()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    protected function getHtmlTemplateValue($value): string
    {
        $values = $this->getValues();
        $url = "/bitrix/admin/user_edit.php?lang=".LANGUAGE_ID."&ID=$value";
        $elementName = '';

        foreach ($values as $item)
        {
            if ((int)$item['KEY'] === (int)$value)
            {
                $elementName = $item['VALUE'];
                break;
            }
        }

        return '[<a href="'.$url.'">'.$value.'</a>] <a href="'.$url.'">'.$elementName."</a>";
    }
}
