<?php

namespace Artixgroup\Shop\Form;


use Artixgroup\Shop\FormFieldTable;
use Artixgroup\Shop\FormResultTable;
use Artixgroup\Shop\FormTable;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Result;

class TextField extends FieldBase
{
    protected $defaultType = 'text';

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_STRING");
        $config['TYPE'] = FormFieldTable::TYPE_TEXT;
        $config['PARAMETERS']['minlength'] = '';
        $config['PARAMETERS']['maxlength'] = 255;
        $config['PARAMETERS']['size'] = '';

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_STRING");
    }

    /**
     * @inheritDoc
     */
    public function showHtmlParameters()
    {
        ?>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_MIN_LENGTH") ?></td>
            <td class="adm-detail-content-cell-r"><input type="number" name="PARAMETERS[minlength]" value="<?= $this->config['PARAMETERS']['minlength']?>"></td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_MAX_LENGTH") ?></td>
            <td class="adm-detail-content-cell-r"><input type="number" name="PARAMETERS[maxlength]" value="<?= $this->config['PARAMETERS']['maxlength']?>"></td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_SIZE") ?></td>
            <td class="adm-detail-content-cell-r"><input type="number" name="PARAMETERS[size]" value="<?= $this->config['PARAMETERS']['size']?>"></td>
        </tr>
	    <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_MASK") ?></td>
            <td class="adm-detail-content-cell-r">
                <select id="type_mask" name="PARAMETERS[type_mask]">
                    <?
                    $typeMask = $this->config['PARAMETERS']['type_mask'];
                    $types = [
                        '' => Loc::getMessage("ARTIXGROUP_FORM_WITHOUT_MASK"),
                        'alias' => Loc::getMessage("ARTIXGROUP_FORM_ALIAS"),
                        'mask' => Loc::getMessage("ARTIXGROUP_FORM_TYPE_MASK"),
                        'regex' => Loc::getMessage("ARTIXGROUP_FORM_REGEX")
                    ];
                    foreach ($types as $value => $name)
                    {
                        ?>
                        <option value="<?= $value?>"<?= $typeMask === $value ? ' selected' : ''?>>
                            <?= $name?>
                        </option>
                        <?
                    }
                    ?>
		        </select>
                <select id="alias_mask" name="PARAMETERS[alias]"<?= $typeMask === 'alias' ? '' : ' style="display:none"'?>>
                    <?
                    $types = [
                        'date' => Loc::getMessage("ARTIXGROUP_FORM_DATE"),
                        'datetime' => Loc::getMessage("ARTIXGROUP_FORM_DATE"),
                        'email' => Loc::getMessage("ARTIXGROUP_FORM_EMAIL"),
                        'decimal' => Loc::getMessage("ARTIXGROUP_FORM_DECIMAL"),
                        'integer' => Loc::getMessage("ARTIXGROUP_FORM_INTEGER"),
                        'currency' => Loc::getMessage("ARTIXGROUP_FORM_CURRENCY"),
                    ];
                    foreach ($types as $value => $name)
                    {
                        ?>
                        <option value="<?= $value?>"<?= $this->config['PARAMETERS']['alias'] === $value ? ' selected' : ''?>>
                            <?= $name?>
                        </option>
                        <?
                    }
                    ?>
                </select>
                <input type="text" id="mask_mask" name="PARAMETERS[mask]" value="<?= $this->config['PARAMETERS']['mask']?>"<?= $typeMask === 'mask' ? '' : ' style="display:none"'?>>
                <input type="text" id="mask_regex" name="PARAMETERS[regex]" value="<?= $this->config['PARAMETERS']['regex']?>"<?= $typeMask === 'regex' ? '' : ' style="display:none"'?>>
                <script>
                    document.getElementById('type_mask').addEventListener('change', function () {
                        var aliasMask = document.getElementById('alias_mask'),
                            maskMask = document.getElementById('mask_mask'),
                            maskRegex = document.getElementById('mask_regex');

                        aliasMask.style.display = 'none';
                        maskMask.style.display = 'none';
                        maskRegex.style.display = 'none';

                        if (this.value === 'alias')
                            aliasMask.style.display = null;
                        else if (this.value === 'mask')
                            maskMask.style.display = null;
                        else if (this.value === 'regex')
                            maskRegex.style.display = null;
                    });
                </script>
            </td>
        </tr>
        <?
    }

    /**
     * @inheritDoc
     */
    protected function prepareValue($value)
    {
        return trim($value);
    }

    /**
     * @inheritDoc
     */
    public function getJsValidator()
    {
        $type = $this->config['PARAMETERS']['type_mask'];

        if ($type !== '' && !empty($this->config['PARAMETERS'][$type]))
        {
            $value = $this->config['PARAMETERS'][$type];
            return [$type => $value];
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    protected function validate($value): bool
    {
        $minlength = $this->config['PARAMETERS']['minlength'];
        $maxlength = $this->config['PARAMETERS']['maxlength'];
        $length = strlen($value);

        if ($minlength > 0 && $length < $minlength)
        {
            $this->errorMessage = Loc::getMessage("ARTIXGROUP_FORM_MIN_LENGTH_ERROR").$minlength;
            return false;
        }

        if ($maxlength > 0 && $length > $maxlength)
        {
            $this->errorMessage = Loc::getMessage("ARTIXGROUP_FORM_MAX_LENGTH_ERROR") .$maxlength;
            return false;
        }

        return true;
    }
}
