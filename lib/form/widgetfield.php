<?php

namespace Artixgroup\Shop\Form;

use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;

class WidgetField extends FieldBase
{
    public function showHtmlBaseParameters()
    {
        ?>
        <tr>
            <td class="adm-detail-content-cell-l"><span class="adm-required-field">Шаблон виджета:</span></td>
            <td class="adm-detail-content-cell-r">
                <select name="PARAMETERS][template]">
                    <?
                    $templates = $this->getTemplates();
                    $templateFolder = isset($this->config['PARAMETERS']['template']) && !empty($this->config['PARAMETERS']['template'])
                        ? $this->config['PARAMETERS']['template']
                        : '.default';

                    foreach ($templates as $folder => $template)
                    {
                        ?>
                        <option value="<?= $folder?>"<?= $templateFolder == $folder ? ' selected' : ''?>>
                            <?= $template?>
                        </option>
                        <?
                    }
                    ?>
                </select>
                <input type="hidden" name="ID" value="<?= $this->config['ID']?>">
                <input type="hidden" name="COL" value="<?= $this->config['COL']?>">
                <input type="hidden" name="ROW" value="<?= $this->config['ROW']?>">
                <input type="hidden" name="SORT" value="<?= $this->config['SORT']?>">
                <input type="hidden" name="TYPE" value="<?= $this->config['TYPE']?>">
            </td>
        </tr>
        <?
    }

    public function showHtmlParameters()
    {
    }

    protected function getClassName(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    protected function getData(): array
    {
        return [];
    }

    protected function render($data)
    {
        if (!empty($data))
            extract($data, EXTR_PREFIX_SAME, "wddx");

        $widgetFolder = $this->getClassName();
        $templateFolder = isset($this->config['PARAMETERS']['template']) && !empty($this->config['PARAMETERS']['template'])
            ? $this->config['PARAMETERS']['template']
            : '.default';

        $folders = [
            '/local/php_interface',
            '/bitrix/php_interface',
            '/local/modules',
            '/bitrix/modules'
        ];

        ob_start();

        foreach ($folders as $folder)
        {
            $path = $_SERVER['DOCUMENT_ROOT'].$folder.'/artixgroup.shop/widgets/'.$widgetFolder.'/'.$templateFolder.'/template.php';

            if (File::isFileExists($path))
            {
                include $path;
                break;
            }
        }

        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    protected function getTemplates(): array
    {
        $widgetFolder = $this->getClassName();

        $folders = [
            '/local/php_interface',
            '/bitrix/php_interface',
            '/local/modules',
            '/bitrix/modules'
        ];

        $templates = [];

        foreach ($folders as $folder)
        {
            $path = $_SERVER['DOCUMENT_ROOT'].$folder.'/artixgroup.shop/widgets/'.$widgetFolder.'/';

            if (Directory::isDirectoryExists($path))
            {
                $dir = new Directory($path);
                $children = $dir->getChildren();

                foreach ($children as $child)
                {
                    if ($child->isDirectory() && !in_array($child->getName(), $templates))
                    {
                        $templates[$child->getName()] = $child->getName();
                    }
                }
            }
        }

        return $templates;
    }

    public function getHtml()
    {
        $data = $this->getData();
        return $this->render($data);
    }

    public function getJsValidator()
    {
        return null;
    }
}