<?php

namespace Artixgroup\Shop\Form;


use Artixgroup\Shop\Form\Interfaces\INotDeletable;
use Artixgroup\Shop\FormFieldTable;
use Artixgroup\Shop\FormResultTable;
use Artixgroup\Shop\FormTable;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Result;

class SubmitField extends FieldBase implements INotDeletable
{
    protected $defaultType = 'submit';

    /**
     * @inheritDoc
     */
    public function showHtmlParameters()
    {
    }

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_SUBMIT");
        $config['TYPE'] = FormFieldTable::TYPE_SUBMIT;

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_BUTTON_SUBMIT");
    }


    /**
     * @inheritDoc
     */
    public function getJsValidator()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    protected function validate($value): bool
    {
        return true;
    }

    public function showAdminHtmlInput()
    {
        ?>
        Кнопка: <?= $this->config['NAME']?><?= $this->isRequired() ? '<span class="form__input-required">*</span>' : ''?>
        <?
    }
}
