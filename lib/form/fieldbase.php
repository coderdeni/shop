<?php

namespace Artixgroup\Shop\Form;

use Bitrix\Main\Application;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;

abstract class FieldBase
{
    protected static $key = -1;

    protected $config = null;
    protected $errorMessage = null;
    protected $value = null;
    protected $defaultType = 'text';//TODO: Хуйня!!!!!!

    /**
     * FieldBase constructor.
     * @param array $config
     * @param int $key
     */
    public function __construct(array $config = [], $key = null)
    {
        $this->config = array_merge($this->getDefaultConfig(), $config);

        if ($key)
            self::$key = $key;
        else
            self::$key++;

        $this->loadValue();
    }

    /**
     * @return string
     */
    public static function getTypeName(): string
    {
        return '';
    }

    /**
     * @return array
     */
    protected function getDefaultConfig(): array
    {
        return [
            'ID' => '',
            'COL' => '',
            'ROW' => '',
            'SORT' => '',
            'NAME' => '',
            'TYPE' => '',
            'MULTIPLE' => 'N',
            'REQUIRED' => 'N',
            'EXTERNAL' => 'N',
            'HINT' => '',
            'PARAMETERS' => []
        ];
    }

    /**
     * @return string
     */
    public function getHtmlType(): string
    {
        return $this->defaultType;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->config['ID'];
    }

    /**
     * @return int
     */
    public function getKey(): int
    {
        return self::$key;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->config['NAME'];
    }

    /**
     * @return string
     */
    public function getInputId(): string
    {
        return uniqid('ag_form_field_');
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return 'FIELD_'.$this->config['ID'];
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->config['REQUIRED'] === 'Y';
    }

    /**
     * @return bool
     */
    public function isMultiple(): bool
    {
        return $this->config['MULTIPLE'] === 'Y';
    }

    /**
     * @return bool
     */
    public function isExternal(): bool
    {
        return $this->config['EXTERNAL'] === 'Y';
    }

    /**
     * @return mixed
     */
    public function getHint()
    {
        return $this->config['HINT'];
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        if ($this->isMultiple() && empty($this->value))
        {
            $this->value = [null];
        }

        return $this->value;
    }

    /**
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = !empty($value) ? $value : null;
    }

    /**
     * @return mixed
     */
    public function getPrepareValue()
    {
        if ($this->isMultiple())
        {
            $result = [];

            foreach ($this->value as $value)
            {
                $result[] = $this->prepareValue($value);
            }

            return $result;
        }

        return $this->prepareValue($this->value);
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function prepareValue($value)
    {
        return $value;
    }

    /**
     * @return string
     */
    public function getHtmlValue(): string
    {
        $html = '';

        if ($this->isMultiple())
        {
            $data = [];
            foreach ($this->getValue() as $value)
            {
                if ($value === null)
                    continue;

                $data[] = $this->getHtmlTemplateValue($value);
            }

            $html .= implode('<br>', $data);
        }
        elseif ($this->getValue() !== null)
        {
            $html = $this->getHtmlTemplateValue($this->getValue());
        }

        return $html;
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function getHtmlTemplateValue($value)
    {
        return $value;
    }

    /**
     * @return string
     */
    public function getTextValue(): string
    {
        $html = '';

        if ($this->isMultiple())
        {
            $data = [];
            foreach ($this->getValue() as $value)
            {
                if ($value === null)
                    continue;

                $data[] = $this->getTextTemplateValue($value);
            }

            $html .= implode(', ', $data);
        }
        elseif ($this->getValue() !== null)
        {
            $html = $this->getTextTemplateValue($this->getValue());
        }

        return $html;
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function getTextTemplateValue($value)
    {
        return $value;
    }

    /**
     * Load values from $_POST
     */
    protected function loadValue()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $value = $request->getPost($this->getCode());
        $this->setValue($value);
    }

    /**
     *
     */
    public function showAdminHtmlInput()
    {
        ?>
        <?= $this->config['NAME']?><?= $this->isRequired() ? '<span class="form__input-required">*</span>' : ''?>
        <?
    }

    /**
     *
     */
    public function showHtmlBaseParameters()
    {
        ?>
        <tr>
            <td width="50%" class="adm-detail-content-cell-l"><span class="adm-required-field"><?= Loc::getMessage('ARTIXGROUP_SHOP_FROM_NAME')?>:</span></td>
            <td class="adm-detail-content-cell-r">
                <input class="form__input_name" type="text" name="NAME" value="<?= $this->getLabel()?>">
                <input type="hidden" name="ID" value="<?= $this->config['ID']?>">
                <input type="hidden" name="COL" value="<?= $this->config['COL']?>">
                <input type="hidden" name="ROW" value="<?= $this->config['ROW']?>">
                <input type="hidden" name="SORT" value="<?= $this->config['SORT']?>">
                <input type="hidden" name="TYPE" value="<?= $this->config['TYPE']?>">
            </td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage('ARTIXGROUP_SHOP_FROM_REQUIRED')?>:</td>
            <td class="adm-detail-content-cell-r">
                <input type="checkbox" class="form__input_required" name="REQUIRED" value="Y"<?= $this->isRequired() ? ' checked': ''?>>
            </td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage('ARTIXGROUP_SHOP_FROM_MULTIPLE')?>:</td>
            <td class="adm-detail-content-cell-r">
                <input type="checkbox" name="MULTIPLE" value="Y"<?= $this->isMultiple() ? ' checked': ''?>>
            </td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage('ARTIXGROUP_SHOP_FROM_EXTERNAL')?>:</td>
            <td class="adm-detail-content-cell-r">
                <input type="checkbox" name="EXTERNAL" value="Y"<?= $this->isExternal() ? ' checked': ''?>>
            </td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage('ARTIXGROUP_SHOP_FROM_HINT')?>:</td>
            <td class="adm-detail-content-cell-r">
                <textarea name="HINT"><?= $this->getHint()?></textarea>
            </td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage('ARTIXGROUP_SHOP_FROM_CSS_CLASS')?>:</td>
            <td class="adm-detail-content-cell-r">
                <input type="text" name="PARAMETERS[css_class]" value="<?= $this->config['PARAMETERS']['css_class']?>">
            </td>
        </tr>
        <?
    }

    /**
     * @return mixed
     */
    public abstract function getJsValidator();

    /**
     * @return mixed
     */
    public abstract function showHtmlParameters();

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        $values = $this->getPrepareValue();

        if ($this->isRequired() && empty($values))
        {
            $this->errorMessage = Loc::getMessage('ARTIXGROUP_SHOP_FORM_ERROR_EMPTY', ['#NAME#' => $this->getLabel()]);
            return false;
        }

        if ($this->isMultiple())
        {
            foreach ($values as $value)
            {
                if (!$this->validate($value))
                {
                    return false;
                }
            }
        }
        elseif (!$this->validate($values))
        {
            return false;
        }

        return true;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function validate($value): bool
    {
        return true;
    }

    /**
     * @return Error
     */
    public function getError(): Error
    {
        return new Error($this->errorMessage);
    }

    /**
     * @return string|null
     */
    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    /**
     * @return false|string
     */
    public function getJson()
    {
        return json_encode($this->config);
    }

    /**
     * @return array|null
     */
    public function toArray(): ?array
    {
        return $this->config;
    }
}