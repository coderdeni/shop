<?php

namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\Form\Interfaces\INotDeletable;
use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Localization\Loc;

class AgreementField extends FlagField implements INotDeletable
{
    protected $defaultType = 'checkbox';

    /**
     * @inheritDoc
     */
    public function __construct(array $config = array(), $key = null)
    {
        parent::__construct($config, $key);

        if ($this->isMultiple())
            $this->defaultType = 'checkbox';
    }

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = 'Пользовательское соглашение';
        $config['TYPE'] = FormFieldTable::TYPE_AGREEMENT;

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return 'Пользовательское соглашение';
    }
}
