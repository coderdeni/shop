<?php

namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Localization\Loc;

class SelectField extends FieldBase
{
    protected $defaultType = 'select';

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_FIELD_LIST");
        $config['TYPE'] = FormFieldTable::TYPE_SELECT;

        return $config;
    }

    /**
     * @inheritDoc
     */
    public function showHtmlParameters()
    {
        ?>
        <tr class="heading"><td colspan="2"><?= Loc::getMessage("ARTIXGROUP_FORM_PARAM_LIST_VALUES") ?></td></tr>
        <tr>
            <td colspan="2">
                <table class="internal" id="tbl_values_<?= self::$key?>" width="100%">
                    <tr class="heading">
                        <td><?= Loc::getMessage("ARTIXGROUP_FORM_PARAM_KEY") ?></td>
                        <td><?= Loc::getMessage("ARTIXGROUP_FORM_PARAM_VALUE") ?></td>
                        <td><?= Loc::getMessage("ARTIXGROUP_FORM_PARAM_SORT") ?></td>
                        <td><?= Loc::getMessage("ARTIXGROUP_FORM_PARAM_DEFAULT") ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2"><?= Loc::getMessage("ARTIXGROUP_FORM_PARAM_NOT_DEF_VALUE") ?></td>
                        <td style="text-align:center"><input type="radio" name="VALUES_DEF" value="0"></td>
                    </tr>
                    <?
                    $key = 0;

                    foreach ($this->getValues() as $value)
                    {
                        ?>
                        <tr>
                            <td>
                                <input type="text" name="PARAMETERS[VALUES][<?= $key?>][KEY]" value="<?= $value['KEY']?>" size="15" maxlength="200" style="width:90%">
                            </td>
                            <td>
                                <input type="text" name="PARAMETERS[VALUES][<?= $key?>][VALUE]" value="<?= $value['VALUE']?>" size="35" maxlength="255" style="width:90%">
                            </td>
                            <td style="text-align:center">
                                <input type="text" name="PARAMETERS[VALUES][<?= $key?>][SORT]" value="<?= $value['SORT']?>" size="5" maxlength="11">
                            </td>
                            <td style="text-align:center">
                                <input type="radio" name="PARAMETERS[VALUES][<?= $key?>][DEF]" value="Y"<?= $value['DEF'] === 'Y' ? ' checked' : ''?>>
                            </td>
                        </tr>
                        <?
                        $key++;
                    }

                    $max = $key + 3;
                    for ($i = $key; $i < $max; $i++)
                    {
                        ?>
                        <tr>
                            <td>
                                <input type="text" name="PARAMETERS[VALUES][<?= $i?>][KEY]" value="" size="15" maxlength="200" style="width:90%">
                            </td>
                            <td>
                                <input type="text" name="PARAMETERS[VALUES][<?= $i?>][VALUE]" value="" size="35" maxlength="255" style="width:90%">
                            </td>
                            <td style="text-align:center">
                                <input type="text" name="PARAMETERS[VALUES][<?= $i?>][SORT]" value="500" size="5" maxlength="11">
                            </td>
                            <td style="text-align:center">
                                <input type="radio" name="PARAMETERS[VALUES][<?= $i?>][DEF]" value="Y">
                            </td>
                        </tr>
                        <?
                    }
                    $key = $i;
                    ?>
                </table>
                <div style="width: 100%; text-align: center; margin: 10px 0;">
                    <input class="adm-btn-big" type="button" id="add_value_btn_<?= self::$key?>" value="Еще...">
                </div>
                <input type="hidden" id="cnt_values_<?= self::$key?>" value="<?= $key?>">
                <script>
                    var btn = document.getElementById('add_value_btn_<?= self::$key?>');
                    btn.addEventListener('click', function () {
                        var inpcnt = document.getElementById('cnt_values_<?= self::$key?>'),
                            row = document.createElement('tr'),
                            cnt = inpcnt.value + 1;

                        row.innerHTML = '<td>' +
                            '    <input type="text" name="PARAMETERS[VALUES]['+cnt+'][KEY]" value="" size="15" maxlength="200" style="width:90%">' +
                            '</td>' +
                            '<td>' +
                            '    <input type="text" name="PARAMETERS[VALUES]['+cnt+'][VALUE]" value="" size="35" maxlength="255" style="width:90%">' +
                            '</td>' +
                            '<td style="text-align:center">' +
                            '    <input type="text" name="PARAMETERS[VALUES]['+cnt+'][SORT]" value="500" size="5" maxlength="11">' +
                            '</td>' +
                            '<td style="text-align:center">' +
                            '    <input type="radio" name="PARAMETERS[VALUES]['+cnt+'][DEF]" value="Y">' +
                            '</td>' +
                            '</tr>';

                        document.getElementById('tbl_values_<?= self::$key?>').appendChild(row);
                        inpcnt.setAttribute('value', cnt);
                        event.preventDefault();
                    });
                </script>
            </td>
        </tr>
        <?
    }

    /**
     * @inheritDoc
     */
    public function getJson()
    {
        $values = $this->getValues();

        if (count($values) > 0)
        {
            $this->config['PARAMETERS']['VALUES'] = [];

            foreach ($values as $value)
            {
                if (empty($value['KEY']) || empty($value['VALUE']))
                {
                    continue;
                }

                if (empty($value['SORT']))
                    $value['SORT'] = 0;

                $this->config['PARAMETERS']['VALUES'][] = $value;
            }

            return json_encode($this->config);
        }

        return parent::getJson();
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return isset($this->config['PARAMETERS']['VALUES']) && is_array($this->config['PARAMETERS']['VALUES'])
            ? $this->config['PARAMETERS']['VALUES']
            : [];
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_FIELD_LIST");
    }

    /**
     * @inheritDoc
     */
    protected function validate($value): bool
    {
        $values = array_column($this->getValues(), 'KEY');
        if (array_search($value, $values) === false)
        {
            return true;
        }

        $this->errorMessage = Loc::getMessage("ARTIXGROUP_FORM_FIELD_ERROR_VALUE");

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getJsValidator()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    protected function getHtmlTemplateValue($value): string
    {
        $values = $this->getValues();
        $elementName = '';

        foreach ($values as $item)
        {
            if ($item['KEY'] === $value)
            {
                $elementName = $item['VALUE'];
                break;
            }
        }

        return $elementName;
    }

    /**
     * @inheritDoc
     */
    protected function getTextTemplateValue($value): string
    {
        $values = $this->getValues();
        $elementName = '';

        foreach ($values as $item)
        {
            if ($item['KEY'] === $value)
            {
                $elementName = $item['VALUE'];
                break;
            }
        }

        return $elementName;
    }
}
