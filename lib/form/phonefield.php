<?php

namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Localization\Loc;

class PhoneField extends TextField
{
    protected $defaultType = 'tel';

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_FIELD_NAME");
        $config['TYPE'] = FormFieldTable::TYPE_PHONE;
        $config['HINT'] = 'Пример: +7 (000) 000 0000';
        $config['PARAMETERS']['type_mask'] = 'mask';
        $config['PARAMETERS']['mask'] = '+7 (999) 999 9999';

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_FIELD_TYPE_NAME");
    }

    /**
     * @inheritDoc
     */
    protected function prepareValue($value)
    {
        return preg_replace('/\s|\+|-|\(|\)/','', $value);
    }

    /**
     * @inheritDoc
     */
    protected function validate($value): bool
    {
        if (parent::validate($value))
        {
            if (is_numeric($value))
            {
                return true;
            }

            $this->errorMessage = Loc::getMessage("ARTIXGROUP_FORM_FIELD_ERROR_NUMBER");
        }

        return false;
    }

    /**
     * @param $phone
     * @return string|string[]|null
     */
    protected function phoneFormat($phone)
    {
        $phone = trim($phone);

        $res = preg_replace(
            array(
                '/[\+]?([7|8])[-|\s]?\([-|\s]?(\d{3})[-|\s]?\)[-|\s]?(\d{3})[-|\s]?(\d{2})[-|\s]?(\d{2})/',
                '/[\+]?([7|8])[-|\s]?(\d{3})[-|\s]?(\d{3})[-|\s]?(\d{2})[-|\s]?(\d{2})/',
                '/[\+]?([7|8])[-|\s]?\([-|\s]?(\d{4})[-|\s]?\)[-|\s]?(\d{2})[-|\s]?(\d{2})[-|\s]?(\d{2})/',
                '/[\+]?([7|8])[-|\s]?(\d{4})[-|\s]?(\d{2})[-|\s]?(\d{2})[-|\s]?(\d{2})/',
                '/[\+]?([7|8])[-|\s]?\([-|\s]?(\d{4})[-|\s]?\)[-|\s]?(\d{3})[-|\s]?(\d{3})/',
                '/[\+]?([7|8])[-|\s]?(\d{4})[-|\s]?(\d{3})[-|\s]?(\d{3})/',
            ),
            array(
                '+7 ($2) $3-$4-$5',
                '+7 ($2) $3-$4-$5',
                '+7 ($2) $3-$4-$5',
                '+7 ($2) $3-$4-$5',
                '+7 ($2) $3-$4',
                '+7 ($2) $3-$4',
            ),
            $phone
        );

        return $res;
    }

    /**
     * @inheritDoc
     */
    protected function getHtmlTemplateValue($value): string
    {
        return '<a href="tel:'.$value.'">'.$this->phoneFormat($value)."</a>";
    }

    /**
     * @inheritDoc
     */
    protected function getTextTemplateValue($value)
    {
        return $this->phoneFormat($value);
    }
}
