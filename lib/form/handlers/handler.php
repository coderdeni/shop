<?php


namespace Artixgroup\Shop\Form\Handlers;


use Artixgroup\Shop\Form\FieldBase;
use Artixgroup\Shop\Form\PhoneField;

class Handler
{
    protected array $parameters = [];

    public function __construct(array $parameters = [])
    {
        $this->parameters = $parameters;
    }

    public function showParameters()
    {
        ?>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <?
    }

    /**
     * @param FieldBase[] $fields
     */
    public function doAction(array &$fields)
    {
        $data = [];

        foreach ($fields as $field)
        {
            $parameter = $this->parameters[$field->getCode()];

            if ($field instanceof PhoneField)
            {
                $data[$parameter['CODE']] = [
                    'PHONE' => $field->getPrepareValue()
                ];
            }
            else
            {
                $data[$parameter['CODE']] = $field->getPrepareValue();
            }
        }

        // send $data to crm
    }
}