<?php

namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Localization\Loc;

class NumberField extends TextField
{
    protected $defaultType = 'number';

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_FIELD_NUMBER");
        $config['TYPE'] = FormFieldTable::TYPE_NUMBER;
        $config['PARAMETERS']['type_mask'] = 'regex';
        $config['PARAMETERS']['regex'] = '\\d*';

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_FIELD_NUMBER");
    }

    /**
     * @inheritDoc
     */
    protected function prepareValue($value)
    {
        return $this->prepareNumber($value);
    }

    /**
     * @param $value
     * @return bool
     */
    protected function prepareNumber($value): bool
    {
        if (is_numeric($value))
        {
            $int = (int)$value;
            $float = (float)$value;
            $value = ($int === $float) ? $int : $float;
        }

        return $value;
    }

    /**
     * @inheritDoc
     */
    protected function validate($value): bool
    {
        if (parent::validate($value))
        {
            if (is_numeric($value))
            {
                return true;
            }

            $this->errorMessage = Loc::getMessage("ARTIXGROUP_FORM_FIELD_ERROR_NUMBER");
        }

        return false;
    }
}
