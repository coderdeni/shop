<?php

namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Iblock\SectionTable;
use \Bitrix\Main\Data\Cache;

class IblockSectionField extends IblockElementField
{
    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_SECTION_LIST");
        $config['TYPE'] = FormFieldTable::TYPE_IBLOCK_SECTION;

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_SECTION_LIST");
    }

    /**
     * @inheritDoc
     */
    public function getValues(): array
    {
        $values = [];
        $iblockId = (int)$this->config['PARAMETERS']['iblock_id'];

        if ($iblockId > 0 && Loader::includeModule('iblock'))
        {
            $cache = Cache::createInstance();

            if ($cache->initCache(7200, "artixgroup_form_iblock_section_field_$iblockId"))
            {
                $values = $cache->getVars();
            }
            elseif ($cache->startDataCache())
            {
                $iterator = SectionTable::getList([
                    'select' => ['ID', 'NAME'],
                    'filter' => ['IBLOCK_ID' => $iblockId, 'ACTIVE' => 'Y']
                ]);

                while ($section = $iterator->Fetch())
                {
                    $values[] = [
                        'KEY' => $section['ID'],
                        'VALUE' => $section['NAME'],
                        'DEF' => ''
                    ];
                }

                $cache->endDataCache($values);
            }
        }

        return $values;
    }

    /**
     * @inheritDoc
     */
    protected function getHtmlTemplateValue($value): string
    {
        $iblockId = (int)$this->config['PARAMETERS']['iblock_id'];
        $values = $this->getValues();
        $url = "/bitrix/admin/iblock_section_edit.php?IBLOCK_ID=$iblockId&lang=".LANGUAGE_ID."&ID=$value";
        $elementName = '';

        foreach ($values as $item)
        {
            if ((int)$item['KEY'] === (int)$value)
            {
                $elementName = $item['VALUE'];
                break;
            }
        }

        return '[<a href="'.$url.'">'.$value.'</a>] <a href="'.$url.'">'.$elementName."</a>";
    }
}
