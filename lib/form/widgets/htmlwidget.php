<?php
namespace Artixgroup\Shop\Form\Widgets;

use Artixgroup\Shop\Form\WidgetField;
use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

class HtmlWidget extends WidgetField
{
    protected $defaultType = 'html';

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = 'HTML виджет';
        $config['TYPE'] = FormFieldTable::TYPE_HTML_WIDGET;

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return 'HTML виджет';
    }

    /**
     * @inheritDoc
     */
    public function getData(): array
    {
        return [
            'arResult' => $this->config['DEFAULT_VALUE']
        ];
    }

    public function showHtmlParameters()
    {
        ?>
        <tr>
            <td class="adm-detail-content-cell-r" colspan="2">
                <?
                Loader::includeModule("fileman");
                $LHE = new \CHTMLEditor;
                $LHE->Show(array(
                    'inputName' => 'DEFAULT_VALUE',
                    'content' => $this->config['DEFAULT_VALUE'],
                    'height' => 200,
                    'width' => '100%',
                    'view' => 'wysiwyg',
                    'minBodyWidth' => 350,
                    'bAllowPhp' => false,
                    'limitPhpAccess' => false,
                    'autoResize' => true,
                    'autoResizeOffset' => 40,
                    'useFileDialogs' => false,
                    'saveOnBlur' => true,
                    'showTaskbars' => true,
                    'showNodeNavi' => false,
                    'askBeforeUnloadPage' => true,
                    'bbCode' => false,
                    'setFocusAfterShow' => false
                ));
                ?>
            </td>
        </tr>
        <?
    }
}