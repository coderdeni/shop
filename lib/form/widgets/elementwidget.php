<?php


namespace Artixgroup\Shop\Form;


use Bitrix\Main\Loader;

class ElementWidget extends WidgetField
{
    protected function getDefaultConfig()
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = 'Элемент';

        return $config;
    }

    public function showHtmlParameters()
    {
        ?>
        <tr>
            <td class="adm-detail-content-cell-l">Обезательное:</td>
            <td class="adm-detail-content-cell-r">
                <input type="checkbox" name="FIELDS[<?= self::$key?>][EXTRANEL]" value="Y"<?= $this->isRequired() ? ' checked': ''?>>
            </td>
        </tr>
        <tr<?= !$this->isRequired() ? ' display="none"': ''?>>
            <td class="adm-detail-content-cell-l">ID элемента:</td>
            <td class="adm-detail-content-cell-r"><input type="number" name="FIELDS[<?= self::$key?>][PARAMETERS]['element_Id']" value="<?= $this->config['PARAMETERS']['element_Id']?>"></td>
        </tr>
        <?
    }

    protected function getData()
    {
        $elementId = (int)$this->config['PARAMETERS']['element_Id'];
        $data = ['arResult' => null];

        if ($elementId > 0 && Loader::includeModule('iblock'))
        {
            $result = \CIBlockElement::GetList([], ['=ID' => $elementId], false, false, ['*', 'PROPERTIES_*']);
            if ($ob = $result->GetNextElement())
            {
                $fields = $ob->GetFields();
                $fields['PROPERTIES'] = $ob->GetProperties();

                $data['arResult'] = $fields;
            }
        }

        return $data;
    }

    public function getJsRegexp()
    {
        // TODO: Implement getJsRegexp() method.
    }

    public function getJsValidator()
    {
        // TODO: Implement getJsValidator() method.
    }
}