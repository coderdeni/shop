<?php

namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Localization\Loc;

class FlagField extends SelectField
{
    protected $defaultType = 'radio';

    /**
     * @inheritDoc
     */
    public function __construct(array $config = array(), $key = null)
    {
        parent::__construct($config, $key);

        if ($this->isMultiple())
            $this->defaultType = 'checkbox';
    }

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_FLAG");
        $config['TYPE'] = FormFieldTable::TYPE_FLAG;

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_FLAG");
    }
}
