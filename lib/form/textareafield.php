<?php

namespace Artixgroup\Shop\Form;

use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Localization\Loc;

class TextareaField extends TextField
{
    protected $defaultType = 'textarea';

    /**
     * @inheritDoc
     */
    protected function getDefaultConfig(): array
    {
        $config = parent::getDefaultConfig();
        $config['NAME'] = Loc::getMessage("ARTIXGROUP_FORM_FIELD_TEXTAREA");
        $config['TYPE'] = FormFieldTable::TYPE_TEXTAREA;

        return $config;
    }

    /**
     * @inheritDoc
     */
    public static function getTypeName(): string
    {
        return Loc::getMessage("ARTIXGROUP_FORM_FIELD_TYPE_TEXTAREA");
    }

    /**
     * @inheritDoc
     */
    public function showHtmlParameters()
    {
        parent::showHtmlParameters();
        ?>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_PARAM_COL") ?></td>
            <td class="adm-detail-content-cell-r"><input type="number" name="PARAMETERS[rows]" value="<?= $this->config['PARAMETERS']['cols']?>"></td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_PARAM_ROW") ?></td>
            <td class="adm-detail-content-cell-r"><input type="number" name="PARAMETERS[cols]" value="<?= $this->config['PARAMETERS']['rows']?>"></td>
        </tr>
        <?
    }
}
