<?php
declare(strict_types=1);
namespace Artixgroup\Shop\Ipgeo;

use Bitrix\Main\Error;
use Bitrix\Main\EventResult;
use Bitrix\Main\Service\GeoIp\Base;
use Bitrix\Main\Text\Encoding;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Service\GeoIp\Data;
use Bitrix\Main\Service\GeoIp\Result;
use Bitrix\Main\Web\HttpClient;
use Gazmyas\Partners\SxGeo;

Loc::loadMessages(__FILE__);

class IpGeoBase extends Base
{
    /**
     * Параметр сортировки этого метода геолокации
     * по отношению к другим аналогичным методам.
     * Чем меньше значение, тем больше приоритет.
     *
     * @var integer
     */
    protected $sort = 200;

    /**
     * Флаг, активен ли обработчик геолокации
     *
     * @var boolean
     */
    protected $active = true;

    /** @var ErrorCollection */
    protected $errors;

    /**
     * Base constructor.
     *
     * @param array $arFields DB fields of handlers settings.
     */
    public function __construct(array $arFields = [])
    {
        $this->errors = new ErrorCollection();
        if (!isset($arFields['SORT'])) {
            $arFields['SORT'] = $this->sort;
        }
        if (!isset($arFields['ACTIVE'])) {
            $arFields['ACTIVE'] = $this->active ? 'Y' : 'N';
        }

        parent::__construct($arFields);
    }

    /**
     * @return integer field sorting.
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Is this handler installed and ready for using.
     * @return bool
     */
    public function isInstalled()
    {
        return true;
    }

    /**
     * @return bool Is handler active, or not.
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Languages supported by handler ISO 639-1
     * @return array
     */
    public function getSupportedLanguages()
    {
        return ['en', 'ru'];
    }

    /**
     * @return string Title of handler.
     */
    public function getTitle()
    {
        return Loc::getMessage('ARTIXGROUP_GEOIP_TITLE');
    }

    /**
     * @return string Handler description.
     */
    public function getDescription()
    {
        return Loc::getMessage('ARTIXGROUP_GEOIP_DESCRIPTION');
    }


    /**
     * функция парсит полученные в XML данные в случае, если на сервере не установлено расширение Simplexml
     * @param string $string адрес в формате 1.2.3.4
     * @return array - возвращает массив с данными
     */
    protected function prepareResult($string)
    {
        $params = ['inetnum', 'country', 'city', 'region', 'district', 'lat', 'lng'];
        $data = $out = [];

        foreach ($params as $param)
        {
            if (preg_match('#<' . $param . '>(.*)</' . $param . '>#is', $string, $out))
            {
                $data[$param] = trim($out[1]);
            }
        }

        return $data;
    }

    public function getProvidingData()
    {
        $result = new \Bitrix\Main\Service\GeoIp\ProvidingData();

        $result->countryCode = true;
        $result->regionName = true;
        $result->cityName = true;
        $result->latitude = true;
        $result->longitude = true;

        return $result;
    }

    /**
     * Метод возвращает результат определения гео-данных
     *
     * @param string $ip   Ip address
     * @param string $lang Language identifier
     *
     * @return Result|null
     */
    public function getDataResult($ip, $lang = '')
    {
        $dataResult = new Result();
        $geoData = new Data();

        $geoData->ip = $ip;
        $lang = empty($lang) ? 'ru' : $lang;
        $geoData->lang = $lang;

        if ($ip)
        {
            $client = new HttpClient([
                "version" => "1.1",
                "socketTimeout" => 5,
                "streamTimeout" => 5,
                "redirect" => true,
                "redirectMax" => 5,
            ]);

            $result = $client->get('http://ipgeobase.ru:7020/geo?ip=' . $ip);
            $errors = $client->getError();

            if ($result && empty($errors))
            {
                if (strtolower(SITE_CHARSET) == 'utf-8')
                    $result = Encoding::convertEncoding($result, 'windows-1251', SITE_CHARSET);

                $result = $this->prepareResult($result);
                $geoData->countryName = $result['country'];
                $geoData->regionName = $result['region'];
                $geoData->cityName = $result['city'];
                $geoData->latitude = $result['lat'];
                $geoData->longitude = $result['lng'];
            }
        }

        $dataResult->setGeoData($geoData);

        return $dataResult;
    }

    /**
     * Returns an array of Error objects.
     *
     * @return Error[]
     */
    public function getErrors(): array
    {
        return $this->errors->toArray();
    }

    /**
     * Метод добавляет ошибку в стек ошибок объекта
     *
     * @param string $error Текст ошибки
     */
    public function addError(string $error)
    {
        $this->errors->add([new Error($error)]);
    }

    /**
     * Подключение собственного обработчика геолокации в битриксе
     *
     * @return EventResult
     */
    public static function getGeoIpHandlers()
    {
        return new EventResult(
            EventResult::SUCCESS,
            [
                __CLASS__ => '/bitrix/modules/artixgroup.shop/lib/ipgeo/ipgeobase.php',
            ]
        );
    }
}