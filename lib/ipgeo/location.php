<?php

namespace Artixgroup\Shop\Ipgeo;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;
use \Bitrix\Main\Service\GeoIp;
use Bitrix\Sale\Location\LocationTable;

class Location
{
    const SESSION_KEY = 'ARTIXGROUP_LOCATION';

    protected $location = null;
    protected static $instance = null;

    /**
     * Singleton
     * @return Favourites|null
     */
    public static function getInstance()
    {
        if (self::$instance === null)
        {
            global $USER;

            if ($USER->IsAuthorized())
                self::$instance = new self($USER->GetID());
            else
                self::$instance = new self();
        }

        return self::$instance;
    }

    public function getLocation()
    {
        if (!$this->location)
        {
            if (isset($_SESSION[self::SESSION_KEY]))
            {
                $this->location = $_SESSION[self::SESSION_KEY];
            }
            elseif ($cookie = Application::getInstance()->getContext()->getRequest()->getCookie(self::SESSION_KEY))
            {
                $_SESSION[self::SESSION_KEY] = unserialize($cookie);
                $this->location = $_SESSION[self::SESSION_KEY];
            }
            elseif (Loader::includeModule('sale'))
            {
                $userIp = GeoIp\Manager::getRealIp();
                $locationId = \Bitrix\Sale\Location\GeoIp::getLocationId($userIp, LANGUAGE_ID);

                if (!$locationId)
                {
                    $locationId = Option::get('sale', 'location');
                }

                $this->setLocation($locationId);
            }
            else
            {
                $userIp = GeoIp\Manager::getRealIp();
                $city = GeoIp\Manager::getCityName($userIp);
                $this->setLocation($city);
            }
        }

        return $this->location;
    }

    public function setLocation($locationId)
    {
        if (Loader::includeModule('sale'))
        {
            $filter = is_string($locationId) ? ['=CODE' => $locationId] : ['=ID' => $locationId];
            $filter['=NAME.LANGUAGE_ID'] = LANGUAGE_ID;

            $this->location = LocationTable::getList([
                'select' => ['ID', 'CODE', 'LOCATION_NAME' => 'NAME.NAME', 'COUNTRY_ID', 'REGION_ID'],
                'filter' => $filter
            ])->fetch();
        }
        else
        {
            $this->location = [
                'LOCATION_NAME' => $locationId
            ];
        }

        $_SESSION[self::SESSION_KEY] = $this->location;
        $cookie = new Cookie(self::SESSION_KEY, serialize($this->location));
        $cookie->setExpires(time() + 604800);
        Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
    }
}