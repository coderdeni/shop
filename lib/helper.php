<?php
namespace Artixgroup\Shop;

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class Helper.
 * @package Artixgroup\Shop
 */
class Helper
{
    /**
     * Builds admin menu
     * @param array $aGlobalMenu
     * @param array $aModuleMenu
     * @return array|bool Global menu item.
     */
    public static function onBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        global $APPLICATION;

        $moduleAccess = $APPLICATION->GetGroupRight('artixgroup.shop');

        if ($moduleAccess < "R")
            return false;

        $resultItems = [];
        $forms = FormTable::getList([])->fetchAll();

        if (count($forms) > 0)
        {
            foreach ($forms as $form)
            {
                $resultItems[] = [
                    "text" => $form['NAME'],
                    "url" => "/bitrix/admin/artixgroup_form_result_list.php?ID=".$form['ID']."&lang=".LANGUAGE_ID,
                    //"icon" => "form_menu_icon",
                    "more_url" => array(
                        "artixgroup_form_list.php"
                    )
                ];
            }
        }

        $menuItems = [
            [
                "text" => Loc::getMessage("ARTIXGROUP_HELPER_FORM"),
                "url" => "/bitrix/admin/artixgroup_form_list.php?lang=".LANGUAGE_ID,
                //"icon" => "form_menu_icon",
                "more_url" => array(
                    "artixgroup_form_list.php"
                )
            ],
            [
                "parent_menu" => "global_menu_artixgroup",
                "section" => "artixgroup",
                "sort" => 100,
                "text" => Loc::getMessage("ARTIXGROUP_HELPER_FORM_RESULT"),
                //"icon" => "form_menu_icon",
                "page_icon" => "form_menu_icon",
                "items_id" => "menu_artixgroup",
                //"url" => "/bitrix/admin/artixgroup_form_result_list.php?ID=lang=".LANGUAGE_ID,
                "more_url" => array("artixgroup_form_list.php", "artixgroup_form_list.php"),
                "items" => $resultItems,
            ]
        ];

        $menu = [
            [
                "parent_menu" => "global_menu_artixgroup",
                "section" => "artixgroup",
                "sort" => 100,
                "text" => Loc::getMessage("ARTIXGROUP_HELPER_FAVOURITES"),
                "icon" => "fav_menu_icon_yellow",
                "page_icon" => "fav_menu_icon_yellow",
                "items_id" => "menu_artixgroup",
                "url" => "/bitrix/admin/artixgroup_favourites_list.php?lang=".LANGUAGE_ID,
                "items" => array(),
            ],
            [
                "parent_menu" => "global_menu_artixgroup",
                "section" => "artixgroup",
                "sort" => 100,
                "text" => Loc::getMessage("ARTIXGROUP_HELPER_FORM"),
                "icon" => "form_menu_icon",
                "page_icon" => "form_menu_icon",
                "items_id" => "menu_artixgroup",
                //"url" => "/bitrix/admin/artixgroup_form_list.php?lang=".LANGUAGE_ID,
                "more_url" => array("artixgroup_form_list.php", "artixgroup_form_list.php"),
                "items" => $menuItems,
            ]
        ];

        return array(
            "global_menu_artixgroup" => array(
                "menu_id" => "artixgroup",
                "text" => Loc::getMessage("ARTIXGROUP_HELPER_GM_TEXT"),
                "title" => Loc::getMessage("ARTIXGROUP_HELPER_GM_TITLE"),
                "sort" => 220,
                "items_id" => "global_menu_artixgroup",
                "help_section" => "artixgroup",
                "items" => $menu
            ));
    }
}