<?php
namespace Artixgroup\Shop;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class FormSiteTable
 *
 * Fields:
 * <ul>
 * <li> FORM_ID int mandatory
 * <li> SITE_ID char(2) mandatory
 * <li> FORM reference to {@link \Artixgroup\Shop\Form}
 * <li> SITE reference to {@link \Bitrix\Main\SiteTable}
 * </ul>
 *
 * @package Bitrix\Iblock
 */
class FormSiteTable extends Entity\DataManager
{
    /**
     * Returns DB table name for entity
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'artixgroup_form_site';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'FORM_ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'title' => Loc::getMessage('IBLOCK_SITE_ENTITY_IBLOCK_ID_FIELD'),
            ),
            'SITE_ID' => array(
                'data_type' => 'string',
                'primary' => true,
                'validation' => array(__CLASS__, 'validateSiteId'),
                'title' => Loc::getMessage('IBLOCK_SITE_ENTITY_SITE_ID_FIELD'),
            ),
            'FORM' => array(
                'data_type' => 'Artixgroup\Shop\Form',
                'reference' => array('=this.FORM_ID' => 'ref.ID')
            ),
            'SITE' => array(
                'data_type' => 'Bitrix\Main\Site',
                'reference' => array('=this.SITE_ID' => 'ref.SITE_ID'),
            ),
        );
    }

    /**
     * Returns validators for SITE_ID field.
     *
     * @return array
     */
    public static function validateSiteId()
    {
        return array(
            new Entity\Validator\Length(null, 2),
        );
    }
}
