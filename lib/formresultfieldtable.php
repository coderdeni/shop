<?php
namespace Artixgroup\Shop;

use Bitrix\Main;
use Bitrix\Main\ORM;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class SettingTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> USER_ID int mandatory
 * <li> ITEMS array
 * <li> USER reference to {@link \Bitrix\User\UserTable}
 * </ul>
 *
 * @package Artixgroup\Favourites
 **/

class FormResultFieldTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'artixgroup_form_result_field';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     * @throws Main\ArgumentException
     * @throws Main\SystemException
     */
    public static function getMap()
    {
        return array(
            new ORM\Fields\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_ID'),
            ]),
            new ORM\Fields\IntegerField('RESULT_ID', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_RESULT_ID'),
            ]),
            new ORM\Fields\IntegerField('FIELD_ID', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_FIELD_ID'),
            ]),
            new ORM\Fields\TextField('VALUE', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_VALUE'),
                'save_data_modification' => function () {
                    return [function ($value) {
                        return is_array($value) ? json_encode($value) : $value;
                    }];
                },
                'fetch_data_modification' => function () {
                    return [function ($value) {
                        $result = json_decode($value, true);
                        return $result ? $result : $value;
                    }];
                },
            ]),
            new ORM\Fields\TextField('RAW_VALUE', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_RAW_VALUE'),
                'save_data_modification' => function () {
                    return [function ($value) {
                        return is_array($value) ? json_encode($value) : $value;
                    }];
                },
                'fetch_data_modification' => function () {
                    return [function ($value) {
                        $result = json_decode($value, true);
                        return $result ? $result : $value;
                    }];
                },
            ]),
            new ORM\Fields\Relations\Reference(
                'RESULT',
                '\Artixgroup\Shop\FormResult',
                ['=this.RESULT_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new ORM\Fields\Relations\Reference(
                'FIELD',
                '\Artixgroup\Shop\FormField',
                ['=this.FIELD_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        );
    }
}