<?php


namespace Artixgroup\Shop\Helpers;


use Bitrix\Iblock\SectionTable;

class Section
{
    /**
     * @param int $iblockId
     * @param array $select
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getTree(int $iblockId, array $select)
    {
        // SELECT * FROM tree ORDER BY left_key

        return SectionTable::getList([
            'select' => $select,
            'filter' => [
                'IBLOCK_ID' => $iblockId
            ],
            'order' => ['LEFT_MARGIN']
        ])->fetchAll();
    }

    /**
     * @param int|array $section
     * @param array $select
     * @return array|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getParents($section, array $select)
    {
        //SELECT * FROM tree WHERE left_key <= $left_key AND right_key >= $right_key ORDER BY left_key

        if ($filter = self::getFilter($section))
        {
            return SectionTable::getList([
                'select' => $select,
                'filter' => [
                    "<=LEFT_MARGIN" => (int)$filter["LEFT_MARGIN"],
                    ">=RIGHT_MARGIN" => (int)$filter["RIGHT_MARGIN"],
                    'IBLOCK_ID' => (int)$filter['IBLOCK_ID']
                ],
                'order' => ['LEFT_MARGIN']
            ])->fetchAll();
        }

        return null;
    }

    /**
     * @param int|array $section
     * @param array $select
     * @return array|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getChild($section, array $select = [])
    {
        // SELECT * FROM tree WHERE left_key >= $left_key AND right_key <= $right_key ORDER BY left_key

        if ($filter = self::getFilter($section))
        {
            return SectionTable::getList([
                'select' => $select,
                'filter' => [
                    ">=LEFT_MARGIN" => (int)$filter["LEFT_MARGIN"],
                    "<=RIGHT_MARGIN" => (int)$filter["RIGHT_MARGIN"],
                    'IBLOCK_ID' => (int)$filter['IBLOCK_ID']
                ],
                'order' => ['LEFT_MARGIN']
            ])->fetchAll();
        }

        return null;
    }

    /**
     * @param int|array $section // Sect
     * @param array $select
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getBranch($section, array $select)
    {
        // SELECT * FROM tree WHERE right_key > $left_key AND left_key < $right_key ORDER BY left_key

        if ($filter = self::getFilter($section))
        {
            return SectionTable::getList([
                'select' => $select,
                'filter' => [
                    ">RIGHT_MARGIN" => (int)$filter["LEFT_MARGIN"],
                    "<LEFT_MARGIN" => (int)$filter["RIGHT_MARGIN"],
                    'IBLOCK_ID' => (int)$filter['IBLOCK_ID']
                ],
                'order' => ['LEFT_MARGIN']
            ])->fetchAll();
        }

        return null;
    }

    /**
     * @param int|array $section
     * @return array|false|int|mixed|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected static function getFilter($section)
    {
        if (is_array($section) && array_diff_key(array_flip(['LEFT_MARGIN', 'RIGHT_MARGIN', 'IBLOCK_ID']), $section))
        {
            $section = isset($section['ID']) ? (int)$section['ID'] : null;
        }

        if (is_numeric($section))
        {
            $section = SectionTable::getById($section['ID'])->fetch();
        }

        return $section;
    }
}