<?php

namespace Artixgroup\Shop\Helpers;

use Bitrix\Main\ArgumentException;

class Filter
{
    protected static array $filters = [];

    public static function create(array $filter): string
    {
        $filterId = uniqid('filter_');
        self::$filters[$filterId] = $filter;

        return $filterId;
    }

    public static function add(string $filterName, array $filter)
    {
        if (!isset(self::$filters[$filterName]))
            throw new ArgumentException('Filter "'.$filterName.'" not found.');

        self::$filters[$filterName] = array_merge(self::$filters[$filterName], $filter);
    }

    public static function merge(string $filterName1, string $filterName2): bool
    {
        if (self::isset($filterName1) && self::isset($filterName2))
        {
            self::$filters[$filterName1] = array_merge(self::$filters[$filterName1], self::$filters[$filterName2]);

            return true;
        }

        return false;
    }

    public static function get(string $filterName)
    {
        return self::isset(self::$filters[$filterName]) ? self::$filters[$filterName] : null;
    }

    public static function isset(string $filterName): bool
    {
        return isset(self::$filters[$filterName]) && !empty(self::$filters[$filterName]);
    }

    public static function getSum(string $filterName): string
    {
        return md5(serialize(self::get($filterName)));
    }

    public static function unset(string $filterName)
    {
        unset(self::$filters[$filterName]);
    }
}