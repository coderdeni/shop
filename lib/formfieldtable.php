<?php
namespace Artixgroup\Shop;

use Bitrix\Main;
use Bitrix\Main\ORM;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class SettingTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> USER_ID int mandatory
 * <li> ITEMS array
 * <li> USER reference to {@link \Bitrix\User\UserTable}
 * </ul>
 *
 * @package Artixgroup\Favourites
 **/

class FormFieldTable extends Main\Entity\DataManager
{
    const TYPE_SUBMIT = 'B';
    const TYPE_AGREEMENT = 'R';

    const TYPE_TEXT = 'T';
    const TYPE_NUMBER = 'N';
    const TYPE_EMAIL = 'M';
    const TYPE_PHONE = 'P';
    const TYPE_TEXTAREA = 'A';
    const TYPE_SELECT = 'L';
    const TYPE_FLAG = 'C';
    const TYPE_IBLOCK_ELEMENT = 'E';
    const TYPE_IBLOCK_SECTION = 'S';
    const TYPE_USER = 'U';
    const TYPE_FILE = 'F';
    const TYPE_HTML_WIDGET = 'H';

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'artixgroup_form_field';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     * @throws Main\ArgumentException
     * @throws Main\SystemException
     */
    public static function getMap()
    {
        return array(
            new ORM\Fields\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_ID'),
            ]),
            new ORM\Fields\IntegerField('FORM_ID', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_FORM_ID'),
            ]),
            new ORM\Fields\IntegerField('COL', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_COL'),
            ]),
            new ORM\Fields\IntegerField('ROW', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_ROW'),
            ]),
            new ORM\Fields\IntegerField('SORT', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_SORT'),
            ]),
            new ORM\Fields\StringField('NAME', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_NAME'),
            ]),
            new ORM\Fields\EnumField('TYPE', [
                'required' => true,
                'values' => array(
                    self::TYPE_TEXT,
                    self::TYPE_NUMBER,
                    self::TYPE_EMAIL,
                    self::TYPE_PHONE,
                    self::TYPE_TEXTAREA,
                    self::TYPE_SELECT,
                    self::TYPE_FLAG,
                    self::TYPE_IBLOCK_ELEMENT,
                    self::TYPE_IBLOCK_SECTION,
                    self::TYPE_USER,
                    self::TYPE_FILE,
                    self::TYPE_HTML_WIDGET,
                    self::TYPE_AGREEMENT,
                    self::TYPE_SUBMIT,
                ),
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_TYPE'),
            ]),
            new ORM\Fields\EnumField('MULTIPLE', [
                'values' => array('N', 'Y'),
                'default_value' => 'N',
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_MULTIPLE'),
            ]),
            new ORM\Fields\EnumField('EXTERNAL', [
                'values' => array('N', 'Y'),
                'default_value' => 'N',
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_EXTERNAL'),
            ]),
            new ORM\Fields\BooleanField('REQUIRED', [
                'values' => array('N', 'Y'),
                'default_value' => 'N',
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_REQUIRED'),
            ]),
            new ORM\Fields\TextField('DEFAULT_VALUE', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_DEFAULT_VALUE'),
                'save_data_modification' => function () {
                    return [function ($value) {
                        return is_array($value) ? json_encode($value) : $value;
                    }];
                },
                'fetch_data_modification' => function () {
                    return [function ($value) {
                        $result = json_decode($value, true);
                        return $result ? $result : $value;
                    }];
                },
            ]),
            new ORM\Fields\TextField('PARAMETERS', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_PARAMETERS'),
                'save_data_modification' => function () {
                    return [function ($value) {
                        return json_encode($value);
                    }];
                },
                'fetch_data_modification' => function () {
                    return [function ($value) {
                        return json_decode($value, true);
                    }];
                },
            ]),
            new ORM\Fields\StringField('HINT', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_HINT'),
            ]),
            new ORM\Fields\Relations\Reference(
                'FORM',
                '\Artixgroup\Shop\Form',
                ['=this.FORM_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        );
    }

    public static function getFieldByType($type)
    {
        $field = self::getFields();

        return isset($field[$type]) ? $field[$type] : null;
    }

    public static function getFields()
    {
        return [
            self::TYPE_TEXT => '\Artixgroup\Shop\Form\TextField',
            self::TYPE_NUMBER => '\Artixgroup\Shop\Form\NumberField',
            self::TYPE_EMAIL => '\Artixgroup\Shop\Form\EmailField',
            self::TYPE_PHONE => '\Artixgroup\Shop\Form\PhoneField',
            self::TYPE_TEXTAREA => '\Artixgroup\Shop\Form\TextareaField',
            self::TYPE_SELECT => '\Artixgroup\Shop\Form\SelectField',
            self::TYPE_FLAG => '\Artixgroup\Shop\Form\FlagField',
            self::TYPE_IBLOCK_ELEMENT => '\Artixgroup\Shop\Form\IblockElementField',
            self::TYPE_IBLOCK_SECTION => '\Artixgroup\Shop\Form\IblockSectionField',
            self::TYPE_USER => '\Artixgroup\Shop\Form\UserField',
            self::TYPE_HTML_WIDGET => '\Artixgroup\Shop\Form\Widgets\HtmlWidget',
            self::TYPE_SUBMIT => '\Artixgroup\Shop\Form\SubmitField',
            self::TYPE_AGREEMENT => '\Artixgroup\Shop\Form\AgreementField'
        ];
    }
}