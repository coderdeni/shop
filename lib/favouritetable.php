<?php

namespace Artixgroup\Shop;

use Bitrix\Main;
use Bitrix\Main\ORM;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class FavouriteTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> USER_ID int mandatory
 * <li> ITEMS array
 * <li> USER reference to {@link \Bitrix\User\UserTable}
 * </ul>
 *
 * @package Artixgroup\Favourites
 **/

class FavouriteTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'artixgroup_favourites';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     * @throws Main\ArgumentException
     * @throws Main\SystemException
     */
    public static function getMap()
    {
        return array(
            new ORM\Fields\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_ID_FIELD'),
            ]),
            new ORM\Fields\IntegerField('USER_ID', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_USER_ID'),
            ]),
            new ORM\Fields\ArrayField('ITEMS', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_ITEMS'),
            ]),
            'USER' => new ORM\Fields\Relations\Reference(
                'USER',
                '\Bitrix\Main\User',
                ['=this.USER_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        );
    }
}