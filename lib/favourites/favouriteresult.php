<?php


namespace Artixgroup\Shop\Favourites;


class FavouriteResult
{
    protected $errors = [];

    public function isSuccess()
    {
        return empty($this->errors);
    }

    public function addError($message)
    {
        $this->errors[] = $message;
    }

    public function getErrorMessages()
    {
        return $this->errors;
    }
}

class AddResult extends FavouriteResult
{
}

class DeleteResult extends FavouriteResult
{
}

class UpdateResult extends FavouriteResult
{
}