<?php

namespace Artixgroup\Shop\Favourites;

use Artixgroup\Shop\FavouriteTable;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Web\Cookie;

class Favourites
{
    const SESSION_KEY = 'ARTIXGROUP_FAVORITES';

    protected $userId = null;
    protected static $instance = null;

    /**
     * Favourites constructor.
     * @param null $userId
     */
    public function __construct($userId = null)
    {
        $this->userId = (int)$userId;
        $this->loadItems();
    }

    /**
     * Singleton
     * @return Favourites|null
     */
    public static function getInstance()
    {
        if (self::$instance === null)
        {
            global $USER;

            if ($USER->IsAuthorized())
                self::$instance = new self($USER->GetID());
            else
                self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Returns a list of item IDs
     * @return array
     */
    public function getItems()
    {
        if (isset($_SESSION[self::SESSION_KEY]['ITEMS']) && is_array($_SESSION[self::SESSION_KEY]['ITEMS']))
        {
            return $_SESSION[self::SESSION_KEY]['ITEMS'];
        }

        return [];
    }

    /**
     * Checks whether an item with this id is in the list
     * @param $elementId
     * @return bool
     */
    public function containsItems($elementId)
    {
        if ($this->getCount() > 0)
        {
            return in_array($elementId, $this->getItems());
        }

        return false;
    }

    /**
     * Returns the number of items in the list
     * @return int
     */
    public function getCount()
    {
        return count($this->getItems());
    }

    /**
     * Returns the user
     * @return mixed|null
     */
    public function getUser()
    {
        if (isset($_SESSION[self::SESSION_KEY]['USER']) && $_SESSION[self::SESSION_KEY]['USER'])
        {
            return $_SESSION[self::SESSION_KEY]['USER'];
        }

        return null;
    }

    /**
     * Adds the item ID to the list and saves the list to storage
     * @param $elementId
     * @return FavouriteResult
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function add($elementId)
    {
        $addResult = new FavouriteResult();

        if (!Loader::includeModule("iblock"))
        {
            $addResult->addError('Module "iblock" is not installed');
            return $addResult;
        }

        $element = ElementTable::getById((int)$elementId)->fetch();

        if ($element)
        {
            $items = $this->getItems();

            if (!in_array($elementId, $items))
            {
                $items[] = $elementId;
                $result = $this->save($items);

                if (!$result->isSuccess())
                {
                    $addResult->addErrors($result->getErrorMessages());
                }
            }
        }
        else
        {
            $addResult->addError('Элемент не найден');
        }

        return $addResult;
    }

    /**
     * Removes the item ID from the list and saves the list to storage
     * @param $elementId
     * @return FavouriteResult
     * @throws \Bitrix\Main\SystemException
     */
    public function remove($elementId)
    {
        $deleteResult = new FavouriteResult();
        $items = $this->getItems();

        if (($key = array_search($elementId, $items)) !== false)
        {
            unset($items[$key]);
            $result = $this->save(array_values($items));

            if (!$result->isSuccess())
            {
                foreach ($result->getErrorMessages() as $errorMessage)
                {
                    $deleteResult->addError($errorMessage);
                }
            }
        }
        else
        {
            $deleteResult->addError('Элемент не найден');
        }

        return $deleteResult;
    }

    /**
     * Loads item IDs from storage
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function loadItems()
    {
        if (isset($_SESSION[self::SESSION_KEY]) && !empty($_SESSION[self::SESSION_KEY]))
        {
            return;
        }

        $_SESSION[self::SESSION_KEY] = [
            'ID' => 0,
            'USER' => null,
            'ITEMS' => []
        ];

        if ($this->userId)
        {
            $favourites = FavouriteTable::getList([
                'select' => ['*', 'USER'],
                'filter' => ['USER_ID' => $this->userId]
            ])->fetchObject();

            if ($favourites)
            {
                $_SESSION[self::SESSION_KEY] = [
                    'ID' => $favourites->getId(),
                    'USER' => $favourites->getUser(),
                    'ITEMS' => $favourites->getItems()
                ];
            }
            elseif ($favouriteId = $this->createList())
            {
                $_SESSION[self::SESSION_KEY]['ID'] = $favouriteId;
            }
        }
        else
        {
            $context = Application::getInstance()->getContext();
            $request = $context->getRequest();
            $cookie = $request->getCookie(self::SESSION_KEY);

            if ($cookie)
            {
                $_SESSION[self::SESSION_KEY]['ITEMS'] = unserialize($cookie);
            }
        }

        $this->checkItems();
    }

    /**
     * Stores item IDs in storage
     * @param array $productsIds
     * @return FavouriteResult
     * @throws \Bitrix\Main\SystemException
     */
    protected function save(array $productsIds)
    {
        $updateResult = new FavouriteResult();

        if ($_SESSION[self::SESSION_KEY]['ID'] > 0)
        {
            $id = (int)$_SESSION[self::SESSION_KEY]['ID'];
            $result = FavouriteTable::update($id, [
                'ITEMS' => $productsIds
            ]);

            if (!$result->isSuccess())
            {
                foreach ($result->getErrorMessages() as $errorMessage)
                {
                    $updateResult->addError($errorMessage);
                }

                return $updateResult;
            }
        }
        else
        {
            $context = Application::getInstance()->getContext();
            $cookie = new Cookie(self::SESSION_KEY, serialize($productsIds));
            $context->getResponse()->addCookie($cookie);
        }

        $_SESSION[self::SESSION_KEY]['ITEMS'] = $productsIds;

        return $updateResult;
    }

    /**
     * Creates an empty list in the database
     * @return array|false|int
     * @throws \Exception
     */
    protected function createList()
    {
        $result = FavouriteTable::add([
            'USER_ID' => $this->userId,
            'ITEMS' => []
        ]);

        if ($result->isSuccess())
        {
            return $result->getId();
        }

        return false;
    }

    /**
     * Checks items from the list for their presence and makes changes to the storage if necessary
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function checkItems()
    {
        if ($this->getCount() > 0 && Loader::includeModule('iblock'))
        {
            $needUpdate = false;
            $items = $this->getItems();
            $elementsIds = [];
            $iterator = ElementTable::getList([
                'select' => ['ID'],
                'filter' => ['ID' => $items]
            ]);

            while ($element = $iterator->fetch())
            {
                $elementsIds[] = $element['ID'];
            }

            if (count($elementsIds) > 0)
            {
                foreach ($items as $key => $itemId)
                {
                    if (!in_array($itemId, $elementsIds))
                    {
                        $needUpdate = true;
                        unset($items[$key]);
                    }
                }
            }
            else
            {
                $items = [];
                $needUpdate = true;
            }

            if ($needUpdate)
                $this->save($items);
        }
    }

    /**
     * User authorization event handler
     * @param $fields
     * @throws \Bitrix\Main\SystemException
     */
    public static function onAfterUserLogin(&$fields)
    {
        if ($fields['USER_ID'] > 0)
        {
            $currentItems = self::getInstance()->getItems();
            unset($_SESSION[self::SESSION_KEY]);
            self::$instance = new self((int)$fields['USER_ID']);

            if ($currentItems)
            {
                $needUpdate = false;
                $dbItems = self::getInstance()->getItems();

                foreach ($currentItems as $item)
                {
                    if (!in_array($item, $dbItems))
                    {
                        $needUpdate = true;
                        $dbItems[] = $item;
                    }
                }

                if ($needUpdate)
                    self::getInstance()->save($dbItems);
            }
        }
    }

    /**
     * User logout event handler
     * @param $fields
     */
    public static function onAfterUserLogout($fields)
    {
        if ($fields['SUCCESS'])
        {
            unset($_SESSION[self::SESSION_KEY]);
            self::$instance = new self();
        }
    }
}