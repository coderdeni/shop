<?php
namespace Artixgroup\Shop;

use Bitrix\Main;
use Bitrix\Main\ORM;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Fields\Relations\OneToMany;

Loc::loadMessages(__FILE__);

/**
 * Class FormHandlerTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> FORM_ID int mandatory
 * <li> OBJECT string mandatory
 * <li> RUN_MODE enum ('B', 'A') optional default 'A'
 * <li> PARAMETERS array
 * <li> FORM reference to {@link FormTable}
 * </ul>
 *
 * @package Artixgroup\Shop
 **/

class FormHandlerTable extends Main\Entity\DataManager
{
    const RUN_BEFORE = 'B';
    const RUN_AFTER = 'A';

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'artixgroup_form_handler';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     * @throws Main\ArgumentException
     * @throws Main\SystemException
     */
    public static function getMap()
    {
        return array(
            new ORM\Fields\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_ID'),
            ]),
            new ORM\Fields\IntegerField('FORM_ID', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_FORM_ID'),
            ]),
            new ORM\Fields\StringField('OBJECT', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_OBJECT'),
            ]),
            new ORM\Fields\EnumField('RUN_MODE', [
                'values' => array(
                    self::RUN_BEFORE,
                    self::RUN_AFTER,
                ),
                'default_value' => self::RUN_AFTER,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_RUN_MODE'),
            ]),
            new ORM\Fields\TextField('PARAMETERS', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_PARAMETERS'),
                'save_data_modification' => function () {
                    return [function ($value) {
                        return json_encode($value);
                    }];
                },
                'fetch_data_modification' => function () {
                    return [function ($value) {
                        return json_decode($value, true);
                    }];
                },
            ]),
            new ORM\Fields\Relations\Reference(
                'FORM',
                '\Artixgroup\Shop\Form',
                ['=this.FORM_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        );
    }
}