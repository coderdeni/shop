<?php
namespace Artixgroup\Shop;

use Bitrix\Main;
use Bitrix\Main\ORM;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Fields\Relations\OneToMany;

Loc::loadMessages(__FILE__);

/**
 * Class SettingTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> USER_ID int mandatory
 * <li> ITEMS array
 * <li> USER reference to {@link \Bitrix\User\UserTable}
 * </ul>
 *
 * @package Artixgroup\Favourites
 **/

class FormResultTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'artixgroup_form_result';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     * @throws Main\ArgumentException
     * @throws Main\SystemException
     */
    public static function getMap()
    {
        return array(
            new ORM\Fields\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_ID'),
            ]),
            new ORM\Fields\IntegerField('FORM_ID', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_FORM_ID'),
            ]),
            new ORM\Fields\IntegerField('USER_ID', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_USER_ID'),
            ]),
            new ORM\Fields\StringField('PAGE_URL', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_PAGE_URL'),
            ]),
            new ORM\Fields\DatetimeField('DATE_CREATE', array(
                'default_value' => function() {
                    return new Main\Type\DateTime();
                },
                'title' => Loc::getMessage('ELEMENT_ENTITY_DATE_CREATE'),
            )),
            (new OneToMany('FIELDS', FormResultFieldTable::class, 'RESULT'))
                ->configureJoinType('inner'),
            new ORM\Fields\Relations\Reference(
                'FORM',
                '\Artixgroup\Shop\Form',
                ['=this.FORM_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new ORM\Fields\Relations\Reference(
                'USER',
                '\Bitrix\Main\User',
                ['=this.USER_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        );
    }
}