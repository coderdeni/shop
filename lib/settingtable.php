<?php
namespace Artixgroup\Shop;

use Bitrix\Main;
use Bitrix\Main\ORM;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class SettingTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> USER_ID int mandatory
 * <li> ITEMS array
 * <li> USER reference to {@link \Bitrix\User\UserTable}
 * </ul>
 *
 * @package Artixgroup\Favourites
 **/

class SettingTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'artixgroup_setting';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     * @throws Main\ArgumentException
     * @throws Main\SystemException
     */
    public static function getMap()
    {
        return array(
            new ORM\Fields\StringField('CODE', [
                'primary' => true,
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_CODE'),
            ]),
            new ORM\Fields\StringField('SITE_ID', [
                'primary' => true,
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_SITE_ID'),
            ]),
            new ORM\Fields\StringField('TEMPLATE', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_CODE'),
            ]),
            new ORM\Fields\StringField('VALUE', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_VALUE'),
            ]),
            new ORM\Fields\Relations\Reference(
                'SITE',
                '\Bitrix\Main\Site',
                ['=this.SITE_ID' => 'ref.LID'],
                ['join_type' => 'LEFT']
            ),
        );
    }
}