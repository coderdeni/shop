<?php

namespace Artixgroup\Shop\Services;

use Artixgroup\Shop\Setting\Parameters;

class Installment
{
    public static function getPayMonth($basePrice)
    {
        $maxPeriod = Parameters::getInstance()->getInt('max_installment_month');

        if ($basePrice > 0 && $maxPeriod > 0)
        {
            return ceil($basePrice / $maxPeriod);
        }

        return 0;
    }
}