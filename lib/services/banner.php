<?php

namespace Artixgroup\Shop\Services;


use Artixgroup\Shop\Helpers\Filter;
use Artixgroup\Shop\Setting\Parameters;
use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;

class Banner
{
    protected static $index = 0;

    public static function show($type, $template = '', $filter = '')
    {
        global $APPLICATION;

        if (!is_string($filter) && !is_array($filter))
            throw new ArgumentException('Argument "filter" must be array or string');

        if (is_array($filter) && !empty($filter))
        {
            $filter = Filter::create($filter);
        }

        $APPLICATION->IncludeComponent(
            'artixgroup:iblock.banner',
            $template,
            array(
                'IBLOCK_ID' => Parameters::getInstance()->getInt('iblock_banner'),
                'SECTION' => $type,
                'FILTER_NAME' => $filter
            ),
            false,
            array('HIDE_ICONS' => 'Y')
        );
    }

    public static function showInList($type, int $key, $template = '', array $filter = [])
    {
        $filterName = Filter::create([
            'IBLOCK_ID' => Parameters::getInstance()->getInt('iblock_banner'),
            'ACTIVE' => 'Y',
            '=SECTION_CODE' => $type
        ]);

        if (!empty($filter))
        {
            Filter::add($filterName, $filter);
        }

        $cache = Application::getInstance()->getManagedCache();
        $cacheId = Filter::getSum($filterName);
        $keys = [];

        if (!$cache->read(3600, $cacheId))
        {
            Loader::includeModule('iblock');

            $iterator = \CIBlockElement::GetList([], Filter::get($filterName), ['PROPERTY_INDEX']);
            while ($ob = $iterator->Fetch())
            {
                $keys[] = (int)$ob['PROPERTY_INDEX_VALUE'];
            }

            $cache->set($cacheId, $keys); // записываем в кеш
        }
        else
        {
            $keys = $cache->get($cacheId); // достаем переменные из кеша
        }

        Filter::unset($filterName);

        if (in_array($key + 1, $keys))
        {
            $filterName = Filter::create(['PROPERTY_INDEX' => $key + 1]);
            Filter::add($filterName, $filter);

            self::show($type, $template, $filterName);
            Filter::unset($filterName);
        }
    }
}