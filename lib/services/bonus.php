<?php

namespace Artixgroup\Shop\Services;

use Artixgroup\Shop\Setting\Parameters;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\BasketItem;
use Bitrix\Sale\Order;

class Bonus
{
    const BONUS_MODE_COURSE = 0;
    const BONUS_MODE_PROPERTY = 2;
    const BONUS_MODE_BOTH = 1;

    /**
     * @param $productId
     * @param $price
     * @param $currency
     * @return float|int
     */
    public static function getProductBonus($productId, $price, $currency)
    {
        $mode = Parameters::getInstance()->getInt('bonus_mode');
        $baseCurrency = Parameters::getInstance()->getText('bonus_currency');
        $bonus = 0;

        if ($mode === self::BONUS_MODE_PROPERTY || $mode === self::BONUS_MODE_BOTH)
        {
            $product = \CCatalogSku::GetProductInfo($productId);
            $iblockId = Parameters::getInstance()->getInt('catalog_id');

            if ($product)
            {
                $bonus = self::getBonusProperty($productId, $product['OFFER_IBLOCK_ID']);

                if ($bonus === null)
                {
                    $bonus = self::getBonusProperty($product['ID'], $product['IBLOCK_ID']);
                }
            }
            else
            {

                $bonus = self::getBonusProperty($productId, $iblockId);
            }
        }

        if ($price > 0 && ($mode === self::BONUS_MODE_COURSE || ($mode === self::BONUS_MODE_BOTH && $bonus === null)))
        {
            if ($baseCurrency !== null && $baseCurrency !== $currency)
                $price = \CCurrencyRates::ConvertCurrency($price, $currency, $baseCurrency);

            $course = Parameters::getInstance()->getInt('bonus_course');
            $bonus = $price / $course;
        }

        return (int)$bonus;
    }

    /**
     * @param $productId
     * @param $iblockId
     * @return int|null
     */
    protected static function getBonusProperty($productId, $iblockId)
    {
        $propertyCode = Parameters::getInstance()->getText('bonus_property');

        if ($iblockId > 0 && !empty($propertyCode))
        {
            $result = \CIBlockElement::GetProperty($iblockId, $productId, ["sort" => "asc"], ["CODE" => $propertyCode]);
            if ($prop = $result->Fetch())
                return !empty($prop["VALUE"]) ? (int)$prop["VALUE"] : null;
        }

        return null;
    }

    /**
     * @param $orderId
     * @param $status
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public static function onSalePayOrderHandler($orderId, $status)
    {
        if (!Parameters::getInstance()->getBool('bonus_enabled'))
            return;

        $order = Order::load($orderId);
        $basket = $order->getBasket();
        $bonusCurrency = Parameters::getInstance()->getText('bonus_currency');
        $sumBonus = 0;

        /** @var BasketItem $basketItem */
        foreach ($basket->getBasketItems() as $basketItem)
        {
            $sumBonus += self::getProductBonus($basketItem->getProductId(), $basketItem->getBasePrice(), $basketItem->getCurrency());
        }

        if ($sumBonus > 0)
        {
            if ($status == 'Y' && !$order->isCanceled())
            {
                $info = Loc::getMessage('USER_ACCOUNT_INFO_DEBIT', ['#ORDER_ID#' => $orderId]);
                \CSaleUserAccount::UpdateAccount($order->getUserId(), $sumBonus, $bonusCurrency, $info, $orderId);
            }
            elseif ($status == 'N' && $orderId > 0)
            {
                $info = Loc::getMessage('USER_ACCOUNT_INFO_CREDIT', ['#ORDER_ID#' => $orderId]);
                \CSaleUserAccount::UpdateAccount($order->getUserId(), -$sumBonus, $bonusCurrency, $info, $orderId);
            }
        }
    }

    /**
     * @param $orderId
     * @param $status
     * @param $info
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public static function onSaleCancelOrderHandler($orderId, $status, $info)
    {
        if (!Parameters::getInstance()->getBool('bonus_enabled'))
            return;

        $order = Order::load($orderId);

        if ($order->isPaid())
        {
            $sumBonus = 0;
            $basket = $order->getBasket();

            /** @var BasketItem $basketItem */
            foreach ($basket->getBasketItems() as $basketItem)
            {
                $sumBonus += self::getProductBonus($basketItem->getProductId(), $basketItem->getBasePrice(), $basketItem->getCurrency());
            }

            if ($status === "Y")
            {
                $sumBonus = -$sumBonus;
                $info = Loc::getMessage('USER_ACCOUNT_INFO_CREDIT_CANCEL', ['#ORDER_ID#' => $orderId]);
            }
            elseif ($status === "N")
            {
                $info = Loc::getMessage('USER_ACCOUNT_INFO_DEBIT_CANCEL', ['#ORDER_ID#' => $orderId]);
            }

            if ($orderId > 0 && $order->getUserId() > 0)
                \CSaleUserAccount::UpdateAccount(
                    $order->getUserId(),
                    $sumBonus,
                    $order->getCurrency(),
                    $info,
                    $orderId
                );
        }
    }
}