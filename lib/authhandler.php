<?php
namespace Artixgroup\Shop;

use Bitrix\Main\Error;
use Bitrix\Main\EventResult;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;
use Gazmyas\Partners\SxGeo;

Loc::loadMessages(__FILE__);

class AuthHandler
{
    /**
     * @param $fields
     */
    public static function onBeforeUserRegisterHandler(&$fields)
    {
        $fields['LOGIN'] = $fields['EMAIL'];
    }

    /**
     * @param $fields
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function onBeforeUserLoginHandler(&$fields)
    {
        if (filter_var($fields['LOGIN'], FILTER_VALIDATE_EMAIL))
        {
            $user = UserTable::getRow(['filter' => ['=LOGIN' => $fields['LOGIN']]]);

            if (!$user)
            {
                $user = UserTable::getRow(['filter' => ['=EMAIL' => $fields['LOGIN']]]);

                if ($user)
                {
                    $fields['LOGIN'] = $user['LOGIN'];
                }
            }
        }
    }
}