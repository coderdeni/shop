<?php
namespace Artixgroup\Shop;

use Bitrix\Main;
use Bitrix\Main\ORM;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Fields\Relations\OneToMany;

Loc::loadMessages(__FILE__);

/**
 * Class FormTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ACTIVE bool optional default 'Y'
 * <li> NAME array
 * <li> SHOW_MODE enum ('S', 'A', 'P') optional default 'S'
 * <li> CAPTCHA bool optional default 'N'
 * <li> AUTOLOAD bool optional default 'N'
 * <li> AUTOLOAD_TIMER int
 * <li> HANDLER string optional
 * <li> RESULT_MESSAGE string mandatory
 * <li> DESCRIPTION string optional
 * <li> CHECK_AGREEMENT bool optional default 'N'
 * <li> MESSAGE_AGREEMENT string optional
 * <li> JS_EVENT_HANDLERS string optional
 * <li> EVENT_NAME string optional
 * <li> EVENT_MESSAGE_ID int optional
 * <li> LANGUAGE_ID string optional
 * <li> FIELDS reference to {@link FormFieldTable}
 * <li> FORM_SITES reference to {@link FormSiteTable}
 * </ul>
 *
 * @package Artixgroup\Shop
 **/

class FormTable extends Main\Entity\DataManager
{
    const SHOW_MODE_SIMPLE = 'S';
    const SHOW_MODE_AJAX = 'A';
    const SHOW_MODE_POPUP = 'P';

    const CAPTCHA_NONE = 'N';
    const CAPTCHA_BITRIX = 'B';
    const CAPTCHA_GOOGLE_V2 = 'G';
    const CAPTCHA_GOOGLE_V3 = 'R';

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'artixgroup_form';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     * @throws Main\ArgumentException
     * @throws Main\SystemException
     */
    public static function getMap()
    {
        return array(
            new ORM\Fields\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_ID'),
            ]),
            new ORM\Fields\BooleanField('ACTIVE', array(
                'values' => array('N', 'Y'),
                'default_value' => 'Y',
                'title' => Loc::getMessage('ELEMENT_ENTITY_ACTIVE'),
            )),
            new ORM\Fields\StringField('NAME', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_NAME'),
            ]),
            new ORM\Fields\EnumField('SHOW_MODE', [
                'values' => array(
                    self::SHOW_MODE_SIMPLE,
                    self::SHOW_MODE_AJAX,
                    self::SHOW_MODE_POPUP
                ),
                'default_value' => self::SHOW_MODE_SIMPLE,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_SHOW_MODE'),
            ]),
            new ORM\Fields\BooleanField('CAPTCHA', array(
                'values' => array(
                    self::CAPTCHA_NONE,
                    self::CAPTCHA_BITRIX,
                    self::CAPTCHA_GOOGLE_V2,
                    self::CAPTCHA_GOOGLE_V3
                ),
                'default_value' => self::CAPTCHA_NONE,
                'title' => Loc::getMessage('ELEMENT_ENTITY_CAPTCHA'),
            )),
            new ORM\Fields\BooleanField('AUTOLOAD', array(
                'values' => array('N', 'Y'),
                'default_value' => 'N',
                'title' => Loc::getMessage('ELEMENT_ENTITY_AUTOLOAD'),
            )),
            new ORM\Fields\IntegerField('AUTOLOAD_TIMER', array(
                'default_value' => 0,
                'title' => Loc::getMessage('ELEMENT_ENTITY_AUTOLOAD_TIMER'),
            )),
            new ORM\Fields\TextField('PARAMETERS', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_PARAMETERS'),
                'save_data_modification' => function () {
                    return [function ($value) {
                        return json_encode($value);
                    }];
                },
                'fetch_data_modification' => function () {
                    return [function ($value) {
                        return json_decode($value, true);
                    }];
                },
            ]),
            new ORM\Fields\TextField('RESULT_MESSAGE', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_RESULT_MESSAGE'),
            ]),
            new ORM\Fields\TextField('DESCRIPTION', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_DESCRIPTION'),
            ]),
            new ORM\Fields\BooleanField('CHECK_AGREEMENT', array(
                'values' => array('N', 'Y'),
                'default_value' => 'Y',
                'title' => Loc::getMessage('ELEMENT_ENTITY_CHECK_AGREEMENT'),
            )),
            new ORM\Fields\TextField('MESSAGE_AGREEMENT', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_MESSAGE_AGREEMENT'),
            ]),
            new ORM\Fields\TextField('JS_EVENT_HANDLERS', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_JS_EVENT_HANDLERS'),
            ]),
            new ORM\Fields\StringField('EVENT_NAME', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_EVENT_NAME'),
            ]),
            new ORM\Fields\IntegerField('EVENT_MESSAGE_ID', [
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_EVENT_MESSAGE_ID'),
            ]),
            new ORM\Fields\StringField('LANGUAGE_ID', [
                'required' => true,
                'title' => Loc::getMessage('ARTIXGROUP_ENTITY_EVENT_LANGUAGE_ID'),
            ]),
            (new OneToMany('FIELDS', FormFieldTable::class, 'FORM'))
                ->configureJoinType('inner'),
            (new OneToMany('FORM_SITES', FormSiteTable::class, 'FORM'))
                ->configureJoinType('inner')
        );
    }
}