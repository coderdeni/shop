<?php
/**
 * @var array $arResult
 */

if ($arResult)
{
    ?>
    <div class="catalogItem">
        <div class="catalogItem__container">
            <a href="<?= $arResult['DETAIL_PAGE_URL']?>" class="catalogItem__content">
                <div class="catalogItem__wrapper">
                    <div class="catalogItem__photo"><img src="<?= $arResult['PREVIEW_PICTURE']['SRC']?>" alt="" class="catalogItem__img"></div>
                    <span class="catalogItem__title"><?= $arResult['NAME']?></span>
                </div>
            </a>
        </div>
    </div>
    <?
}
