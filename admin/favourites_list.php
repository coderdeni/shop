<?
/** @global CMain $APPLICATION */

use Bitrix\Main,
	Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\UserTable,
    Artixgroup\Shop\FavouriteTable;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');

$publicMode = $adminPage->publicMode;
$selfFolderUrl = $adminPage->getSelfFolderUrl();

$saleModulePermissions = $APPLICATION->GetGroupRight('dk.tag');
$readOnly = ($saleModulePermissions < 'W');
if ($saleModulePermissions < 'R')
	$APPLICATION->AuthForm('');

Loader::includeModule('artixgroup.shop');
Loader::includeModule('iblock');
Loc::loadMessages(__FILE__);

$catalogNamePostfix = ' (' . Loc::getMessage('ARTIXGROUP_FAVOURITES_DISCOUNT_LIST_MESS_TITLE_CATALOG_ID') . ')';
$catalogNamePostfixLength = strlen($catalogNamePostfix);

$adminListTableID = 'tbl_artixgroup_favourites';

$adminSort = new CAdminSorting($adminListTableID, 'ID', 'ASC');
$adminList = new CAdminUiList($adminListTableID, $adminSort);


$filterFields = [
	[
		"id" => "ID",
		"name" => "ID",
		"type" => "number",
		"filterable" => "=",
		"default" => true
	],
	[
		"id" => "USER_ID",
		"name" => GetMessage("ARTIXGROUP_FAVOURITES_LIST_FILTER_NAME"),
        "filterable" => "="
    ]
];

$filter = [];

$adminList->AddFilter($filterFields, $filter);


if (!$readOnly && ($listID = $adminList->GroupAction()))
{
	if ($_REQUEST['action_target'] == 'selected')
	{
		$listID = [];
		$tagIterator = FavouriteTable::getList([
			'select' => ['ID'],
			'filter' => $filter
        ]);
		while ($tag = $tagIterator->fetch())
			$listID[] = $tag['ID'];
	}

	$listID = array_filter($listID);
	if (!empty($listID))
	{
		switch ($_REQUEST['action'])
		{
			case 'delete':
				foreach ($listID as &$tagID)
				{
					$result = TagTable::delete($tagID);
					if (!$result->isSuccess())
						$adminList->AddGroupError(implode('<br>', $result->getErrorMessages()), $tagID);
					unset($result);
				}
				unset($tagID);
				break;
		}
	}
	unset($listID);

	if ($adminList->hasGroupErrors())
	{
		$adminSidePanelHelper->sendJsonErrorResponse($adminList->getGroupErrors());
	}
	else
	{
		$adminSidePanelHelper->sendSuccessResponse();
	}
}

$headerList = [];
$headerList['ID'] = [
	'id' => 'ID',
	'content' => 'ID',
	'title' => '',
	'sort' => 'ID',
	'default' => true
];
$headerList['USER_ID'] = [
	'id' => 'USER_ID',
	'content' => Loc::getMessage('ARTIXGROUP_FAVOURITES_ADM_TITLE_USER_ID'),
	'title' => Loc::getMessage('ARTIXGROUP_FAVOURITES_ADM_DSC_HEADER_TITLE_USER_ID'),
	'default' => true
];

$adminList->AddHeaders($headerList);

$selectFields = array_fill_keys($adminList->GetVisibleHeaderColumns(), true);
$selectFields['ID'] = true;
$selectFieldsMap = array_fill_keys(array_keys($headerList), false);

global $by, $order;
if (!isset($by))
	$by = 'ID';
if (!isset($order))
	$order = 'ASC';

$usePageNavigation = true;
$navyParams = [];
if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'excel')
{
	$usePageNavigation = false;
}
else
{
	$navyParams = CDBResult::GetNavParams(CAdminUiResult::GetNavSize($adminListTableID));
	if ($navyParams['SHOW_ALL'])
	{
		$usePageNavigation = false;
	}
	else
	{
		$navyParams['PAGEN'] = (int)$navyParams['PAGEN'];
		$navyParams['SIZEN'] = (int)$navyParams['SIZEN'];
	}
}
$getListParams = [
	'select' => array_keys($selectFields),
	'filter' => $filter,
	'order' => [$by => $order]
];
if ($usePageNavigation)
{
	$getListParams['limit'] = $navyParams['SIZEN'];
	$getListParams['offset'] = $navyParams['SIZEN']*($navyParams['PAGEN']-1);
}
$totalCount = 0;
$totalPages = 0;
if ($usePageNavigation)
{
	$totalCount = FavouriteTable::getCount($getListParams['filter']);
	if ($totalCount > 0)
	{
		$totalPages = ceil($totalCount/$navyParams['SIZEN']);
		if ($navyParams['PAGEN'] > $totalPages)
			$navyParams['PAGEN'] = $totalPages;
	}
	else
	{
		$navyParams['PAGEN'] = 1;
	}
	$getListParams['limit'] = $navyParams['SIZEN'];
	$getListParams['offset'] = $navyParams['SIZEN']*($navyParams['PAGEN']-1);
}

$favouriteIterator = new CAdminUiResult(FavouriteTable::getList($getListParams), $adminListTableID);
if ($usePageNavigation)
{
    $favouriteIterator->NavStart($getListParams['limit'], $navyParams['SHOW_ALL'], $navyParams['PAGEN']);
    $favouriteIterator->NavRecordCount = $totalCount;
    $favouriteIterator->NavPageCount = $totalPages;
    $favouriteIterator->NavPageNomer = $navyParams['PAGEN'];
}
else
{
    $favouriteIterator->NavStart();
}
$adminList->SetNavigationParams($favouriteIterator, ["BASE_LINK" => $selfFolderUrl."artixgroup_favourites_edit.php"]);

$userList = [];
$arUserID = [];
$nameFormat = CSite::GetNameFormat(true);

$arRows = [];
while ($favourite = $favouriteIterator->Fetch())
{
    $favourite['ID'] = (int)$favourite['ID'];

	$urlEdit = $selfFolderUrl.'artixgroup_favourites_edit.php?ID='.$favourite['ID'].'&lang='.LANGUAGE_ID;
	$urlEdit = $adminSidePanelHelper->editUrlToPublicPage($urlEdit);

	$arRows[$favourite['ID']] = $row = &$adminList->AddRow(
        $favourite['ID'],
        $favourite,
		$urlEdit,
		Loc::getMessage('ARTIXGROUP_FAVOURITES_DISCOUNT_LIST_MESS_EDIT_DISCOUNT')
	);
	$row->AddViewField('ID', '<a href="'.$urlEdit.'">'.$favourite['ID'].'</a>');

	if ($arRows[$favourite['USER_ID']])
    {
        $user = UserTable::getById((int)$favourite['USER_ID'])->fetch();
        $row->AddViewField('USER_ID', '[<a href="'.$urlEdit.'">'.$favourite["ID"].'</a>] '.$user['LAST_NAME'].' '.$user['NAME'].' '.$user['SECOND_NAME']);
    }

	$arActions = [];
	$arActions[] = [
		'ICON' => 'edit',
		'TEXT' => Loc::getMessage('ARTIXGROUP_FAVOURITES_EDIT_LINK'),
		'LINK' => $urlEdit,
		'DEFAULT' => true
    ];
	if (!$readOnly)
	{
		$arActions[] = [
			'ICON' => 'delete',
			'TEXT' => Loc::getMessage('ARTIXGROUP_FAVOURITES_DELETE_LINK'),
			'ACTION' => "if(confirm('".Loc::getMessage('ARTIXGROUP_FAVOURITES_CONFIRM_DELETE')."')) ".$adminList->ActionDoGroup($tag['ID'], 'delete'),
			'DEFAULT' => false,
        ];
	}

	$row->AddActions($arActions);
}
if (isset($row))
	unset($row);

$adminList->AddGroupActionTable([
	"delete" => true,
]);

$adminList->CheckListMode();
$APPLICATION->SetTitle(Loc::getMessage('ARTIXGROUP_FAVOURITES_LIST_MESS_TITLE'));
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

$adminList->DisplayFilter($filterFields);
$adminList->DisplayList();

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');