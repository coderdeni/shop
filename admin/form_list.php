<?
/** @global CMain $APPLICATION */

use Artixgroup\Shop\FormTable;
use Bitrix\Main,
	Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\UserTable,
    Artixgroup\Favourites\FavouriteTable;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');

$publicMode = $adminPage->publicMode;
$selfFolderUrl = $adminPage->getSelfFolderUrl();

$saleModulePermissions = $APPLICATION->GetGroupRight('dk.tag');
$readOnly = ($saleModulePermissions < 'W');
if ($saleModulePermissions < 'R')
	$APPLICATION->AuthForm('');

Loader::includeModule('artixgroup.shop');
Loader::includeModule('iblock');
Loc::loadMessages(__FILE__);

$catalogNamePostfix = ' (' . Loc::getMessage('ARTIXGROUP_FAVOURITES_DISCOUNT_LIST_MESS_TITLE_CATALOG_ID') . ')';
$catalogNamePostfixLength = strlen($catalogNamePostfix);

$adminListTableID = 'tbl_artixgroup_form';

$adminSort = new CAdminSorting($adminListTableID, 'ID', 'ASC');
$adminList = new CAdminUiList($adminListTableID, $adminSort);


$filterFields = [
	[
		"id" => "ID",
		"name" => "ID",
		"type" => "number",
		"filterable" => "=",
		"default" => true
	],
	[
		"id" => "NAME",
		"name" => GetMessage("ARTIXGROUP_FAVOURITES_LIST_FILTER_NAME"),
        "filterable" => "="
    ]
];

$filter = [];

$adminList->AddFilter($filterFields, $filter);

if (!$readOnly && ($listID = $adminList->GroupAction()))
{
	if ($_REQUEST['action_target'] == 'selected')
	{
		$listID = [];
		$tagIterator = FavouriteTable::getList([
			'select' => ['ID'],
			'filter' => $filter
        ]);
		while ($tag = $tagIterator->fetch())
			$listID[] = $tag['ID'];
	}

	$listID = array_filter($listID);
	if (!empty($listID))
	{
		switch ($_REQUEST['action'])
		{
			case 'delete':
				foreach ($listID as &$tagID)
				{
					$result = TagTable::delete($tagID);
					if (!$result->isSuccess())
						$adminList->AddGroupError(implode('<br>', $result->getErrorMessages()), $tagID);
					unset($result);
				}
				unset($tagID);
				break;
		}
	}
	unset($listID);

	if ($adminList->hasGroupErrors())
	{
		$adminSidePanelHelper->sendJsonErrorResponse($adminList->getGroupErrors());
	}
	else
	{
		$adminSidePanelHelper->sendSuccessResponse();
	}
}

$headerList = [];
$headerList['ID'] = [
	'id' => 'ID',
	'content' => 'ID',
	'title' => '',
	'sort' => 'ID',
	'default' => true
];
$headerList['NAME'] = [
	'id' => 'NAME',
	'content' => Loc::getMessage('ARTIXGROUP_FAVOURITES_ADM_TITLE_NAME'),
	'title' => Loc::getMessage('ARTIXGROUP_FAVOURITES_ADM_DSC_HEADER_TITLE_NAME'),
	'default' => true
];

$adminList->AddHeaders($headerList);

$selectFields = array_fill_keys($adminList->GetVisibleHeaderColumns(), true);
$selectFields['ID'] = true;
$selectFieldsMap = array_fill_keys(array_keys($headerList), false);

global $by, $order;
if (!isset($by))
	$by = 'ID';
if (!isset($order))
	$order = 'ASC';

$usePageNavigation = true;
$navyParams = [];
if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'excel')
{
	$usePageNavigation = false;
}
else
{
	$navyParams = CDBResult::GetNavParams(CAdminUiResult::GetNavSize($adminListTableID));
	if ($navyParams['SHOW_ALL'])
	{
		$usePageNavigation = false;
	}
	else
	{
		$navyParams['PAGEN'] = (int)$navyParams['PAGEN'];
		$navyParams['SIZEN'] = (int)$navyParams['SIZEN'];
	}
}
$getListParams = [
	'select' => array_keys($selectFields),
	'filter' => $filter,
	'order' => [$by => $order]
];
if ($usePageNavigation)
{
	$getListParams['limit'] = $navyParams['SIZEN'];
	$getListParams['offset'] = $navyParams['SIZEN']*($navyParams['PAGEN']-1);
}
$totalCount = 0;
$totalPages = 0;
if ($usePageNavigation)
{
	$totalCount = FormTable::getCount($getListParams['filter']);
	if ($totalCount > 0)
	{
		$totalPages = ceil($totalCount/$navyParams['SIZEN']);
		if ($navyParams['PAGEN'] > $totalPages)
			$navyParams['PAGEN'] = $totalPages;
	}
	else
	{
		$navyParams['PAGEN'] = 1;
	}
	$getListParams['limit'] = $navyParams['SIZEN'];
	$getListParams['offset'] = $navyParams['SIZEN']*($navyParams['PAGEN']-1);
}

$formDataIterator = new CAdminUiResult(FormTable::getList($getListParams), $adminListTableID);
if ($usePageNavigation)
{
    $formDataIterator->NavStart($getListParams['limit'], $navyParams['SHOW_ALL'], $navyParams['PAGEN']);
    $formDataIterator->NavRecordCount = $totalCount;
    $formDataIterator->NavPageCount = $totalPages;
    $formDataIterator->NavPageNomer = $navyParams['PAGEN'];
}
else
{
    $formDataIterator->NavStart();
}
$adminList->SetNavigationParams($formDataIterator, ["BASE_LINK" => $selfFolderUrl."artixgroup_favourites_edit.php"]);

$userList = [];
$arUserID = [];
$nameFormat = CSite::GetNameFormat(true);

$arRows = [];
while ($formData = $formDataIterator->Fetch())
{
    $formData['ID'] = (int)$formData['ID'];

	$urlEdit = $selfFolderUrl.'artixgroup_form_edit.php?ID='.$formData['ID'].'&lang='.LANGUAGE_ID;
	$urlEdit = $adminSidePanelHelper->editUrlToPublicPage($urlEdit);

	$arRows[$formData['ID']] = $row = &$adminList->AddRow(
        $formData['ID'],
        $formData,
		$urlEdit,
		Loc::getMessage('ARTIXGROUP_FAVOURITES_DISCOUNT_LIST_MESS_EDIT_DISCOUNT')
	);
	$row->AddViewField('ID', '<a href="'.$urlEdit.'">'.$formData['ID'].'</a>');

	if ($arRows[$formData['NAME']])
    {
        $row->AddViewField('NAME', $formData['NAME']);
    }

	$arActions = [];
	$arActions[] = [
		'ICON' => 'edit',
		'TEXT' => Loc::getMessage('ARTIXGROUP_FAVOURITES_EDIT_LINK'),
		'LINK' => $urlEdit,
		'DEFAULT' => true
    ];
	if (!$readOnly)
	{
		$arActions[] = [
			'ICON' => 'delete',
			'TEXT' => Loc::getMessage('ARTIXGROUP_FAVOURITES_DELETE_LINK'),
			'ACTION' => "if(confirm('".Loc::getMessage('ARTIXGROUP_FAVOURITES_CONFIRM_DELETE')."')) ".$adminList->ActionDoGroup($tag['ID'], 'delete'),
			'DEFAULT' => false,
        ];
	}

	$row->AddActions($arActions);
}
if (isset($row))
	unset($row);

$adminList->AddGroupActionTable([
	"delete" => true,
]);

$adminList->AddAdminContextMenu([
    [
        "TEXT"	=> Loc::getMessage("ARTIXGROUP_FAVOURITES_ADD_FORM"),
        "LINK"	=> "artixgroup_form_edit.php?lang=".LANGUAGE_ID,
        "TITLE"	=> Loc::getMessage("ARTIXGROUP_FAVOURITES_ADD_FORM"),
        "ICON"	=> "btn_new"
    ]
]);

$adminList->CheckListMode();
$APPLICATION->SetTitle(Loc::getMessage('ARTIXGROUP_FAVOURITES_LIST_MESS_TITLE'));
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

$adminList->DisplayFilter($filterFields);
$adminList->DisplayList();

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');