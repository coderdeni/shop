<?php

use Artixgroup\Shop\Form\FieldBase;
use Artixgroup\Shop\FormFieldTable;
use Bitrix\Main\Loader;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/admin_tools.php");

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$action = $request->get('action');
$parameters = $request->getPostList()->toArray();
$type = $request->getPost('TYPE') ? $request->getPost('TYPE') : $request->get('type');

if (Loader::includeModule('artixgroup.shop') && $action && $type)
{
    $className = FormFieldTable::getFieldByType($type);
    /** @var FieldBase $field */
    $field = new $className($parameters ? $parameters : [], (int)$request->get('key'));

    if ($action === 'new')
    {
        ?>
        <div class="form__input" draggable="true" ondragstart="event.dataTransfer.setData('text/plain',null)">
            <div class="form__input-drag"></div>
            <div class="form__input-container"><?= $field->showAdminHtmlInput()?></div>
            <div class="form__input-setting">
                <a class="form__input-setting-link" href="javascript:void(0)">
                    <span class="fa fa-pencil"></span>
                </a>
            </div>
            <div class="form__input-delete">
                <a class="form__input-delete-link" href="javascript:void(0)">
                    <span class="fa fa-remove"></span>
                </a>
            </div>
            <textarea class="form__input-parameters" name="FIELDS[<?= $field->getKey()?>]"><?= $field->getJson()?></textarea>
        </div>
        <div class="form__sort-drag-helper"></div>
        <?
    }
    elseif ($action === 'parameters')
    {
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
        ?>
        <form method="POST" action="/bitrix/admin/artixgroup_form_input.php?action=save" enctype="multipart/form-data">
            <input type="hidden" name="TYPE">
            <table width="100%" id="edit-table">
                <?
                $field->showHtmlBaseParameters();
                $field->showHtmlParameters();
                ?>
            </table>
            <script>
                (function() {
                    BX.adminFormTools.modifyFormElements('edit-table');
                })();
            </script>
        </form>
        <?
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
        die();
    }
    elseif ($action === 'save')
    {
        ?>
        <script type="text/javascript">
            var currentWindow = top.window;
            if (currentWindow.formControl)
                currentWindow.formControl.setParameters(<?= $field->getJson()?>);
            currentWindow.BX.closeWait();
            currentWindow.BX.WindowManager.Get().AllowClose();
            currentWindow.BX.WindowManager.Get().Close();
        </script>
        <?
    }
}

require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog.php");
