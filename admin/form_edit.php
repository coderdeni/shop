<?php

use Artixgroup\Shop\Form\FieldBase;
use Artixgroup\Shop\Form\Interfaces\INotDeletable;
use Artixgroup\Shop\FormFieldTable;
use Artixgroup\Shop\FormSiteTable;
use Artixgroup\Shop\FormTable;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Mail\Internal\EventMessageSiteTable;
use Bitrix\Main\Mail\Internal\EventMessageTable;
use Bitrix\Main\Mail\Internal\EventTypeTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/admin_tools.php");

$blogModulePermissions = $APPLICATION->GetGroupRight("artixgroup.shop");
if ($blogModulePermissions < "R")
{
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

Loc::loadMessages(__FILE__);
Loader::includeModule('artixgroup.shop');
Loader::includeModule('iblock');

\Bitrix\Main\Page\Asset::getInstance()->addCss('/bitrix/css/artixgroup.shop/form_control.css');
\Bitrix\Main\Page\Asset::getInstance()->addCss('/bitrix/css/main/font-awesome.css');
\Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/artixgroup.shop/form_control.js');
\Bitrix\Main\UI\Extension::load("ui.buttons");
\Bitrix\Main\UI\Extension::load("ui.buttons.icons");
?>
<link rel="stylesheet" type="text/css" href="/bitrix/css/artixgroup.shop/form_control.css">
<link rel="stylesheet" type="text/css" href="/bitrix/css/main/font-awesome.css">
<?
$errorMessage = "";
$bVarsFromForm = false;

$ID = (int)$ID;

$form = [
    'RESULT_MESSAGE' => Loc::getMessage("ARTIXGROUP_FORM_RESULT_MESSAGE"),
    'MESSAGE_AGREEMENT' => Loc::getMessage("ARTIXGROUP_FORM_MESSAGE_AGREEMENT_VALUE"),
    'PARAMETERS' => [
        'FORM_CSS_PREFIX' => 'form',
        'BUTTON_TEXT' => Loc::getMessage("ARTIXGROUP_FORM_BUTTON_TEXT"),
        'BUTTON_CSS' => 'button',
    ]
];

$formFields = [
    [
        'ID' => '',
        'COL' => 1,
        'ROW' => 0,
        'SORT' => 1,
        'NAME' => 'Отправить',
        'TYPE' => FormFieldTable::TYPE_SUBMIT,
    ],
    [
        'ID' => '',
        'COL' => 1,
        'ROW' => 1,
        'SORT' => 2,
        'NAME' => Loc::getMessage("ARTIXGROUP_FORM_MESSAGE_AGREEMENT_VALUE"),
        'TYPE' => FormFieldTable::TYPE_FLAG,
    ]
];

$formSites = [];
$eventType = null;

if ($ID > 0)
{
    $form = FormTable::getByPrimary($ID, [
        'select' => ['*']
    ])->fetch();

    $iterator = FormSiteTable::getList(['filter' => ['FORM_ID' => $ID]]);
    while ($formSite = $iterator->fetch())
        $formSites[] = $formSite['SITE_ID'];

    $formFields = FormFieldTable::getList([
        'select' => ['*'],
        'filter' => ['FORM_ID' => $ID],
        'order' => ['COL' => 'ASC', 'ROW' => 'ASC', 'SORT' => 'ASC']
    ])->fetchAll();

    if (!empty($form['EVENT_NAME']))
    {
        $eventType = EventTypeTable::getList([
            'filter' => ['EVENT_NAME' => $form['EVENT_NAME']]
        ])->fetch();
    }
}


$aTabs = [
    [
        "DIV" => "edit1",
        "TAB" => Loc::getMessage("ARTIXGROUP_FAVOURITES_TAB_COMMON"),
        "ICON" => "blog",
        "TITLE" => Loc::getMessage("ARTIXGROUP_FAVOURITES_TAB_COMMON_TITLE")
    ],
];
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$errors = [];
$fieldsErrors = [];
$request = Application::getInstance()->getContext()->getRequest();

if ($request->isPost() && check_bitrix_sessid() && ($request->getPost('save') || $request->getPost('apply')))
{
    Application::getConnection()->startTransaction();

    $lids = $request->getPost('LID');
    $postFields = $request->getPost('FIELDS');

    foreach ($postFields as $key => $postField)
    {
        $postFields[$key] = json_decode($postField, true);
    }

    usort($postFields, function ($a, $b) {
        if ($a['SORT'] === $b['SORT'])
            return 0;
        return ($a['SORT'] < $b['SORT']) ? -1 : 1;
    });

    $data = [
        'NAME' => $request->getPost('NAME'),
        'ACTIVE' => $request->getPost('ACTIVE') ? $request->getPost('ACTIVE') : 'N',
        'SHOW_MODE' => $request->getPost('SHOW_MODE'),
        'CAPTCHA' => $request->getPost('CAPTCHA') ? $request->getPost('CAPTCHA') : FormTable::CAPTCHA_NONE,
        'AUTOLOAD' => $request->getPost('AUTOLOAD') ? $request->getPost('AUTOLOAD') : 'N',
        'AUTOLOAD_TIMER' => $request->getPost('AUTOLOAD_TIMER'),
        'PARAMETERS' => $request->getPost('PARAMETERS'),
        'RESULT_MESSAGE' => $request->getPost('RESULT_MESSAGE'),
        'CHECK_AGREEMENT' => $request->getPost('CHECK_AGREEMENT') ? $request->getPost('CHECK_AGREEMENT') : 'N',
        'MESSAGE_AGREEMENT' => $request->getPost('MESSAGE_AGREEMENT'),
        'DESCRIPTION' => $request->getPost('DESCRIPTION'),
        'EVENT_NAME' => $request->getPost('EVENT_NAME'),
        'EVENT_MESSAGE_ID' => (int)$request->getPost('EVENT_MESSAGE_ID'),
        'LANGUAGE_ID' => $request->getPost('LANGUAGE_ID'),
    ];

    if (empty($lids))
    {
        $errors[] = Loc::getMessage("ARTIXGROUP_FAVOURITES_ERROR_LID");
    }

    if (empty($errors))
    {
        $result = ($ID > 0)
            ? FormTable::update($ID, $data)
            : FormTable::add($data);

        if ($result->isSuccess())
        {
            if (!empty($formSites))
            {
                foreach ($formSites as $formSite)
                {
                    if (in_array($formSite, $lids))
                        continue;

                    $formSiteResult = FormSiteTable::delete(['FORM_ID' => $result->getId(), 'SITE_ID' => $formSite]);
                    if (!$formSiteResult->isSuccess())
                    {
                        Application::getConnection()->rollbackTransaction();
                        $errors = $formSiteResult->getErrorMessages();
                        break;
                    }
                }
            }

            foreach ($lids as $lid)
            {
                if (in_array($lid, $formSites))
                    continue;

                $formSiteResult = FormSiteTable::add(['FORM_ID' => $result->getId(), 'SITE_ID' => $lid]);

                if (!$formSiteResult->isSuccess())
                {
                    Application::getConnection()->rollbackTransaction();
                    $errors = $formSiteResult->getErrorMessages();
                    break;
                }
            }

            if (empty($errors))
            {
                $deleteIds = [];

                foreach ($formFields as $key => $formField)
                {
                    $found = false;

                    foreach ($postFields as $postField)
                    {
                        if (isset($postField['ID']) && $postField['ID'] === $formField['ID'])
                        {
                            $found = true;
                        }
                    }

                    if (!$found)
                    {
                        $deleteIds[] = $formField['ID'];
                    }
                }

                if (count($deleteIds) > 0)
                {
                    foreach ($deleteIds as $fieldId)
                    {
                        $deleteResult = FormFieldTable::delete($fieldId);
                        if (!$deleteResult->isSuccess())
                        {
                            $errors = $deleteResult->getErrorMessages();
                            Application::getConnection()->rollbackTransaction();
                            break;
                        }
                    }
                }
            }

            if (empty($errors))
            {
                foreach ($postFields as $key => $field)
                {
                    $field['FORM_ID'] = $result->getId();
                    $fieldResult = isset($field['ID']) && $field['ID'] > 0 ? FormFieldTable::update($field['ID'], $field) : FormFieldTable::add($field);

                    if (!$fieldResult->isSuccess())
                    {
                        $fieldsErrors[$key] = $fieldResult->getErrorMessages();
                        $errors = $fieldResult->getErrorMessages();
                        Application::getConnection()->rollbackTransaction();
                        break;
                    }
                    else
                    {
                        $postFields[$key]['ID'] = $fieldResult->getId();
                    }
                }
            }

            if (empty($errors))
            {
                if ($request->getPost('ADD_EMAIL_EVENT_TYPE') === 'Y')
                {
                    $eventDescription = '';
                    $eventName = !empty($data['EVENT_NAME'])
                        ? $data['EVENT_NAME']
                        : 'ARTIXGROUP_NEW_REQUEST_FORM_'.$result->getId();

                    foreach ($postFields as $field)
                    {
                        $eventDescription .= "#FIELD_{$field['ID']}# - ".$field['NAME']."\n";
                    }

                    $eventDescription .= "#REQUEST_SITE_URL# - Адрес страницы с каторой была отправлена форма\n";
                    $eventDescription .= "#ADMIN_URL# - Адрес в административной панели\n";

                    $type = [
                        "SORT" => 150,
                        "NAME" => $request->get('NAME'),
                        "DESCRIPTION" => $eventDescription,
                        "LID" => $data['LANGUAGE_ID'],
                        "EVENT_NAME" => $eventName,
                        "EVENT_TYPE" => EventTypeTable::TYPE_EMAIL,
                    ];

                    $eventTypeResult = !empty($eventType)
                        ? EventTypeTable::update($eventType['ID'], $type)
                        : EventTypeTable::add($type);

                    if ($eventTypeResult->isSuccess())
                    {
                        $hasEventMessage = $data['EVENT_MESSAGE_ID'] > 0;

                        if (!$hasEventMessage || $request->getPost('UPDATE_TEMPLATE_EVENT_TYPE') === 'Y')
                        {
                            $message = "Информационное сообщение сайта #SITE_NAME#\n";
                            $message .= "------------------------------------------\n";
                            $message .= "\n";

                            foreach ($postFields as $field)
                            {
                                $message .= $field['NAME'].": #FIELD_{$field['ID']}#\n";
                            }

                            $message .= "Адрес страницы формы: #REQUEST_SITE_URL#\n";
                            $message .= "Посмотреть в административной панели: #ADMIN_URL#\n";
                            $message .= "\n";
                            $message .= "------------------------------------------\n";
                            $message .= "Сообщение сгенерировано автоматически.";

                            $eventMessageData = [
                                "ACTIVE" => 'Y',
                                "EVENT_NAME" => $eventName,
                                "LID" => $lids[0],
                                "EMAIL_FROM" => '#DEFAULT_EMAIL_FROM#',
                                "EMAIL_TO" => '#DEFAULT_EMAIL_FROM#',
                                "SUBJECT" => '#SITE_NAME#: Заполнена форма - '.$data['NAME'],
                                "MESSAGE" => $message,
                                "BODY_TYPE"	=> 'text',
                                "LANGUAGE_ID" => $data['LANGUAGE_ID'],
                            ];

                            $eventMessageResult = !$hasEventMessage
                                ? EventMessageTable::add($eventMessageData)
                                : EventMessageTable::update($data['EVENT_MESSAGE_ID'], $eventMessageData);

                            if ($eventMessageResult->isSuccess())
                            {
                                $updateResult = FormTable::update($result->getId(), [
                                    'EVENT_NAME' => $eventName,
                                    'EVENT_MESSAGE_ID' => $eventMessageResult->getId(),
                                ]);

                                if ($updateResult->isSuccess())
                                {
                                    if ($hasEventMessage)
                                    {
                                        $eventMessageSiteResult = EventMessageSiteTable::delete($data['EVENT_MESSAGE_ID']);

                                        if (!$eventMessageSiteResult->isSuccess())
                                        {
                                            $errors = $eventMessageSiteResult->getErrorMessages();
                                            Application::getConnection()->rollbackTransaction();
                                        }
                                    }
                                }
                                else
                                {
                                    $errors = $updateResult->getErrorMessages();
                                    Application::getConnection()->rollbackTransaction();
                                }

                                if (empty($errors))
                                {
                                    foreach ($lids as $lid)
                                    {
                                        $eventMessageSiteResult = EventMessageSiteTable::add([
                                            'EVENT_MESSAGE_ID' => $eventMessageResult->getId(),
                                            'SITE_ID' => $lid
                                        ]);

                                        if (!$eventMessageSiteResult->isSuccess())
                                        {
                                            $errors = $eventMessageSiteResult->getErrorMessages();
                                            Application::getConnection()->rollbackTransaction();
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                $errors = $eventMessageResult->getErrorMessages();
                                Application::getConnection()->rollbackTransaction();
                            }
                        }
                    }
                    else
                    {
                        $errors = $eventTypeResult->getErrorMessages();
                        Application::getConnection()->rollbackTransaction();
                    }
                }

                if (empty($errors))
                {
                    Application::getConnection()->commitTransaction();
                    if ($request->getPost('save'))
                        LocalRedirect("/bitrix/admin/artixgroup_form_list.php?lang=".LANGUAGE_ID);
                    else
                        LocalRedirect("/bitrix/admin/artixgroup_form_edit.php?ID=".$result->getId()."&lang=".LANGUAGE_ID."&".$tabControl->ActiveTabParam());
                }
            }
        }
        else
        {
            $errors = $result->getErrorMessages();
            Application::getConnection()->rollbackTransaction();
        }
    }

    $form = array_merge($form, $data);
    $formFields = $postFields;
    $formSites = $lids;

    unset($postFields, $data);
}

$APPLICATION->SetTitle(Loc::getMessage("ARTIXGROUP_FAVOURITES_TAB_COMMON"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = [
    [
        "TEXT" => Loc::getMessage("ARTIXGROUP_FAVOURITES_2_LIST"),
        "ICON" => "btn_list",
        "LINK" => "/bitrix/admin/artixgroup_form_list.php?lang=".LANG."&".GetFilterParams("filter_", false)
    ]
];

$context = new CAdminContextMenu($aMenu);
$context->Show();

if (!empty($errors))
{
    CAdminMessage::ShowMessage(join("\n", $errors));
}

?>
<form method="POST" action="<?echo $APPLICATION->GetCurUri()?>" name="form1">
    <?echo GetFilterHiddens("filter_");?>
    <input type="hidden" name="Update" value="Y">
    <input type="hidden" name="lang" value="<?echo LANG ?>">
    <input type="hidden" name="ID" value="<?echo $ID ?>">
    <input type="hidden" name="EVENT_NAME" value="<?echo $form['EVENT_NAME'] ?>">
    <input type="hidden" name="EVENT_MESSAGE_ID" value="<?echo $form['EVENT_MESSAGE_ID'] ?>">
    <?=bitrix_sessid_post()?>
    <?
    $tabControl->Begin();
    $tabControl->BeginNextTab();

    if ($ID)
    {
        ?>
        <tr>
            <td class="adm-detail-content-cell-l">ID:</td>
            <td class="adm-detail-content-cell-r"><?= $ID?></td>
        </tr>
        <?
    }
    ?>
    <tr>
        <td class="adm-detail-content-cell-l"><label for="active"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_ACTIVE")?>:</label></td>
        <td class="adm-detail-content-cell-r">
            <input type="checkbox" name="ACTIVE" id="active" value="Y"<?= $form['ACTIVE'] === 'Y' || !$ID ? ' checked=""' : ''?> class="adm-designed-checkbox">
            <label class="adm-designed-checkbox-label" for="active" title=""></label>
        </td>
    </tr>
    <tr class="adm-detail-required-field">
        <td class="adm-detail-valign-top adm-detail-content-cell-l"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_LID")?>:</td>
        <td class="adm-detail-content-cell-r">
            <div class="adm-list">
                <?
                $lids = $request->getPost('LID') ? $request->getPost('LID') : [];
                $sites = \Bitrix\Main\SiteTable::getList([
                    'filter' => ['ACTIVE' => 'Y'],
                    'order' => ['SORT']
                ])->fetchAll();

                foreach ($sites as $site)
                {
                    ?>
                    <div class="adm-list-item">
                        <div class="adm-list-control">
                            <input type="checkbox" name="LID[]" value="<?= $site['LID']?>" id="<?= $site['LID']?>" class="typecheckbox adm-designed-checkbox"<?= in_array($site['LID'], $formSites) ? ' checked=""' : ''?>>
                            <label class="adm-designed-checkbox-label typecheckbox" for="<?= $site['LID']?>" title=""></label>
                        </div>
                        <div class="adm-list-label">
                            <label for="<?= $site['LID']?>">[<?= $site['LID']?>]&nbsp;<?= $site['NAME']?></label>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l"><span class="adm-required-field"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_LANGUAGE")?>:</span></td>
        <td class="adm-detail-content-cell-r">
            <?
            $languages = \Bitrix\Main\Localization\LanguageTable::getList([
                'filter' => ['ACTIVE' => 'Y'],
                'order' => ['DEF']
            ])->fetchAll();
            ?>
            <select name="LANGUAGE_ID">
                <?
                foreach ($languages as $language)
                {
                    ?>
                    <option<?= ($language['LID'] === $form['LANGUAGE_ID']) || (empty($form['LANGUAGE_ID']) && $language['DEF'] === 'Y') ? ' selected=""' : ''?> value="<?= $language['LID']?>">
                        <?echo $language['NAME']?>
                    </option>
                    <?
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l" width="50%"><span class="adm-required-field"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_NAME")?>:</span></td>
        <td class="adm-detail-content-cell-r" width="50%">
            <input type="text" name="NAME" value="<?= $form['NAME']?>">
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l"><span class="adm-required-field"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_SHOW_MODE")?>:</span></td>
        <td class="adm-detail-content-cell-r">
            <?$showMode = ($form['SHOW_MODE'] ? $form['SHOW_MODE'] : FormTable::SHOW_MODE_SIMPLE)?>
            <select name="SHOW_MODE" id="show_mode">
                <option<?= ($showMode === FormTable::SHOW_MODE_SIMPLE) ? ' selected' : ''?> value="<?= FormTable::SHOW_MODE_SIMPLE?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_SHOW_MODE_SIMPLE")?>
                </option>
                <option<?= ($showMode === FormTable::SHOW_MODE_AJAX) ? ' selected' : ''?> value="<?= FormTable::SHOW_MODE_AJAX?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_SHOW_MODE_AJAX")?>
                </option>
                <option<?= ($showMode === FormTable::SHOW_MODE_POPUP) ? ' selected' : ''?> value="<?= FormTable::SHOW_MODE_POPUP?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_SHOW_MODE_POPUP")?>
                </option>
            </select>
        </td>
    </tr>
    <?
    $showAutoload = isset($form['SHOW_MODE']) && $form['SHOW_MODE'] !== FormTable::SHOW_MODE_SIMPLE;
    ?>
    <tr id="autoload_row"<?= $showAutoload ? '' : ' style="display:none"'?>>
        <td class="adm-detail-content-cell-l"><label for="autoload"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_AUTOLOAD")?>:</label></td>
        <td class="adm-detail-content-cell-r">
            <input type="checkbox" name="AUTOLOAD" id="autoload" value="Y"<?= $form['AUTOLOAD'] === 'Y' ? ' checked=""' : ''?> class="adm-designed-checkbox">
            <label class="adm-designed-checkbox-label" for="autoload" title=""></label>
        </td>
    </tr>
    <tr id="autoload_timer_row"<?= $showAutoload && $form['AUTOLOAD'] === 'Y' ? '' : ' style="display:none"'?>>
        <td class="adm-detail-content-cell-l" width="50%">
            <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_AUTOLOAD_TIMER")?>:
        </td>
        <td class="adm-detail-content-cell-r" width="50%">
            <input type="text" name="AUTOLOAD_TIMER" value="<?= (int)$form['AUTOLOAD_TIMER']?>">
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l"><span class="adm-required-field"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_CAPTCHA")?>:</span></td>
        <td class="adm-detail-content-cell-r">
            <?$captcha = ($form['CAPTCHA'] ? $form['CAPTCHA'] : FormTable::CAPTCHA_NONE)?>
            <select name="CAPTCHA">
                <option<?= ($captcha === FormTable::CAPTCHA_NONE) ? ' selected' : ''?> value="<?= FormTable::CAPTCHA_NONE?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_CAPTCHA_NONE")?>
                </option>
                <option<?= ($captcha === FormTable::CAPTCHA_BITRIX) ? ' selected' : ''?> value="<?= FormTable::CAPTCHA_BITRIX?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_CAPTCHA_BITRIX")?>
                </option>
                <option<?= ($captcha === FormTable::CAPTCHA_GOOGLE_V2) ? ' selected' : ''?> value="<?= FormTable::CAPTCHA_GOOGLE_V2?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_CAPTCHA_GOOGLE_V2")?>
                </option>
                <option<?= ($captcha === FormTable::CAPTCHA_GOOGLE_V3) ? ' selected' : ''?> value="<?= FormTable::CAPTCHA_GOOGLE_V3?>">
                    <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_CAPTCHA_GOOGLE_V3")?>
                </option>
            </select>
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l"><span class="adm-required-field"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_RESULT_MESSAGE")?>:</span></td>
        <td class="adm-detail-content-cell-r">
            <textarea name="RESULT_MESSAGE"><?= $form['RESULT_MESSAGE']?></textarea>
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_DESCRIPTION")?>:</td>
        <td class="adm-detail-content-cell-r">
            <textarea name="DESCRIPTION"><?= $form['DESCRIPTION']?></textarea>
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l"><?echo Loc::getMessage("ARTIXGROUP_FORM_JS_EVENT_HANDLERS")?>:</td>
        <td class="adm-detail-content-cell-r">
            <textarea name="JS_EVENT_HANDLERS"><?= $form['JS_EVENT_HANDLERS']?></textarea>
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l">
            <label for="ADD_EMAIL_EVENT_TYPE">
                <?
                echo empty($form['EVENT_NAME'])
                    ? Loc::getMessage("ARTIXGROUP_FAVOURITES_ADD_EVENT_TYPE")
                    : Loc::getMessage("ARTIXGROUP_FAVOURITES_UPDATE_EVENT_TYPE")
                ?>:
            </label>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="checkbox" name="ADD_EMAIL_EVENT_TYPE" id="ADD_EMAIL_EVENT_TYPE" value="Y"<?= $request->getPost('ADD_EMAIL_EVENT_TYPE') === 'N' ? '' : ' checked=""'?> class="adm-designed-checkbox">
            <label class="adm-designed-checkbox-label" for="ADD_EMAIL_EVENT_TYPE" title=""></label>
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l">
            <label for="CHECK_AGREEMENT"><?= Loc::getMessage("ARTIXGROUP_FORM_CHECK_AGREEMENT") ?></label>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="checkbox" name="CHECK_AGREEMENT" id="CHECK_AGREEMENT" value="Y"<?= $form['CHECK_AGREEMENT'] === 'N' ? '' : ' checked=""'?> class="adm-designed-checkbox">
            <label class="adm-designed-checkbox-label" for="CHECK_AGREEMENT" title=""></label>
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_MESSAGE_AGREEMENT") ?></td>
        <td class="adm-detail-content-cell-r">
            <textarea name="MESSAGE_AGREEMENT"><?= $form['MESSAGE_AGREEMENT']?></textarea>
        </td>
    </tr>
    <?
    if (!empty($form['EVENT_MESSAGE_ID']))
    {
        ?>
        <tr>
            <td class="adm-detail-content-cell-l">
                <label for="UPDATE_TEMPLATE_EVENT_TYPE">
                    <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_UPDATE_TEMPLATE_EVENT_TYPE")?>:
                </label>
            </td>
            <td class="adm-detail-content-cell-r">
                <input type="checkbox" name="UPDATE_TEMPLATE_EVENT_TYPE" id="UPDATE_TEMPLATE_EVENT_TYPE" value="Y"<?= $request->getPost('UPDATE_TEMPLATE_EVENT_TYPE') === 'Y' ? ' checked=""' : ''?> class="adm-designed-checkbox">
                <label class="adm-designed-checkbox-label" for="UPDATE_TEMPLATE_EVENT_TYPE" title=""></label>
            </td>
        </tr>
        <?
    }
    ?>
    <tr class="heading">
        <td colspan="2">Настройка стилей</td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l" width="50%"><span class="adm-required-field">CSS префикс формы:</span></td>
        <td class="adm-detail-content-cell-r" width="50%">
            <input type="text" name="PARAMETERS[FORM_CSS_PREFIX]" value="<?= $form['PARAMETERS']['FORM_CSS_PREFIX']?>">
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l" width="50%"><span class="adm-required-field">Текст кнопки:</span></td>
        <td class="adm-detail-content-cell-r" width="50%">
            <input type="text" name="PARAMETERS[BUTTON_TEXT]" value="<?= $form['PARAMETERS']['BUTTON_TEXT']?>">
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l" width="50%"><span class="adm-required-field">Стили кнопки:</span></td>
        <td class="adm-detail-content-cell-r" width="50%">
            <input type="text" name="PARAMETERS[BUTTON_CSS]" value="<?= $form['PARAMETERS']['BUTTON_CSS']?>">
        </td>
    </tr>
    <tr class="heading">
        <td colspan="2"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_FORM")?></td>
    </tr>
   <?/*<tr><td colspan="2"><pre><?print_r($formFields)?></pre></td></tr>*/?>
    <tr>
        <td colspan="2">
            <div class="form">
                <?
                $countFields = 0;

                if (count($formFields) > 0)
                {
                    $prevCol = -1;
                    $prevRow = -1;

                    foreach ($formFields as $key => $formField)
                    {
                        $fieldClassName = FormFieldTable::getFieldByType($formField['TYPE']);

                        if ($fieldClassName && class_exists($fieldClassName))
                        {
                            /** @var FieldBase $fieldBase */
                            $fieldBase = new $fieldClassName($formField, $key);

                            if ($prevCol !== $formField['COL'])
                            {
                                $cols = $prevCol > 0 ? $formField['COL'] - $prevCol : $formField['COL'];

                                if ($cols > 0)
                                {
                                    for ($i = 0; $i < $cols; $i++)
                                    {
                                        ?>
                                        <div class="form__col">
                                            <div class="form__row-drag-helper"></div>
                                        </div>
                                        <?
                                    }
                                }

                                if ($prevCol >= 0 && $formField['COL'] > 0)
                                {
                                    ?>
                                        </div>
                                        <div class="form__row-drag-helper"></div>
                                    </div>
                                    <?
                                }
                                ?>
                                <div class="form__col">
                                    <div class="form__row-drag-helper"></div>
                                    <div class="form__row">
                                        <div class="form__sort-drag-helper"></div>
                                <?
                            }
                            elseif ($prevRow !== $formField['ROW'])
                            {
                                if ($prevRow >= 0)
                                {
                                    ?>
                                    </div>
                                    <div class="form__row-drag-helper"></div>
                                    <?
                                }
                                ?>
                                <div class="form__row">
                                    <div class="form__sort-drag-helper"></div>
                                <?
                            }
                            ?>
                            <div class="form__input" draggable="true" ondragstart="event.dataTransfer.setData('text/plain',null)">
                                <div class="form__input-drag"></div>
                                <div class="form__input-container"><?= $fieldBase->showAdminHtmlInput()?></div>
                                <div class="form__input-setting">
                                    <a class="form__input-setting-link" href="javascript:void(0)">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                </div>
                                <?
                                if (!$fieldBase instanceof INotDeletable)
                                {
                                    ?>
                                    <div class="form__input-delete">
                                        <a class="form__input-delete-link" href="javascript:void(0)">
                                            <span class="fa fa-remove"></span>
                                        </a>
                                    </div>
                                    <?
                                }
                                ?>
                                <textarea class="form__input-parameters" name="FIELDS[<?= $key?>]"><?= $fieldBase->getJson()?></textarea>
                            </div>
                            <div class="form__sort-drag-helper"></div>
                            <?
                            $countFields = $key + 1;
                        }

                        $prevCol = $formField['COL'];
                        $prevRow = $formField['ROW'];
                    }
                    ?>
                    </div>
                    <div class="form__row-drag-helper"></div>
                    </div>
                    <div class="form__col">
                        <div class="form__row-drag-helper"></div>
                    </div>
                    <?
                }
                ?>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center" id="btn-container">
        </td>
    </tr>
    <?
    if ($ID > 0)
    {
        ?>
        <tr>
            <td colspan="2" align="center">
                <div class="adm-info-message-wrap">
                    <div class="adm-info-message">
                        <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_COPY_CODE")?>
                        <div id="copy_area" style="display: none"><?
                            if ($form['SHOW_MODE'] === FormTable::SHOW_MODE_SIMPLE)
                            {
                                $paramFields = '';
                                foreach ($formFields as $field)
                                {
                                    if ($field['EXTERNAL'] === 'Y')
                                    {
                                        $paramFields .= '"FIELD_'.$field['ID'].'" => "",'."\n";
                                    }
                                }

                                echo htmlspecialchars('<?$APPLICATION->IncludeComponent(
                                    "artixgroup:form",
                                    "",
                                    Array(
                                        "FORM_ID" => "'.$ID.'",
                                        '.$paramFields.'
                                    ),
                                    false
                                );?>');
                            }
                            else
                            {
                                $paramFields = '';
                                foreach ($formFields as $field)
                                {
                                    if ($field['EXTERNAL'] === 'Y')
                                    {
                                        $paramFields .= ' data-field-'.$field['ID'].'=""';
                                    }
                                }

                                echo htmlspecialchars('<button class="button artixgroup_form_trigger" data-form-id="'.$ID.'"'.($form['SHOW_MODE'] === FormTable::SHOW_MODE_POPUP ? ' data-form-popup="true"' : '').($form['AUTOLOAD'] === 'Y' ? ' data-form-autoload="'.(int)$form['AUTOLOAD_TIMER'].'"' : '').$paramFields.'>'.$form['NAME'].'</button>');
                            }
                        ?></div>
                        <script>
                            document.getElementById('copy_code').addEventListener('click', function () {
                                event.preventDefault();
                                var text = document.createElement('textarea');
                                text.value = document.getElementById('copy_area').innerText;
                                document.body.appendChild(text);
                                text.select();
                                document.execCommand('copy');
                                document.body.removeChild(text);
                                setTimeout(function () {
                                    alert('<?echo Loc::getMessage("ARTIXGROUP_FORM_COPY_CODE_COMPLETE")?>');
                                }, 1000);
                            });
                        </script>
                    </div>
                </div>
            </td>
        </tr>
        <?
    }

    $tabControl->EndTab();
    $tabControl->Buttons(array("back_url"=>"artixgroup_form_list.php?lang=".LANGUAGE_ID));
    $tabControl->End();

    $fieldTypes = [];
    $fields = FormFieldTable::getFields();
    foreach ($fields as $type => $className)
        $fieldTypes[$type] = $className::getTypeName();
    ?>
    <script>
        var formControl = new FormControl({
            actionUrl: '/bitrix/admin/artixgroup_form_input.php',
            countFields: <?= $countFields?>,
            fieldTypes: <?= json_encode($fieldTypes)?>
        });
    </script>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>