<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
/*
return [
	"parent_menu" => "global_menu_services",
	"section" => "tags",
	"sort" => 100,
	"text" => Loc::getMessage("ARTIXGROUP_FAVOURITES_MENU_TITLE"),
	"title" => Loc::getMessage("ARTIXGROUP_FAVOURITES_MENU_DESC"),
	"icon" => "fav_menu_icon_yellow",
	"url" => "artixgroup_favourites_list.php?lang=".LANG,
	"page_icon" => "fav_menu_icon_yellow",
	"items_id" => "menu_artixgroup_favourites",
	"more_url" => [
		"artixgroup_favourites_list.php",
	],
	"items" => []
];
*/