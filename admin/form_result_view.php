<?php

use Artixgroup\Shop\Form\FieldBase;
use Artixgroup\Shop\FormFieldTable;
use Artixgroup\Shop\FormResultFieldTable;
use Artixgroup\Shop\FormResultTable;
use Artixgroup\Shop\FormTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/admin_tools.php");

$blogModulePermissions = $APPLICATION->GetGroupRight("artixgroup.shop");
if ($blogModulePermissions < "R")
{
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

Loc::loadMessages(__FILE__);
Loader::includeModule('artixgroup.shop');
Loader::includeModule('iblock');

$errorMessage = "";
$bVarsFromForm = false;

$ID = (int)$ID;

$form = [];
$formSites = [];
$formFields = [];
$eventType = null;

if ($ID > 0)
{
    $formResult = FormResultTable::getByPrimary($ID, [
        'select' => ['*']
    ])->fetch();

    $iterator = FormResultFieldTable::getList([
        'select' => ['*'],
        'filter' => ['RESULT_ID' => $formResult['ID']],
    ]);
    $formResultFields = [];
    while ($formResultField = $iterator->fetch())
        $formResultFields[$formResultField['FIELD_ID']] = $formResultField;

    $form = FormTable::getByPrimary($formResult['FORM_ID'], [
        'select' => ['*']
    ])->fetch();

    $formFields = FormFieldTable::getList([
        'select' => ['*'],
        'filter' => ['FORM_ID' => $formResult['FORM_ID']],
        'order' => ['COL' => 'ASC', 'ROW' => 'ASC', 'SORT' => 'ASC']
    ])->fetchAll();
}

$aTabs = [
    [
        "DIV" => "edit1",
        "TAB" => Loc::getMessage("ARTIXGROUP_FORM_TAB_COMMON"),
        "ICON" => "blog",
        "TITLE" => Loc::getMessage("ARTIXGROUP_FORM_TAB_COMMON_TITLE")
    ],
];
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle(Loc::getMessage("ARTIXGROUP_FORM_TAB_COMMON"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = [
    [
        "TEXT" => Loc::getMessage("ARTIXGROUP_BACK_TO_LIST"),
        "ICON" => "btn_list",
        "LINK" => "/bitrix/admin/artixgroup_form_result_list.php?lang=".LANG."&".GetFilterParams("filter_", false)
    ]
];

$context = new CAdminContextMenu($aMenu);
$context->Show();

if (!empty($errors))
{
    CAdminMessage::ShowMessage(join("\n", $errors));
}

$tabControl->Begin();
$tabControl->BeginNextTab();
?>
<tr>
    <td class="adm-detail-content-cell-l">ID:</td>
    <td class="adm-detail-content-cell-r"><?= $formResult['ID']?></td>
</tr>
<tr>
    <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_DATE_CREATE") ?>:</td>
    <td class="adm-detail-content-cell-r"><?= $formResult['DATE_CREATE']?></td>
</tr>
<tr>
    <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_USER") ?>:</td>
    <td class="adm-detail-content-cell-r">
        <?
        if ($formResult['USER_ID'] > 0)
        {
            $user = \Bitrix\Main\UserTable::getById((int)$formResult['USER_ID'])->fetch();
            ?>
            [<a href="<?= $user['ID']?>"><?= $user['ID']?></a>] (<?= $user['LOGIN']?>) <?= $user['NAME']?> <?= $user['SECOND_NAME']?>
            <?
        }
        else
        {
            echo Loc::getMessage("ARTIXGROUP_FORM_GUEST");
        }
        ?>
    </td>
</tr>
<tr>
    <td class="adm-detail-content-cell-l"><?= Loc::getMessage("ARTIXGROUP_FORM_PAGE_URL") ?>:</td>
    <td class="adm-detail-content-cell-r"><a href="<?= $formResult['PAGE_URL']?>"><?= $formResult['PAGE_URL']?></a></td>
</tr>
<tr class="heading">
    <td colspan="2"><?= Loc::getMessage("ARTIXGROUP_FORM_FIELDS") ?></td>
</tr>
<?
foreach ($formFields as $field)
{
    $className = FormFieldTable::getFieldByType($field['TYPE']);
    /** @var FieldBase $fieldBase */
    $fieldBase = new $className($field);
    $fieldBase->setValue($formResultFields[$field['ID']]['VALUE']);
    ?>
    <tr>
        <td class="adm-detail-content-cell-l"><?= $field['NAME']?>:</td>
        <td class="adm-detail-content-cell-r"><?= $fieldBase->getHtmlValue()?></td>
    </tr>
    <?
}

$tabControl->EndTab();
$tabControl->End();
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>