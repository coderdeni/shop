<?php

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Artixgroup\Favourites\FavouriteTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/admin_tools.php");

$blogModulePermissions = $APPLICATION->GetGroupRight("blog");
if ($blogModulePermissions < "R")
{
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

Loc::loadMessages(__FILE__);
Loader::includeModule('artixgroup.shop');
Loader::includeModule('iblock');

$errorMessage = "";
$bVarsFromForm = false;

$ID = (int)$ID;
$favourites = [];
$items = [];

if ($ID > 0)
{
    $favourites = FavouriteTable::getByPrimary($ID, [
        'select' => ['*', 'USER']
    ])->fetchObject();

    $itemIds = $favourites->getItems();

    if (count($itemIds) > 0)
    {
        $items = ElementTable::getList([
            'select' => ['ID', 'NAME'],
            'filter' => ['ID' => $itemIds]
        ])->fetchAll();
    }
}

$aTabs = [
    [
        "DIV" => "edit1",
        "TAB" => Loc::getMessage("ARTIXGROUP_FAVOURITES_TAB_COMMON"),
        "ICON" => "blog",
        "TITLE" => Loc::getMessage("ARTIXGROUP_FAVOURITES_TAB_COMMON_TITLE")
    ],
];
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle(Loc::getMessage("ARTIXGROUP_FAVOURITES_TAB_COMMON"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = [
    [
        "TEXT" => Loc::getMessage("ARTIXGROUP_FAVOURITES_2_LIST"),
        "ICON" => "btn_list",
        "LINK" => "/bitrix/admin/artixgroup_favourites_list.php?lang=".LANG."&".GetFilterParams("filter_", false)
    ]
];

$context = new CAdminContextMenu($aMenu);
$context->Show();

if (!empty($errors))
{
    CAdminMessage::ShowMessage(join("\n", $errors));
}

?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?" name="form1">
    <?echo GetFilterHiddens("filter_");?>
    <input type="hidden" name="Update" value="Y">
    <input type="hidden" name="lang" value="<?echo LANG ?>">
    <input type="hidden" name="ID" value="<?echo $ID ?>">
    <?=bitrix_sessid_post()?>
    <?
    $tabControl->Begin();
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td class="adm-detail-content-cell-l">ID:</td>
        <td class="adm-detail-content-cell-r"><?= $ID?></td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l"><span class="adm-required-field"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_LOGIN")?>:</span></td>
        <td class="adm-detail-content-cell-r">
            <a href="/bitrix/admin/user_edit.php?ID=<?= $favourites->getUser()->getId()?>&lang=<?= LANG?>">
                <?= $favourites->getUser()->getLogin() ?>
            </a>
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l"><span class="adm-required-field"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_USERNAME")?>:</span></td>
        <td class="adm-detail-content-cell-r">
            <?= $favourites->getUser()->getLastName() ?> <?= $favourites->getUser()->getName() ?> <?= $favourites->getUser()->getSecondName() ?>
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l"><span class="adm-required-field"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_EMAIL")?>:</span></td>
        <td class="adm-detail-content-cell-r">
            <a href="mailto:<?= $favourites->getUser()->getEmail() ?>">
                <?= $favourites->getUser()->getEmail() ?>
            </a>
        </td>
    </tr>
    <tr class="heading">
        <td colspan="2"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_ITEMS")?></td>
    </tr>
    <?
    if (count($items) > 0)
    {
        ?>
        <tr>
            <td colspan="2">
                <table width="100%" class="internal" cellspacing="0" cellpadding="0" border="0">
                    <tr class="heading">
                        <td valign="top">
                            <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_NN")?>
                        </td>
                        <td valign="top">
                            <?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_ITEM_NAME")?>
                        </td>
                    </tr>
                    <?
                    foreach ($items as $key => $item)
                    {
                        $index = $key + 1;
                        ?>
                        <tr>
                            <td><?= $index?></td>
                            <td>
                                <a href="/bitrix/admin/iblock_element_edit.php?ID=<?= $item['ID']?>&lang=<?= LANG?>">
                                    [<?= $item['ID']?>] <?= $item['NAME']?>
                                </a>
                            </td>
                        </tr>
                        <?
                    }
                    ?>
                </table>
            </td>
        </tr>
        <?
    }
    else
    {
        ?>
        <tr>
            <td colspan="2"><?echo Loc::getMessage("ARTIXGROUP_FAVOURITES_ITEMS_EMPTY")?></td>
        </tr>
        <?
    }

    $tabControl->EndTab();
    $tabControl->End();
    ?>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>